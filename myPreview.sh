#! /bin/sh
#
#	myPreview.sh -- generate a preview release archive
#
#ident "@(#)smail:RELEASE-3_2_0_121:myPreview.sh,v 1.17 2005/11/17 21:22:14 woods Exp"

if [ -f defs.sh -o -f src/defs.sh ]; then
	echo "$0: you must run 'make clobber' first!" 1>&2
	exit 1
fi

VERSION=$(awk '$1 ~ /^3.*-Pre/ {print $1}' level)
if [ -z "$VERSION" ]; then
	echo "$0: the current version doesn't seem to be a preview!" 1>&2
	exit 1
fi

if [ "$(echo /usr/pkgsrc/mail/smail/patches/p*)" != '/usr/pkgsrc/mail/smail/patches/p*' ]; then
	echo "$0: there should not be any patches in pkgsrc/mail/smail!" 1>&2
	exit 1
fi

DATE=${OLD_DATE:-$(date '+%Y%m%d')}

ARCHIVE_DIR=/most/var/spool/ftp/pub/NetBSD/distfiles
ARCHIVE=${ARCHIVE_DIR}/smail-${VERSION}-${DATE}.tar.gz

echo "Creating ChangeLog-${VERSION} ..."

rm -f ChangeLog-${VERSION}.OLD
sh ./mkChangeLog.sh

echo "Creating preview release in ${ARCHIVE} ... "

find -L . \( -type f -a ! -name '.?*' -a ! -name '*~' -print \) -o -name CVS -prune | \
	fgrep -x -v -e ./conf/EDITME -e ./conf/Make.local | \
	pax -wz -s ,./,smail-${VERSION}-${DATE}/, -f ${ARCHIVE}

rm -f ${ARCHIVE_DIR}/smail-pkgsrc.shar
(
	cd /usr/pkgsrc/mail/smail
	#
	# Fix up the SMAIL_VERSION line to fetch this new preview package.
	#
	ed Makefile.common <<_EOF_
/^SMAIL_VERSION/p
s/^SMAIL_VERSION=	.*$/SMAIL_VERSION=	${VERSION}-${DATE}/p
w
q
_EOF_
	make makesum mps
)
(
	cd /usr/pkgsrc/mail/smail
	# NOTE:  keep this file list in sync with the one in ./mkRelease.sh
	shar DESCR Makefile Makefile.common distinfo INSTALL PLIST files/ files/mailer.conf files/clean-sendmail files/config.hubclient
) > ${ARCHIVE_DIR}/smail-pkgsrc.shar

echo "Done."

exit 0
