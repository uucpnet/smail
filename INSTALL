# -*-indented-text-*-
#ident	"@(#)smail:RELEASE-3_2_0_121:INSTALL,v 1.34 2004/07/25 17:37:03 woods Exp"

INITIAL UNPACKING

After you've unpacked the source (and thus been able to read this far in
this file), we strongly recommend you begin by first checking the entire
distribution into some form of source code control system such as CVS.
CVS offers a feature called "vendor branch support" which makes it
trivial to merge local changes with new "vendor" releases of a product.
It will also make it far easier for you to submit accurate patches to
the Smail developer(s) (see below for information about submitting
patches).

To check this release of Smail into CVS (assuming you already have CVS
installed and a default repository created) you might do something like
this:

    cvs import -I ! -m 'Smail RELEASE-3_2_0_121 Distribution' smail SMAIL RELEASE-3_2_0_121

If you don't have CVS, but you are familiar with RCS, or SCCS, you can
also use those tools to track any local changes you might make during
the course of installation and configuration and local customisation of
Smail.

If you do use CVS, do _not_ add any "product files" -- i.e. files
created during the build and install procedures.  We include .cvsignore
files to ignore the common products by default, and you may add any
system-specific product files you encounter to these lists.


BUILD CONFIGURATION

We recommend that you read through the various manual pages before
setting up and installing Smail.  To generate the manual pages, change to
the man directory and type "make".  This will generate manual pages for
the default Smail configuration.  Detailed information on Smail can be
found in the manual pages smail(5) and smail(8), with further details on
the specific configuration files in the other section 5 manual pages
such as smailconf(5) and so on.

Most run-time configuration files are optional (with the exception of
those created during installation), and the smail program itself can
usually create any directories or other files that it absolutely needs.
Thus in the simplest scenario, the installation procedure is as simple
as typing "make install", and starting the mailer!

The only two files that you will likely have to edit are conf/EDITME,
which configures the compilation defaults for the smail binary and the
several accompanying utilities; and conf/Make.local, which provides
for local Makefile configuration defaults.

The conf/EDITME file describes the locations of various files and
directories, enables or disables various capabilities, and one of the
settings points to a file describing your architecture and operating
system.  It should be copied from the source file conf/EDITME-dist, or
from one of the small example files (conf/EDITME-*) if one happens to
nearly match your system.  Note that if you have generated the Smail
manual pages as described above, then the conf/EDITME file will have
already been created for you as a verbatim copy of conf/EDITME-dist,
though you will still have to review and edit it, or alternately replace
it with one of the other simple examples.

The conf/Make.local file provides settings for locally tunable Makefile
macros.  It should be copied from the source file conf/Make.local-dist,
or from one of the small example files (conf/Make.local-*) if one
happens to nearly match your system.

Future changes to Smail may include changes to conf/EDITME-dist,
conf/Make.local-dist (and the other example files) and when upgrading
your Smail intallations it will be your responsibility to make sure that
these changes are reflected in your locally edited conf/EDITME and
conf/Make.local files, as needed.

The full conf/EDITME-dist mentions just about every compilation and
installation option available in Smail.  It appears to be very large and
complex at first glance.  However you'll see some comparativley tiny
sample configurations in other files named conf/EDITME-*.  These files
are actual working configurations used for production systems.  In
general the configuration system is set up in such a way that if you
define only the compiler flags (CPPFLAGS, CFLAGS, LDFLAGS, etc.), and
set the OS_TYPE, Smail will build and install as if it were to be the
native vendor-supplied mailer for the system, and will attempt to mimic
(and thus replace) a sendmail installation in terms of the command-line
interface and standard supplementary files such as the aliases table.

Some sites may also need to create (or fix) an operating system
description file.  To do this, change to the conf/os directory and copy
the file "template" to a name descriptive of your operating system.
Then edit the copy as appropriate.  The basename of this file can then
be used as a value for the OS_TYPE variable in the conf/EDITME file.  If
you create a new operating system description file, please mail it to us
so that we may add it to the distribution.

A simple way to test Smail is to set the variable TEST_BASE in the
conf/EDITME file to a test installation directory.  A "make install"
will create a tree under this directory, with all of the Smail binaries
and utilities.  Smail can then be tested in this directory without
affecting the operation of any other mailers currently working on your
system, including a previous installation of Smail itself.

WARNING:  TEST_BASE is little tested itself, and it is somewhat
	  fragile.  It can affect things outside the test directory if
	  the entire configuration is not complete.

If you're trying to debug an EDITME file, or an OS_TYPE file, you can
simply generate defs.* files in the conf directory by running the
following command from within the conf directory:

	  ROOT=.. sh lib/mkdefs.sh


BUILDING AND INSTALLATION

NOTE:  You should probably do a test build install before installing
       Smail onto a live system.  To do this, setup the TEST_BASE
       variable as described above, paying heed to the warning above
       too, and go through the steps in this section.  Then, to install
       on to a live system, comment out TEST_BASE in the conf/EDITME
       file and perform these steps again.  The second time around, it
       will not be necessary to build the makefile dependencies.


  Makefile Dependencies:

When everything is ready to build, you may optionally create the
Makefile dependencies for your system and configuration.  These
dependencies are stored in ".depend" files in each sub-directory of the
source tree.  Some versions of "make" read these files automatically if
they exist but just in case your version doesn't all the "make" commands
run by the top level make will automatically specify these files
explicitly wherever they matter most.

To build all the ".depend" files, type:

	make depend 2>&1 | tee mkdep.out

If you are a C shell (/bin/csh, or tcsh) user (poor you!), try:

	make depend |& tee mkdep.out

at the top of the Smail source tree.

NOTE:  If you have any problems with "make depend" then you might want
       to start over completely and skip this step.  Just remember that
       you'll probably have to do "make clean && make" if you are doing
       any fixing or making other changes to the sources.

If you ran "make depend", scan the output produced by for any errors.
In particular, watch out for missing include files.  If any messages
about missing include files are generated, please send us mail
describing your operating system and the name of the include file which
was not found (you should also enclose your conf/EDITME file, and any
conf/os file you might have modified and be using for the failed build.
Please tell us of any similarly named include files which DO exist,
which may be used instead.

NOTE:  If you do not run "make depend", you should be aware that you'll
likely have to run "make clean" if you change anything between builds to
ensure everything is properly up to date.  This applies especially to
changes in the conf/EDITME file.


  The Build:

Smail requires the PCRE library for parsing regular expressions.  If
your system already has this library, either natively or locally
installed, and you have configured the right compiler options in your
conf/EDITME file to find the PCRE header files and library archives,
then you may proceed with building Smail proper.  However if your system
does not have the PCRE library installed then you must first install it.
There is a copy of the PCRE distribution in the "pcre" sub-directory.
Read the file pcre/README for further instructions.

The next step is to build the Smail binaries, utilities and localised
manual pages.  This step is performed with the command:

	make 2>&1 | tee mk.out

or for those stuck on csh:

	make |& tee mk.out

(use "-a" with "tee" if you want to preserve the previous contents of
the output log file)

If any errors were encountered, please mail them to us.  Please send a
complete copy of the mk.out file, and a copy of your conf/EDITME file
along with a "patch" (i.e. context diff) of any changes you've made to
the official distribution sources.  In particular if you wrote your own
operating system configuration file, please send that, too.  If you have
any comments about your build environment, and/or if you have your own
fixes for these errors, please send those as well.


  Testing:

If you are upgrading an existing Smail installation then you should do
at least some basic tests of the new binary to make sure it will
function properly.  You can do things like viewing the runtime
configuration with "src/smail -v -bP ALL", testing address routing with
either "src/smail -v -bt" or "src/smail -v -bv address", etc., and in
each case you might compare the output to what the existing smail binary
reports.


  The Install:

When Smail builds correctly, the final step is to install it by typing:

	make install

NOTE:  Some operating systems prevent a running binary from being
over-written and others exhibit strange behaviour when the running
program is replaced.  If you are re-installing Smail on a system where
it is already running it is best to stop it first.

WARNING:  If you had installed the manual pages for any very old release
of Smail (i.e. one which included "routers.5", "transports.5",
"config.5", etc.), you should remove these old manual pages from the
destination directory.  These manual pages have been renamed and now
have the prefix "smail" as part of their names.


  Cleanup:

The following additional make commands can be useful:

	make clean	- to clear out make intermediate files.
	make clobber	- to clean intermediate and target files.


GENERAL COMPILATION NOTES

On ISC's Interactive UNIX (aka 386/ix), and probably on other systems as
well, if you don't use an ANSI C compiler you will get the following
messages while compiling:

	../../util/dbm_compat.h: 13: bad include syntax
	../../util/dbm_compat.h: 22: bad include syntax

Ignore these messages.  They result from an #ifdef'd out:

	#include DBM_INCLUDE

where the C compiler does not allow macros to name include files.  This
doesn't cause any problems when compiling on Interactive, beyond the
generation of the above messages.  If your compiler really bombs out,
remove the offending lines from util/dbm_compat.h.


SYSTEM-SPECIFIC CAVEATS

  * There have been reports that some versions of sed(1) [in particular
    the one supplied with HP/UX 9.x and HP/UX 10.x and of course earlier
    HP/UX versions too; as well as Digital UNIX 3.x] cannot handle the
    number of expressions in the defs.sed files produced by the mkdefs
    script, and indeed cause other kinds of breakage when generating the
    "defs.h" files.  The easy work-around is to install a more capable
    version of sed that doesn't trip over this limit, such as the GNU
    Sed.  No other fixes are expected, or desired, as this problem will
    hopefully be moot with the intended conversion to GNU Autoconf.

  * HP/UX 10.50 and 11.00 seem to work fine with the conf/os/hp-ux9.x
    OS_TYPE file provided that you have replaced sed (as above), and
    that you add NEED_HSTRERROR and "-Dconst=" as recommended in
    conf/EDITME-dist.

  * On MIPS systems, and possibly on other dual-universe systems, be
    sure to set the BSD and System V time-zones to the same value.  For
    MIPS systems, the System V time-zone is in /etc/TIMEZONE.  The
    time-zone value used by BSD routines is stored in the kernel and is
    set by the date command.  For example, if you are in the Pacific
    time-zone, set /etc/TIMEZONE to:

	TZ=PST8PDT
	export TZ

    and set the kernel time-zone with:

	date -t 480

    At least one user was surprised to find Smail using a different time-zone
    than some other commands.

  * On SGI systems running IRIX 5.x (and possibly others) the
    <unistd.h> file defines an undocumented instance of "EX_OK" which
    clashes with the normal manifest of the same name in <sysexits.h>.
    You'll get a warning something like this:

	cfe: Warning 581: /usr/include/sysexits.h:107: Macro EX_OK redefined.

    This is OK, though if you see /usr/include/unistd.h instead then you
    may have a problem as the unistd.h value is incorrect for our use.
    In this case you should try re-ordering the include files to make
    sure that <sysexits.h> comes *after* <unistd.h>.

  * Unfortunately SunOS 5.6 (and earlier) still seems to use extremely
    ancient and buggy BIND 4.9.3 resolver code, even in the
    "libresolv.so.2" version introduced by one of the patches.  If you
    do not first install a modern version of BIND, *and* its associated
    resolver library, you will get the following errors at link time:

	Undefined                       first referenced
	 symbol                             in file
	hstrerror                           bindlib.o
	inet_net_pton                       lookup.o
	inet_net_ntop                       smtprecv.o

    You may also get errors like this earlier on if you've got various
    compiler warnings enabled:

	smtprecv.c:342: warning: implicit declaration of function `inet_net_ntop'

    You MUST install and link against the resolver from the latest
    BIND-8.x or BIND-9.x release in order to correct this problem.

  * SunOS 5.x uses a "name service cache daemon" that's not very
    effective at what it's supposed to do (in particular it gets in the
    way of a real DNS cache and can cause delays and thrashing on a
    system that does a lot of DNS lookups).  If you really insist on
    using the native resolver code then you should consider either
    disabling nscd completely, or at minimum adding "enable-cache hosts
    no" to /etc/nscd.conf to avoid it getting in the way of DNS lookups.

  * It is said that some systems with 32-bit UIDs (eg. Linux with
    glibc2) will suffer problems in places if the nobody ID isn't set
    right.


CREATING CONFIGURATION FILES

If you have need of a more complex configuration than can be provided
with the internal defaults, read over the manual pages carefully.  We
believe that the process of writing files is reasonably straightforward,
though if you have any questions, or if you dispute this claim, please
send mail to the smail3-users list mentioned in the README.

The complete current (live) configuration can be displayed with the
command:

	 smail -v -bP ALL

Note that this is a concatenation of *all* of the configuration files.
See the manual page smail(8) to learn how to extract a specific
configuration file's default content.

If you run this command without any config files, or specify , it will
display the default compiled in configuration and if you already have
config files in place you can fool it into ignoring them:

       smail -v -oL /nosuchdirectory -bP ALL

The configurations printed with '-bP' can be redirected to a temporary
filename, edited as necessary, and copied to the appropriate file.

It is *strongly* recommended that you create only the configuration
files you absolutely need to make Smail work for your environment and
requirements.  Otherwise you should stick as close as possible to the
default configuration and don't set anything that you are not explicitly
setting different from the default.  This way any improvements in future
releases to default parameters will not be hidden by your local
configuration values (eg. the "Received:" header format).

Note also that the old example files are of extremely limited value.
The new default configurations include many improvements over the old
examples from previous releases and indeed some old examples are not
compatible with the current code.

If you're running a simple "leaf" Internet node for one or a few local
domains you likely only need a 'config' file with only a very few custom
settings such as "domains", possibly "visible_name" and
"more_hostnames", and maybe "smtp_accept_max" and "smtp_accept_queue".
Some of the newer security related settings may also be of interest.

If you have been running previous releases of Smail it is *critical*
that you read down through the CHANGES file until you come to the notes
regarding the version you are, or were, running.  There may be important
differences between this release and any previous releases that may
affect the proper operation of Smail in your environment.  For example
the default transport name was changed from "smtp" to "inet_zone_smtp"
so if you have an old routers file with router instances using that
transport name you'll have to make the same change.  Some duplicate
config file variable names have been deprecated too -- those will be
warned about when you test with the old config and logged in the
paniclog if you try to start the daemon without updating your config.


RUNNING SMAIL

Smail is used in basically the same way as Sendmail.  It is normally run
as a stand-alone daemon that both listens for incoming SMTP connections
(assuming you've used one of the *-network driver configurations) as
well as periodically checking for and attempting to deliver any messages
in the input queue(s).  It is started in this way with a command like
"sendmail -bd -q30m" and with the '-bd' flag it will automatically put
itself into the background and continue running.  If your system already
runs sendmail then the startup call for it is probably already in
place.  See the smail(8) manual page for further information

Smail normally checks each of its configuration files at the start of
every queue run and if any are found to be newer than the start time of
the current process then smail will restart itself automatically in
order to have the new configuration take effect.

There's also a maintenance script called 'checkerr' that you should run
periodically (daily, or perhaps weekly) from 'cron'.  See the manual
page for checkerr(8) for further information.


SMAIL ON THE INTERNET (VERY IMPORTANT FOR INTERNET USERS)

Users on the Internet should configure Smail to use the Domain Name
Service for routing on the Internet.  To use the DNS, you will have to
have the bind resolver library, and you will have to tell Smail that you
have it.  For some systems, these are configured into Smail by default.
For other systems, you will need to configure in the bind router driver
by modifying the EDITME file.  This involves adding the setting:

	DRIVER_CONFIGURATION=arpa-network

and adding "BIND" to the HAVE list.  See the EDITME file in the conf
directory for more details.


SMAIL WITH SYSTEM V RELEASE 2&3 AND SUNOS-4

SunOS-4 and System V Release 2&3 systems typically have a /bin/mail
program that both delivers mail and reads mail.  Smail provides a
replacement for the /bin/mail program, in pd/binmail, that uses the old
/bin/mail for reading mail, and Smail for delivering mail.  To use the
replacement /bin/mail, you will need to define the LMAIL variable in
conf/EDITME.  See conf/EDITME for more instructions.

The System V mailx utility will need to be updated to know how to find
Smail.  Presuming that Smail is installed as /usr/lib/sendmail, you
will need to add the following line to the file /usr/lib/mailx/mailx.rc:

	set sendmail=/usr/lib/sendmail

The SunOS-4 Mail utility already has this defined.

In any case you should ensure that /usr/lib/sendmail is included in
conf/EDITME as either the value for SMAIL_NAME, or is included in the
OTHER_SMAIL_NAMES setting.  For example on SunOS-4:

	OTHER_SMAIL_NAMES=/usr/lib/sendmail:/usr/ucb/mailq


SMAIL AND SCO UNIX

Smail under SCO UNIX must be installed as both /bin/rmail and
/usr/lib/mail/execmail.  To do this, set OTHER_SMAIL_NAMES in the
conf/EDITME file to "/bin/rmail:/usr/lib/mail/execmail".  Some newer SCO
UNIX systems also have a /usr/bin/rmail, as a link to /usr/bin/mailx.
This link should be deleted.  Note that (at least in 3.2v4.2)
/usr/bin/mailx, /usr/bin/mail, and /bin/mail are all links to the same
binary and they can all be left that way.

Some versions of SCO UNIX store mail messages in /usr/spool/mail,
while others use /usr/mail, like System V.  If your system uses
/usr/spool/mail, you will need to add the following line to the
conf/EDITME file:

	MAILBOX_DIR=/usr/spool/mail

Current SCO releases use MMDF mailbox file formats.  Many users prefer
to use public domain mail readers, such as elm or mush, that can be
configured to use regular UNIX mailbox files.  Smail will use MMDF
format by default on SCO UNIX systems unless you add NO_MMDF_MAILBOXES
to MISC_DEFINES.

The SCO MMDF version of mailx supplies its own From: line using a
hostname taken from an MMDF configuration file.  I have no idea why
they decided it was a good idea to hard-code the hostname in a
configuration file, but they did.  Thus, if you change your hostname,
your From: lines won't change until you change the MMDF configuration
file.  To correct the From: line change the MLDOMAIN and MLNAME
definitions in /usr/mmdf/mmdftailor.

To get the MMDF-ified SCO to call Smail via /usr/lib/mail/execmail from
mailx, add the line:

	set execmail

to the file /usr/lib/mail/mailrc.  I'm not sure if this really does
anything, or not though, as in tests on SCO UNIX 3.2v4.2 mailx always
invoked /usr/lib/mail/execmail.


SMAIL AND SYSTEM V RELEASE 4

On many SVR4 implementations the mailbox file format defines a
Content-Length field that indicates the length of each message, in
bytes.  This obviates the need for inserting "> before lines beginning
with "From " (and indeed, there are some problems with the AT&T-supplied
version of mailx concerning message splitting, if you don't use the
Content-Length header).  Smail can be configured to generated
Content-Length fields (and Content-Type fields).  However, the
compiled-in transports cannot do this.  To configure generation of these
fields, copy the file samples/generic/transports to the /usr/lib/smail
directory, and modify it as suggested at the top of the file.

In SVR4 the uux command is given message grades by using long names,
which differs from older versions of HoneyDanBer UUCP, where message
grades were a single character.  Smail does not support these longer
names.  Fortunately, you can configure SVR4 to support single
character message grades.  To make SVR4 compatible with Smail, add the
following lines to your /etc/uucp/Grades file:

9	9	Any	User	Any
A	A	Any	User	Any
C	C	Any	User	Any
a	a	Any	User	Any
n	n	Any	User	Any

This list is sufficient for the default list of Precedence header
field values supported by Smail.

It is possible to configure an SVR4 system to call Smail by replacing
the file /etc/mail/mailsurr with the line:

	'(.+)'	'(.+)'	'< /usr/local/smail/bin/smail -em -i \\2'

This works, but it has the annoying side effect of invoking smail
multiple times for each address given as an argument to /bin/mail or
/bin/rmail.  It is preferable to replace /bin/rmail with smail, and to
replace /bin/mail with the mail program supplied in pd/binmail.

To configure SVR4 to use Smail for handling SMTP, move the file
/etc/rc2.d/S88smtpd to /etc/rc2.d/no-S88smtpd, move the file
/etc/rc0.d/K74smtpd to /etc/rc0.d/no-S74smtpd, and then add a new file
/etc/rc2.d/S88smail containing:

	/usr/lib/sendmail -bd -q30m

Then, kill any existing smtpd program and start Smail by executing the
above command.

To convert an SVR4.2 system create the same file above, remove the
smtpd entry in /etc/inetd.conf, then execute the following commands:

	sacadm -k -p inetd
	sacadm -s -p inetd
	/usr/lib/sendmail -bd -q30m


SMAIL AND 4.3BSD OR ULTRIX

Some versions of Ultrix, plus stock 4.3BSD, have had problems in with
some shell scripts supplied with Smail.  We have made some attempts to
simplify the offending shell scripts to avoid problems.  However, if
errors are still encountered with /bin/sh, then use a different shell,
such as ksh, zsh, pdksh, or bash, by compiling and installing Smail with
commands like:

	make SHELL=/bin/ksh depend
	make SHELL=/bin/ksh all
	make SHELL=/bin/ksh install

Some problems have also been reported with /bin/sh on Xenix.  I don't
know if there is an alternate shell available on Xenix, though I expect
pdksh would install and work just fine.  I realize that it seems absurd
that regular /bin/sh cannot be used on some systems.  Hopefully the
specific problems encountered can be found and remedied for future
releases.


RFC1413 SUPPORT

Smail contains support for RFC1413 identification of incoming SMTP/TCP
traffic.  This allows tracking of who sent mail within a trusting
community.  (By "trusting community", we mean that if you don't trust
root on the sending system then you cannot trust the RFC1413 ident
info supplied by the sending system.)  It also allows you to record the
identity of the connection originator from the sending system should
they ever request it back from you (often the sending system will encode
a cryptographically secure token that will be returned by Smail's ident
query.  This token will be logged by the sending system, and of course
by Smail too, which will enable either site to establish the identity of
the sending user should this ever be required.

The support is via the libident library, which is not part of the Smail
package.  Libident, currently at version 0.22 (November 4, 1998), is
available from a number of sites (use http://ftpsearch.lycos.com/, etc.,
to find one) or get it from the master site by anonymous FTP:

	<URL:ftp://ftp.lysator.liu.se/pub/ident/libs/>

This site also has information about RFC1413, servers for RFC1413 and
all sorts of other ident related goodies.

When you have the library code, you must compile it, and put the library
somewhere where it will be seen by the compiler and linker (use the "-I"
and "-L" switches in EDITME for CFLAGS and LDFLAGS respectively).
Change EDITME to define "RFC1413" in the HAVE lines and add "-lident" to
the LIBS.


WHOSON SUPPORT

Smail contains support for Eugene Crosser's WHOSON daemon.  This daemon
allows use of the special keyword "whoson" to be used when testing remote
client IP addresses for authorisation of such functions as remote SMTP
mail relay (i.e. in the configuration variable "smtp_remote_allow" and
other related variables).  If you have integrated WHOSON support into
your RADIUS daemon (or other authentication servers, such as your
POP/IMAP servers) then you may wish include WHOSON support in Smail to
allow roaming users to also use your system as an outgoing mail relay
just as they would if they were connecting from one of your own access
points of presence.

The WHOSON package is not part of the Smail distribution.  It is
currently at version 1.09 (Sept. 3, 1999) and is available separately
from a number of sites (use http://ftpsearch.lycos.com/, etc., to find
one) as well as from its master site by anonymous FTP:

	  <URL:ftp://ftp.average.org/pub/whoson/>

Change your conf/EDITME to define "LIBWRAP" in the HAVE lines and add
"-lwrap" to the LIBS.  Remember to add the necessary "-I" and/or "-L"
switches for CFLAGS and LDFLAGS respectively in your conf/EDITME as
necessary.

For more information about the WHOSON protocol and daemon please see the
following WWW site:

	  <URL:http://www.average.org/whoson/>


MEMORY ALLOCATION DEBUGGING SUPPORT

Smail contains support for memory allocation debugging.  Currently only
the SunOS-4 debugging malloc and Malloc Library by Mark Moraes (U. of
Toronto CSRI Malloc, or "Yet another malloc()") are supported.  You can
probably only use the former if you're running on SunOS-4 (or perhaps
under NetBSD with SunOS-4 emulation).  The latter library is available
from the following FTP site:

	<URL:ftp://ftp.cs.utoronto.ca/pub/moraes/>

Please see the conf/EDITME-dist file for more details.
