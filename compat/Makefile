#
#ident	"@(#)smail/compat:RELEASE-3_2_0_121:Makefile,v 1.25 2004/07/25 17:22:00 woods Exp"
#
# Makefile for the smail compat library.
#
#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

ROOT=..
THIS_DIR=compat

include ${ROOT}/conf/Make.local

MKDEPEND=${ROOT}/conf/lib/mkdepend.sh
MKDEFS=${ROOT}/conf/lib/mkdefs.sh
CHECKDEFS=${ROOT}/conf/lib/checkdefs.sh
XEXEC=${SHELL} ${ROOT}/conf/lib/xexec.sh
DEFS_SH=defs.sh
DEFS_H=defs.h
DEFS_SED=defs.sed

DUMMY_OBJ=dummy.o
DUMMY_CSRC=dummy.c
SIGNAME_OBJ=signame.o
SIGNAME_CSRC=signame.c
SYS5_STRLIB=_str2set.o strpbrk.o strspn.o strcspn.o
STRLIB_DIR=${ROOT}/pd/strlib
GETOPT_OBJ=get_opt.o
GETOPT_DIR=${ROOT}/pd/getopt

COMPAT_LIB=libcompat.a

.c.o:
	@. ./${DEFS_SH}; ${XEXEC} $$CC $$CFLAGS -I${ROOT}/src $$INCLUDES -c $*.c

all:	${COMPAT_LIB}

# NOTE: even though this target's rules use ${MAKE}, it should not
# depend on .MAKE.  Splitting this out would mandate "make depend"
#
# Versions of 'make' that always execute rules containing "${MAKE}"
# will end up running 'ar' and 'ranlib', but usually with no harmful
# side effects.
#
${COMPAT_LIB}: Makefile ${DEFS_SH} ${DUMMY_OBJ}
	@. ./${DEFS_SH};						\
	   case "$$HAVE_SYS5_STRLIB" in					\
	   '')	if [ -f ${STRLIB_DIR}/.depend ] ; then			\
			DEPMKFILE="-f Makefile -f .depend";		\
		else DEPMKFILE=""; fi;					\
		echo "	cd ${STRLIB_DIR} && ${MAKE} $$DEPMKFILE ${SYS5_STRLIB}"; \
		(cd ${STRLIB_DIR} && ${MAKE} $$DEPMKFILE ${SYS5_STRLIB}); \
		rc=$$?;							\
		if [ $$rc -ne 0 ] ; then				\
			exit $$rc;					\
		fi;							\
		for i in ${SYS5_STRLIB}; do				\
			${XEXEC} ${AR} rc ${COMPAT_LIB} ${STRLIB_DIR}/$$i; \
		done;;							\
	   esac;							\
	   case "$$HAVE_GETOPT" in					\
	   '')	if [ -f ${GETOPT_DIR}/.depend ] ; then			\
			DEPMKFILE="-f Makefile -f .depend";		\
		else DEPMKFILE=""; fi;					\
		echo "	cd ${GETOPT_DIR} && ${MAKE} $$DEPMKFILE ${GETOPT_OBJ}"; \
		(cd ${GETOPT_DIR} && ${MAKE} $$DEPMKFILE ${GETOPT_OBJ}); \
		rc=$$?;							\
		if [ $$rc -ne 0 ] ; then				\
			exit $$rc;					\
		fi;							\
		for i in ${GETOPT_OBJ}; do				\
			${XEXEC} ${AR} rc ${COMPAT_LIB} ${GETOPT_DIR}/$$i; \
		done;;							\
	   esac;							\
	   case "$$HAVE_SYS_SIGNAME/$$HAVE_SIG2STR" in			\
	   '/')	if [ -f .depend ] ; then				\
			DEPMKFILE="-f Makefile -f .depend";		\
		else DEPMKFILE=""; fi;					\
		echo "	${MAKE} $$DEPMKFILE ${SIGNAME_OBJ}";		\
		${MAKE} $$DEPMKFILE ${SIGNAME_OBJ};			\
		rc=$$?;							\
		if [ $$rc -ne 0 ] ; then				\
			exit $$rc;					\
		fi;							\
		for i in ${SIGNAME_OBJ}; do				\
			${XEXEC} ${AR} rc ${COMPAT_LIB} $$i;		\
		done;;							\
	   esac;							\
	   ${XEXEC} ${AR} rc ${COMPAT_LIB} ${DUMMY_OBJ};		\
	   ${XEXEC} $$RANLIB ${COMPAT_LIB}

${SIGNAME_OBJ}: ${DEFS_SH} ${SIGNAME_CSRC} Makefile

${SIGNAME_CSRC}: mksigname.sh siglist.in ${DEFS_SH} Makefile
	@rm -f ${SIGNAME_CSRC}
	@. ./${DEFS_SH}; \
	   ${XEXEC} ${SHELL} mksigname.sh siglist.in ${SIGNAME_CSRC}

mkdefs defs ${DEFS_SH} ${DEFS_H} ${DEFS_SED}: ${ROOT}/conf/EDITME
	ROOT=${ROOT} ${SHELL} ${MKDEFS}

.PRECIOUS: ${ROOT}/conf/EDITME
${ROOT}/conf/EDITME: ${DOT_MAKE} # cannot depend on anything else!
	cd ${ROOT}/conf && ${MAKE} EDITME

# XXX note that we don't go off to 'make depend' for STRLIB_DIR, nor
# for GETOPT_DIR.  Those should be created by the initial "make depend"
# done from the top level.
#
depend: check_defs
	@rm -f .depend
	@. ./${DEFS_SH};						\
	   files="dummy.c";						\
	   case "$$HAVE_SYS_SIGNAME/$$HAVE_SIG2STR" in			\
	   '/')								\
	       ${XEXEC} ${MAKE} ${SIGNAME_CSRC};			\
	       files="$$files ${SIGNAME_CSRC}";;			\
	   esac;							\
	   ${XEXEC} ${SHELL} ${MKDEPEND} $$CPPFLAGS $$INCLUDES $$files
	@. ./${DEFS_SH};						\
	   case "$$HAVE_SYS5_STRLIB" in					\
	   '')								\
		for i in ${SYS5_STRLIB}; do				\
		    echo "	echo \"${COMPAT_LIB}: $$i\" >> .depend"; \
		    echo "${COMPAT_LIB}: $$i" >> .depend;		\
		done;;							\
	   esac;							\
	   case "$$HAVE_GETOPT" in					\
	   '')								\
		for i in ${GETOPT_OBJ}; do				\
		    echo "	echo \"${COMPAT_LIB}: $$i\" >> .depend"; \
		    echo "${COMPAT_LIB}: $$i" >> .depend;		\
		done;;							\
	   esac;							\
	   case "$$HAVE_SYS_SIGNAME/$$HAVE_SIG2STR" in			\
	   '/')								\
		for i in ${SIGNAME_OBJ}; do				\
		    echo "	echo \"${COMPAT_LIB}: $$i\" >> .depend"; \
		    echo "${COMPAT_LIB}: $$i" >> .depend;		\
		done;;							\
	   esac
	@. ./${DEFS_SH};						\
	   echo "$$DEFS_DEPEND" >> .depend

check_defs:
	SHELL=${SHELL} ROOT=${ROOT} ${SHELL} ${CHECKDEFS}

install:

clean:
	rm -f .${DEFS_SH} .${DEFS_H} .${DEFS_SED}
	rm -f ${DUMMY_OBJ} ${SYS5_STRLIB} ${GETOPT_OBJ} ${BSTRING}
	rm -f ${SIGNAME_OBJ} ${SIGNAME_CSRC}
	rm -f a.out core
	rm -f ${COMPAT_LIB}

clobber: clean
	rm -f .depend
	rm -f ${DEFS_SH} ${DEFS_H} ${DEFS_SED}

.PHONY: all install local-install clean local-clean clobber local-clobber depend local-depend lint mkdefs defs check_defs tags TAGS
