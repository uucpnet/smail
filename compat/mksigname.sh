#! /bin/sh
:
#ident	"@(#)smail/compat:RELEASE-3_2_0_121:mksigname.sh,v 1.4 2003/06/11 17:18:41 woods Exp"

#
# Script to generate signame.c
#
# The technique for doing this comes from the siglist.sh script
# included in the PD-KSH sources, apparently written by Michael
# Rendell <michael@panda.cs.mun.ca>

set -e

. ./defs.sh

tmpin="tmpi$$.c"
tmpout="tmpo$$.c"
trapsigs='0 1 2 13 15'

if [ $# -gt 0 ]; then
	siglist_file=$1
	shift
	exec < $siglist_file
else
	siglist_file="<standard-input>"
fi

if [ $# -gt 0 ]; then
	outfile=$1
	shift
	exec > $outfile
else
	outfile="<standard-output>"
fi


trap 'rc=$?; rm -f $tmpin $tmpout; trap 0; exit $rc' $trapsigs

CC="${CC:-cc}"
CPP="${CPP:-${CC} -E -C}"

cat <<__EOF__
/*
 * $outfile:
 *
 *	define an array listing the names of system signals
 *
 * #     #                                                   ###
 * #  #  #    ##    #####   #    #     #    #    #   ####    ###
 * #  #  #   #  #   #    #  ##   #     #    ##   #  #    #   ###
 * #  #  #  #    #  #    #  # #  #     #    # #  #  #         #
 * #  #  #  ######  #####   #  # #     #    #  # #  #  ###
 * #  #  #  #    #  #   #   #   ##     #    #   ##  #    #   ###
 *  ## ##   #    #  #    #  #    #     #    #    #   ####    ###
 *
 * THIS FILE IS GENERATED AUTOMATICALLY BY THE SCRIPT $0 FROM
 * THE SIGNAL LIST IN THE FILE "$FROM_ROOT/$siglist_file".
 *
 * IF YOU SEE ANY "Unknown Signal" ENTRIES YOU MUST MAKE ADDITIONS TO
 * THE SIGNAL LIST FILE AND REBUILD RATHER THAN EDITING THIS FILE
 * DIRECTLY.
 */

#include "defs.h"
#include <sys/types.h>
#include "../src/smail.h"
#include "compat.h"
#include "../src/smailport.h"

__EOF__

(
	echo '#include <signal.h>';
	# note sed reads siglist.in from stdin
	sed -e '/^[	 ]*#/d' \
	    -e '/^[ 	]*$/d' \
	    -e 's/^[	 ]*\([^ 	][^ 	]*\)[	 ][	 ]*\(.*[^ 	]\)[ 	]*$/#ifdef SIG\1\
	QwErTy SIG\1	"\1",   	\/\* \2 \*\/\
#endif/'
) > $tmpin

$CPP $CPPFLAGS $tmpin > $tmpout

sed -n -e 's|^[ 	]*QwErTy \([0-9]\)[ 	]|/*  \1 */|p' \
       -e 's|^[ 	]*QwErTy \([0-9][0-9]*\)[ 	]|/* \1 */|p' < $tmpout | \
	sort -n +1 | \
	awk 'BEGIN {
		last = 0;
		nsigs = 0;
		printf("const char *const sys_signame[] = {\n");
		printf("/*  0 */\t\"Signal 0\",\t/* Fake value for zero */\n");
	}
	{
		n = $2;
		if (n > 0 && n != last) {
			while (++last < n) {
				printf("/* %2d */\t\"#%d\",\t\t/* Unknown Signal %d !!! */\n", last, last, last);
			}
			last = n;
			print $0;
		}
	}
	END {
		printf("};\n\n");
		printf("const unsigned int sys_nsigname = sizeof(sys_signame) / sizeof(sys_signame[0]);\n");
	}'

exit 0
