/*
#ident	"@(#)smail/compat:RELEASE-3_2_0_121:compat.h,v 1.2 2003/12/14 22:42:45 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * compat.h:
 */

/* external functions defined in dummy.c */
extern int dummy_reference __P((void));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
