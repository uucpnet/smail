/*
#ident	"@(#)smail/compat:RELEASE-3_2_0_121:dummy.c,v 1.6 2003/12/14 22:42:45 woods Exp"
 */

#include "defs.h"
#include <sys/types.h>
#include "../src/smail.h"
#include "compat.h"
#include "../src/smailport.h"

/*
 * Provide a dummy reference so there is at least one item in
 * compat.a.  This will keep ld from complaining about empty libraries
 * and such.
 */
int
dummy_reference()
{
    return 1;
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
