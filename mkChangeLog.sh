#! /bin/sh
#
#	genenerated the ChangeLog file since the last release
#
#ident "@(#)smail:RELEASE-3_2_0_121:mkChangeLog.sh,v 1.7 2004/12/14 00:34:53 woods Exp"

# always use the latest rcs2log!
#
RCS2LOG="rcs2log"

# current level
#
LEVEL=$(awk '/^3\./ {print $1;}' level)

# previous level (i.e. previous release tag)
#
# this works even for preview releases since awk does atoi() on
# expression terms before doing arithmetic with them
#
PREVIOUS=$(awk -F . '/^3\./ {printf("RELEASE-%d_%d_%d_%d\n", $1, $2, $3, $4 - 1);}' level)

# also use the date to avoid junk from old removed files and new
# untagged files
#
# get it from the RCS Id
#
ODATE=$(cvs log -N -r${PREVIOUS} level | awk '/^date:/ {print $2}')

cvs -q rlog -d">${ODATE}" -r${PREVIOUS}:: m-smail > smail-${LEVEL}.log

export pository=$(cat CVS/Repository)/

${RCS2LOG} -c smail.changelog -L smail-${LEVEL}.log > ChangeLog-${LEVEL}

rm smail-${LEVEL}.log
