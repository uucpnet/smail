#ident	"@(#)smail/NOTES:RELEASE-3_2_0_121:ESMTP,v 1.1 1996/03/05 19:32:41 woods Exp"

------------------------------------------------------------
ESMTP and Smail-3
------------------------------------------------------------

What is ESMTP?
--------------

ESMTP (Extended SMTP) is a framework for extending the SMTP service by
defining a means whereby a server SMTP can inform a client SMTP as to
the service extensions it supports. Standard extensions to the SMTP
service are registered with the IANA (Internet Assigned Number
Authority).  This framework does not require modification of existing
SMTP clients or servers unless the features of the service extensions
are to be requested or provided.

Ref: Paraphased from RFC-1651.1 (Abstract)


Reason for ESTMP?
-----------------

Ref: Paraphased from RFC-1651.2 (Introduction)

The Simple Mail Transfer Protocol (SMTP) has provided a stable,
effective basis for the relay function of message transfer agents.
Although a decade old, SMTP has proven remarkably resilient.
Nevertheless, the need for a number of protocol extensions has become
evident. The ESMTP framework enhances SMTP in a straightforward
fashion that provides a framework in which all future extensions can
be built in a single consistent way.

Ref: Paraphased from RFC-1651.2 (Introduction)


What are the ESMTP extensions?
------------------------------

ESMTP MAIL KEYWORDS

[RFC1651] specifies that extension to SMTP can be identified with
keywords.

Keywords	Description				 Reference
------------    --------------------------------         ---------
SEND		Send as mail				  [RFC821]
SOML		Send as mail or terminal		  [RFC821]
SAML		Send as mail and terminal		  [RFC821]
EXPN		Expand the mailing list			  [RFC821]
HELP		Supply helpful information		  [RFC821]
TURN		Turn the operation around		  [RFC821]
8BITMIME	Use 8-bit data				 [RFC1652]
SIZE		Message size declaration		 [RFC1653]
VERB		Verbose		                     [Eric Allman]
ONEX		One message transaction only	     [Eric Allman]


MAIL EXTENSION TYPES

The Simple Mail Transfer Protocol [RFC821] specifies a set of
commands or services for mail transfer.  A general procedure for
extending the set of services is defined in [RFC1651].  The set of
service extensions is listed here.

Service Ext   EHLO Keyword Parameters Verb       Reference
-----------   ------------ ---------- ---------- ---------
Send             SEND         none       SEND     [RFC821]
Send or Mail     SOML         none       SOML     [RFC821]
Send and Mail    SAML         none       SAML     [RFC821]
Expand           EXPN         none       EXPN     [RFC821]
Help             HELP         none       HELP     [RFC821]
Turn             TURN         none       TURN     [RFC821]
8 Bit MIME	 8BITMIME     none       none    [RFC1652]
Size		 SIZE         number     none    [RFC1653]

Ref: ftp://ftp.isi.edu/in-notes/iana/assignments/mail-parameters


Where can I look for more detailed information?
-----------------------------------------------

[RFC821] Postel, J., "Simple Mail Transfer Protocol", STD 10, RFC 821,
         USC/Information Sciences Institute, August 1982.

[RFC822] Crocker, D., "Standard for the Format of ARPA-Internet Text
         Messages", STD 11, RFC 822, UDEL, August 1982.

[RFC1651] Klensin, J., Freed, N., Rose, M., Stefferud, E., and D.
          Crocker, "SMTP Service Extensions", RFC 1651, MCI, Innosoft,
          Dover Beach Consulting, Inc., Network Management Associates,
          Inc., Silicon Graphics, Inc., July 1994.

[RFC1652] Klensin, J., Freed, N., Rose, M., Stefferud, E., and D.
          Crocker, "SMTP Service Extension for 8bit-MIMEtransport", 
          RFC 1652, MCI, Innosoft, Dover Beach Consulting, Inc.,
          Network Management Associates, Inc., Silicon Graphics, Inc.,
          July 1994.

[RFC1653] Klensin, J., Freed, N., and K. Moore, "SMTP Service
          Extension for Message Size Declaration", RFC 1653,
          MCI, Innosoft, University of Tennessee, July 1994.

[RFC1700] J. Reynolds, J. Postel, "ASSIGNED NUMBERS", 10/20/1994. 
          (Pages=230) (Format=.txt) (Obsoletes RFC1340) (STD 2) 

Ref: ftp://ftp.internic.net/rfc/


Who authored the ESMTP support for smail?
-----------------------------------------

Simon Leinen <simon@lia.di.epfl.ch>


What smail releases have ESMTP support?
---------------------------------------

Any smail version greater than or equal to 3.2 can support ESMTP provided
the conf/EDITME file has the EHLO option enabled.

#HAVE=EHLO				# have ESMTP sending support


What ESMTP extensions does SMAIL support?
-----------------------------------------

Date: Wed, 14 Jun 95 17:01 MDT
From: Simon Leinen <simon@lia.di.epfl.ch>
Subject: Finding the list of registered ESMTP services

Smail doesn't support SEND, SOML or SAML (writing directly to a user's
terminal).  EXPN and HELP are supported without further work;  the
ESMTP patches simply announce their presence.  SIZE and (partially)
8BITMIME are supported by my patches, as is the non-yet-registered
PIPELINING extension.  I don't know what would need to be done to
support TURN, or whether that would be worthwhile.

The PIPELINING extension is described in an Internet Draft,
draft-ietf-mailext-pipeline-02.txt.  A couple of other proposed
extensions which I haven't implemented can be found under
<URL:http://www.ietf.cnri.reston.va.us/ids.by.wg/mailext.html>.

Of all the mail servers I know, PMDF implements the largest number of
SMTP extensions.  Try and say "EHLO" to THOR.INNOSOFT.COM!

Other extensions can be defined locally - their names must start with
an "X".  Some Sendmail 8 versions support an extension called "XVRB"
to announce that they understand "VERB" (...ose) which is useful for
debugging.  Smail with V2 of my ESMTP patches recognizes this and
turns verbose mode on when debugging SMTP transactions.  However, the
XVRB extension is *not* implemented in Smail's SMTP server.

Eric Allman seems to have registered VERB and ONEX, so I should change
the code to recognize VERB instead of (or in addition to) XVRB.


Does smail support the 8BITMIME ESTMP extension?
------------------------------------------------

Date: Wed, 14 Jun 95 07:45 MDT
From: Simon Leinen <simon@lia.di.epfl.ch>
Subject: Tweaks to src/geniobpeek.sh to support BSDI-1.1

>>>>> "Jon" == Jon Diekema <diekema@cideas.com> writes:

Jon> I noticed that an ELHO to onramp.i2k.com doesn't return the
Jon> 250-8BITMIME message.

In general, 8BITMIME is useful only for people who communicate in
languages like french, swedish or german, which use 8-bit (ISO-8859-x)
characters to represent accents etc., and which don't have transparent
MIME encoding/decoding in their mailers.  8BITMIME allows them to send
out and receive these characters transparently.  This has never really
worked in the presence of mailers that clean the 8th bit, but there
are large "islands" where users rely on the fact that this works.
8BITMIME is a standard way to announce 8-bit-transparency.

The support for 8BITMIME isn't enabled in the default configuration of
my ESMTP patch.  I decided to do this because it is not implemented
completely - the difficult part is missing.

If you never expect to forward mail to non-8BITMIME mailers (you can
narrow this to "non-8-bit-transparent mailers"), then you could enable
my 8BITMIME support so that a remote 8BITMIME-capable mailer can detect
that it can send you 8-bit text without having to encode it using
QUOTED-PRINTABLE or BASE64.

But if you claim 8BITMIME support and you do transmit messages to
non-8-bit mailers, then the RFC obliges you to encode these messages
so that they are 7-bit clean.  Smail cannot do this - it would be nice
if it could! Unfortunately it is a bit tricky - you have to parse
multipart MIME messages and encode per subpart.

The necessity of parsing multipart messages should probably be
explained: The MIME standard forbids to encode the top level of
multipart/... and similar (message/...) contents using BASE64 or
QUOTED-PRINTABLE.  You have to (recursively) descend to the subparts
and encode them individually instead.  It's a reasonable requirement
because otherwise it would be more difficult to parse multipart
documents.

I think that there are now Sendmail configurations with the same
incomplete level of 8BITMIME support provided by my patch, but this is
not really an argument that you should use it too :-)

Jon> What is needed to active the 8BITMIME support?  It doesn't appear
Jon> that this is turn on by default.  What did Simon do to enable it?
Jon> Is it as simple as defining HAVE_ESMTP_8BITMIME, and recompiling?
Jon> Are there any ill side-effects from doing this?

Yes, I think all you have to do is define HAVE_ESMTP_8BITMIME.
The ill side-effects are that you aren't RFC-compliant anymore...

Actually it can be worse: in a scenario where you get mail from a
mailer that supports 8BITMIME correctly (such as Sendmail 8.7), and
have to relay it to a non-8-bit-transparent mailer, enabling Smail's
pseudo-8BITMIME support actually causes the eighth bit to be lost.


What is the PIPELINE ESTMP extension?
-------------------------------------

This is an extension to the SMTP service whereby a server can indicate
the extent of its ability to accept multiple commands in a single TCP
send operation. Using a single TCP send operation for multiple
commands can improve SMTP performance significantly over high-latency
connections.
