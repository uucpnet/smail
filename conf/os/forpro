# -*-sh-*-
#ident	"@(#)smail/conf/os:RELEASE-3_2_0_121:forpro,v 1.8 2003/03/27 19:43:12 woods Exp"
#
# forpro - fortune forpro operating system configuration description

# OSNAMES - Names defining this operating system
OSNAMES="UNIX_V7:UNIX:UNIX_FORPRO"

# ARCH_TYPE - this is likely true as these are mc68k....
ARCH_TYPE=32bit

# LOCKING_PROTOCOL - macros for efficient file locking
LOCKING_PROTOCOL="\
#define F_ULOCK 0
#define F_LOCK  1
#define F_TLOCK 2
#define LOCK_REQUIRES_WRITE
#define lock_fd(fd)             (lockf((fd), F_TLOCK, 0L) < 0? FAIL: SUCCEED)
#define lock_fd_wait(fd)        (lockf((fd), F_LOCK, 0L) < 0? FAIL: SUCCEED)
#define unlock_fd(fd)           ((void) lockf((fd), F_ULOCK, 0L))
#define unlock_fd_wait(fd)      ((void) lockf((fd), F_ULOCK, 0L))
"

# LOCK_BY_NAME - should spool files use named lock-files for locking
LOCK_BY_NAME=

# MAILBOX_DIR - in which directory are user mailbox files
MAILBOX_DIR=/usr/spool/mail                     # Almost anything else

# CONSOLE - name of the console device file
CONSOLE=/dev/console

# DECLARE_STRINGS - declare string routines, using macros as needed
DECLARE_STRINGS="\
extern char	*strcat();
extern char	*strncat();
extern int	strcmp();
extern int	strncmp();
extern char	*strcpy();
extern char	*strncpy();
extern int	strlen();
extern char	*index();
extern char	*rindex();
  /* the following are defined in string.c, if they are not in libc */
extern int	bzero();
extern int	bcopy();
extern int	bcmp();
extern char	*strpbrk();
extern int	strspn();
#define memcpy(b1,b2,n)	(bcopy(b2,b1,n))
#define memcmp(b1,b2,n)	(bcmp(b1,b2,n))
#define strchr	index
#define strrchr	rindex
"

# SECURE_PATH - directories containing system-supplied user programs
SECURE_PATH=/bin:/usr/bin

# OSLIBS - name any object libraries containing routines we will need
OSLIBS="-ldbm"

# DRIVER_CONFIGURATION - configuration file describing smail drivers
DRIVER_CONFIGURATION=unix-generic

# RANLIB - how do we organize an existing object archive library
RANLIB=ranlib                           # Behavior compatible with BSD

# CHOWN - command to use for accessing the chown program
CHOWN=/etc/chown                        # Behavior compatible with BSD

# SITENAME_FILE - file containing name of local host
SITENAME_FILE=/etc/sitename             # correct for Fortune FOR:Pro system

# HAVE - what features should be used with this operating system
HAVE=$HAVE:DBM:DUP2

# MISC_DEFINES - miscellaneous definitions
#
# forpro doesn't quite understand the void type.
MISC_DEFINES=$MISC_DEFINES:void=int

# UUCP_SYSTEM_FILE - path to UUCP file containing remote systems
UUCP_SYSTEM_FILE=/usr/lib/uucp/L.sys
