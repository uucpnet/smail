# -*-sh-*-
#ident	"@(#)smail/conf/os:RELEASE-3_2_0_121:template,v 1.57 2005/10/28 17:17:37 woods Exp"
#
# template - template for operating system configuration description
#
# NOTE: variables come in two basic types: lists and simple variables.
# Lists are : or white-space-separated lists of strings (ususally
# just names), though some lists have specific constraints on separation
# characters.  Simple variables have only one value.
#
# For filenames, the os config should specify what would be "native" if
# Smail were to be installed and shipped by the vendor.
#
# The EDITME file can be used to override any simple variable defined
# in this file.  The EDITME file can be used to add to list variables.


# OSNAMES(list) - names for this operating system
#
# This is a colon-separated list of names which generally, or specifically
# define the operating system that smail will be executed under.
#
# The following names should be used where applicable:
#
#	UNIX_BSD4_1	- the 4.1 release of Berkeley UNIX
#	UNIX_BSD4_2	- the 4.2 or 4.3 release of Berkeley UNIX
#	UNIX_BSD4_3	- the 4.3 release of Berkeley UNIX
#	UNIX_BSD4_4	- the 4.4 release of Berkeley UNIX
#	UNIX_BSD4_4_LITE - the 4.4-Lite release of Berkeley UNIX
#	UNIX_BSD	- any release of Berkeley UNIX after 4.1
#	UNIX_SYS5_2	- release two of AT&T System V
#	UNIX_SYS5_3	- release three of AT&T System V
#	UNIX_SYS5	- any release of AT&T System V
#	UNIX_SUN_OS_3	- SunOS 3.x, greater than 3.2
#	UNIX_SUN_OS_4	- SunOS 4.0.x
#	UNIX_SUN_OS_4	- SunOS 4.1.x
#	UNIX_SUN_OS	- any release of SunOS
#	UNIX_UTS_1	- Amdahl UTS/580 1.x
#	UNIX_UTS_2	- Amdahl UTS/580 2.x
#	UNIX_UTS	- Any release of UTS/580
#	UNIX_V7		- Bell Labs UNIX Release 7
#	UNIX		- Any release of UNIX
#	POSIX_OS	- Any POSIX P1003.1-conformant UNIX system
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

OSNAMES="UNIX"


# ARCH_TYPE - system architecture fundamental size
#
# required
#
# The ARCH_TYPE of 16bit refers to machines with 16 bit words.	This
# mode is untested.  It is assumed that 16bit machines have extended
# address spaces as smail is more than 64k bytes long.
#
#	64bit		- still somewhat uncommon - but fun!
#	32bit		- common for most modern systems
#	small32bit	- 32 bit without reasonable VM
#	16bit		- untested (i286'en, PDP11's, etc.)

ARCH_TYPE="32bit"


# LOCKING_PROTOCOL(variable) - macros for efficient file locking
#
# Define macros for performing high-performance locking on files
# These should include the following macros:
#
#	lock_fd		- given a file descriptor, either lock the
#			  file for exclusive (write) access without
#			  blocking and return SUCCEED or fail to lock
#			  and return FAIL.  This is used for locking
#			  spool files.
#
#	lock_fd_wait	- given a file descriptor, lock it for
#			  exclusive (write) access and return SUCCEED
#			  or return FAIL.  Blocking is permitted.
#			  This may be used for locking mailbox files.
#
#	unlock_fd	- given the file descriptor of a file locked
#			  with the lock_fd() macro, unlock that file.
#			  This can be an expression cast to void.
#
#	unlock_fd_wait	- given the file descriptor of a file locked
#			  with the lock_fd_wait() macro, unlock that
#			  file.  This can be an expression cast to
#			  void.
#
#	lock_fd_rd	- given a file descriptor, lock it for shared
#			  access without blocking.  This is used when
#			  reading database files that might be edited
#			  manually (those using lsearch, bsearch).
#			  Closing the file shoud be sufficient for
#			  releasing the lock.  This should return
#			  SUCCEED or FAIL.
#
#	lock_fd_rd_wait - given a file descriptor, lock it for shared
#			  access.  Blocking is permitted.  This is
#			  used when reading database files that are
#			  normally updated by programs.  Closing the
#			  file shoud be sufficient for releasing the
#			  lock.  This should return SUCCEED or FAIL.
#
# If no reasonable high-efficiency locking method is available for
# your system, then do not define this macro, or set it to a null
# string.  WARNING: this may no longer be possible!  [see src/retry.c]
#
# If the locking protocol requires that the file being locked be
# writable, define LOCK_REQUIRES_WRITE.
#
# To use the System V shared lock fcntl(), define USE_FCNTL_RD_LOCK,
# and define lock_fd_rd() to be (fcntl_rd_lock(fd)), define
# lock_fd_rd_wait() to be (fcntl_rd_lock_wait(fd)), and declare
# fcntl_rd_lock() to be extern int.

LOCKING_PROTOCOL=


# LOCK_BY_NAME(variable) - should spool files use named lock-files for locking
#
# If no locking protocol is defined, or a site prefers to use named
# lock files for locking spool files, LOCK_BY_NAME should be defined
# to be "TRUE", otherwise it should not be defined.

LOCK_BY_NAME=TRUE


# FLOCK_MAILBOX(variable) - should mailbox files be locked with lock_fd_wait
#
# Set this to "TRUE" if the lock_fd_wait macro should be used to lock
# mailbox files.  This should be used only with cooperation from all
# mail user agents.  Otherwise, do not set this.

#FLOCK_MAILBOX=TRUE


# MAILBOX_DIR(variable) - in which directory are user mailbox files found
#
# Normally this is either /usr/spool/mail for a V7/BSD-derived system
# or /usr/mail for a System V derived system.  On many newer POSIX-type
# systems, mail is in /var/mail.

#MAILBOX_DIR=/var/mail				# POSIX
#MAILBOX_DIR=/var/spool/mail			# screwy systems like SunOS
#MAILBOX_DIR=/usr/mail				# System V
MAILBOX_DIR=/usr/spool/mail			# Almost anything else


# DAEMON_PID_FILE(variable) - file in which master daemon's PID is stored
#
# Normally this is either /var/run/smail/pid for a BSD-derived system
# or /usr/adm for all others (may only be right for a System V derived
# system?).

#DAEMON_PID_FILE=/var/run/smail.pid
DAEMON_PID_FILE=/usr/adm/smail.pid


# CONSOLE(variable) - name of the console device file
#
# This pathname is used as a last resort in writing panic messages from
# smail.  Normally, it is /dev/console.  Some System V machines can also
# use /dev/sysmsg

#CONSOLE=/dev/sysmsg
CONSOLE=/dev/console


# DECLARE_STRINGS(variable) - declare string routines, using macros as needed
#
# The following function names are used by smail and should be
# declared using the C code below:
#
#	strcat(s1,s2)	- concatenate string s2 onto the end of s1
#	strncat(s1,s2,n)- concatenate s2 onto s1 with length constraint n
#	strcmp(s1,s2)	- compare strings s1 and s2
#	strncmp(s1,s2,n)- compare s1 and s2 with length constraint n
#	strcpy(s1,s2)	- copy string s2 to s1
#	strncpy(s1,s2,n)- copy string s2 to s1 for at most n bytes
#	strlen(s1)	- return the length of string s1
#	strchr(s,c)	- point to first occurance of c in string s
#	strrchr(s,c)	- point to last occurance of c in string s
#	memset(b,c,n)	- set the n bytes starting at location b to c
#	memcpy(b1,b2,n)	- copy b2 to b1 for n bytes
#	memcmp(b1,b2,n)	- compare n bytes of b1 and b2, returning 0 if equal
#
# Use of #include or #define is permitted.

DECLARE_STRINGS="\
extern char	*strcat();
extern char	*strncat();
extern int	strcmp();
extern int	strncmp();
extern char	*strcpy();
extern char	*strncpy();
extern int	strlen();
extern char	*strchr();
extern char	*strrchr();
extern char	*strpbrk();
extern int	strspn();
extern char	*memset();
extern char	*memcpy();
extern char	*memcmp();
"


# OSLIBS(list) - name any object libraries containing routines we will need
#
# In particular, if networking routines and dbm routines are in libraries
# other than libc, these libraries should be named here.  Systems that
# can use shared libraries may specify a shared version of libc here.
#
# Object library names must be white-space separated.
#
# NOTE:	To compile arpatxt (in the pathalias sources), one of the listed
#	libraries must include regular expression functions.  However,
#	arpatxt is not compiled by default, so such functions are not
#	normally necessary.
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

#OSLIBS="-ldbm -lsocket"	# example
#OSLIBS="-lc_s"			# shared C library for System V.3
#[ "$FROM_ROOT" = src ] && OSLIBS="-lresolv" # resolv library for src directory


# LDFLAGS(list) - flags that must be passed to ld when creating an executable
#
# Some systems require that particular flags be passed to the loader
# such as for allocating extra stack space.
#
# Loader flags must be white-space separated.
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

#LDFLAGS="-F 2000"	# loader flags for XENIX/286


# CFLAGS(list) - flags that should be passed to the C compiler
#
# This is a white-space-separated list of arguments that are passed
# to the C compiler when compiling anything.  This can be used to
# set some very special case #defines, or to pass in compiler flags
# such as selection of large-model addressing for the Xenix/286.
#
# NOTE:  Use of -O or -g is normally selected from the EDITME file.

#CFLAGS="-Xt"		# select transitional ANSI C for SVR4.0 C compiler


# CPPFLAGS(list) - flags to pass into C preprocessor
#
# This list is appended to the CFLAGS list, it also passed to mkdepend,
# for building dependencies, which is not true of CFLAGS.  This is useful
# mostly for systems that require "-systype ..." as input the the C
# preprocessor to select default include file locations.
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

#CPPFLAGS="-systype bsd"
#CPPFLAGS="-systype sysv"


# DRIVER_CONFIGURATION(variable) - configuration file describing smail drivers
#
# Name the configuration file in the conf/driver directory which defines
# a suitable set of smail director, router and transport drivers for
# this operating system.

#DRIVER_CONFIGURATION=arpa-network		# includes BIND/DNS router
#DRIVER_CONFIGURATION=bsd-network		# mini-LAN configs
DRIVER_CONFIGURATION=unix-generic


# RANLIB(variable) - how do we organize an existing object archive library
#
# RANLIB should define the base for a shell command which, given an
# object file archive, will organize that library for the loader.
# If no such command is appropriate, set RANLIB to ":", to prevent
# it from doing anything interesting.

#RANLIB=ranlib				# Behavior compatible with BSD
RANLIB=:				# many other versions of UNIX


# CHOWN(variable) - command to use for accessing the chown program
#
# Under Berkeley UNIX, chown is under /etc.  On most other computer systems
# chown is in a normal PATH.  The CHOWN variable should be set appropriately
# for this operating system.

#CHOWN=/etc/chown			# 4.3BSD & SunOS w/ BSD features
#CHOWN=/usr/sbin/chown			# 4.4BSD
CHOWN=chown				# Most other versions of UNIX


# SITENAME_FILE(variable) - file containing name of local host
#
# Some operating systems store the name of the local host in a file, such
# as /etc/sitename or /etc/whoami.  Define SITENAME_FILE if your site
# requires this.
#
# Don't define this if your system has gethostname(2) or uname(2).

#SITENAME_FILE=/etc/sitename		# correct for Fortune FOR:Pro system


# HAVE(list) - list of attributes identified for your operating system
#
# For any item that is supported by your operating system, add the
# item name to the HAVE list.  This list translates into #defines
# of the form HAVE_feature, which can be tested in C source files.
# The definitive list of possible HAVE attributes can be found by
# looking through the source code for references to these macros.
#
# Current list of defined names:
#
#	BIND		- system supports the DNS (Domain Name System)
#	BSD_NETWORKING	- system supports BSD-style socket/TCP/IP networking
#	COMSAT		- system has the comsat daemon
#	CHSIZE		- system has chsize system call
#	DBM		- system has the dbm(3x) library, *including* dbmclose
#	DUP2		- system has dup2 function
#	FGETPWENT	- system has fgetpwent(3) [needed for pwfile in user.c]
#	FSYNC		- system has fsync system call
#	FTRUNCATE	- system has ftruncate system call
#	GETHOSTNAME	- system has gethostname function, as in BSD
#	GETOPT		- system has getopt function
#	HASH_BANG	- system can use "#! /bin/sh" in bourne shell scripts
#	HDB_UUCP	- system uses HoneyDanBer(-like) UUCP (uux has -g & -a)
#	LIMITS_H	- system has <limits.h>
#	LOGIN_CAP_H	- system has <login_cap.> as in BSD/OS (setusercontext())
#	MEMORY_H	- system does not have STDC_HEADERS but has <memory.h>
#	MKDIR		- system has mkdir function
#	NDBM		- system has the newer ndbm(3) functions or db(3) ndbm.h
#	READDIR		- system has opendir/readdir/closedir functions
#	RENAME		- system has rename system call
#	RLIMIT		- system has the setrlimit/getrlimit functions
#	SETGROUPS	- system has setgroups system call, as in BSD/Posix
#	SETUSERCONTEXT	- system has setusercontext(3) as in BSD/OS
#	SIG2STR		- system has sig2str(3) function, as in SunOS-5
#	STATFS		- system has either 4.4BSD or SysVr3 statfs(2)
#	STATVFS		- system has SysVr4 statvfs(2) [<sys/statvfs.h> assumed]
#	STDLIB_H	- system has ANSI C <stdlib.h> w/out <stddef.h>
#	STRERROR	- system has ANSI C strerror(3) function
#	STRINGS_H	- system only has old 4BSD <strings.h>
#	STRSIGNAL	- system has strsignal(3) function
#	SYS_SIGNAME	- system has global sys_signame array
#	SYS_STATFS_H	- system has <sys/statfs.h>
#	SYS_TIME_H	- system has <sys/time.h>
#	SYS_VFS_H	- system has <sys/vfs.h>
#	SYS5_STRLIB	- system has strspn and strpbrk
#	SYSEXITS_H	- system has <sysexits.h>
#	TYPE_GID_T	- system has gid_t (presumably in <sys/types.h>)
#	TYPE_PID_T	- system has pid_t (presumably in <sys/types.h>)
#	TYPE_UID_T	- system has uid_t (presumably in <sys/types.h>)
#	TYPE_SSIZE_T	- system has ssize_t (presumably in <sys/types.h>)
#	TYPE_BLKCNT_T	- system has blkcnt_t (presumably in <sys/types.h>)
#	TYPE_IN_ADDR_T	- system has in_addr_t (presumably in <sys/types.h>)
#	TYPE_IN_PORT_T	- system has in_port_t (presumably in <sys/types.h>)
#	TYPE_SOCKLEN_T	- system has socklen_t (presumably in <sys/types.h>)
#	ULIMIT		- system has ulimit system call, as in System V.3
#	UNAME		- system has uname system call, as in System V
#	UNISTD_H	- system has <unistd.h>
#	UTIME_H		- system has <utime.h> but isn't necessarily POSIX.
#	VFPRINTF	- system has vfprintf function
#	YP		- system has SunOS YP (NIS) service
#	MEMMOVE		- system has ANSI C memmove() function
#
# NOTE: if you don't define DBM or NDBM, then smail will use sdbm.
# If you don't have ndbm, and your dbm library doesn't have dbmclose(),
# then don't define DBM, because smail can't use your dbm, and will
# have to use sdbm instead.
#
# NOTE: if both STRING_H and STRINGS_H are not defined then STRING_H
# will be assumed.  If you need to include both then define STRING_H
# as well.  If you don't have STDC_HEADERS but you do have <memory.h>
# then define MEMORY_H.
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

#HAVE=$HAVE:GETOPT:UNAME:READDIR


# NO_HAVE(list) - define features that this system specifically does not have
#
# The NO_HAVE variable is a list of features that should be named this
# way.  Note that the OS type is far too often used to indicate that a
# set of features exist.  If smail thinks that your OSTYPE file has a
# particular feature, then you may have to use the NO_HAVE list in
# your EDITME to insist that the feature does not exist (or that the
# feature should not be used).
#
# Some possible feature names:
#
#	DBM_PAGFNO	- ndbm missing dbm_pagfno() (4.4BSD ndbm emulation)
#	MEMMOVE		- the system uses ANSI C, but lacks memmove()

#NO_HAVE=$NO_HAVE:MEMMOVE


# MISC_DEFINES(list) - miscellaneous definitions
#
# This is a list of names that will be defined for use by any C
# program or shell script that loads defs.sh files.  These generally
# define very system-specific items that are tested in the sources.
# A simple value can be given using the form NAME=value
#
# Some of the currently useful names are:
#
#	GETHOSTNAME_USE_PTR
#		The Genix4.1 gethostname() system call takes a pointer 
#		to a buffer size rather than the buffer size itself.
#		Define this for systems that have this behavior.
#
#	INET_ADDR_USE_STRUCT
#		There appear to be some systems where inet_addr()
#		returns a struct in_addr, rather than an unsigned
#		long.  We have been told that some versions of Ultrix
#		are inconsistent between what is in the man page and
#		the #include files and how the library routine is
#		actually defined.  Define this for systems that have
#		this non-BSD behavior.
#
#	REQUIRE_TZ
#		It has been reported that the Xenix5.0 ctime() function
#		dumps core if the TZ environment variable is not set.
#		If your system has this rather buggy behavior, define
#		this.  If this is defined, smail will set the
#		environment variable TZ to GMT0 if it is not already
#		set.
#
#	USE_TZNAME
#		The POSIX/SysV way of finding the timezone name.  You
#		may need this if your OS definition doesn't define
#		UNIX_SYS5 or POSIX_OS.
#
#	USE_TM_ZONE
#		The SunOS-4 (and 4.3BSD?) preferred way of finding the
#		timezone name.
#
#	DECLARE_UTIMBUF
#		Some versions of System V do not define the utimbuf
#		structure in <sys/time.h>, or in <utime.h>.  If your
#		system is one of these, then define DECLARE_UTIMBUF,
#		and smail will define the utimbuf structure itself.
#
#	RLIM_T_DECLARED
#		Define this if your system provides its own
#		declaration of rlim_t for use in struct rlimit, as in
#		4.4BSD, SVID-III, P1003.1a, etc..
#
#	SYS_SIGNAME_DECLARED
#		Define this if your system provides its own
#		declaration for the sys_signame array which maps
#		signal numbers to signal names (without the SIG
#		prefix), as in 4.4BSD.
#
#	MALLOC_DECLARED
#		Define this if your system provides its own
#		declarations for malloc() and friends, especially if
#		they would clash with the ones provided in various
#		Smail source files.
#
#	STDC_HEADERS
#		system has ANSI C compatible <stdlib.h> and <stddef.h>
#		but you're not compiling with an ANSI C compiler,
#		e.g. on SunOS-4 with /usr/5bin/cc.
#
#	TIME_WITH_SYS_TIME
#		<sys/time.h> may be included with (and before) <time.h>
#		This should be true on all but the oldest systems
#		where <sys/time.h> itself includes <time.h> and the
#		latter is not protected from multiple inclusion.
#		
#	LOCAL_MAIL_COMMAND
#		Use the command defined to deliver local mail instead of
#		delivering to a file.
#
#			LOCAL_MAIL_COMMAND='/usr/libexec/mail.local -f ${shquote:sender} $(${lc:user}$)'
#
#	USE_LOCAL_MAIL_COMMAND
#		Use the default LOCAL_MAIL_COMMAND to deliver local mail
#		instead of delivering to a file.
#
#	HAVE_SENDTO_WITH_3RD_PARAM_INT
#		This quiets a parameter passing complaint from ANSI C
#		compilers on systems with COMSAT.
#
#	void=int
#		This must be used for systems that do not support the
#		void type.  In particular, some older systems that
#		support void still require this for compiling smail.
#		To test for these systems, try the code fragment:
#
#			void f();
#			void (*avf[])() = { f };
#
#		If this yields an error then you will need to set
#		void=int.
#
#	const
#	volatile
#		If you're compiling with a traditional K&R compiler you
#		may have to define these keywords as nothing.
#
#	private=smail_private_tag
#		Some C comilers, notably some releases of the Sequent
#		C comiler, recognize private as a keyword.  To avoid
#		uses of this name within smail causing problems for
#		these compilers, use private=smail_private_tag to remap
#		all uses of "private" to a safer name.
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

#MISC_DEFINES=$MISC_DEFINES:REQUIRE_TZ:DECLARE_UTIMBUF:void=int

# MISC_C_DEFINES(list) - miscellaneous C defines
#
# More complex C defines can be specified using the MISC_C_DEFINES
# variable.  This is a list of lines that will be included in the
# defs.h file, included by smail source files.
#
# Items in this "list" must be newline separated and probably need
# to be protected by single quotes.
#
# IMPORTANT: These lines are included in the generated "defs.h" file
# which is almost always included at the very top of each source file
# and thus no other macros or types from any system includes will be
# available.
#
# WARNING: Any setting of this variable in the EDITME is appended to
# the value given in the OSTYPE file.

#MISC_C_DEFINES='
#'


# SMAIL_NOBODY(variable) - username to use for anonymous situations
#
# In situations where smail does not trust a user ID, or where it
# does not have any other user ID to use when running a program,
# it will use this user ID for this login name.  Under SunOS and
# BSD systems (4.3 or later) this will normally be "nobody".  If
# undefined, a large ID will be used (32767).

#SMAIL_NOBODY=nobody		# BSD and SunOS-like systems
#SMAIL_NOBODY=unknown		# possiblity for some System V systems


# SECURE_PATH(variable) - a secure, :-separated list of exectuable directories
#
# Shell scripts used by smail will normally use this path, to ensure
# that they can access all normal executables, without the possibility
# of executing something that they shouldn't.

SECURE_PATH=/bin:/usr/bin


# COMPRESS(variable) - an executable that compresses files
# COMP_FLAG(variable) - flag to use to force compression
# DOT_Z(variable) - suffix generated by compression program
# UNCOMPRESS(variable) - program to uncompress files
# ZCAT(variable) - program to write contents of compressed files to stdout
#
# The savelog utility can save space by compressing old log files.
# Normally when a file is compressed, the suffix is added to the
# filename.
#
# The COMPRESS symbol is the name of a program such that:
#
#		$COMPRESS $COMP_FLAG filename ...
#
# the file: filename is replaced by: filename$DOT_Z
# regardless of the size of filename.  The command:
#
#		$UNCOMPRESS filename$DOT_Z
#
# will replace filename$DOT_Z with the original filename.
# The command:
#		$ZCAT filename$DOT_Z
#
# will read the compressed file: filename$DOT_Z and write the
# plain text to standard output while leaving the file compressed.

#COMPRESS=compress		# for compress
#COMP_FLAG=-f
#DOT_Z=.Z
#UNCOMPRESS=uncompress
#ZCAT=zcat

#COMPRESS=pack			# system V standard
#COMP_FLAG=-f
#UNCOMPRESS=unpack
#ZCAT=pcat
#DOT_Z=.z

COMPRESS=:			# if no compress is used
COMP_FLAG=""
UNCOMPRESS=true
ZCAT=cat
DOT_Z=""


# UUCP_SYSTEM_FILE - path to UUCP file containing remote systems
#
# The normal smail configuration defines a router that scans the
# output of the /usr/bin/uuname command for neighboring UUCP sites.
# When smail is running as a daemon, it can cache the output of uuname
# so that the uuname command need not be executed for each mail
# message.  In order to be able to detect when the output of uuname
# will change, smail daemons will stat the UUCP configuration file
# which contains the names of neighboring hosts.  This EDITME variable
# defines the full pathname to this file.  If this variable is not
# defined, then the output of the uuname will not be cached.

#UUCP_SYSTEM_FILE=/usr/lib/uucp/Systems	# use this for old HoneyDanBer UUCP
#UUCP_SYSTEM_FILE=/etc/uucp/Systems	# use this for new HoneyDanBer UUCP
#UUCP_SYSTEM_FILE=/usr/lib/uucp/L.sys	# use this for pre-HDB UUCP
#UUCP_SYSTEM_FILE=/etc/uucp/L.sys	# use this for 4.3+bsd UUCP
#UUCP_SYSTEM_FILE=/etc/uucp/sys		# use this for 4.4bsd UUCP

# MAN1 - where smail user command man pages are to be installed
# MAN5 - where smail file format man pages are to be installed
# MAN8 - where smail administrator man pages should be installed
# MAN1_EXT - file extension for user command man pages
# MAN5_EXT - file extension for file format man pages
# MAN8_EXT - file extension for adminstrator man pages
#
# The man page for "foo.an" in the smail man/man5 source directory
# will be installed in MAN5/foo.MAN5_EXT, if MAN5 is defined.

#MAN1=/usr/man/man1				# common for BSD systems
#MAN1=/usr/share/man/man1			# 4.4BSD derrived & SunOS, etc.
#MAN1=/usr/man/u_man/man1			# common for System V systems
#MAN1=/local/share/man/man1			# common for local man pages
#MAN1=/usr/local/man/man1			# common for local man pages
#MAN1_EXT=1					# common

#MAN5=/usr/man/man5				# common for BSD systems
#MAN5=/usr/share/man/man5			# 4.4BSD derrived & SunOS, etc.
#MAN5=/local/share/man/man5			# common for local man pages
#MAN5=/usr/local/man/man5			# common for local man pages
#MAN5_EXT=5					# common for BSD systems
#MAN5=/usr/man/a_man/man4			# common for System V systems
#MAN5=/usr/local/man/man4			# for local man pages on System V
#MAN5_EXT=4					# common for System V systems

#MAN8=/usr/man/man8				# common for BSD systems
#MAN8=/usr/share/man/man8			# 4.4BSD derrived & SunOS, etc.
#MAN8=/local/share/man/man8			# common for local man pages
#MAN8=/usr/local/man/man8			# common for local man pages
#MAN8_EXT=8					# common
#MAN8=/usr/man/a_man/man1			# common for System V systems
#MAN8=/usr/local/man/man1			# for Sys V local man pages
#MAN8_EXT=1m					# system V suffi


# SMAIL_BIN_DIR - directory where copies of the smail binary are kept
#
# The Smail program comes in user callable names:  smail, uupath,
# pathto, optto, and so on.  A copy of smail will be linked to files
# under SMAIL_BIN_DIR.	The SMAIL_BIN_DIR should be a directory
# that is commonly in users search path (i.e., $PATH).

SMAIL_BIN_DIR=/usr/bin				# most everything


# SMAIL_NAME - file where the primary working copy of smail is located
#
# Any program that needs to call smail, including smail itself will
# attempt to execute the program named by SMAIL_NAME.
#
# Often the primary working copy of smail is /usr/lib/sendmail.	 This
# should be used for systems that used to run sendmail.	 Programs such
# as Berkeley Mail, System V mailx or /bin/mail can be made to, or do
# call /usr/lib/sendmail for mailer activity.  It is common for public
# domain programs to expect a mailer to exist under this name, also.
#
# If SMAIL_NAME is not defined here, or is set to a null string, then
# $SMAIL_BIN_DIR/smail is used instead.	 If this is the name that you
# want to use as the primary binary pathname, then do set SMAIL_NAME
# to the null string.  This will prevent the smail src/Makefile from
# installing it as $SMAIL_BIN_DIR/smail twice.
#
# WARNING:  This variable should only be set in an OSTYPE file only if
# it is empty or unset.

#SMAIL_NAME=/usr/sbin/sendmail			# 4.4BSD derrived, native
#SMAIL_NAME=					# use smail in bin directory
SMAIL_NAME=/usr/lib/sendmail			# common


# OTHER_SMAIL_NAMES - other names under which to install smail
#
# Some systems will wish to install smail as /bin/rmail to catch mail
# coming in over UUCP directly with smail.  To install under this name
# set OTHER_SMAIL_NAMES to /bin/rmail.  As implied by the name, other
# pathnames can be specified as well, if a system has other potential
# rendezvous points for mail.  This should be a colon or white-space
# separated list of full pathnames.  Any value specified in the EDITME
# file overrides the setting in the OS file.
#
# NOTE:  Rmail is sendmail friendly on 4.4BSD derrived systems.
#
# Users of XENIX may wish to use smail as their execmail interface.
# To do this, add /usr/lib/mail/execmail to this list.
#
# NOTE:  A pathname CANNOT be in both SMAIL_NAME and OTHER_SMAIL_NAMES.

#OTHER_SMAIL_NAMES=/bin/rmail			# common
#OTHER_SMAIL_NAMES=/usr/bin/mailq:/bin/rmail	# SysVr3 and the like
#OTHER_SMAIL_NAMES=/usr/ucb/mailq		# pre 4.4BSD derrived
#OTHER_SMAIL_NAMES=/usr/bin/rmail:/usr/lib/mail/execmail # for XENIX
#OTHER_SMAIL_NAMES=/usr/lib/sendmail:/usr/sbin/rmail:/usr/sbin/rsmtp:/usr/sbin/sendmail:/usr/bin/mailq
						# (ugliness above) for Linux
#OTHER_SMAIL_NAMES=/usr/bin/mailq		# 4.4BSD derrived


# NEWALIASES - alternate pathname for mkaliases program
#
# For compatibility with sendmail, mkaliases can be installed under
# a name such as /usr/ucb/newaliases, or /usr/lib/newaliases.  To setup
# smail to perform this installation, set NEWALIASES to the desired full
# pathname.  When testing, this is set to the empty string, signifying
# that mkaliases will be installed only under the name mkaliases.

#NEWALIASES=$UTIL_BIN_DIR/newaliases		# default if unassigned
#NEWALIASES=/usr/ucb/newaliases			# BSD location, SunOS-4, etc.
#NEWALIASES=/usr/lib/newaliases			# UTS/580 location
NEWALIASES=/usr/bin/newaliases			# 4.4BSD derrived, etc.


# LIB_DIR - directory where various smail files are found
#
# The LIB_DIR is where various static smail files reside, by default.
# Files which may reside under this directory are: the primary config
# file, the directors, routers and transports files, an aliases file,
# pathalias database, uuwho database, and the COPYING file.
#
# The common subdirectories under LIB_DIR are: "methods", where method
# files are by default found; "maps", where local pathalias files,
# mkmap configuration files and the getmap batch file are located;
# and "lists" where mailing lists are commonly located.
#
# It should be noted that none of these files, except for COPYING,
# is required by the smail binary as it is released.  See smail(5)
# and smail(8) for more details on this and related topics.

#LIB_DIR=/usr/lib/smail				# common convention
#LIB_DIR=/usr/share/smail			# not heterogeneous
LIB_DIR=/etc/smail				# Linux, 4.4BSD native, etc.


# UTIL_BIN_DIR - directory where smail utilities are located
#
# The smail system has a number of programs such as pathalias, mkline,
# mksort, mkdbm that users normally need not execute directly.	Such
# utilities will be placed under the UTIL_BIN_DIR directory.
#
# UTIL_BIN_DIR is often the same as LIB_DIR.

#UTIL_BIN_DIR=$LIB_DIR				# common, same as LIB_DIR
#UTIL_BIN_DIR=/usr/libexec/smail		# 4.4BSD native
UTIL_BIN_DIR=/usr/lib/smail			# Linux


# SPOOL_DIRS - smail spooling directories
#
# Smail can use one or more spooling directories, where spool directories
# other than the first are used if earlier spool directories were
# inaccessible or were on file systems which filled up.

#SPOOL_DIRS=/usr/spool/smail			# common
#SPOOL_DIRS=/usr/smail/spool			# alternative
#SPOOL_DIRS=/var/smail/spool			# Another alternative
SPOOL_DIRS=/var/spool/smail			# for System V Release 4, SunOS, etc.


# LOG_DIR - smail logging directory
#
# Smail creates two log files in this directory: logfile and paniclog.
# The first is a file that logs all incoming messages and deliveries,
# plus many errors.  The second file (paniclog) logs important system
# errors that smail can manage to write into the log file.

#LOG_DIR=/usr/adm/smail				# SysV
#LOG_DIR=/var/spool/smail/log			# alt for System V Release 4
#LOG_DIR=/usr/smail/log				# alternative
#LOG_DIR=/var/smail/log				# Another alternative
LOG_DIR=/var/log/smail				# 4.4BSD derrived and SunOS, etc.


# TMP_DIR - secure temp directory used by smail utilities
#
# Some smail utilities use TMP_DIR to define a directory in which
# their temporary files can be created.	 Utilities that use this
# directory are those that wish to use a secure temporary area.
#
# The only smail utilities that need to use TMP_DIR are utilities that
# should be run as root, or as some other user with appropriate
# privledges.  It is therefore recommended that TMP_DIR not be
# globally writable, and perhaps not even globally accessable.
#
# NOTE:  This is not the same as the TMPDIR environment variable used
# by most implementations of the sandard tmpnam(3) function.
#
# WARNING:  If you use /tmp or /usr/tmp the install procedure will
# clear the write bits on the directory specified.

#TMP_DIR=$LIB_DIR/tmp				# common
#TMP_DIR=$UNSHAR_MAP_DIR/tmp			# common
TMP_DIR=$SPOOL_DIRS/tmp				# best, if SPOOL_DIRS is one dir

# DAEMON_PIDFILE - location of the PID file for the daemon
# 
# The sample "rc" or "init" script supplied as util/init.sh uses this
# PID file to ensure it finds the correct PID for the daemon process.
# The daemon, started with '-bd' writes its PID to this file after
# having forked into the background.

#DAEMON_PIDFILE=$SPOOL_DIRS/`basename $SMAIL_NAME`.pid	# default if not BSD
DAEMON_PIDFILE=/var/run/`basename $SMAIL_NAME`.pid	# default for BSD
