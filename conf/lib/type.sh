#ident	"@(#)smail/conf/lib:RELEASE-3_2_0_121:type.sh,v 1.1 2003/03/24 18:36:05 woods Exp"
#
# a half-assed implementation of "type"
#
# NOTE: pdksh, at least as of v5.2.14, barfs on a function
# definition found in an 'if' statement, thus we source it
#
type () {
	if [ $# -ne 1 ]; then
		echo "Usage: type command" >&2
		return 2
	fi
	func=`hash | grep "function $1\$"`
	if [ -n "$func" ]; then
		echo "$func" | sed 's/^\([^ ]*\) \(.*\)$/\2 is a \1/'
		unset func
		return 0
	fi
	unset func
	rc=1
	case "$1" in
	.|command|echo|eval|exec|exit|export|getopts|hash|pwd|read|readonly|return|set|shift|trap|umask|unset|wait)
		typeout="$1 is a builtin"
		rc=0
		;;
	*)
		typeout="$1 not found"
		oifs="$IFS"
		IFS=":"
		for pathseg in $PATH
		do
			if [ -x $pathseg/$1 ]; then
				typeout="$1 is $pathseg/$1"
				rc=0
				break
			fi
		done
		IFS="$oifs"
		unset oifs
		;;
	esac
	echo $typeout
	unset pathseg typeout oifs
	return $rc
}
