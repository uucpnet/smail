#! /bin/sh
:
#ident	"@(#)smail/conf/lib:RELEASE-3_2_0_121:mkdefs.sh,v 1.79 2004/07/29 19:11:06 woods Exp"

#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

# Setup shell variables for use in makefiles and some shell scripts used
# in building smail.  The file EDITME is used to gather information
# specified by the site administrator.  Files under conf/os and conf/arch
# are used to learn about a particular operating system and architecture.
# All of the information learned is summarized in a dump to the files
# defs.sh (a source-able shell script), defs.h, (an #include-able
# C header file) and defs.sed (a sed script).

# The variable ROOT should be defined as a relative path to the root
# of the smail source directory.  It should be defined in the
# environment before calling this shell script, and normally the
# Makefiles arrange to do this.
#
# Establish the full root of the smail source directory
#
RELATIVE_ROOT="$ROOT"
ROOT=`(cd "$ROOT"; pwd)`
FROM_ROOT=`pwd | sed -e 's,^'$ROOT'/,,' -e 's,^'$ROOT'$,,'`

# the I/O re-direction doesn't actually get rid of the "type: not
# found" message from the old Ash implementation....
#
# We could get rid of it if we redirected the output of the whole "if"
# statement, but that would also hide any errors from the "else" clause.
#
if type type >/dev/null 2>&1 ; then
	:
else
	# NOTE: pdksh, at least as of v5.2.14, barfs on a function
	# definition found in an 'if' statement, thus we source it
	#
	. $ROOT/conf/lib/type.sh
fi

# determine how to echo without a newline
#
HAVEPRINT=false ; export HAVEPRINT
if expr "`type print 2> /dev/null`" : 'print is a shell builtin$' > /dev/null 2>&1 ; then
	HAVEPRINT=true
fi
HAVEPRINTF=false ; export HAVEPRINTF
if expr "`type printf 2> /dev/null`" : 'printf is a shell builtin$' > /dev/null 2>&1 ; then
	HAVEPRINTF=true
fi
###
### NOTE: we assume "echo" is builtin and we do not want to prefer an
### external $echo even if it is more capable
###
###elif expr "`type printf`" : '.* is .*/printf$' >/dev/null 2>&1 ; then
###	HAVEPRINTF=true
###fi
#
# always use ``$echo'' if any of the other variables are used...
#	$nl - print a newline (always required at end of line if desired)
#	$n - option to turn off final newline
#	$c - escape sequence to turn off final newline
# usage for a prompt is:
#	$echo $n "prompt: $c"
# and for a normal line
#	$echo "message$nl"
#
if $HAVEPRINT ; then
	echo=print
	nl='\n'
	n='-n'
	# XXX in theory '\c' is equivalent of '-n' in most shells
	c=''
elif $HAVEPRINTF ; then
	echo=printf
	nl='\n'
	n=''
	c=''
else
	echo=echo
	: ${HOME:=/tmp}
	(echo "hi there\c" ; echo " ") >$HOME/echotmp
	# Configure checks to make sure grep returns a status...
	if grep c $HOME/echotmp >/dev/null 2>&1 ; then
		nl=''
		n='-n'
		c=''
	else
		nl='\n'
		n=''
		c='\c'
	fi
	rm -f $HOME/echotmp
fi

# The list of "simple" variables that can be set in the EDITME, OS or
# ARCH files.
#
# If these variables are set in the EDITME file, then the version
# from the EDITME file will override any other setting.
#
# NOTE: The complete VARS string is too long for A/UX and perhaps
#	other old /bin/sh implementations.  Split it into multiple
#	parts which are concatenated.
#
VARS="\
 ALIGNED_TYPE\
 ALIASES_FILE\
 ALIASES_REMOVE_SENDER\
 ALIASES_TYPE\
 AUTH_DOMAINS\
 BITS_PER_CHAR\
 CC\
 CHOWN\
 COMPRESS\
 COMP_FLAG\
 CYRUS_DELIVER_PATH\
 CYRUS_GROUP\
 CYRUS_NOQUOTA_PASSPHRASE\
 CYRUS_USER\
 DAEMON_PIDFILE\
 DOMAINS\
 DOT_Z\
 DRIVER_CONFIGURATION\
 FLOCK_MAILBOX\
 FORCE_PATHS_FILE\
 FORCE_PATHS_TYPE\
 FORCE_SMTP_FILE\
 FORCE_SMTP_TYPE\
 REWRITE_FILE\
 REWRITE_TYPE\
 GATEWAY_NAMES\
 HOSTNAMES\
 LIB_DIR\
 LISTS_REMOVE_SENDER\
 LMAIL\
"
VARS="$VARS\
 LOCAL_MAIL_ADDRS\
 LOCAL_MAIL_COMMAND\
 LOCAL_MAIL_FILE\
 LOCKING_PROTOCOL\
 LOCK_BY_NAME\
 LOG_DIR\
 MAILBOX_DIR\
 MAN1\
 MAN1_EXT\
 MAN5\
 MAN5_EXT\
 MAN8\
 MAN8_EXT\
 MAXINT_B10_DIGITS\
 MAXLONG_B10_DIGITS\
 MORE_HOSTNAMES\
 NEWALIASES\
 NEWS_SPOOL_DIR\
 OTHER_SMAIL_NAMES\
 POINTER_TYPE\
 POSTMASTER\
 RANLIB\
 RETRY_DURATION\
 RETRY_INTERVAL\
 SECOND_CONFIG_FILE\
 SECURE_PATH\
 SENDER_ENV_VARIABLE\
"
VARS="$VARS\
 SMAIL_BIN_DIR\
 SMAIL_NAME\
 SMAIL_NOBODY\
 SMALL_MEMORY\
 SMTP_RECEIVE_COMMAND_TIMEOUT\
 SMTP_RECEIVE_MESSAGE_TIMEOUT\
 SPOOL_DIRS\
 STRIP\
 TMP_DIR\
 UNCOMPRESS\
 UNSHAR_MAP_DIR\
 USE_SYMLINKS\
 UTIL_BIN_DIR\
 UUCP_NAME\
 UUCP_SYSTEM_FILE\
 UUCP_ZONE\
 UUWHO_FILE\
 UUWHO_USE_DBM\
 VISIBLE_NAME\
 ZCAT\
"

# variables where the values from the EDITME settings, if any, are
# appended to the values from the OS and ARCH files
#
LISTS="\
 CFLAGS\
 CPPFLAGS\
 HAVE\
 INCLUDES\
 LDFLAGS\
 LIBS\
 MISC_DEFINES\
 NO_HAVE\
 OSLIBS\
 OSNAMES\
"

# lists to append together with any newlines intact
#
NL_LISTS="\
 MISC_C_DEFINES\
 MISC_DEFS_DEPEND\
 MISC_SH_DEFINES\
"

# HAVE values which are required to exist in either the HAVE or
# NO_HAVE list.  If not in HAVE, these will be added to NO_HAVE.
#
HAVE_REQUIRE="\
"

# list vars that are set below, and *not* in EDITME, OS, & ARCH, etc.
# but are set by this script from derrived values....
#
NO_EDITME_VARS="\
 MAIN_SPOOL_DIR\
 PATH_ALIASES_FILE\
 PATH_FORCE_PATHS_FILE\
 PATH_FORCE_SMTP_FILE\
 PATH_PATHS_FILE\
 PATH_REWRITE_FILE\
 UTIL_PATH\
"

# separators for list variables
#
sep=":$IFS"
saved_IFS="$IFS"

# unset all the private, EDITME, OS, & ARCH variables in case the
# caller had any of them set in his or her environment.
#
for v in $VARS $NO_EDITME_VARS $LISTS $NL_LISTS; do
	eval "unset $v"
done

# clearing TEST_BASE in EDITME means "test in place"
#
TEST_BASE="<not defined>"

####
####	Read in the EDITME file, creating it if necessary
####

# smail EDITME file specified in environment variable
#
case "$SMAIL_EDITME" in
/* )
	EDITME="$SMAIL_EDITME"
	RELATIVE_EDITME="$EDITME"
	;;
?* )
	EDITME="$ROOT/$SMAIL_EDITME"
	RELATIVE_EDITME="\${ROOT}/$SMAIL_EDITME"
	;;
"" )
	EDITME="$ROOT/conf/EDITME"
	RELATIVE_EDITME='${ROOT}/conf/EDITME'
	;;
esac

case "$EDITME" in
*/./* | */../* )
	# build a regular pathname from /
	dn="`echo "$EDITME" | sed 's|/[^/]*$||'`"
	EDITME="`cd $dn; pwd`/`basename "$EDITME"`"
	;;
esac

# If the EDITME file does not exist, create it from the distributed version
#
if [ ! -f "$EDITME" ]; then
	echo "Copy $ROOT/conf/EDITME-dist to $EDITME ..."
	if cat < "$ROOT/conf/EDITME-dist" > "$EDITME"; then
		:
	else
		exit 1
	fi
fi

# Read the site-specific information from the EDITME file
#
$echo $n "Read $RELATIVE_EDITME ... $c" 1>&2
if [ ! -r $EDITME ]; then
	echo "Cannot open $EDITME" 1>&2
	exit 1
fi

. $EDITME

if [ "X${OS_TYPE}X" = "XX" ]; then
	echo "You must define OS_TYPE in $ROOT/conf/EDITME" 1>&2
	exit 1
fi

# clear all private variables again after sourcing EDITME
# (warning if any were set non-empty)
#
for v in $NO_EDITME_VARS; do
	eval "
		case \"\${${v}}\" in
		?*)
			echo \"\" 1>&2
			echo \"WARNING: private ${v} was set in conf/EDITME; unsetting it.\" 1>&2
			unset ${v}
			;;
		esac
	     "
done

# save EDITME versions of variables in EDITME_* variables, if they are
# set and not empty, and then they are unset (unconditionally)
#
for v in $VARS $LISTS $NL_LISTS; do
	eval "
		case \"\${${v}}\" in
		?*)
			EDITME_${v}=\"\${${v}}\";
			;;
		esac
		unset ${v}
	     "
done

# Now that we've saved the EDITME variables we can set some defaults
# (which may be overridden by the OS, ARCH, or EDITME_* values later)
#
CC=cc
POSTMASTER=root
RANLIB=:
CHOWN=chown
COMPRESS=:
COMP_FLAG=""
UNCOMPRESS=true
ZCAT=cat
DOT_Z=""

# Set a default value for SECURE_PATH.  OS file can override this.
# The EDITME file can override this default or the OS setting.
#
case "$OSNAMES" in
*:UNIX_SUN_OS_5:*)
	# stranger things are only found in the land of the midnight sun!
	SECURE_PATH=/usr/bin
	;;
*)
	# this is a safe bet....
	SECURE_PATH=/bin:/usr/bin
	;;
esac

# Set a default value for NEWS_SPOOL_DIR.  OS file can override this.
# The EDITME file can override this default or the OS setting.
#
if [ -d /var/spool ] ; then
	NEWS_SPOOL_DIR=/var/spool/news
else
	NEWS_SPOOL_DIR=/usr/spool/news
fi

# Read the OS-specific information
#
$echo $n "conf/os/$OS_TYPE ... $c" 1>&2
if [ ! -r "$ROOT/conf/os/$OS_TYPE" ]; then
	echo "Cannot open $ROOT/conf/os/$OS_TYPE" 1>&2
	exit 1
fi

. "$ROOT/conf/os/$OS_TYPE"

# Read the architecture-specific information
#
$echo $n "conf/arch/$ARCH_TYPE ... $c" 1>&2
if [ ! -r $ROOT/conf/arch/$ARCH_TYPE ]; then
	echo "Cannot open $ROOT/conf/arch/$ARCH_TYPE" 1>&2
	exit 1
fi

. "$ROOT/conf/arch/$ARCH_TYPE"

####
####	done reading all configuration files now....
####

# for all the simple variables, make EDITME_* values override the OS
# and ARCH values
#
for v in $VARS; do
	eval "
		case \"\${EDITME_${v}}\" in
		?*)
			${v}=\"\${EDITME_${v}}\"
			;;
		esac
	     "
done

# now check to see if the EDITME specified a TEST_BASE configuration
# and if so then re-ajust various settings as necessary
#
case "$TEST_BASE" in

"<not defined>" )
	if [ "$UTIL_BIN_DIR" = "$SMAIL_BIN_DIR" ]; then
		UTIL_PATH="$UTIL_BIN_DIR"
	else
		UTIL_PATH="$UTIL_BIN_DIR:$SMAIL_BIN_DIR"
	fi
	TEST_BASE=""
	DONT_INSTALL=""
	;;

"" )
	SMAIL_BIN_DIR="$ROOT/src"
	SMAIL_NAME="$ROOT/src/smail"
	OTHER_SMAIL_NAMES=""
	LIB_DIR="$ROOT/test_lib"
	SPOOL_DIRS="$ROOT/test_spool"
	LOG_DIR="$ROOT/test_log"
	SECOND_CONFIG_FILE=""
	UTIL_BIN_DIR="$ROOT/test_util_bin"
	UTIL_PATH="$ROOT/pd/getopt:$ROOT/pd/pathalias:$ROOT/pd/uuwho"
	UTIL_PATH="$UTIL_PATH:$ROOT/util:$SMAIL_BIN_DIR"
	TMP_DIR="$ROOT/test_tmp"
	TEST_BASE="$ROOT"
	NEWALIASES=""
	DONT_INSTALL="yes"
	echo "Testing: \$ROOT/$FROM_ROOT/Makefile install rule is now disabled"
	;;

/* )
	SMAIL_BIN_DIR="$TEST_BASE/$SMAIL_BIN_DIR"
	SMAIL_NAME=""
	OTHER_SMAIL_NAMES=""
	LIB_DIR="$TEST_BASE/$LIB_DIR"
	SPOOL_DIRS="$TEST_BASE/$SPOOL_DIRS"	# can only support ONE
	LOG_DIR="$TEST_BASE/$LOG_DIR"
	SECOND_CONFIG_FILE=""
	UTIL_BIN_DIR="$TEST_BASE/$UTIL_BIN_DIR"
	if [ "$UTIL_BIN_DIR" = "$SMAIL_BIN_DIR" ]; then
		UTIL_PATH="$UTIL_BIN_DIR"
	else
		UTIL_PATH="$UTIL_BIN_DIR:$SMAIL_BIN_DIR"
	fi
	TMP_DIR="$TEST_BASE/$TMP_DIR"
	NEWALIASES=""
	DONT_INSTALL=""
	echo "Testing: \$ROOT/$FROM_ROOT/Makefile install will use $TEST_BASE"
	;;

* )
	SMAIL_BIN_DIR="$ROOT/$TEST_BASE/$SMAIL_BIN_DIR"
	SMAIL_NAME=""
	OTHER_SMAIL_NAMES=""
	LIB_DIR="$ROOT/$TEST_BASE/$LIB_DIR"
	SPOOL_DIRS="$ROOT/$TEST_BASE/$SPOOL_DIRS" # can only support ONE
	LOG_DIR="$ROOT/$TEST_BASE/$LOG_DIR"
	SECOND_CONFIG_FILE=""
	UTIL_BIN_DIR="$ROOT/$TEST_BASE/$UTIL_BIN_DIR"
	if [ "$UTIL_BIN_DIR" = "$SMAIL_BIN_DIR" ]; then
		UTIL_PATH="$UTIL_BIN_DIR"
	else
		UTIL_PATH="$UTIL_BIN_DIR:$SMAIL_BIN_DIR"
	fi
	TMP_DIR="$ROOT/$TEST_BASE/$TMP_DIR"
	TEST_BASE="$ROOT/$TEST_BASE"
	NEWALIASES=""
	DONT_INSTALL=""
	echo "Testing: \$ROOT/$FROM_ROOT/Makefile install will use $TEST_BASE"
	;;

esac

# Set the value for MAIN_SPOOL_DIR from SPOOL_DIRS
#
case "$SPOOL_DIRS" in
*:*)
	MAIN_SPOOL_DIR=`expr "\$SPOOL_DIRS" : '\([^:]*\):.*'`
	;;
*)
	MAIN_SPOOL_DIR="$SPOOL_DIRS"
	;;
esac

# Set the value for TMP_DIR if it is still unset.
#
if [ -z "${TMP_DIR}" ]; then
	TMP_DIR="${MAIN_SPOOL_DIR}/tmp"
fi

# Set the value for DAEMON_PIDFILE if it is still unset
#
if [ -z "${DAEMON_PIDFILE}" ]; then
	case "$OSNAMES" in
	*:UNIX_BSD:*|*:UNIX_SUN_OS_5:*|*:UNIX_SYS_5_4:*|*:LINUX:*)
		DAEMON_PIDFILE=/var/run/smail.pid
		;;
	*)
		# some ancient systems prefer /etc, but we don't...
		DAEMON_PIDFILE=$MAIN_SPOOL_DIR/smail.pid
		;;
	esac
fi

# Special case: some variables related to compression may be empty, so
# if COMPRESS was specified in the EDITME file, set them all from the
# EDITME file variables.
#
if [ -n "$EDITME_COMPRESS" ]; then
	COMPRESS="$EDITME_COMPRESS"
	COMP_FLAG="$EDITME_COMP_FLAG"
	UNCOMPRESS="$EDITME_UNCOMPRESS"
	ZCAT="$EDITME_ZCAT"
	DOT_Z="$EDITME_DOT_Z"
fi

# for the LISTS and NL_LISTS variables, append the EDITME values
#
# NOTE: some lists require space separation, some require : separation
#	others don't care.  Use space separation by default.
#
for v in $LISTS; do
	case $v in
	MISC_DEFINES|OSNAMES|HAVE|NO_HAVE)
		listsep=:
		;;
	*)
		listsep=' '
		;;
	esac
	eval "
		case \"\${${v}}\" in
		''|'[ 	]')
			${v}=\"\${EDITME_${v}}\"
			;;
		?*)
			if [ -n \"\${EDITME_${v}}\" ] ; then
				${v}=\"\${${v}}${listsep}\${EDITME_${v}}\";
			fi
			;;
	   	esac
	     "
done
for v in $NL_LISTS; do
	eval "
		case \"\${${v}}\" in
		'')
			${v}=\"\${EDITME_${v}}\"
			;;
		?*)
			if [ -n \"\${EDITME_${v}}\" ] ; then
				${v}=\"\${${v}}\${EDITME_${v}}\";
			fi
			;;
	   	esac
	     "
done

# For now we concatenate CPPFLAGS onto CFLAGS...
#
CFLAGS="$CFLAGS $CPPFLAGS"

# check for required HAVE/NO_HAVE values, and add values to NO_HAVE
# as needed
#
for v in $HAVE_REQUIRE; do
	found=no
	IFS="$sep"
	for v2 in $HAVE; do
		case "$v" in
		"$v2")
			found=yes
			break
			;;
		esac
	done
	for v2 in $NO_HAVE; do
		case "$v" in
		"$v2")
			found=yes
			break
			;;
		esac
	done
	IFS="$saved_IFS"
	case "$found" in
	no)
		case "$NO_HAVE" in
		"")
			NO_HAVE="$v"
			;;
		*)
			NO_HAVE="$NO_HAVE $v"
			;;
		esac
		;;
	esac
done

case "$LOG_DIR" in
"")
	LOG_DIR=$MAIN_SPOOL_DIR/log
	;;
esac
LOGFILE="$LOG_DIR/logfile"
PANICLOG="$LOG_DIR/paniclog"
OLD_LOGDIR="$LOG_DIR"

# Append .sort to $ALIASES_FILE for sorted files.
#
case "$ALIASES_TYPE" in
bsearch )
	ALIASES_FILE="$ALIASES_FILE.sort"
	;;
esac

case "$ALIASES_TYPE" in

yp | aliasyp)
	PATH_ALIASES_FILE="$ALIASES_FILE"
	;;

* )
	case "$ALIASES_FILE" in
	"")
		PATH_ALIASES_FILE=""
		;;
	/*  )
		PATH_ALIASES_FILE="$ALIASES_FILE"
		;;
	*   )
		PATH_ALIASES_FILE="$LIB_DIR/$ALIASES_FILE"
		;;
	esac
	;;
esac

case "$PATHS_TYPE" in

yp | aliasyp)
	PATH_PATHS_FILE="$PATHS_FILE"
	;;

*)
	case "$PATHS_FILE" in
	"")
		PATH_PATHS_FILE=""
		;;
	/*  )
		PATH_PATHS_FILE="$PATHS_FILE"
		;;
	*   )
		PATH_PATHS_FILE="$LIB_DIR/$PATHS_FILE"
		;;
	esac
	;;
esac

case "$FORCE_PATHS_TYPE" in

yp | aliasyp)
	PATH_FORCE_PATHS_FILE="$FORCE_PATHS_FILE"
	;;

*)
	case "$FORCE_PATHS_FILE" in
	"")
		PATH_FORCE_PATHS_FILE=""
		;;
	/*  )
		PATH_FORCE_PATHS_FILE="$FORCE_PATHS_FILE"
		;;
	*   )
		PATH_FORCE_PATHS_FILE="$LIB_DIR/$FORCE_PATHS_FILE"
		;;
	esac
	;;
esac

case "$FORCE_SMTP_TYPE" in

yp | aliasyp)
	PATH_FORCE_SMTP_FILE="$FORCE_SMTP_FILE"
	;;

*)
	case "$FORCE_SMTP_FILE" in
	"")
		PATH_FORCE_SMTP_FILE=""
		;;
	/*  )
		PATH_FORCE_SMTP_FILE="$FORCE_SMTP_FILE"
		;;
	*   )
		PATH_FORCE_SMTP_FILE="$LIB_DIR/$FORCE_SMTP_FILE"
		;;
	esac
	;;
esac

case "$REWRITE_TYPE" in

yp | aliasyp)
	PATH_REWRITE_FILE="$REWRITE_FILE"
	;;

*)
	case "$REWRITE_FILE" in
	"")
		PATH_REWRITE_FILE=""
		;;
	/*  )
		PATH_REWRITE_FILE="$REWRITE_FILE"
		;;
	*   )
		PATH_REWRITE_FILE="$LIB_DIR/$REWRITE_FILE"
		;;
	esac
	;;
esac

# If SMAIL_NAME was not specified, set it to be the main smail binary.
# Otherwise, add it to the OTHER_SMAIL_NAMES.
#
case "$SMAIL_NAME" in
"")
	SMAIL_NAME="$SMAIL_BIN_DIR/smail"
	;;
*)
	OTHER_SMAIL_NAMES="$OTHER_SMAIL_NAMES $SMAIL_NAME"
	;;
esac

# replace : with ' ' in the OTHER_SMAIL_NAMES variable
#
list=$OTHER_SMAIL_NAMES
OTHER_SMAIL_NAMES=""
IFS="$sep"
for i in $list; do
	case "$OTHER_SMAIL_NAMES" in
	?*)
		OTHER_SMAIL_NAMES="$OTHER_SMAIL_NAMES $i"
		;;
	*)
		OTHER_SMAIL_NAMES="$i"
		;;
	esac
done
IFS="$saved_IFS"

echo "done" 1>&2

# if DRIVER_CONFIGURATION does not begin with /, put it under the
# conf/driver directory.
#
case "$DRIVER_CONFIGURATION" in
/* )
	:
	;;
*  )
	DRIVER_CONFIGURATION=$RELATIVE_ROOT/conf/driver/$DRIVER_CONFIGURATION
	;;
esac

# append OSLIBS onto the end of LIBS, so that makefiles only need refer
# to LIBS.  The reason for having two is that OSLIBS is intended to
# be libraries that are linked after other libraries.  By specifically
# ordering them here, writers of OS and EDITME files don't need to
# be so careful.
#
LIBS="$LIBS $OSLIBS"

# determine how to get the dbm we need
#
case ":$HAVE:" in
*:DBM:*)
	:
	;;
*:NDBM:*)
	:
	;;
*)
	DBM_INCLUDES="-I$RELATIVE_ROOT/pd/sdbm"
	DBM_LIB="$RELATIVE_ROOT/pd/sdbm/sdbm.a"
	MISC_DEFINES="$MISC_DEFINES:SDBM"
	;;
esac

# If no LOCAL_MAIL_FILE is specified, derive it from MAILBOX_DIR
#
case "$LOCAL_MAIL_FILE" in
"")
	LOCAL_MAIL_FILE="$MAILBOX_DIR/\${lc:user}"
	;;
esac

# If the user specifies a LOCAL_MAIL_ADDRS, then they obviously want
# to use it, so do so
#
if [ -n "$LOCAL_MAIL_ADDRS" ] ; then
	DEFINE_LOCAL_MAIL_ADDRS="#define LOCAL_MAIL_ADDRS	\"$LOCAL_MAIL_ADDRS\""
fi

# If the user specifies a LOCAL_MAIL_COMMAND, then they obviously want
# to use it, so do so
#
if [ -n "$LOCAL_MAIL_COMMAND" ] ; then
	DEFINE_LOCAL_MAIL_COMMAND="#define LOCAL_MAIL_COMMAND	\"$LOCAL_MAIL_COMMAND\""
fi

####
####	Build the shell include file: defs.sh
####

$echo $n "Build \$ROOT/$FROM_ROOT/defs.sh ... $c" 1>&2

rm -f defs.sh

exec > "defs.sh"
case $? in
0)
	:
	;;
*)
	exit 1
	;;
esac

cat <<EOF
# DO NOT EDIT THIS FILE DIRECTLY, IT IS CREATED AUTOMATICALLY
# BY THE SCRIPT $ROOT/conf/lib/mkdefs.sh.
#
# IF YOU MAKE CHANGES TO THIS FILE THEY WILL DISAPPEAR WITHOUT WARNING.
#
# SEE THE FILE $EDITME
# FOR INFORMATION ON HOW TO CONTROL THE CONTENTS OF THIS FILE.
#
FROM_ROOT='$FROM_ROOT'
DEFS_DEPEND='
defs.sh defs.h defs.sed: $RELATIVE_EDITME
defs.sh defs.h defs.sed: \${ROOT}/src/smailarch.h
defs.sh defs.h defs.sed: \${ROOT}/conf/lib/mkdefs.sh
defs.sh defs.h defs.sed: \${ROOT}/conf/os/$OS_TYPE
defs.sh defs.h defs.sed: \${ROOT}/conf/arch/$ARCH_TYPE
version.sh version.h version.sed: \${ROOT}/conf/lib/mkversion.sh
version.sh version.h version.sed: \${ROOT}/level
$MISC_DEFS_DEPEND' 
EOF

# Write out all the non-boolean variables
#
for v in $VARS $NO_EDITME_VARS $LISTS $NL_LISTS; do
	eval "
		echo \"\${v}='\${${v}}'\";
	     "
done


# write out the `true' boolean list variables
#
(
	IFS="$sep"
	for i in $HAVE; do
		echo HAVE_$i=yes
	done
)
(
	IFS="$sep"
	for i in $OSNAMES; do
		echo $i=yes
	done
)

# clear the `false' boolean list variables
# XXX should use unset?
#
(
	IFS="$sep"
	for i in $NO_HAVE; do
		echo HAVE_$i=
	done
)

# write the set of `extensible' variables that may have values
#
(
	IFS="$sep"
	for i in $MISC_DEFINES; do
		echo "$i"
	done | sed -n -e 's/^\(.[^=]*\)$/\1=yes/p' -e 's/^\(.[^=]*\)=\(.*\)/\1='"'"'\2'"'"'/p'
)

cat <<EOF
DBM_INCLUDES='$DBM_INCLUDES'
DBM_LIB='$DBM_LIB'
LOGFILE='$LOGFILE'
PANICLOG='$PANICLOG'
OLD_LOGDIR='$OLD_LOGDIR'
MAIN_SPOOL_DIR='$MAIN_SPOOL_DIR'
DAEMON_PIDFILE='$DAEMON_PIDFILE'
EDITME='$EDITME'
OS_TYPE='$OS_TYPE'
$MISC_SH_DEFINES
EOF

####
####	Build the C include file: defs.h
####

$echo $n "defs.h ... $c" 1>&2

rm -f defs.h

exec > "defs.h"
case $? in
0)
	:
	;;
*)
	exit 1
	;;
esac

# Write out the file header and first set of #define's
cat <<EOF
/*
 * DO NOT EDIT THIS FILE DIRECTLY, IT IS CREATED AUTOMATICALLY
 * BY THE SCRIPT $ROOT/conf/lib/mkdefs.sh.
 *
 * IF YOU MAKE CHANGES TO THIS FILE THEY ARE LIKELY TO GO AWAY.
 *
 * SEE THE FILE $EDITME
 * FOR INFORMATION ON HOW TO CONTROL THE CONTENTS OF THIS FILE.
 */

#define MAILBOX_DIR		"$MAILBOX_DIR"
#define CONSOLE			"$CONSOLE"
$DEFINE_LOCAL_MAIL_ADDRS
$DEFINE_LOCAL_MAIL_COMMAND
#define LOCAL_MAIL_FILE		"$LOCAL_MAIL_FILE"
$LOCKING_PROTOCOL
$DECLARE_STRINGS

EOF


# Handle string variables which should be turned off with a NO_ prefix
sed -n 	-e 's/\([A-Z0-9_]*\)=\(..*\)/#define \1		"\2"/p' \
	-e 's/\([A-Z0-9_]*\)=$/#define NO_\1/p' <<EOF
ALIASES_FILE=$ALIASES_FILE
PATH_ALIASES_FILE=$PATH_ALIASES_FILE
ALIASES_PROTO=$ALIASES_TYPE
PATHS_FILE=$PATHS_FILE
PATH_PATHS_FILE=$PATH_PATHS_FILE
PATHS_PROTO=$PATHS_TYPE
FORCE_PATHS_FILE=$FORCE_PATHS_FILE
PATH_FORCE_PATHS_FILE=$PATH_FORCE_PATHS_FILE
FORCE_PATHS_PROTO=$FORCE_PATHS_TYPE
FORCE_SMTP_FILE=$FORCE_SMTP_FILE
PATH_FORCE_SMTP_FILE=$PATH_FORCE_SMTP_FILE
FORCE_SMTP_PROTO=$FORCE_SMTP_TYPE
REWRITE_FILE=$REWRITE_FILE
PATH_REWRITE_FILE=$PATH_REWRITE_FILE
REWRITE_PROTO=$REWRITE_TYPE

EOF

# Handle string variables which should revert to defaults if undefined
sed -n 	-e 's/\([A-Z0-9_]*\)=\(..*\)/#define \1		"\2"/p' \
	-e 's/\([A-Z0-9_]*\)=$/#undef \1/p' <<EOF
CYRUS_USER=$CYRUS_USER
CYRUS_GROUP=$CYRUS_GROUP
CYRUS_NOQUOTA_PASSPHRASE=$CYRUS_NOQUOTA_PASSPHRASE
CYRUS_DELIVER_PATH=$CYRUS_DELIVER_PATH
SMAIL_NOBODY=$SMAIL_NOBODY
SMAIL_LIB_DIR=$LIB_DIR
SMAIL_UTIL_DIR=$UTIL_BIN_DIR
UUCP_SYSTEM_FILE=$UUCP_SYSTEM_FILE
SPOOL_DIRS=$SPOOL_DIRS
UNSHAR_MAP_DIR=$UNSHAR_MAP_DIR
LOGFILE=$LOGFILE
OLD_LOGDIR=$OLD_LOGDIR
PANIC_LOG=$PANICLOG
SMAIL=$SMAIL_NAME
DAEMON_PIDFILE=$DAEMON_PIDFILE
POSTMASTER_ADDRESS=$POSTMASTER
UUWHO_FILE=$UUWHO_FILE
NEWS_SPOOL_DIR=$NEWS_SPOOL_DIR
EOF

# Handle non-string variables which should revert to defaults if undefined
sed -n 	-e 's/\([A-Z0-9_]*\)=\(..*\)/#define \1		(\2)/p' \
	-e 's/\([A-Z0-9_]*\)=$/#undef \1/p' <<EOF
RETRY_INTERVAL=$RETRY_INTERVAL
RETRY_DURATION=$RETRY_DURATION

EOF

# Handle NULL or string variables
sed -n  -e 's/\([A-Z0-9_]*\)=\(..*\)/#define \1		"\2"/p' \
	-e 's/\([A-Z0-9_]*\)=$/#define \1		NULL/p' <<EOF
EDITME=$EDITME
OS_TYPE=$OS_TYPE
RELATIVE_EDITME=$RELATIVE_EDITME
MAIN_SPOOL_DIR=$MAIN_SPOOL_DIR
HOSTNAMES=$HOSTNAMES
MORE_HOSTNAMES=$GATEWAY_NAMES
VISIBLE_DOMAINS=$DOMAINS
UUCP_NAME=$UUCP_NAME
VISIBLE_NAME=$VISIBLE_NAME
SECOND_CONFIG_FILE=$SECOND_CONFIG_FILE
SECURE_PATH=$SECURE_PATH
SENDER_ENV_VARIABLE=$SENDER_ENV_VARIABLE
SMAIL_TMP_DIR=$TMP_DIR
LMAIL=$LMAIL
EOF

# Handle empty or string variables
sed -n 	-e 's/\([A-Z0-9_]*\)=\(.*\)/#define \1		"\2"/p' <<EOF
FROM_ROOT=$FROM_ROOT
EOF

# Handle TRUE or FALSE variables
sed -n 	-e 's/\([A-Z0-9_]*\)=..*/#define \1		TRUE/p' \
	-e 's/\([A-Z0-9_]*\)=$/#define \1		FALSE/p' <<EOF
LOCK_BY_NAME=$LOCK_BY_NAME
FLOCK_MAILBOX=$FLOCK_MAILBOX

EOF

# Handle on or off variables
sed -n 	-e 's/\([A-Z0-9_]*\)=..*/#define \1/p' \
	-e 's/\([A-Z0-9_]*\)=$/#undef \1/p' <<EOF
SMALL_MEMORY=$SMALL_MEMORY
UUWHO_USE_DBM=$UUWHO_USE_DBM
UUCP_ZONE=$UUCP_ZONE
ALIASES_REMOVE_SENDER=$ALIASES_REMOVE_SENDER
LISTS_REMOVE_SENDER=$LISTS_REMOVE_SENDER

EOF

# write out the "true" boolean list variables
(
	IFS="$sep"
	for i in $HAVE; do
		echo "#define HAVE_$i	1"
	done
)
(
	IFS="$sep"
	for i in $OSNAMES; do
		echo "#define $i	1"
	done
)

# clear the "false" boolean list variables
(
	IFS="$sep"
	for i in $NO_HAVE; do
		echo "#undef HAVE_$i"
	done
)

# write the extensible set of variables that may have values.
# 
# NOTE: we first undef these values and then define them again as
# this is necessary to allow EDITME settings to override OS & ARCH
# settings.
# 
# WARNING:  this may hide unintentional conflicts with system defines!
(
	IFS="$sep"
	for i in $MISC_DEFINES; do
		echo "$i"
	done | sed -n -e 's/^\(.[^=]*\)$/#undef \1\
#define \1		1/p'\
		-e 's/^\(.[^=]*\)=\(.*\)/#undef \1\
#define \1		\2/p'
)

cat <<EOF
#define POINTER_TYPE	$POINTER_TYPE
#define ALIGNED_TYPE	$ALIGNED_TYPE
#define BITS_PER_CHAR	$BITS_PER_CHAR
#define MAXINT_B10_DIGITS $MAXINT_B10_DIGITS
#define MAXLONG_B10_DIGITS $MAXLONG_B10_DIGITS

$MISC_C_DEFINES

#if ((__STDC__ - 0) > 0) && !defined(NO_ANSI_C) && !defined(ANSI_C)
# define ANSI_C		1
#endif

#if !defined(STDC_HEADERS) && defined(ANSI_C)
# define STDC_HEADERS	1
#endif

#if !defined(HAVE_STRING_H) && !defined(HAVE_STRINGS_H)
# define HAVE_STRING_H	1
#endif

/*
 *	End Of "defs.h"
 */
EOF

####
####	Build the sed change script: defs.sed
####

$echo $n "defs.sed ... $c" 1>&2

rm -f defs.sed

exec > "defs.sed"
case $? in
0)
	:
	;;
*)
	exit 1
	;;
esac

case "$MISC_SED_DEFINES" in
?*)
	cat <<EOF
$MISC_SED_DEFINES

EOF
	;;
esac

cat <<EOF
s|X_LOGFILE_X|$LOGFILE|g
s|X_PATHS_FILE_X|$PATHS_FILE|g
s|X_FORCE_PATHS_FILE_X|$FORCE_PATHS_FILE|g
s|X_FORCE_SMTP_FILE_X|$FORCE_SMTP_FILE|g
s|X_REWRITE_FILE_X|$REWRITE_FILE|g
s|X_PANICLOG_X|$PANICLOG|g
s|X_OLD_LOGDIR_X|$OLD_LOGDIR|g
s|X_EDITME_X|$EDITME|g
s|X_OS_TYPE_X|$OS_TYPE|g
s|X_DAEMON_PIDFILE_X|$DAEMON_PIDFILE|g
EOF

# Write out sed expressions for the ``simple'' variables.
# I.e. ignore those with values containing newlines, `|', and `\' chars.
#
# WARNING:  Once upon a time it was found that HP-UX 7.0 and bash
# v1.12.1 cannot handle newlines within a case pattern.
#
nl='
'
for v in $VARS $NO_EDITME_VARS; do
	eval "
		case \"\$$v\" in
		*\$nl*|*'|'*|*'\\'*)
			:
			;;
		*)
			echo \"s|X_${v}_X|\$$v|g\"
			;;
		esac
	     "
done

# clear the negative boolean list variables
(
	IFS="$sep"
	for i in $NO_HAVE; do
		echo "s|X_HAVE_${i}_X||g"
	done
)

# write out the positive boolean list variables
(
	IFS="$sep"
	for i in $HAVE; do
		echo "s|X_HAVE_${i}_X|yes|g"
	done
)
(
	IFS="$sep"
	for i in $OSNAMES; do
		echo "s|X_${i}_X|yes|g"
	done
)

# If this OS cannot correctly use #! /bin/sh at the beginning of bourne
# shell scripts, then convert them not to do this.
(
	IFS="$sep"
	sed_hash_bang=true
	for i in $HAVE; do
		case "$i" in
		HASH_BANG)
			sed_hash_bang=false
			;;
		esac
	done
	if $sed_hash_bang; then
		echo '1s|^#! */bin/sh|: /bin/sh|'
	fi
)

echo "done." 1>&2

exit 0
