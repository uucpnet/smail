#! /bin/sh
:
#ident	"@(#)smail/conf/lib:RELEASE-3_2_0_121:mkdirs.sh,v 1.7 1997/05/01 04:56:17 woods Exp"
#
# Create directories, if they do not already exist.
#
# usage: mkdirs [-u user] [-g group] [-m mode] dir ...
#
#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

# Don't use getopt(1), as we can't rely on it being built yet, or
# existing on the system.

argv0=`basename $0`
usage="Usage: $argv0 [-u user] [-g group] [-m mode] dir ..."
user=
group=
mode=
err=0
dbg=

# put /etc, etc., into path for chown
PATH="$PATH:/etc:/usr/etc:/sbin:/usr/sbin"

CHOWN="${CHOWN:-chown}"

# Note:  this shell script uses case rather than test, where possible
#	 to prevent a fork/exec on systems where test is not a builtin.

# process the list of options.
# Note:  the option letters and option arguments must be separate tokens.
while : ; do
	case $# in
	0)
		break
		;;
	esac
	case "$1" in
	--)
		shift
		break
		;;
	-[ugm])
		case $# in
		1)
			err=1
			;;
		esac
		case "$1" in
		-u)
			user="$2"
			;;
		-g)
			group="$2"
			;;
		-m)
			mode="$2"
			;;
		esac
		shift
		shift
		;;
	-D)
		dbg=echo
		shift
		;;
	-*)
		err=1
		break
		;;
	*)
		break
		;;
	esac
done

# There must be some operands
case $# in
0)
	err=1
	;;
esac

# If an error occured, spit out a usage message.
case "$err" in
1)
	echo "$usage" 1>&2
	exit 2
	;;
esac

for dir in ${1+"$@"} ; do
	# the following loop borrowed from GNU mkinstalldirs
	# Author: Noah Friedman <friedman@prep.ai.mit.edu>
	# Created: 1993-05-16
	# Public domain
	set -- fnord `echo ":$dir" | sed -ne 's/^:\//#/;s/^://;s/\// /g;s/^#/\//;p'`
	shift
	errstatus=0
	created=0
	pathcomp=""
	for d in ${1+"$@"} ; do
		pathcomp="$pathcomp$d"
		case "$pathcomp" in
		-* )
			pathcomp=./$pathcomp
			;;
		esac

		if [ ! -d "$pathcomp" ]; then
			echo "mkdir $pathcomp" 1>&2
			$dbg mkdir "$pathcomp" || errstatus=$?
			created=1
		fi

		pathcomp="$pathcomp/"
	done
	if [ ! -d "$dir" ]; then
		# if the directory doesn't exist by now, complain and die
		echo "$argv0: failed to create directory $dir" 1>&2
		$dbg exit 1
	fi
	case "$errstatus" in
	0)
		;; # press on....
	*)
		echo "$argv0: encountered errors creating $dir, but it exists..." 1>&2
		;;
	esac
	case "$user" in
	?*)
		if [ $created -eq 0 ]; then
			echo "$argv0: $pathcomp exists, not changing owner to $user."
		elif $dbg $CHOWN "$user" "$dir"; then
			: # we did it
		else
			echo "$argv0: failed to change owner of $dir to $user." 1>&2
		fi
		;;
	esac
	case "$group" in
	?*)
		if [ $created -eq 0 ]; then
			echo "$argv0: $pathcomp exists, not changing group to $group."
		elif $dbg chgrp "$group" "$dir"; then
			: # we did it
		else
			echo "$argv0: failed to change group of $dir to $group." 1>&2
		fi
		;;
	esac
	case "$mode" in
	?*)
		if [ $created -eq 0 ]; then
			echo "$argv0: $pathcomp exists, not changing mode to $mode."
		elif $dbg chmod "$mode" "$dir"; then
			: # we did it
		else
			echo "$argv0: failed to change mode of $dir to $mode." 1>&2
		fi
		;;
	esac
done

exit 0
