#! /bin/sh
:
#ident	"@(#)smail/conf/lib:RELEASE-3_2_0_121:mkdepend.sh,v 1.16 2004/06/25 18:09:34 woods Exp"
#
# build a list of dependencies and insert them at the end of the make file
#
# usage: makedepend -Idir ... -Dflag ... file.c ...
#
#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

# gather together the arguments to be passed to cc
CPPFLAGS=
while :; do
    case $1 in
    -I* | -D* | -U*) CPPFLAGS="$CPPFLAGS '$1'"; shift;;
    -systype)	shift; CPPFLAGS="$CPPFLAGS -systype '$1'"; shift;;
    -?*)	shift;;
    --)		break;;
    *)		break;;
    esac
done

dependfile=".depend"

# read relevant configuration parameters
CC="`echo X_CC_X | sed -f defs.sed`"
[ -z "$CC" ] && CC=cc

# remove any previous attempts at building the new dependfile
/bin/rm -f $dependfile

# build dependencies for all remaining files in the arg list
for i in "$@"; do
	# ignore empty arguments
	if [ ! "$i" ]; then
		continue
	fi

	# the following line grabs all of the included files
	echo "	$CC -E -DDEPEND $CPPFLAGS $i" 1>&3
	eval $CC -E -DDEPEND $CPPFLAGS "$i" |
		# grab filenames from /lib/cpp line and filename information
		# this is of the form:
		#
		#	# line-number "filename"
		#
		# use grep to avoid line-length limits in some seds
		grep '^#\(line\)*[ 	]*[0-9]' |
		# the compiler with the sun 3 produces extra garbage after
		# the quoted filename.
		# gcc-3.2 produces these stupid "fake" filenames
		sed -n -e '/<built-in>/d' -e '/<command line>/d' \
			-e 's/^#\(line\)*[ 	]*[0-9]*[ 	]*"\(.*\)"[ 0-9]*$/\2/p' |
		# remove ./ prefixes
		sed -e 's%^\./%%' |
		# next remove duplicates
		sort | uniq |
		# finally use awk to put a reasonable number of them on a line
		awk '
		    BEGIN {
			srcfile="'"$i"'"
			objfile=substr(srcfile, 1, length(srcfile)-2) ".o"
			line=objfile ": "
			n = 0
		    }
		    { if (length(line) + length($0) > 78 && n > 0) {
			 print line ""; line=objfile ": "; n = 0;
		      }
		      line=line " " $0; n++;
		    }
		    END { if (n > 0) { print line } }'
done 3>&1 > $dependfile

# all done
exit 0
