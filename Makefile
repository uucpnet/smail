#
#ident	"@(#)smail:RELEASE-3_2_0_121:Makefile,v 1.30 2004/07/29 19:08:27 woods Exp"
#
# Top-level makefile for the smail source tree
#
#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

ROOT=.
THIS_DIR=

# Note: the Sys-III/Sys-V compatible "include" statement is supported
# in all reasonably modern versions of Make in common use today,
# including GNU Make and all major derivatives of the 4.4BSD PMake
# (except FreeBSD-2.1 and older which didn't turn the feature on in
# the default build, but that was a long time ago already :-).

include ${ROOT}/conf/Make.local

MKDEPEND=${ROOT}/conf/lib/mkdepend.sh
MKDIRS=${ROOT}/conf/lib/mkdirs.sh
MKDEFS=${ROOT}/conf/lib/mkdefs.sh
CHECKDEFS=${ROOT}/conf/lib/checkdefs.sh
INST=${ROOT}/conf/lib/inst.sh
INSTM=${ROOT}/conf/lib/instm.sh
XEXEC=${SHELL} ${ROOT}/conf/lib/xexec.sh

DEFS_SH=defs.sh
DEFS_H=defs.h
DEFS_SED=defs.sed

SRC_SUBDIRS=util src
SUB_DIRS=conf compat pd ${SRC_SUBDIRS} man
MORE_SUB_DIRS=guide NOTES

.PHONY: all install local-install clean local-clean clobber local-clobber depend local-depend
.PHONY: lint mkdefs defs check_defs tags TAGS
.PHONY: everything all-more-subdirs install-everything install-more-subdirs installman install.man

# NOTE:  The old-style "include" can't be used for the .depend files
# because they may not exist and there's no way to ignore file access
# errors with that syntax.

all install clean clobber depend lint mkdefs defs: ${DOT_MAKE}
	@for i in ${SUB_DIRS}; do			\
		echo "Making $@ under $$i ...";		\
		(cd $$i && { if [ -f .depend ] ; then	\
			DEPMKFILE="-f Makefile -f .depend"; \
		fi; ${MAKE} $$DEPMKFILE $@; } );	\
		rc=$$?;					\
		if [ $$rc -ne 0 ] ; then		\
			exit $$rc;			\
		fi;					\
	done

all install: ${DEFS_SH}

everything: all all-more-subdirs

all-more-subdirs: ${DOT_MAKE}
	@for i in ${MORE_SUB_DIRS}; do			\
		echo "Making all under $$i ...";	\
		(cd $$i && { if [ -f .depend ] ; then	\
			DEPMKFILE="-f Makefile -f .depend"; \
		fi; ${MAKE} $$DEPMKFILE all; } );	\
		rc=$$?;					\
		if [ $$rc -ne 0 ] ; then		\
			exit $$rc;			\
		fi;					\
	done

etags ETAGS tags TAGS: ${DOT_MAKE}
	@for i in ${SRC_SUBDIRS}; do			\
		echo "Making $@ under $$i ...";		\
		(cd $$i && { if [ -f .depend ] ; then	\
			DEPMKFILE="-f Makefile -f .depend"; \
		fi; ${MAKE} $$DEPMKFILE $@; } );	\
		rc=$$?;					\
		if [ $$rc -ne 0 ] ; then		\
			exit $$rc;			\
		fi;					\
	done

${DEFS_H} ${DEFS_SH} ${DEFS_SED}: ${ROOT}/conf/EDITME
	ROOT=${ROOT} ${SHELL} ${MKDEFS}

.PRECIOUS: ${ROOT}/conf/EDITME
${ROOT}/conf/EDITME: ${DOT_MAKE} # cannot depend on anything else!
	cd ${ROOT}/conf && ${MAKE} EDITME

depend:	local-depend

local-depend: ${DEFS_SH} check_defs
	@rm -f .depend
	. ./${DEFS_SH}; echo "$$DEFS_DEPEND" >> .depend

# Note: don't try to source DEFS_SH and thus don't use XEXEC
#
check_defs:
	SHELL=${SHELL} ROOT=${ROOT} ${SHELL} ${CHECKDEFS}

mkdefs defs: ${DEFS_H} ${DEFS_SH} ${DEFS_SED}

install: local-install

local-install: ${DEFS_SH}
	@. ./${DEFS_SH};					\
	   case "$$DONT_INSTALL" in				\
	   ?*)	echo Testing only ... $@ ignored; exit 0;;	\
	   esac;						\
	   ${XEXEC} ${SHELL} ${INSTM} -r -m 0444 $$LIB_DIR COPYING

install-everything: install install-more-subdirs

install-more-subdirs: ${DOT_MAKE}
	@for i in ${MORE_SUB_DIRS}; do			\
		echo "Install targets under $$i ...";	\
		(cd $$i && { if [ -f .depend ] ; then	\
			DEPMKFILE="-f Makefile -f .depend"; \
		fi; ${MAKE} $$DEPMKFILE install; } );	\
		rc=$$?;					\
		if [ $$rc -ne 0 ] ; then		\
			exit $$rc;			\
		fi;					\
	done

clean: local-clean

local-clean:
	rm -f .${DEFS_SH} .${DEFS_H} .${DEFS_SED}

clobber: clean local-clobber

local-clobber: local-clean
	rm -f .depend
	rm -f ${DEFS_SH} ${DEFS_H} ${DEFS_SED}
