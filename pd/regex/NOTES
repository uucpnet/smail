#ident "@(#)smail/pd/regex:RELEASE-3_2_0_121:NOTES,v 1.2 2004/02/02 03:16:49 woods Exp"

We include Philip Hazel's PCRE instead.  It also has a 100% compatible
POSIX interface and a BSD-like copyright license.  It implements Perl
EREs, not POSIX EREs, so Perl nuts would really love it, but it also
has support for the "(?)"  option-setting construct.  It's very fast.
It's very well maintained.

There's a nifty new POSIX ERE implementation called TRE.
<URL:http://laurikari.net/tre>.  It has a GPL copyright.  It's
apparently reasonably small (though it's code is considerably larger
than Philip's PCRE, which is considerably larger then Henry's)

There's another new POSIX ERE implementation called Oni Guruma.
<URL:http://raa.ruby-lang.org/list.rhtml?name=oniguruma>.  It was
apparently written to replace the RE code in Ruby and it supports many
important character encodings.  It also supports the widest known
varieties of ERE syntax, including PERL.  It's got a BSD-style
license, and it has GNU regex and its own native API as well as the
POSIX API.  The 20031224 release dumps core when used in grep with the
long MIME content-type pattern from dead-mail.egrep.

We could include the 4.4BSD version based on Henry Spencer's original
instead, but it has the more restrictive BSD license and it's SLOW.

And then there's Tom Lord's RX, which is also fully 100% POSIX
compatible.  It is copyright Tom Lord, Cygnus Support, and FSF, each
under the GPL Version #2.  It's not very fast though.

There's also Tatu Ylonen's regexpr from comp.sources.misc Volume 27,
Issue 23 (with a patch in Volume 29, Issue 59).  Guido van Rossum
(Python) says GNU Regexpr is faster than Ylonen's implementation,
though Tatu himself said:

  - speed comparable to that of the GNU library (searches seem a bit
    faster, matches about the same and compiling a bit slower than in
    the gnu library)

Unfortunately I don't think there's a variant of Ylonen's code with a
POSIX interface though (currently it's GNU Regex compatible), and it
also doesn't come with separate documentation.

Ozan's library is in the Public Domain.  Unfortunately it doesn't
(yet) have a POSIX API and it also only supportes BREs.  :-)

There's also of course the GNU Regexpr package, but it doesn't seem to
have a POSIX API.

Times using the dead-mail.egrep patterns (with -i) over ~64MB of junk:

NetBSD-regex:	      192.77 real       191.70 user         0.04 sys
pcre-4.5:	        9.21 real         8.84 user         0.03 sys
tre-0.6.4:	       65.11 real        64.30 user         0.13 sys
rx-1.5:		      140.42 real       139.26 user         0.11 sys

i386 w/ gcc-2.95.3 NetBSD nb3:
                text    data    bss     dec     hex     filename
NetBSD-regex    144646  8688    12856   166190  2892e   grep
pcre-4.5        153982  9776    12824   176582  2b1c6   grep
tre-0.6.4       156046  8944    12824   177814  2b696   grep
rx-1.5          156810  10512   13240   180562  2c152   grep
onig-20031224   200490  18320   12824   231634  388d2   grep
