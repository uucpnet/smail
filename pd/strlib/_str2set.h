/* @(#) _str2set.h,v 1.2 1990/10/24 05:19:17 tron Exp */
/*
 * File   : _str2set.h
 * Updated: 10 April 1984
 * Purpose: External declarations for strprbk, strspn, strcspn &c
 * By: Richard A. O'Keefe.
 */

extern	int	_set_ctr;
extern	char	_set_vec[];
extern	void	_str2set(/*char^*/);
