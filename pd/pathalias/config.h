/*
#ident "@(#)smail/pd/pathalias:RELEASE-3_2_0_121:config.h,v 1.20 2003/05/14 18:20:26 woods Exp"
 */

/* pathalias -- by steve bellovin, as told to peter honeyman */

/**************************************************************************
 * +--------------------------------------------------------------------+ *
 * |                    begin configuration section                     | *
 * +--------------------------------------------------------------------+ *
 **************************************************************************/

/*
 * For smail3, get definitions from the generated "defs.h" file (#included at
 * the top of every source file) and convert them for use with pathalias
 */

#include <sys/types.h>

#ifdef STDC_HEADERS
# if !defined(UNIX_SUN_OS_4_1)	/* declares malloc() as "void *", but yacc doesn't */
#  include <stdlib.h>
# endif
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include "smail.h"

#undef EQ		/* to avoid conflict with macro defined in smail.h */

#define SMAIL_3	1	/* special changes for SMAIL-3 are #ifdef'd with this */

#undef SMALL		/* smail-3 is big already, so pathalias can be too */

#define MEMSET	1	/* smail-3 code uses memset() exclusively */
#define STRCHR	1	/* smail-3 code use str[r]chr() exclusively */

#undef UNAME
#undef GETHOSTNAME
#ifdef HAVE_UNAME
# define UNAME
#else
# ifdef HAVE_GETHOSTNAME
#  define GETHOSTNAME
# endif
#endif

/* default place for dbm output of makedb (or use -o at run-time) */
#ifdef PATH_PATHS_FILE
# define ALIASDB	PATH_PATHS_FILE
#else
# define ALIASDB	"/usr/local/lib/palias"
#endif

/*
 * Pathalias defines a type named link, which clashes with the link system
 * call.  The link system call may have been defined in one of the header files
 * included by defs.h.
 */

#define link	palias_link

/**************************************************************************
 * +--------------------------------------------------------------------+ *
 * |                    end of configuration section                    | *
 * +--------------------------------------------------------------------+ *
 **************************************************************************/


#ifdef MAIN
#ifndef lint
static char	*c_sccsid = "@(#)config.h	9.6 95/05/04";
static char	*c_rcsid = "@(#)smail/pd/pathalias:RELEASE-3_2_0_121:config.h,v 1.20 2003/05/14 18:20:26 woods Exp";
#endif /*lint*/
#endif /*MAIN*/

/* the usual case: unix */
#define	NULL_DEVICE	"/dev/null"
#define	OK		0

#include <exitcodes.h>

/*
 * malloc/free fine tuned for pathalias.
 *
 * MYMALLOC should work everwhere, so it's not a configuration
 * option (anymore).  nonetheless, if you're getting strange
 * core dumps (or panics!), comment out the following manifest,
 * and use the inferior C library malloc/free.
 */
#if !defined(SMAIL_3) && !defined(NO_PALIAS_MYMALLOC)
#define MYMALLOC	/**/
#endif

#ifdef MYMALLOC

# define malloc mymalloc
# define calloc(n, s) malloc ((n)*(s))
# define free(s)
# define cfree(s)
extern char *memget();

#else /* !MYMALLOC */

# ifndef MALLOC_DECLARED
#  if defined(SMAIL_3) && !defined(NO_VOID_MALLOC)
extern void *malloc __P((size_t));
extern void *calloc __P((size_t, size_t));
#  else
extern char *malloc();
extern char *calloc();
#  endif
# endif

#endif /* MYMALLOC */

#ifdef STRCHR
# define index strchr
# define rindex strrchr
#else
# define strchr index
# define strrchr rindex
#endif

#ifdef BZERO
# define strclear(s, n)	((void) bzero((s), (n)))
#else /*!BZERO*/
# ifdef MEMSET
#  ifndef SMAIL_3
extern char	*memset();
#  endif
#  define strclear(s, n) ((void) memset((s), 0, (n)))
# else /*!MEMSET*/
extern void	strclear();
# endif /*MEMSET*/
#endif /*BZERO*/

#ifndef SMAIL_3
extern char	*strcpy(), *index(), *rindex();
#endif

#ifndef STATIC

#ifdef DEBUG
#define STATIC extern
#else /*DEBUG*/
#define STATIC static
#endif /*DEBUG*/

#endif /*STATIC*/
