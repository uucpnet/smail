/*
#ident "@(#)smail/pd/sdbm:RELEASE-3_2_0_121:util.c,v 1.2 2003/03/27 16:49:22 woods Exp"
 */

#include "defs.h"				/* smail-3 */

#include <sys/types.h>
#include <stdio.h>
#include <errno.h>

#ifdef SDBM
# include "tune.h"
# include "sdbm.h"
#else
# include <ndbm.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
# define VA_START(args, lastarg)       va_start(args, lastarg)
#else
# include <varargs.h>
# define VA_START(args, lastarg)       va_start(args)
#endif

#ifndef __STDC__
extern int errno, sys_nerr;
extern char *sys_errlist[];
#endif

extern void oops __P((register char *fmt, ...));
extern int okpage __P((char *));

extern char *progname;

/* VARARGS1 */
void
#ifdef __STDC__
oops(register char *fmt, ...)
#else
oops(fmt, va_alist)
	register char *fmt;
	va_dcl
#endif
{
	int e = errno;
	char *emsg = NULL;
	va_list ap;

	if (progname)
		fprintf(stderr, "%s: ", progname);
	VA_START(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
#ifdef HAVE_STRERROR
	emsg = strerror(e);
#else
	if (e > 0 && e < sys_nerr)
		emsg = sys_errlist[e];
#endif
	if (emsg)
		fprintf(stderr, " (%s)", emsg);
	fprintf(stderr, "\n");
	exit(1);
}

int
okpage(pag)
char *pag;
{
	register unsigned n;
	register int off;
	register short *ino = (short *) pag;

	if ((n = ino[0]) > PBLKSIZ / sizeof(short))
		return 0;

	if (!n)
		return 1;

	off = PBLKSIZ;
	for (ino++; n; ino += 2) {
		if (ino[0] > off || ino[1] > off ||
		    ino[1] > ino[0])
			return 0;
		off = ino[1];
		n -= 2;
	}

	return 1;
}
