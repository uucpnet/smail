/*
#ident "@(#)smail/pd/sdbm:RELEASE-3_2_0_121:tune.h,v 1.3 2003/06/09 21:22:41 woods Exp"
 */
/*
 * sdbm - ndbm work-alike hashed database library
 * tuning and portability constructs [not nearly enough]
 * author: oz@nexus.yorku.ca
 */

/*
 * ANSI?
 */

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#if ((__STDC__ - 0) > 0)

# define BYTESIZ	CHAR_BIT

#else	/* ! ANSI_C */

extern int errno;
extern char *malloc();
extern void free();

# define BYTESIZ	8

#endif	/* ! ANSI_C */

#include <limits.h>

/*
 * POSIX?
 */

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#else
extern long lseek();
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

/*
 * BSD?
 */

#ifndef SEEK_SET
# define SEEK_SET	L_SET
#endif

/*
 * important tuning parms (hah)
 */

#define SEEDUPS			/* always detect duplicates */
#define BADMESS			/* generate a message for worst case:
				   cannot make room after SPLTMAX splits */

/*
 * misc
 */

#ifndef NULL
# define NULL	0
#endif

#ifndef SEEK_SET
# define SEEK_SET	0
#endif

#ifdef DEBUG
# define debug(x)	printf x
#else
# define debug(x)
#endif

#if !defined(__P)
# if ((__STDC__ - 0) > 0)
#  define __P(p)	p
# else
#  define __P(p)	()
# endif
#endif
