/*
#ident "@(#)smail/pd/sdbm:RELEASE-3_2_0_121:pair.h,v 1.2 2003/03/27 16:49:22 woods Exp"
 */

extern int fitpair __P((char *, int));
extern void putpair __P((char *, datum, datum));
extern datum getpair __P((char *, datum));
extern int delpair __P((char *, datum));
extern int chkpage __P((char *));
extern datum getnkey __P((char *, int));
extern void splpage __P((char *, char *, long));
#ifdef SEEDUPS
extern int duppair __P((char *, datum));
#endif
