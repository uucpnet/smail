#! /bin/sh
#
#	mkRelease.sh -- generate a new release
#
# Once upon a time I did this all by hand, step by step...
#
# Now the steps to create a full release are reduced to just the following:
#
#	- make sure all changes have been committed
#
#	- make a preview release with ./myPreview.sh and test it using the
#	  pkgsrc infrastructure
#
#	- write a RELEASE-${VERSION} (that's the about-to-be-released version)
#	  release notes file (use previous README-FIRST as a template)
#
#	- run this script (optionally with version: "VERSION=3.4 ./mkRelease.sh")
#
#	- re-link the symlinks and archive the old version on ftp.planix.com
#
#	- retest pkgsrc/mail/smail
#
#	- update ~/public_html/projects/smail.html and publish it
#
#	- post the README-FIRST file to UseNet and smail3-users
#
#	- go tell FreshMeat.net about it!
#
#ident "@(#)smail:RELEASE-3_2_0_121:mkRelease.sh,v 1.20 2005/11/17 21:27:15 woods Exp"

debug=false
if [ "$1" = "-d" ]; then
	debug=true
fi

# NOTE: must be run from within an up-to-date CVS working directory
#
CVSROOT=$(cat CVS/Root)

ARCHIVE_DIR="/most/var/spool/ftp/pub/local"
DISTFILES="/most/var/spool/ftp/pub/NetBSD/distfiles"
RELEASE_DIR="/work/woods/release.d"


# by default the new version is the current level less the "-Pre" suffix
#
# However VERSION can also be specified in the environment
# XXX should it only be from the command line?
#
if [ -z "${VERSION}" ]; then
	VERSION=$(awk '/^3/ {sub(/-Pre/, "", $1); print $1;}' level)
fi

if expr "${VERSION}" : '.*\.0$' > /dev/null; then
	echo "$0: proper version numbers do not ever end in '.0'!" 1>&2
	exit 1
fi

# this might not be a good idea -- and if it is then it'll
# need a LOT more work!
# 
# MINOR: increment minor #, go to two levels
# PATCH: increment patch #, go to three levels
#
# ... and NEXT always goes to BETA of next patch level
#
# We still need to determine an existing tag for PREV value.
#
# XXX automatically choose "PATCH" if there's a sticky branch tag?
#
NF=$(echo ${VERSION} | awk -F . '{print NF}')
case ${NF} in
4)
	TYPE="BETA"
	;;
3)
	TYPE="PATCH"
	;;
2)
	TYPE="MINOR"
	;;
esac
TYPE=${TYPE:-"BETA"}
case ${TYPE} in
BETA)
	PREV=$(echo ${VERSION} | awk -F . '{
		if ($4 == 90) {
			printf("%d.%d\n", $1, $2);
		} else {
			printf("%d.%d.%d.%d\n", $1, $2, $3, $4 - 1);
		}
	}')
	NEXT=$(echo ${VERSION} | awk -F . '{
		printf("%d.%d.%d.%d\n", $1, $2, $3, $4 + 1);
	}')
	;;
PATCH)
	PREV=$(echo ${VERSION} | awk -F . '{
		if ($3 == 1) {
			printf("%d.%d\n", $1, $2);
		} else {
			printf("%d.%d.%d\n", $1, $2, $3 - 1);
		}
	}')
	# should we check for the X.X.99.90 clash?
	NEXT=$(echo ${VERSION} | awk -F . '{
		printf("%d.%d.%d.90\n", $1, $2, $3);
	}')
	;;
MINOR)
	PREV=$(echo ${VERSION} | awk -F . '{
		printf("%d.%d\n", $1, $2 - 1);
	}')
	# the next beta for the next patch is X.X.0.90,
	# but the next beta for the next minor is X.X.99.90
	NEXT=$(echo ${VERSION} | awk -F . '{
		printf("%d.%d.0.90\n", $1, $2);
	}')
	;;
*)
	echo "$0: Don't know how to make a ${TYPE} release yet!" 1>&2
	exit 1
esac

TAG=RELEASE-$(echo ${VERSION} | sed 's/\./_/g')
PREV_TAG=RELEASE-$(echo ${PREV} | sed 's/\./_/g')

#
# XXX here we need to find the true valid PREV_TAG that exists!
#

echo "$0: will be tagging ${VERSION} with ${TAG}, diffing with ${PREV_TAG}, next is ${NEXT}."

if [ ! -r level -o ! -d ${RELEASE_DIR} -o ! -d ${ARCHIVE_DIR} ] ; then
	echo "$0: pre-requisite files or directories are missing!" 1>&2
	exit 1
fi

if [ ! -f RELEASE-${VERSION} ] ; then
	echo "$0: you haven't written new ${TYPE} release notes in RELEASE-${VERSION} yet!" 1>&2
	exit 1
fi

if [ "$(echo /usr/pkgsrc/mail/smail/patches/p*)" != '/usr/pkgsrc/mail/smail/patches/p*' ]; then
	echo "$0: there should not be any patches in pkgsrc/mail/smail!" 1>&2
	exit 1
fi

# "cvs log" exit code is zero here, and the warning message is on stderr
#
if expr "$(cvs log -r${TAG} level 2>&1 1>/dev/null)" : '^.*no revision' > /dev/null ; then
	:
else
	echo "$0: it seems you've already made a ${VERSION} tag: ${TAG}!" 1>&2
	exit 1
fi

if [ -d ${RELEASE_DIR}/smail-$VERSION ]; then
	echo "$0: it seems you've already made a ${RELEASE_DIR}/smail-$VERSION directory!" 1>&2
	exit 1
fi

if $debug; then
	echo "$0: debugging enabled, quitting before making any changes." 1>&2
	exit 1
fi

#
#	NOTICE!!!  At this point we're "committed"!
#

(
	sed 1q level
	echo ${VERSION} $(date)
) > level.new
mv level.new level

cvs commit -m "- this is release ${VERSION}" level || exit 1

cvs tag ${TAG} . || exit 1

# mkChangeLog.sh uses the "level" file too so it must be run before
# updating "level" again
#
sh ./mkChangeLog.sh

(
	sed 1q level
	echo ${NEXT}-Pre $(date)
) > level.new
mv level.new level

cvs commit -m "- post ${VERSION} -- get ready to preview release ${NEXT}" level || exit 1

( 
	echo ""
	echo "Release ${VERSION} generated:  $(date)"
) >> RELEASE-${VERSION}

OPWD=$(/bin/pwd)

cd ${RELEASE_DIR} || exit 1

rm -f smail-${PREV}-${VERSION}.diffs*
cvs rdiff -u -kv -r ${PREV_TAG} -r ${TAG} m-smail > smail-${PREV}-${VERSION}.diffs
gzip -v smail-${PREV}-${VERSION}.diffs

cvs -d $CVSROOT export -kv -r ${TAG} -d smail-${VERSION} m-smail || exit 1

# keep a copy of these inside the archive too....
cp -f ${OPWD}/ChangeLog-${VERSION} smail-${VERSION}/
cp -f ${OPWD}/RELEASE-${VERSION} smail-${VERSION}/

cp -f ${OPWD}/RELEASE-${VERSION} smail-${VERSION}-README.FIRST

pax -w -z -f smail-${VERSION}.tar.gz smail-${VERSION} || exit 1

cp -f smail-${VERSION}.tar.gz ${ARCHIVE_DIR}
cp -f smail-${VERSION}-README.FIRST ${ARCHIVE_DIR}
cp -f smail-${VERSION}/README ${ARCHIVE_DIR}/smail-${VERSION}-README
cp -f smail-${VERSION}/ChangeLog-${VERSION} ${ARCHIVE_DIR}/smail-${VERSION}.ChangeLog
cp -f smail-${PREV}-${VERSION}.diffs.gz ${ARCHIVE_DIR}

cp -f ${ARCHIVE_DIR}/smail-${VERSION}.tar.gz ${DISTFILES}
rm -f ${DISTFILES}/smail-${PREV}.tar.gz ${DISTFILES}/smail-*-Pre-*.tar.gz

(
	cd /usr/pkgsrc/mail/smail
	#
	# Fix up the SMAIL_VERSION line to fetch this new release.
	#
	ed Makefile.common <<_EOF_
/^SMAIL_VERSION/p
s/^SMAIL_VERSION=	.*$/SMAIL_VERSION=	${VERSION}/p
w
q
_EOF_
	make makesum mps
)

rm -f ${ARCHIVE_DIR}/smail-pkgsrc.shar
(
	cd /usr/pkgsrc/mail/smail
	# NOTE:  keep this file list in sync with the one in ./myPreview.sh
	# XXX perhaps we should use "make" itself to generate this list!
	shar DESCR Makefile Makefile.common distinfo INSTALL PLIST files/ files/mailer.conf files/clean-sendmail files/config.hubclient
) > ${ARCHIVE_DIR}/smail-pkgsrc.shar

cp -f ${ARCHIVE_DIR}/smail-pkgsrc.shar ${DISTFILES}

cd ${ARCHIVE_DIR} || exit 1

# leave the old README.FIRST
# XXX if making BETA then should leave whatever old RELEASE points to
#
rm smail-${PREV}.tar.gz smail-${PREV}-README smail-${PREV}.ChangeLog smail-*-${PREV}.diffs.gz

FILES="smail-${VERSION}-README.FIRST smail-${VERSION}-README smail-${VERSION}.tar.gz smail-${VERSION}.ChangeLog smail-${PREV}-${VERSION}.diffs.gz smail-pkgsrc.shar"

md5 ${FILES} > smail-${VERSION}.md5

echo rm smail-${TYPE}.tar.gz
echo ln -s smail-${VERSION}.tar.gz smail-${TYPE}.tar.gz
rm smail-${TYPE}.tar.gz
ln -s smail-${VERSION}.tar.gz smail-${TYPE}.tar.gz

echo rm smail.tar.gz
echo ln -s smail-${VERSION}.tar.gz smail.tar.gz
rm smail.tar.gz
ln -s smail-${VERSION}.tar.gz smail.tar.gz

echo rm README-${TYPE}
echo ln -s smail-${VERSION}-README README-${TYPE}
rm smail-README-${TYPE}
ln -s smail-${VERSION}-README smail-README-${TYPE}

echo rm README.FIRST-${TYPE}
echo ln -s smail-${VERSION}-README.FIRST README.FIRST-${TYPE}
rm smail-README.FIRST-${TYPE}
ln -s smail-${VERSION}-README.FIRST smail-README.FIRST-${TYPE}

echo rm README
echo ln -s smail-${VERSION}-README README
rm smail-README
ln -s smail-${VERSION}-README smail-README

echo rm README.FIRST
echo ln -s smail-${VERSION}-README.FIRST README.FIRST
rm smail-README.FIRST
ln -s smail-${VERSION}-README.FIRST smail-README.FIRST

# XXX if making a PATCH or a RELEASE then remove old BETA links...

# XXX ftp.planix.com runs OpenSSH, which rarely wants to do scp with me so
# echo the command out to make it easy to re-do the transfer some other way

echo ""
echo "About to run the command:"
echo "    scp ${FILES} smail-${VERSION}.md5 ftp.planix.com:/u5/ftp/pub/Smail/"

scp ${FILES} smail-${VERSION}.md5 ftp.planix.com:/u5/ftp/pub/Smail/

echo ""
echo "OK, if that worked then we're almost done!"
echo ""
echo "First go re-link as above and archive the old version on ftp.planix.com ..."
echo ""
echo "Then re-test the pkgsrc module and update ~/public_html/projects/smail.html"
echo ""
echo "... then go tell FreshMeat.net, smail3-users, and UseNet !!!"
