.\"#ident	"@(#)smail/man/man5:RELEASE-3_2_0_121:aliases.an,v 1.1 2004/06/25 18:05:56 woods Exp"
.\" 
.\" 
.de Vb
.ft CW
.nf
.ne \\$1
..
.de Ve
.ft R

.fi
..
.if n .na
.if n .nh
.\"
.TH ALIASES X_MAN5_EXT_X "RELEASE-3_2_0_121" "Smail-3" "Local"
.\"
.SH "NAME"
aliases \- X_ALIASES_FILE_X: Smail default local address aliases
.SH "THE ALIASES FILE"
The
.I aliases
is a sendmail-compatible alaises table provided for by the default
.B aliases
director which specifies this file as its database.
.PP
The default database type for the
.I X_ALIASES_FILE_X
file on this system is
.BR X_ALIASES_TYPE_X .
.PP
When the database type is one that is indexed or sorted, such as when it
has the type
.BR bsearch ,
or
.BR dbm ;
or when it is shared, such as when it has the type
.BR yp ,
then the file must be processed by
.IR mkaliases (X_MAN8_EXT_X),
(which itself uses
.IR mkline (X_MAN8_EXT_X)
to pre-process its intput) after every change before the change will
take effect.
.B mkaliases
is usually run for the default
.I aliases
director's file by running
.IR newaliases (X_MAN8_EXT_X),
which is a sendmail-compatible command-line alias provided by
.BR smail .
.PP
Each entry in the database is of the form:
.PP
.Vb 1
\&    alias-name:   address, ...
.Ve
.PP
Each
.I address
may be another alias, a local username, a fully qualified filename
(i.e. one which starts with a ``/'' character, a command string
(preceded by a ``|'' (pipe) character), or another e-mail address in RFC
2822 format.  Note that most acceptable Unix filenames are legal syntax
for an unquoted dot-atom, but command strings must be enclosed in double
quotes if they include whitespace for parameter separation since the
whitespace is of course not allowed in an unquoted dot-atom.
.PP
Lines beginning with whitespace are continuationuations of the previous
entry.
.PP
Comments are preceded by a ``#'' character and continue to the end of
the line.
.PP
If a director using the
.I aliasinclude
director driver is defined (see the sub-section
.I "The Mailing-list Drivers"
in the
.IR smailcdrc (X_MAN5_EXT_X)
manual) then any
.I address
may also be of the form
.PP
.Vb 1
\&    :include:/pathname/to/list/of/addresses
.Ve
.PP
If a director using the
.I error
director driver is defined (see the sub-section
.I "The Error Drivers"
in the
.IR smailcdrc (X_MAN5_EXT_X)
manual) then any
.I address
may also be of the forms:
.PP
.Vb 1
\&    :fail:"error message text"
\&    :defer:"defer message text"
.Ve
.PP
If Smail has been compiled with the USE_LSEARCH_REGEXCMP manifest
constant defined then an
.I alias-name
enclosed in double quotes (\(dq) is matched as a regular expression.
.\"
.SH EXAMPLE
The following is a sample alias file for a machine called ``nsavax'':
.PP
.Vb 20
\&    #
\&    # X_ALIASES_FILE_X - Sample aliases file for nsavax 
\&    #
\&    
\&    # root's alias -- this must be filled in -- root should never
\&    # ever read mail directly as himself!
\&    #
\&    # redirect root's mail to brown, copying to casey
\&    root: brown, casey
\&    
\&    #
\&    # The following aliases are required by RFC 2142
\&    #
\&    postmaster:     root
\&    info:           staff
\&    marketing:      staff
\&    sales:          staff
\&    support:        staff
\&    
\&    # More standard aliases defined by RFC 2142
\&    #
\&    # address to report network abuse (like spam)
\&    abuse:          postmaster
\&    # reports of network infrastructure difficulties
\&    noc:            root
\&    # address to report security problems
\&    security:       root
\&    # DNS administrator (DNS SOA records should use this)
\&    hostmaster:     root
\&    # Usenet news service administrator
\&    usenet:         root
\&    news:           usenet
\&    # http/web service administrator
\&    webmaster:      root
\&    www:            webmaster
\&    # UUCP service administrator
\&    uucp:           root
\&    # FTP administrator (especially anonymous FTP)
\&    ftp:            root
\&    
\&    # Commonly used group aliases:
\&    #
\&    staff:          postmaster
\&    office:         staff
\&    all:            staff
\&    tech:           staff
\&    ops:            staff
\&    
\&    # mail sent to MAILER-DAEMON should go to postmaster
\&    MAILER-DAEMON: postmaster 
\&    
\&    # system accounts
\&    #
\&    bin:            root
\&    dns:            hostmaster
\&    daemon:         root
\&    games:          root
\&    local:          root
\&    nobody:         root
\&    operator:       root
\&    rc5des:         operator
\&    src:            root
\&    syssup:         root
\&    
\&    # a wildcard (see also the user director suffix attribute)
\&    "woods-.*":     woods
\&    
\&    # post important information to network 
\&    msgs: local-msgs@nsavax
\&    local-msgs: "|/usr/bin/msgs -s"     # deliver to msgs program
\&    #
\&    # administrivia
\&    rnews: |/usr/libexec/news/uurec     # read news messages from mail
\&    #
\&    # mailing lists for accessing users on the local network 
\&    nsavax-users:   :include:/etc/mail/nsavax-users 
\&    #
\&    good-and-bad:   root, reagan
\&    reagan:         :fail:"Ronnie doesn't work here any more."
\&    #
\&    # a list including both the KGB and Bond as you can see!
\&    covert-bugs: 
\&            KGB <richard.sorge@kremvax.ru>,
\&            007 <james.bond@mi5cray.gov.uk>
\&            /var/log/covert-bugs # also save all messages here,
\&    covert-bugs-request: postmaster, 007 <james.bond@mi5cray.gov.uk>
.Ve
.\"
.SH "FILES"
.IP "X_ALIASES_FILE_X\fR " 4
the default aliases database, of type
.BR X_ALIASES_TYPE_X .
.\"
.SH "SEE ALSO"
.IR smail (X_MAN5_EXT_X),
.IR smaildrct (X_MAN5_EXT_X),
.IR mkaliases (X_MAN8_EXT_X),
.IR mkline (X_MAN8_EXT_X),
.IR newaliases (X_MAN8_EXT_X).
.IR "Smail Administration and Installation Guide" .
DARPA Internet Requests for Comments RFC 2822.
.PP
Perl Compatible Regular Expression documentation, in
.IR pcrepattern (3),
and
.IR pcre (3).
.\"
.SH "COPYRIGHT"
.I Copyright
(C) In The Public Domain 
.\"
