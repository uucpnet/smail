#
#ident	"@(#)smail/man/man1:RELEASE-3_2_0_121:Makefile,v 1.17 2004/07/25 17:21:58 woods Exp"
#
# Makefile for the smail section 1 man pages
#
#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

ROOT=../..
THIS_DIR=man/man1

include ${ROOT}/conf/Make.local

MKDEPEND=${ROOT}/conf/lib/mkdepend.sh
MKDEFS=${ROOT}/conf/lib/mkdefs.sh
CHECKDEFS=${ROOT}/conf/lib/checkdefs.sh
MKVERSION=${ROOT}/conf/lib/mkversion.sh
MKDIRS=${ROOT}/conf/lib/mkdirs.sh
INSTM=${ROOT}/conf/lib/instm.sh
INST=${ROOT}/conf/lib/inst.sh
XEXEC=${SHELL} ${ROOT}/conf/lib/xexec.sh
DEFS_SH=defs.sh
DEFS_H=defs.h
DEFS_SED=defs.sed
VERSION_SH=version.sh
VERSION_H=version.h
VERSION_SED=version.sed

PATHTO_LINKS=uupath.1
MAN1=pathto.1 uuwho.1 ${PATHTO_LINKS}

SED=sed

.SUFFIXES: .an .1

.an.1:
	@rm -f $*.1
	${SED} -f ${DEFS_SED} $*.an > $*.1

all:	${MAN1}

${PATHTO_LINKS}:
	@rm -f $@
	echo '.\"' > $@
	@. ./${DEFS_SH}; \
	   echo echo "\".so $$MAN1/pathto.$$MAN1_EXT\" >> $@"; \
	   echo ".so $$MAN1/pathto.$$MAN1_EXT" >> $@

${MAN1}: ${DEFS_SED}

mkdefs defs ${DEFS_SH} ${DEFS_H} ${DEFS_SED}: ${ROOT}/conf/EDITME
	ROOT=${ROOT} ${SHELL} ${MKDEFS}

.PRECIOUS: ${ROOT}/conf/EDITME
${ROOT}/conf/EDITME: ${DOT_MAKE} # cannot depend on anything else!
	cd ${ROOT}/conf && ${MAKE} EDITME

${VERSION_SH} ${VERSION_H} ${VERSION_SED}:
	ROOT=${ROOT} ${SHELL} ${MKVERSION}

install installman install.man: all ${DEFS_SH}
	@. ./${DEFS_SH}; \
	   case "$$DONT_INSTALL" in \
	   ?*)	echo Testing ... $@ ignored; exit 0;; \
	   esac; \
	   ${XEXEC} ${SHELL} ${MKDIRS} -m 0755 $$MAN1; \
	   case "$$MAN1_EXT" in \
	   1)	${XEXEC} ${SHELL} ${INSTM} -r -m 0444 $$MAN1 ${MAN1};; \
	   *)	for i in ${MAN1}; do \
			bn=`basename $$i .1`; \
			${XEXEC} ${SHELL} ${INST} -r -m 0444 $$i \
					 $$MAN1/$$bn.$$MAN1_EXT; \
		done;; \
	   esac

depend: check_defs
	@rm -f .depend
	. ./${DEFS_SH}; echo "$$DEFS_DEPEND" >> .depend

check_defs:
	SHELL=${SHELL} ROOT=${ROOT} ${SHELL} ${CHECKDEFS}

clean:
	rm -f .${DEFS_SH} .${DEFS_H} .${DEFS_SED}
	rm -f a.out core

clobber: clean
	@rm -f .depend
	rm -f ${DEFS_SH} ${DEFS_H} ${DEFS_SED}
	rm -f ${MAN1}

.PHONY: all install local-install clean local-clean clobber local-clobber depend local-depend lint mkdefs defs check_defs tags TAGS
.PHONY: installman install.man
