.\"ident	"@(#)smail/man/man8:RELEASE-3_2_0_121:unfreezemail.an,v 1.11 2005/09/06 19:53:56 woods Exp"
.\"
.if n .na
.if n .nh
.\"
.TH UNFREEZEMAIL X_MAN8_EXT_X "RELEASE-3_2_0_121" "Smail-3" "Local"
.SH NAME
unfreezemail \- move a message from the Smail error queue back to the input queue
.SH SYNOPSIS
.B unfreezemail
.RB "[" \-D " [" \-f "]]"
.RB "[" \-i "]"
.RB "[" \-n "]"
.RB "[" \-m "]"
.RB "[" \-v
.RI "\&" " N" "] "
.RB "[" \-u
.RI "\&" " addr" " | "
.RI message-queue-id "" " ... ]" 
.SH DESCRIPTION
.I Unfreezemail
safely reprocesses the specified error queue files, i.e. those displayed
by
.RB "``" "mailq -E" "''."
This script can be used to retry messages that were frozen in the error
queue by
.I smail
during the time when a configuration error existed, for example.
.PP
Note that if the message is already a bounce message (i.e. is from
``<>'' or ``<+>''), or if the error which caused the message to be
frozen in the first place has not yet been repaired, then another
failure to deliver the bounce will simply move it back into the error
queue immediately.  If the message is truly unimportant then you can use
the
.B \-D
option to delete the re-frozen message.
.PP
.B WARNING:
.I Unfreezemail
will cause the message delivery to be re-attempted to all recipient
addresses which had previously failed.  If this is not intended then the
message and/or its corresponding log file will have to be manually
edited and moved.
.SH OPTIONS
.TP
.B \-D
Delete the frozen message instead of re-queueing it.
.TP
.B \-f
Force the deletion.  By default each file must be confirmed before it is
deleted.
.TP
.B \-i
Confirm each re-delivery attempt interactively.
.TP
.B \-m
Call
.I mailq
before re-attempting delivery.
.TP
.B \-n
Do not immediately start a
.I runq
process to re-attempt delivery.
.TP
.BI \-u " addr"
Unfreeze all of the messages with a matching recipient address.
.TP
.BI \-v " N"
Tell the
.I runq
to be verbose when re-processing the message file. 
.SH FILES
.TP \w'X_MAIN_SPOOL_DIR_X/msglog'u+3n
.I "X_MAIN_SPOOL_DIR_X"
The default name of the Smail spool directory.
.TP \w'X_MAIN_SPOOL_DIR_X/msglog'u+3n
.I "X_MAIN_SPOOL_DIR_X/error"
The location of the default Smail spool's message error queue where
frozen messages are stored.
.TP \w'X_MAIN_SPOOL_DIR_X/msglog'u+3n
.I "X_MAIN_SPOOL_DIR_X/input"
The location of the default Smail spool's message input queue where
undelivered messages, and messages currently being delivered, are
stored.
.TP \w'X_MAIN_SPOOL_DIR_X/msglog'u+3n
.I "X_MAIN_SPOOL_DIR_X/msglog"
The location of the default Smail spool's message log files for both the
error and input queues.
.SH "SEE ALSO"
.IR smail (X_MAN5_EXT_X),
.IR smail (X_MAN8_EXT_X),
.SH AUTHOR
Written by Greg A. Woods <woods@planix.com>.  Contributed to the Public Domain.
.\"
