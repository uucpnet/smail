#ident "@(#) directors,v 1.2 1990/10/24 05:20:58 tron Exp"

# the following two directors are magic for accessing mailing list literal
# addresses supplied by the "aliasfile" driver and the "forwardfile" driver
# respectively.

# aliasinclude - expand ":include:filename" addresses produced by alias files
aliasinclude:
	driver = aliasinclude,		# use this special-case driver
	nobody;				# associate nobody user with addresses
					#  when mild permission violations
					#  are encountered

	copysecure,			# get permissions from alias director
	copyowners			# get owners from alias director

# forwardinclude - expand ":include:filename" addrs produced by forward files
forwardinclude:
	driver = forwardinclude,	# use this special-case driver
	nobody;

	copysecure,			# get perms from forwarding director
	copyowners			# get owners from forwarding director

# get aliases from /private/usr/lib/smail/aliases, which is optional
private_aliases:
	owner=owner-$user,
	driver=aliasfile;
	file=/private/usr/lib/smail/aliases,
	optional,
	proto=lsearch			# straight text ASCII file

# aliases - search for alias expansions stored in a database
aliases:
	driver = aliasfile,		# general-purpose aliasing director
	owner = owner-$user;		# problems go to an owner address

	file = mail.aliases,		# use this for YP
	proto = yp			# use this for YP

# forward - search for forward expansions stored in a database
forward:
	driver = aliasfile,		# general-purpose aliasing director
	sender_okay,			# sender can be in the list
	owner = postmaster@bargw.bar.foo.com;# problems to forward maintainers

	file = mail.forward,		# use this for YP
	proto = yp			# use this for YP

# dotforward - expand .forward files in user home directories
dotforward:
	driver = forwardfile,		# general-purpose forwarding director
	owner = Postmaster,		# problems go to the Postmaster
	nobody,
	sender_okay;			# sender never removed from expansion

	file = ~/.forward,		# .forward file in home directories
	checkowner,			# the user can own this file
	owners = root,			# or root can own the file
	modemask = 002,			# it should not be globally writable
	caution = daemon:root,		# don't run things as root or daemon
	# be extra carefull of remotely accessible home directories
	unsecure = "~ftp:~uucp:~nuucp:/tmp:/usr/tmp"

# forwardto - expand a "Forward to " in user mailbox files
#
# This emulates the V6/V7/System-V forwarding mechanism which uses a
# line of forward addresses stored at the beginning of user mailbox files
# prefixed with the string "Forward to "
forwardto:
	driver = forwardfile,
	owner = real-$user, nobody, sender_okay;

	file = /usr/spool/mail/${lc:user},	# point at user mailbox files
	forwardto,			# enable "Forward to " function
	checkowner,			# the user can own this file
	owners = root,			# or root can own the file
	modemask = 0022,		# only owner should be able to write
	caution = daemon:root		# don't run things as root or daemon

# user - match users on the local host with delivery to their mailboxes
user:	driver = user;			# driver to match usernames

	transport = local		# local transport goes to mailboxes

# real_user - match usernames when prefixed with the string "real-"
#
# This is useful for allowing an address which explicitly delivers to a
# user's mailbox file.  For example, errors in a .forward file expansion
# can be delivered here, or forwarding loops between multiple machines
# can be resolved by using a real-username address.
real_user:
	driver = user;

	transport = local,
	prefix = "real-"		# for example, match real-root

# let users put mailing lists on their local machines
private_lists:
	caution,
	owner=owner-$user,
	driver=forwardfile;
	file=/private/usr/lib/smail/lists/${lc:user}

# lists - expand mailing lists stored in a list directory
#
# mailing lists can be created simply by creating a file in the
# /usr/lib/smail/lists directory.
lists:	driver = forwardfile,
	caution,			# flag all addresses with caution
	nobody,				# and then associate the nobody user
	owner = owner-$user;		# system V sites may wish to use
					# o-$user, as owner-$user may be
					# too long for a 14-char filename.

	# map the name of the mailing list to lower case
	file = lists/${lc:user}

# smart_user - a partially specified smartuser director
#
# If the config file attribute smart_user is defined as a string such as
# "$user@domain-gateway" then users not matched otherwise will be sent
# off to the host "domain-gateway".
#
# If the smart_user attribute is not defined, this director is ignored.
smart_user:
	driver = smartuser;		# special-case driver

	# do not match addresses which cannot be made into valid
	# RFC822 local addresses without the use of double quotes.
	well_formed_only
