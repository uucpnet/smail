/*
#ident	"@(#)smail/util:RELEASE-3_2_0_121:mkline.c,v 1.30 2005/07/12 18:47:14 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * mkline.c:
 *	Take an alias, pathalias or similarly formed file and collapse
 *	into single-line records with comments stripped.
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#include "config.h"
#include "smail.h"
#include "alloc.h"
#include "list.h"
#include "main.h"
#include "parse.h"
#include "addr.h"
#include "exitcodes.h"
#include "log.h"
#include "field.h"
#include "smailstring.h"
#include "dys.h"
#include "smailsock.h"
#include "route.h"
#include "bindlib.h"
#include "transports/smtplib.h"			/* rfc821_is_*() decls. */
#include "spool.h"
#include "extern.h"
#include "smailport.h"

/* variables provided by this file */
char *program;				/* argv[0] from main */
int debug = 0;
FILE *errfile;
int use_tabs_p = FALSE;
int ignore_keys_p = FALSE;

/* functions local to this file */
static int mkaliasline __P((FILE *, char *));
static int mklistline __P((FILE *, char *));

/*ARGSUSED*/
int
main(argc, argv)
    int argc GCC_UNUSED_HACK;
    char *argv[];
{
    int list_files_p = FALSE;		/* TRUE for -l option */

    program = *argv++;
    errfile = stderr;

    /* if -l, process mailing list files */
    while (*argv && (*argv)[0] == '-' && (*argv)[1] != '\0') {
	register char *p;

	for (p = *argv + 1; *p; p++) {
	    switch (*p) {
	    case 'l':
		list_files_p = TRUE;
		break;

	    case 't':
		use_tabs_p = TRUE;
		break;

	    case 'n':
		ignore_keys_p = TRUE;
		break;

	    default:
		(void) fprintf(stderr, "Usage: %s [-ltn] [file ...]\n",
			       program);
		exit(EX_USAGE);
	    }
	}
	argv++;
    }

    if (*argv == NULL) {
	if ((list_files_p? mklistline(stdin, "stdin") : mkaliasline(stdin, "stdin")) < 0) {
	    exit(EX_DATAERR);
	}
    }
    while (*argv) {
	FILE *f;
	char *fn;

	if (EQ(*argv, "-")) {
	    f = stdin;
	    fn = "STDIN";
	} else {
	    fn = *argv;
	    f = fopen(fn, "r");
	    if (f == NULL) {
		(void) fprintf(stderr, "%s: cannot open %s: %s\n", program, *argv, strerror(errno));
		exit(1);
	    }
	}
	if ((list_files_p? mklistline(f, fn): mkaliasline(f, fn)) < 0) {
	    exit(EX_DATAERR);
	}
	argv++;
    }

    exit(0);

    /* NOTREACHED */
}

static int
mkaliasline(f, fn)
    FILE *f;
    char *fn;
{
    char *s;

    /*
     * read an entry of the form:
     *
     *		name:value
     *  or	name value
     */
    while ((s = read_entry(f, fn))) {
	register char *sp = s;
	char *error;
	struct token *tokens;
	register struct token *toks;

	if (! ignore_keys_p) {
	    while (!isspace((int) *sp) && *sp != ':') {
		sp++;
	    }

	    /* split the line in two */
	    *sp++ = '\0';
	}

	/* turn it into tokens */
	error = tokenize(sp, &tokens, TRUE, TRUE);

	if (error) {
	    if (ignore_keys_p) {	/* wasn't split above... */
		sp = s;
		while (!isspace((int) *sp) && *sp != ':') {
		    sp++;
		}

		/* split the line in two */
		*sp++ = '\0';
	    }
	    (void) fprintf(stderr, "%s: tokenize error in '%s': %s\n", program, s, error);
	    return -1;
	}

	if (! ignore_keys_p) {
	    /* write out the name */
	    printf((use_tabs_p? "%s\t": "%s:"), s);
	}

	/* write out the data, with most white space squeezed out */
	for (toks = tokens; toks; toks = toks->succ) {
	    fputs(toks->text, stdout);
	    if ((QUOTETOK(toks->form) || TEXTTOK(toks->form)) &&
		toks->succ &&
		(QUOTETOK(toks->succ->form) || TEXTTOK(toks->succ->form)))
	    {
		putchar(' ');
	    }
	}
	putchar('\n');
    }

    return 0;
}

static int
mklistline(f, fn)
    register FILE *f;
    char *fn;
{
    struct str s;
    register int c;
    char *error = NULL;
    struct addr *list = NULL;
    struct addr *cur;

    STR_INIT(&s);
    while ((c = getc(f)) != EOF) {
	STR_NEXT(&s, c);
    }
    STR_NEXT(&s, '\0');
    (void) process_field(NULL, s.p, NULL, NULL, &list, F_ALIAS, &error);
    if (error) {
	(void) fprintf(stderr, "%s: %s: %s\n", program, fn, error);
	return -1;
    }

    for (cur = list; cur; cur = cur->succ) {
	printf("%s\n", cur->in_addr);
    }

    return 0;
}

/*
 * standalone versions of some referenced routines
 */

/*ARGSUSED*/
char *
qualify_domain(s)
    char *s GCC_UNUSED_HACK;
{
    return NULL;
}

void
freeze_message()
{
    return;
}

char *
bind_compute_domain()
{
    return NULL;
}

char *
bind_lookup_txt_rr(s, ep)
    char *s GCC_UNUSED_HACK;
    struct error **ep GCC_UNUSED_HACK;
{
    return NULL;
}

/*
 * standalone versions of some referenced variables
 */

enum er_proc error_processing = MAIL_BACK;
int force_zero_exitvalue = FALSE;
pid_t daemon_pid = 0xDEAD;
char *message_id = NULL;		/* unique ID for this message */
enum op_mode operation_mode = MODE_DEFAULT;	/* mode of operation */
int only_testing = TRUE;
char *spool_dir;			/* directory used to spool message */
char *spool_fn = NULL;			/* basename of open spool file */
int islocal = TRUE;
char *sender = "nsc!tron";
char *sender_name = "MasterControlProgram";
char *smtp_local_net = NULL;
char *local_sender;
uid_t real_uid = 0;
gid_t prog_egid = 0;

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
