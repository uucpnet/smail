/*
#ident	"@(#)smail/src/util:RELEASE-3_2_0_121:dbm_compat.h,v 1.12 2003/12/14 22:42:34 woods Exp"
 */

/*
 * dbm_compat.h:
 *	Low-functionality compatibility header for dbm and ndbm.
 */
/* Imitate dbm using ndbm */

#if defined(HAVE_DBM) && !defined(HAVE_NDBM)

# undef NULL			/* dbm.h often defines NULL -- prevent redfine */
# include <dbm.h>
# undef NULL			/* in case dbm.h does not define NULL.... */
# define NULL	0		/* we will re-define it */

#else /* !HAVE_DBM */

# ifdef HAVE_NDBM
#  include <ndbm.h>
# else
#  include "sdbm.h"
#  define HAVE_NDBM
# endif

/*
 * emulate dbm using ndbm
 */

static DBM *db;

# define dbminit(f) 	((db = dbm_open((f), O_RDWR | O_CREAT, 0666)) ? 0 : (db = dbm_open((f), O_RDONLY, 0666)) ? 0 : -1)
# define dbmclose()	((db ? (dbm_close(db), 0) : 0), db = (DBM *)0, 0)
# define store(k,v)	dbm_store(db, (k), (v), DBM_INSERT)
# define fetch(k)	dbm_fetch(db, (k))

#endif /* HAVE_DBM */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
