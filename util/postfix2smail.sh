#! /bin/sh
#
#	postfix2smail.sh - convert pcre_table to smail *_checks list content.
#
#ident "smail/util:RELEASE-3_2_0_121:postfix2smail.sh,v 1.1 2004/02/16 22:48:15 woods Exp"

# NOTE:  pcre_table entries have a default flag of "i"

exec sed -e 's/:\.\*/: /'			\
    -e 's/Subject: /Subject:[[:blank:]]*/'	\
    -e 's/\\/\\\\/g'				\
    -e 's/"/\\"/g'				\
    -e '/^[^#]/s/^/	:/'			\
    -e 's,/\([^ 	]*\)[ 	]*REJECT,/\1i;\
		,'
