#! /bin/sh
#
#       cyrus-deliver-noquota
#
# remove evidence of the magic from the message headers
#
#ident "@(#)smail/util:RELEASE-3_2_0_121:cyrus-deliver-noquota.sh,v 1.1 2003/12/21 00:32:35 woods Exp"

# XXX the substitutions should probably be stopped after the blank
# line indicating the end of headers.  How to do this with sed?

PATH="X_UTIL_PATH_X:X_SECURE_PATH_X"; export PATH

: ${SMAIL_PROGRAM:="X_SMAIL_NAME_X"}

CYRUS_DELIVER_PATH=$(${SMAIL_PROGRAM} -bP cyrus_deliver_path)
CYRUS_NOQUOTA_PASSPHRASE=$(${SMAIL_PROGRAM} -bP cyrus_noquota_passphrase)

# NOTE:  It is critical that your shell is POSIX-compliant in that it
# exits with the exit code from the last command in the pipeline!!!
#
sed -e "s/${CYRUS_NOQUOTA_PASSPHRASE}-//g" | ${CYRUS_DELIVER_PATH} -q $*
