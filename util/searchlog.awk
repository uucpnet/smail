#! awk -f 

#ident	"@(#)smail/util:RELEASE-3_2_0_121:searchlog.awk,v 1.2 2003/12/14 22:14:14 woods Exp"
#
#	searchlog.awk
# 
# This wee script shows how to search and print a complete record set for
# any and all matching log entries -- i.e. all the records associated with a
# transaction matching a given set of address(es).  (except any initial SMTP
# connection related records prior to when the message-id was assigned)
#
# NOTE:  Most awk implementations are not very good at garbage collection and
# feeding this script too many records at once will eventually run the
# interpreter out of memory.
#

BEGIN {
	undeliv[""] = "";
	r[""] = 0;
}

$5 == "Received" {
	undeliv[$4] = $0;
	r[$4] = 0;
}

$5 == "Delivered" || $5 == "Failed" || $5 == "Returned" {
	undeliv[$4] = undeliv[$4] "\n" $0;
}

# NOTE:  the match operator in awk does not allow a variable to be on the
# right-hand-side.
#
$5 == "Completed." {
	if (undeliv[$4] ~ /:userid-1[ @]/ ||
	    undeliv[$4] ~ /:userid-2[ @]/ ||
	    undeliv[$4] ~ /:userid-3[ @]/) {
		print undeliv[$4];
		print $0;
	}
	# at least try to hint that the string memory can be freed
	udeliv[$4] = "";
}

# Local Variables:
# c-file-style: "personal-awk"
# End:
