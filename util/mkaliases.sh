#! /bin/sh
:
#ident	"@(#)smail/src:RELEASE-3_2_0_121:mkaliases.sh,v 1.14 1998/10/01 05:50:12 woods Exp"
#
#    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
#    Copyright (C) 1992  Ronald S. Karr
# 
# See the file COPYING, distributed with smail, for restriction
# and warranty information.

# mkaliases - create an aliases database for a smail aliasfile director

# Rebuilds the aliasfile X_PATH_ALIASES_FILE_X, or the argument, if given.
#
# Different types for the aliasfile are rebuilt in different ways.
# In the case of lsearch, verification is performed on the file,
# but no other changes are made.
#
# XXX FIXME!!!!  Only rebuilds one alias file.

PATH='X_UTIL_PATH_X:X_SECURE_PATH_X'; export PATH

# XXX for some reason umask doesn't seem to be inherited properly on some
# XXX systems such as NetBSD-1.1 and BSDI-1.1....
# XXX This should be derived from modemask in the current configuration...
umask 002

# XXX FIXME?  this should be derived from current configuration....
ALIASES_FILE='X_PATH_ALIASES_FILE_X'

# XXX FIXME!!!  this should be derived from current configuration....
ALIASES_TYPE='X_ALIASES_TYPE_X'

prog=$0
GETOPT="X_UTIL_BIN_DIR_X/getopt"
DEBUG=0

USAGE="Usage: $prog [-v N] [alias_file [alias_type]"

set -- `$GETOPT v: $*`

if [ $? -ne 0 ]; then
	echo $USAGE 1>&2
	exit 64			# EX_USAGE
fi

for i in $*; do
	case $i in
	-v) DEBUG=$2; shift 2;;
	--) shift; break;;
	esac
done

if [ $# -gt 0 ]; then
	ALIASES_FILE="$1"
	shift
fi

if [ $# -gt 0 ]; then
	ALIASES_TYPE="$1"
	shift
fi

if [ ! -w "$ALIASES_FILE" ] ; then
	echo "$prog: '$ALIASES_FILE' does not appear to be a regular writable file." 1>&2
	exit 77			#  EX_NOPERM
fi

if [ ! -w "`dirname $ALIASES_FILE`" ] ; then
	echo "$prog: directory '`dirname $ALIASES_FILE`' does not appear to be writable." 1>&2
	exit 77			#  EX_NOPERM
fi

if [ "$DEBUG" -gt 5 ] ; then
	echo "$prog: running as: `id`"
fi

if [ "$DEBUG" -gt 1 ] ; then
	echo "$prog: processing $ALIASES_FILE as type $ALIASES_TYPE"
fi

# NOTE: list of possible types defined in lookup.c:protos[]
#
case "$ALIASES_TYPE" in
aliasyp | yp | nialias | nisplus )
	mkline "$ALIASES_FILE" | mkdbm -y -v -o "$ALIASES_FILE"
	rc=$?
	if [ $rc -ne 0 ] ; then
		echo "$prog: failed to create YP/NIS DBM file for '$ALIASES_FILE'." 1>&2
		exit $rc
	fi
	;;
dbm )
	mkline "$ALIASES_FILE" | mkdbm -f -v -o "$ALIASES_FILE"
	rc=$?
	if [ $rc -ne 0 ] ; then
		echo "$prog: failed to create DBM file for '$ALIASES_FILE'." 1>&2
		exit $rc
	fi
	;;
bsearch )
	ALIASES_DIR=`dirname "$ALIASES_FILE"`
	case "$ALIASES_FILE" in
	*.sort)
		TEXT_FILE="`basename "$ALIASES_FILE" .sort`"
		;;
	* )
		TEXT_FILE="`basename "$ALIASES_FILE"`".txt
		;;
	esac
	if [ ! -r "$ALIASES_DIR/$TEXT_FILE" ] ; then
		echo "$prog: $ALIASES_DIR/$TEXT_FILE: unable to read file." 1>&2
		exit 77		#  EX_NOPERM
	fi
	mkline "$ALIASES_DIR/$TEXT_FILE" | wc -lc | awk '{ print $1 " entries, " $2 " bytes total" }'
	rc=$?
	if [ $rc -ne 0 ] ; then
		echo "$prog: not rebuilding '$ALIASES_FILE'." 1>&2
		exit $rc
	fi
	mkline "$ALIASES_DIR/$TEXT_FILE" | mksort -f > "$ALIASES_DIR/.$TEXT_FILE"
	rc=$?
	if [ $rc -ne 0 ] ; then
		echo "$prog: failed to rebuild '$ALIASES_FILE' from '$ALIASES_DIR/$TEXT_FILE'." 1>&2
		exit $rc
	fi
	mv -f "$ALIASES_DIR/.$TEXT_FILE" "$ALIASES_FILE"
	;;
lsearch )
	mkline "$ALIASES_FILE" | wc -lc | awk '{ print $1 " entries, " $2 " bytes total" }'
	rc=$?
	if [ $rc -ne 0 ] ; then
		echo "$prog: failed to parse '$ALIASES_FILE'." 1>&2
		exit $rc
	fi
	;;
* )
	echo "$prog: $ALIASES_FILE: unknown file type: $ALIASES_TYPE" 1>&2
	exit 78			# EX_CONFIG
	;;
esac

exit 0
