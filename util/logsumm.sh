#! /bin/sh
:
#ident	"@(#)smail/util:RELEASE-3_2_0_121:logsumm.sh,v 1.10 2005/07/15 20:00:46 woods Exp"
#
# 	logsumm - daily logfile summary for Smail
#
# Attempts to summarize log files generated when SMAIL_LOG_STYLE=2

# common location
PATH="X_UTIL_PATH_X:X_SECURE_PATH_X"; export PATH

: ${SMAIL_PROGRAM:="X_SMAIL_NAME_X"}
: ${UTIL_BIN_DIR:="X_UTIL_BIN_DIR_X"}
: ${DOT_Z:="X_DOT_Z_X"}
: ${ZCAT:="X_ZCAT_X"}
: ${TMPDIR:="X_TMP_DIR_X"}
#
# gawk-3.1.1 is twice as slow as mawk-1.3.3
# The One True AWK (20001115) is in the middle
#
# WARNING:  Old AWK and old NAWK are not compatible with the
# command-line syntax used to invoke awk by this script (-v).
# (newer NAWKs, e.g. on SunOS-5.6 and newer, as well as of course
# those adapted to be XPG-4 compatible, work just fine though)
#
: ${AWK:="awk"}

argv0=`basename $0`

GETOPT=${UTIL_BIN_DIR}/getopt
LOGSUMM_AWK=${UTIL_BIN_DIR}/logsumm.awk

USAGE="Usage: $argv0 [-v] [-d] [-E] [-L logsumm.awk] [-t tmpdir] [logfile ...]"

SMAIL_LIB_DIR=`${SMAIL_PROGRAM} -bP smail_lib_dir`
LOGFILE=`${SMAIL_PROGRAM} -bP logfile`
# Old fashioned smaillog form
OLD_LOGFILE=`echo ${LOGFILE} | sed -e 's,^\(.*\)/\([^/][^/]*\)$,\1/OLD/\2,'`

debug=0				# initialises an awk variable
errors=0			# initialises an awk variable
verbose=0			# initialises an awk variable
SORT_OK="true"

OPTIONS="dEL:t:v"
set -- `${GETOPT} -n $argv0 -q ${OPTIONS} ${1+"$@"}`
if [ "$?" -ne 0 ]; then
	echo ${USAGE} 1>&2
	exit 2
fi
for i in ${1+"$@"} ; do
	case "$i" in
	-d)
		debug=`expr $debug + 1`
		shift
		;;
	-E)
		errors=1
		shift
		;;
	-L)
		LOGSUMM_AWK=$2
		shift 2
		;;
	-t)
		TMPDIR=$2
		shift 2
		;;
	-v)
		verbose=1
		shift
		;;
	--)
		shift
		break
		;;
	-?)
		echo ${USAGE} 1>&2
		exit 2
		;;
	esac
done

while [ $# -gt 0 ] ; do
	case "$1" in
	"-")
		# /dev/stdin doesn't exist on every system, but that's
		# mostly OK as we're only using it as a placeholder
		# token -- only sorting of the STATS_LOG list may have
		# to be avoided.
		#
		STATS_LOGS="${STATS_LOGS} /dev/stdin"
		if [ ! -f /dev/stdin ]; then
			SORT_OK="false"
		fi
		;;
	*)
		STATS_LOGS="${STATS_LOGS} ${1}"
		;;
	esac
	shift
done

if [ -z "${STATS_LOGS}" ]; then
	#
	# By default we look at "yesterday's" logfile (i.e. the
	# previously archived log)
	#
	if [ -f ${LOGFILE}.0 ]; then
		STATS_LOGS=${LOGFILE}.0
	elif [ -f ${LOGFILE}.0${DOT_Z} ]; then
		STATS_LOGS=${LOGFILE}.0${DOT_Z}
	elif [ -f ${OLD_LOGFILE}.0 ]; then
		STATS_LOGS=${OLD_LOGFILE}.0
	elif [ -f ${OLD_LOGFILE}.0${DOT_Z} ]; then
		STATS_LOGS=${OLD_LOGFILE}.0${DOT_Z}
	fi
fi

if [ -z "${STATS_LOGS}" ]; then
	echo "$argv0: cannot find yesterday's logfile." 1>&2
	exit 1
fi

if [ ! -d ${TMPDIR} ] ; then
	if mkdir ${TMPDIR} ; then
		chmod 700 ${TMPDIR}
	else
		echo "$argv0: ${TMPDIR} may exist as a file!" 1>&2
		exit 1
	fi
fi

# Try to process the logs in cronological order...
#
# Note: ls reports errors about missing files....
#
if [ $SORT_OK = "true" ]; then
	STATS_LOGS=`ls -1rt ${STATS_LOGS}`
fi

if [ -z "${STATS_LOGS}" ]; then
	echo "$argv0: no log files to process." 1>&2
	exit 1
fi

NEEDS_ZCAT=false
for LOGFILE in ${STATS_LOGS} ; do
	case ${LOGFILE} in
	*${DOT_Z})
		NEEDS_ZCAT=true
		;;
	esac
done

if $NEEDS_ZCAT; then
	for LOGFILE in ${STATS_LOGS} ; do
		case ${LOGFILE} in
		"/dev/stdin")
			LOGFILE="-"
			CAT_STATS_LOG="cat"
			;;
		*${DOT_Z})
			CAT_STATS_LOG="${ZCAT}"
			;;
		*)
			CAT_STATS_LOG="cat"
			;;
		esac
		${CAT_STATS_LOG} ${LOGFILE}
	done | ${AWK} -v DEBUG=${debug} -v VERBOSE=${verbose} -v ERRORS=${errors} -v TMPDIR=${TMPDIR} -f ${LOGSUMM_AWK}
	rc=$?
else
	exec ${AWK} -v DEBUG=${debug} -v VERBOSE=${verbose} -v ERRORS=${errors} -v TMPDIR=${TMPDIR} -f ${LOGSUMM_AWK} ${STATS_LOGS}
fi

exit $rc
