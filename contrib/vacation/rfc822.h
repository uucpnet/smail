/*
 * Copyright (C) 1996-2000 Michael R. Elkins <me@cs.hmc.edu>
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.
 */

/*
 * Modifications from the original:
 *
 * Copyright (c) 2001, 2002	Greg A. Woods <woods@planix.com>
 */

#ident "@(#)vacation:RELEASE-3_2_0_121:rfc822.h,v 1.2 2004/08/17 20:45:30 woods Exp"

#ifndef __rfc822_h__
# define __rfc822_h__

/* possible values for RFC822Error */
enum {
	RFC822_ERR_MEMORY = 1,
	RFC822_ERR_MISMATCH_PAREN,
	RFC822_ERR_MISMATCH_QUOTE,
	RFC822_ERR_BAD_ROUTE,
	RFC822_ERR_BAD_ROUTE_ADDR,
	RFC822_ERR_BAD_ADDR_SPEC
};

#define NRFC822Errors	RFC822_ERR_BAD_ADDR_SPEC

/*
 * A list node describing a parsed address:
 *
 * If start_group is non-zero then the mailbox field is the name of a group
 * address and the elements in the group address follow as a sub-list in the
 * next nodes up until a node is encountered with a null mailbox field.
 */
typedef struct MBXLST {
	char           *original;	/* the un-parsed original address */
	char           *personal;	/* real name of address */
	char           *mailbox;	/* mailbox and host address (or group name) */
	int             start_group;	/* start of a group mailbox sub-list? */
	struct MBXLST  *next;		/* next struct in the list */
} mbxlst_t;

#define rfc822_new_addr()	safe_calloc((size_t) 1, sizeof(mbxlst_t))

void            rfc822_free_addr_list __P((mbxlst_t **));

mbxlst_t       *rfc822_parse_addrlist __P((mbxlst_t *, const char *s));

mbxlst_t       *rfc822_cpy_addr __P((mbxlst_t *addr));
mbxlst_t       *rfc822_cpy_addr_list __P((mbxlst_t *addr));

mbxlst_t       *rfc822_append_list __P((mbxlst_t **a, mbxlst_t *b));

void            rfc822_write_addr __P((char *, size_t, mbxlst_t *));
void            rfc822_write_addr_list __P((char *, size_t, mbxlst_t *));
void            rfc822_cat __P((char *, size_t, const char *, const char *));

void            rfc822_qualify_addr __P((mbxlst_t *, const char *));

void            strip_rfc822_comments __P((char *));
void            strip_rfc822_whitespace __P((char *));
char           *rfc822_end_of_mbox __P((char *));

extern unsigned int    RFC822Error;
extern const char     *RFC822Errors[];

#define rfc822_error(x)		RFC822Errors[x]

static const char      RFC822Specials[] = "@.,:;<>[]\\\"()";

#define is_rfc822_special(x)	strchr(RFC822Specials, x)

#endif /* __rfc822_h__ */
