/*
 * Copyright (c) 2001, 2002	Greg A. Woods <woods@planix.com>
 */

#ident "@(#)vacation:RELEASE-3_2_0_121:vacation.h,v 1.2 2004/08/17 20:45:30 woods Exp"

#ifndef TRUE
# define TRUE	1
#endif
#ifndef FALSE
# define FALSE	0
#endif

extern int debug;

void *safe_malloc __P((size_t));
void *safe_calloc __P((size_t, size_t));
void safe_free __P((void **));
char *safe_strdup __P((const char *));
char *strrchrs __P((char *, char *, const char *));

