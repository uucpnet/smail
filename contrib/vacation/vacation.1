.\"ident "@(#)vacation: :vacation.1,v 1.6 2002/02/14 18:49:16 woods Exp "
.\"
.\" Copyright (c) 1983  Eric P. Allman
.\"
.\" Copyright (c) 1985, 1987, 1990, 1991, 1993
.\"	The Regents of the University of California.  All rights reserved.
.\"
.\" Copyright (c) 2001, 2002	Greg A. Woods <woods@planix.com>
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by the University of
.\"	California, Berkeley and its contributors.
.\" 4. Neither the name of the University nor the names of its contributors
.\"    may be used to endorse or promote products derived from this software
.\"    without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"	@(#)vacation.1	8.2 (Berkeley) 4/28/95
.\"
.Dd 2004/08/17 20:45:30
.Dt VACATION 1
.Os BSD 4.4
.Sh NAME
.Nm vacation
.Nd reply to incoming e-mail with an
.Dq I am not here
message
.Sh SYNOPSIS
.Nm
.Fl i
.Op Fl d
.Op Fl f Ar db
.Op Fl r Ar interval
.Op Fl x
.br
.Nm vacation
.Fl x
.Op Fl d
.Op Fl f Ar db
.Op Fl r Ar interval
.br
.Nm vacation
.Fl l
.Op Fl d
.Op Fl f Ar db
.br
.Nm vacation
.Oo
.Op Fl a Ar alias
\&...
.Oc
.Op Fl d
.Op Fl f Ar db
.Op Fl m Ar msg
.Op Fl r Ar interval
.Op Ar login
.\"
.Sh DESCRIPTION
.Nm
processes RFC-2822 compliant messages fed to its standard input and may
reply with a message telling the originator of the incoming message that
you are currently not reading your mail.  The intended use is as a pipe
command in a
.Pa ~/.forward
file.
.\"
.Ss Options
Available options:
.Bl -tag -width Ds
.\"
.It Fl a
Treat
.Ar alias
as an alias for the user running
.Nm vacation .
There can be multiple instances of
.Dq Fl a Ar alias
given on the command line and each
.Ar alias
will be added to an internal list of names.  These should only be
given as mailboxes as they are only compared with the mailbox portion of
any recipient addresses.
.\"
.It Fl d
Turn on debugging.  This flag causes informtaional and error messages to
be printed to
.Dv stderr
as well as logged.
.Em Never
use this option on the invocation in your
.Pa ~/.forward
file!
.\"
.It Fl f Ar db
Use
.Ar db
as the recent correspondents database file instead of the default
.Pa ~/.vacation.db .
.\"
.It Fl i
(Re-)Initialize the recent correspondents database file.  It should be
used before you modify your
.Pa ~/.forward
file, and it must
.Em never
be used on the invocation in your
.Pa ~/.forward
file.  Except when listing the contents the database will always be
created if it does not exist so this option is only necessary if you
want to clear an existing database.
.\"
.It Fl l
List the current contents of recent correspondents database file.
.\"
.It Fl m Ar msg
Use
.Ar msg
as the reply message contents file instead of the default
.Pa ~/.vacation.msg .
.\"
.It Fl r Ar interval
Set the reply interval to
.Ar interval
days.  This value is stored in the
.Pa ~/.vacation.db
file and is usually used with the
.Fl i
option.  The default reply interval is one week.  An interval of
zero
.Pq Dq 0
is not allowed as that would mean a reply would be sent to each message,
which could accidentally cause a mail loop between another less
carefully programmed autoresponder.  An interval of
.Dq Li infinite
will never send more than one reply.
.\"
.It Fl x
Reads a list of addresses from standard input, one per line (ignoring
blank lines and lines that begin with a
.Dq #
character), and adds them to the recent correspondents database.  Mail
coming from these excluded addresses will not ever get a reply.  Whole
domains can be excluded using the syntax
.Dq @domain .
.Pp
Note it is critical to pre-load the injection addresses of any mailing
lists you subscribe to which are handled by broken mailing list software
(e.g. Lsoft's LISTSERV as of the last inspection) which doesn't include
a proper
.Dq precedence:
header as described below.  This is necessary prevent replies from being
sent to mailing lists when the
.Dq reply-to:
header points to that
injection address and the recipient is (also) in either the
.Dq to:
or
.Dq cc:
header.
.\"
.El
.\"
.Ss Theory of Operation
No message will be sent unless
.Ar login
(or the invoking user's name), or an
.Ar alias
supplied using the
.Fl a
option, is part of either the
.Dq to:
or
.Dq cc:
headers of the mail.
.Pp
No messages from
.Dq -OUTGOING ,
.Dq -RELAY ,
.Dq LISTSERV ,
.Dq -REQUEST ,
.Dq MAILER ,
or
.Dq MAILER-DAEMON
will be replied to (where these strings are case insensitive suffixes of
the base mailbox name for the sender or originator address).
.Pp
Finally, and most importantly, no notification is sent if a
.Dq precedence:
header is found that contains the value
.Dq bulk ,
.Dq list ,
or
.Dq junk .
.Pp
.Nm
expects to find a file called
.Pa .vacation.msg ,
in your home directory (or whatever file was specified with
.Fl m ) ,
containing an RFC-2822 compliant message to be sent back in response to
an incoming message.  It must be an entire message, including headers,
but without recipient headers, and
.Em especially
without the
.Ux
mailbox
.Dq From_
header).   The recipient address(es) will be supplied in a
.Dq to:
header that will be automatically prepended to the message.  Any
occurrence of the string
.Dq $SUBJECT
in the template message's headers will be replaced by the content of the
.Dq subject:
header from the message being read on the standard input.  A
.Dq precedence: bulk
header is also automatically inserted in the outgoing message headers.
.Pp
The reply is sent to the address(es) given in the
.Dq reply-to:
header if one is found, and if not then to the address(es) given in the
.Dq from:
header (as per the RFC-2822 rules for any agent replying to a message).
.Pp
The addresses to which replies have been sent are recorded, along with
the time the latest reply was sent to them, in a
.Xr db 3
database in the file
.Pa .vacation.db
in your home directory.  No reply will be sent to any address which has
been logged in this file unless the last reply was sent more than
.Ar interval
(see
.Fl r
above) days ago.
.Pp
Fatal errors, such as calling
.Nm
with incorrect arguments, or with a non-existent
.Ar login Ns Ar s ,
and any errors accessing files or errors sending messages are logged in
the system log file, using
.Xr syslog 3 ,
and if
.Fl d
is given they are also printed to the standard error descriptor.
.\"
.Sh EXIT STATUS
The
.Nm
utility exits with a value of zero (0) on success, and greater than zero
(>0) if an error occurs.  The non-zero exit values are defined in
.Aq Pa sysexits.h .
.\"
.Sh FILES
.Bl -tag -width "vacation.123xxx" -compact
.\"
.It Pa ~/.vacation.db
default correspondents database file name in the user's home directory
.\"
.It Pa ~/.vacation.msg
default name of the file containing the message to send
.El
.\"
.Sh EXAMPLES
An example
.Pa ~/.forward
file might contain:
.Bd -literal -offset indent
\e\&eric, "|/usr/bin/vacation -a postmaster eric"
.Ed
.Pp
which would save messages sent to you in your system mailbox (assuming
your login name was eric) and would also reply with copies of your
.Pa ~/.vacation.msg
message (modulo the constraints outlined above).
.Pp
An example
.Pa ~/.vacation.msg
file might contain a message similar to the following:
.Pp
.Bd -unfilled -offset indent -compact
From: eric@CS.Berkeley.EDU (Eric Allman)
Subject: I am on vacation
Summary: Re: $SUBJECT
Delivered-By-The-Graces-Of: The Vacation program

I am on vacation until July 22.  If you have something urgent,
please contact Keith Bostic <bostic@CS.Berkeley.EDU>.
-- 
eric
.Ed
.\"
.Sh SEE ALSO
.Xr syslog 3 ,
.Xr sendmail 8
.\"
.Sh HISTORY
The
.Nm
command first appeared in
.Bx 4.3 .
.Pp
.Nm
was rewritten by Greg A. Woods
.Aq Li woods@planix.com
to obey RFC-2822.
.\"
.Sh BUGS
The value of
.Dq infinity
for the reply interval is actually stored as
.Dv LONG_MAX .
.Pp
There's no facility for folding and/or filling text or headers in the
outgoing message.
.Pp
Multiple
.Ar login
parameters should be treated as if they were given as
.Fl a
options.
.Pp
Failure to create the
.Pa ~/.vacation.msg
file causes a runtime error.
