#ident	"@(#)vacation:RELEASE-3_2_0_121:COPYING,v 1.2 2004/08/17 20:45:30 woods Exp"

Original software and documentation (vacation.c & vacation.1):

/*
 * Copyright (c) 1983  Eric P. Allman
 *
 * Copyright (c) 1983, 1987, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


Other additional source files may be covered by a separate copyright
license.  Please inspect each for any specific copyright terms.


The complete package, all modifications to the original files, and noted
individual new files are covered by the following copyright license:

/*
 * Copyright (c) 2004 Greg A. Woods
 *
 * Contact:  Greg A. Woods <woods-vacation-v2@weird.com>
 * 
 * Redistribution of this software in both source and binary forms, with
 * or without modification, is permitted provided that all of the
 * following conditions are met:
 * 
 * 1. Redistributions of source code, either alone or as part of a
 *    collective work, must retain this entire copyright notice, and the
 *    following disclaimer, without alteration, in each file that
 *    contains part of this software.
 * 
 * 2. Redistributions of this software in binary form, either alone or
 *    as part of a collective work, must reproduce this entire copyright
 *    notice, and the following disclaimer, without alteration, in
 *    either the documentation (as text files in electronic media, or in
 *    printed matter), and/or any original header files from this
 *    software as per the previous term, and/or other materials provided
 *    as part of the distribution.
 * 
 * 3. Collective works including this software must also include the
 *    following acknowledgement, either alone or as part of this entire
 *    copyright license, in any printed documentation accompanying a
 *    physical distribution (if there is printed documentation), and in
 *    a plain text file separate from the archive files (but perhaps
 *    along with other similar acknowledgments) on any electronic
 *    medium:
 * 
 * 	This product includes software developed by Greg A. Woods.
 * 
 * 4. The name of the author may NOT be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.  The use of the author's name strictly to meet the
 *    requirements of the previous terms is not to be considered
 *    promotion or endorsement under this term.
 * 
 * 5. Altered versions (derivative works) must be plainly marked as
 *    such, and must not be misrepresented as being the original
 *    software.  This copyright notice, and the following disclaimer,
 *    must not be removed from any derivative work and must not be
 *    changed in any way.
 * 
 * All other rights are reserved.
 * 
 * DISCLAIMER:
 * 
 * THIS SOFTWARE IS PROVIDED BY GREG A. WOODS ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
