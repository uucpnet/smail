/*
 * Copyright (c) 2004	Greg A. Woods <woods@planix.com>
 */

#ident "@(#)librfc2822: :ishfield.c,v 1.1 2004/08/09 06:13:05 woods Exp "

#include <sys/cdefs.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "rfc2822.h"

/*
 * rfc2822_ishfield() --
 *
 * Check whether the passed line is an RFC-2822 header line with the desired
 * field name.  Return a pointer to the field body, or NULL if the field name
 * does not match or the field is not in the proper form (no colon, devoid of
 * content, etc.)
 *
 * Note storage in linebuf is modified (which is why it's not declared const)
 */
char *
rfc2822_ishfield(linebuf, colon, field)
	char linebuf[];			/* array containing header */
	char *colon;			/* pointer to ':' in header */
	const char *field;		/* field name to match */
{
	char *cp = colon;

	assert(linebuf);
	assert(field);
	if (!colon) {
		rfc2822_errno = RFC2822_BAD_HEADER;
		return NULL;
	}

	*cp = '\0';
	if (strcasecmp(linebuf, field) != 0) {
		*cp = ':';
		return NULL;
	}
	*cp = ':';
	/* skip past any whitespace after the colon */
	for (cp++; *cp == ' ' || *cp == '\t'; cp++) {
		;
	}
	if (!*cp) {			/* empty fields are useless */
		rfc2822_errno = RFC2822_EMPTY_HEADER;
		return NULL;
	}

	return cp;
}
