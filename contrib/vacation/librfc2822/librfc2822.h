/*
 * Copyright (c) 2004	Greg A. Woods <woods@planix.com>
 */

/*
 *  shared internal definitions & declarations for the lexer and scanner
 */

#ident "@(#)librfc2822: :librfc2822.h,v 1.2 2004/08/14 02:50:51 woods Exp "

#ifndef __LIBRFC2822_H__
# define __LIBRFC2822_H__ 1

#define YYSTYPE		char *

extern YYSTYPE		rfc2822_lval;

extern YYSTYPE		rfc2822_text;

/*
 * This macro is a variation on strncpy() that makes up for its fundamental
 * design flaw -- this macro always leaves 'dest' as a properly NUL-terminated
 * string.
 *
 * Note that since not all of 'src' might be copied the only way to find out if
 * the buffer is "full" afterwards is to count its characters with strlen().
 *
 * Idea and name for this macro borrowed from Mutt.
 */
#define STRFCPY(dest, src, len)						\
		{							\
			if (len) {					\
				strncpy(dest, src, len);		\
				dest[len - 1] = '\0';			\
			}						\
		}

/*
 * Declarations for global variables from the lexer
 */
extern char		*rfc2822_yytext;
extern char		*rfc2822_lex_bufp;
extern unsigned int	rfc2822_lex_ncomm;
extern FILE		*rfc2822_in;
extern void		rfc2822_restart __P((FILE *));
extern int		rfc2822_lex __P((void));
extern void		rfc2822_lex_restart __P((void));

/*
 * Declarations for other internal routines
 */
extern char            *safe_strdup __P((const char *));

#endif /* __LIBRFC2822_h__ */
