/* @(#) striphdrs.c,v 1.3 2000/09/13 21:55:25 lyndon Exp */

/*
 * striphdrs - clean up headers in mail destined for a mailing list.
 *
 * Removes nuisance headers from messages destined for a mailing list.
 * It also adds a "Precedence: bulk" header to the message.
 *
 * I usually invoke this from the smail alias file as follows:
 *
 * foo:	"|/usr/local/smail/striphdrs|/usr/local/bin/smail -oi -q -f foo-request foo-redist"
 *
 * Written January 1991 (or there abouts) by Lyndon Nerenberg.
 * Updated for ANSI C and MDNs September 2000.
 *
 * This program is in the public domain.
 *
 * --lyndon@orthanc.ab.ca
 */

#ifndef lint
static char RCSid[] = "@(#)contrib/striphdrs: striphdrs.c,v 1.3 2000/09/13 21:55:25 lyndon Exp";
#endif /* ! lint */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define INBUFSIZE 4096		/* Size of input buffer. Lines longer than */
				/* this will be truncated. */
#define	TRUE	1
#define FALSE	0

static char *hdr_del[] = {	/* NULL terminated list of hdrs to delete */
  "Disposition-Notification-Options:",
  "Disposition-Notification-To:",
  "Errors-To:",
  "Original-Recipient:",
  "Precedence:",
  "Received:",
  "Return-Path:",
  "Return-Receipt-To:",
  "Sender:",
  NULL };

int
main(argc, argv)
  int argc; char *argv[];
{
  
  register char *inbuf;		/* Input buffer */
  register char **c;		/* Temporary pointer */
  int  in_headers = TRUE;	/* Set to 0 when last header encountered */
  int  deleting = FALSE;	/* Set to 1 if actively deleting header */
  
  inbuf = (char *) malloc((unsigned) INBUFSIZE);
  if (inbuf == NULL) {
    (void) fprintf(stderr, "%s: malloc(INBUFSIZE) failed!\n", argv[0]);
    exit(1);
  }
  
  while ((fgets(inbuf, INBUFSIZE, stdin)) != NULL) {
    
    if (in_headers) {
      
      if (*inbuf == '\n') {
	in_headers = FALSE;	/* Header/body seperator found */
	(void) fputs("Precedence: bulk\n\n", stdout);
	continue;
      }

      if (deleting && ((*inbuf == ' ') || (*inbuf == '\t')))
	continue;		/* Skip any continuation lines */
      else
	deleting = FALSE;
      
      /* See if this is a bogus header */
      for (c = hdr_del; *c != NULL; c++)
	if (strncasecmp(inbuf, *c, strlen(*c)) == 0)
	  deleting = TRUE;

      if (!deleting)
	(void) fputs(inbuf, stdout);
    }
    else
      (void) fputs(inbuf, stdout);
  }
  exit(0);
/*NOTREACHED*/
}
