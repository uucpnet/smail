# @(#) directors,v 1.2 1990/10/24 05:17:14 tron Exp

# This file defines the configuration of the director subsystem as
# compiled into the smail binary.  By modifying the source file config.h
# the actual internal configuration can be changed.  Thus, this should
# be matched up against config.h and default.c before assuming this is
# completely correct.

# aliasinclude - expand ":include:filename" addresses produced by alias files
aliasinclude:
	driver = aliasinclude,		# use this special-case driver
	nobody;				# associate nobody user with addresses
					#  when mild permission violations
					#  are encountered

	copysecure,			# get permissions from alias director
	copyowners			# get owners from alias director

# forwardinclude - expand ":include:filename" addrs produced by forward files
forwardinclude:
	driver = forwardinclude,	# use this special-case driver
	nobody;

	copysecure,			# get perms from forwarding director
	copyowners			# get owners from forwarding director

# aliases - search for alias expansions stored in a database
aliases:
	driver = aliasfile,		# general-purpose aliasing director
	-nobody,			# all addresses are associated
					# with nobody by default, so setting
					# this is not useful.
	owner = owner-$user;		# problems go to an owner address

	file = aliases,
#	file = mail.aliases,		# use this for YP
	modemask = 002,
	optional,			# ignore if file does not exist
	proto = lsearch 
#	proto = aliasyp			# use this for YP

# dotforward - expand .forward files in user home directories
dotforward:
	driver = forwardfile,		# general-purpose forwarding director
	owner = real-$user,		# problems go to the user's mailbox
	nobody,
	sender_okay;			# sender never removed from expansion

	file = ~/.forward,		# .forward file in home directories
	checkowner,			# the user can own this file
	owners = root,			# or root can own the file
	modemask = 002,			# it should not be globally writable
	caution = daemon:root,		# don't run things as root or daemon
	# be extra carefull of remotely accessible home directories
	unsecure = "~ftp:~uucp:~nuucp:/tmp:/usr/tmp"

# forwardto - expand a "Forward to " in user mailbox files
#
# This emulates the V6/V7/System-V forwarding mechanism which uses a
# line of forward addresses stored at the beginning of user mailbox files
# prefixed with the string "Forward to "
forwardto:
	driver = forwardfile,
	owner = real-$user, nobody, sender_okay;

	file = /usr/spool/mail/${lc:user},	# point at user mailbox files
#	file = /usr/mail/${lc:user},	# the mailbox file for System V
	forwardto,			# enable "Forward to " function
	checkowner,			# the user can own this file
	owners = root,			# or root can own the file
	modemask = 0022,		# only owner should be able to write
#	modemask = 0002,		# under System V, group mail can write
	caution = daemon:root		# don't run things as root or daemon

# akcs - match AKCS V6 users
#
# This is the entry which remaps addresses of the form "akcs.xxxxx" to the
# form user "xxxxx" in the AKCS system.  It also validates the user against
# the AKCS user database.  This is a passive director entry in that any
# address that this director cannot resolve is passed onward.
#
# WARNING:  If this director entry is used smart-user is dangerous -- it
#	    will cause all mismatched entries (ie: no local AKCS user) to be
#	    bounced to another site for re-inspection!  Use care if both are
#	    active to prevent mailer frenzy.
#
akcs:
	driver = akcs;			# Special AKCS driver is used

	transport = akcs,		# Append to AKCS files 
	prefix = "akcs."		# for example, match akcs.test


# user - match users on the local host with delivery to their mailboxes
user:	driver = user;			# driver to match usernames

	transport = local		# local transport goes to mailboxes

# real_user - match usernames when prefixed with the string "real-"
#
# This is useful for allowing an address which explicitly delivers to a
# user's mailbox file.  For example, errors in a .forward file expansion
# can be delivered here, or forwarding loops between multiple machines
# can be resolved by using a real-username address.

real_user:
	driver = user;

	transport = local,
	prefix = "real-"		# for example, match real-root

# lists - expand mailing lists stored in a list directory
#
# mailing lists can be created simply by creating a file in the
# /usr/lib/smail/lists directory.
lists:	driver = forwardfile,
	caution,			# flag all addresses with caution
	nobody,				# and then associate the nobody user
	owner = owner-$user;		# system V sites may wish to use
					# o-$user, as owner-$user may be
					# too long for a 14-char filename.

	# map the name of the mailing list to lower case
	file = lists/${lc:user}

# smart_user - a partially specified smartuser director
#
# If the config file attribute smart_user is defined as a string such as
# "$user@domain-gateway" then users not matched otherwise will be sent
# off to the host "domain-gateway".
#
# If the smart_user attribute is not defined, this director is ignored.
smart_user:
	driver = smartuser;		# special-case driver

	# do not match addresses which cannot be made into valid
	# RFC822 local addresses without the use of double quotes.
	well_formed_only
