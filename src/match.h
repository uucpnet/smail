/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:match.h,v 1.10 2005/06/23 21:01:55 woods Exp"
 */

/*
 *    Copyright (C) 2004  Greg A. Woods
 *
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 *
 * Some or all of this code may also be released under a separate copyright
 * license.  Contact the author for details.
 */

/*
 * match.h:
 *	Interface file for match.c
 */

typedef struct pcre_info {
    char *re;			/* the RE, first to fool print_voidplist() */
    char *msg;			/* message text */
    char *opts;			/* RE options letters */
    char delim_c;		/* RE delimiter character */
    int matchresult;		/* return value if successful match */
    int flags;			/* translated option letters, passed to pcre_compile() */
    pcre *code;			/* pcre_compile() results */
    pcre_extra *hints;		/* pcre_study() results */
    int ovecsize;		/* # of int's for table of 3 int's per capture */
} pcre_info_t;

/* macros used in communicating with functions in match.c */
#define MATCH_FAIL	(-1)		/* unrecoverable failure */
#define MATCH_NOMATCH	0		/* no match was found */
#define MATCH_MATCHED	1		/* operation was successful */

/*
 * match_hostname - match a hostname against a list of REs
 */
#define match_hostname(hostname, patterns, reasonp)	match_re_list(hostname, patterns, TRUE, reasonp)

/* external functions defined in match.c */
extern int match_re __P((char *, char *, int, char **, char **, char **, char **));
extern int match_re_list __P((char *, char *, int, char **));
extern void free_pcre_info __P((void *));
extern int match_pcre_list __P((char *, size_t, voidplist_t *, int, char **));
extern unsigned int match_ip __P((char *, char *, int, int, int, char **));
extern int match_ip_net __P((char *, char *, int));
extern int match_dnsbl __P((char *, char *, char *, char **, char **, char **));
extern char *flip_inet_addr __P((char *));
extern voidplist_t *compile_pcre_list __P((char *, char **));
extern void free_pcre_list __P((voidplist_t *re));
extern char *format_pcre_list __P((voidplist_t *re));
extern int trusted_invoker __P((void));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
