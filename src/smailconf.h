/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:smailconf.h,v 1.24 2005/07/06 23:47:57 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 by Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 *
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */
/*
 * smailconf.h:
 *	interface file for routines in smailconf.c.
 */

/* types used in smailconf.c */

enum attribute_type_e {
    t_string,				/* a string attribute */
    t_boolean,				/* boolean, on or off, attribute */
    t_char,				/* single character attribute */
    t_int,				/* integer attribute */
    t_mode,				/* file (permissions) mode - octal int */
    t_long,				/* long integer attribute */
    t_interval,				/* time interval (hour/min/sec) */
    t_double,				/* double precision attribute */
    t_proc,				/* attribute handled by procedure */
    t_infoproc				/* procedure which displays info */
};
typedef enum attribute_type_e attrtype_t;

union u_attr {
    char *v_string;			/* string variable */
    smail_bool_t v_boolean;		/* boolean variable */
    int v_char;				/* char variable, accessed as int */
    int v_int;				/* integer variable */
    long v_long;			/* long variable */
    time_t v_interval;			/* time interval variable */
    double v_double;			/* double variable */
    char *(*v_proc) __P((void));	/* procedure to handle attribute */
    char *(*v_infoproc) __P((void));	/* procedure for info attribute */
};
typedef union u_attr u_attr_t;

/*
 * attribute tables are arrays of these structures.
 */
struct attr_table {
    char *name;				/* name of attribute */
    attrtype_t type;			/* type of attribute */
    unsigned int fmt_hints;		/* flags for formatting hints */
    char *value;			/* un-converted value from config file */
    u_attr_t *uptr;			/* point to configuration variable */
    unsigned long offset;		/* offset into data structure,
					 * OR bit value for t_boolean */
};

/* convenience typedef for use in initializing conf_form tables */
typedef union u_attr tup_t;

/*
 * Optional config
 * files which do not exist have a 0 stored in the mtime field.
 */
/* FIXME: maybe move this declaration to smailconf.h */
struct config_stat {
    struct config_stat *succ;
    char    *fn;			/* name of config file */
    time_t  mtime;			/* time of last mod */
    dev_t   dev;			/* device file resides on */
    ino_t   ino;			/* inode number for file */
};

/* Flag values used for fmt_hints */
#define FA_FMT_IS_LIST		0x0001	/* show as list (escaped newline before colons) */
#define FA_FMT_IS_PCRE_LIST	0x0002	/* show as RE list (escaped newline before entries) */
#define FA_FMT_DEPRECATED	0x0004	/* name is deprecated */

/* Flag values returned by format_attribute() */
#define FA_USE_PREFIX	0x0001		/* show prefix */
#define FA_USE_VALUE	0x0002		/* show value */
#define FA_IS_COMMENT	0x0004		/* show result as a comment */

/* external functions defined in smailconf.c */
extern char *read_config_file __P((char *));
extern int format_attribute __P((struct attr_table *, char *, int, char **, char **, char *, smail_bool_t));
extern void print_config_variable __P((char *));
extern char *read_standard_file __P((FILE *, char *, char *, size_t, int, int, int, struct attr_table *, struct attr_table *, char *(*)(char *, struct attribute *), char **));
extern void dump_standard_config __P((FILE *, char *, char *, smail_bool_t, struct attr_table *, struct attr_table *));
extern char *fill_attributes __P((char *, struct attribute *, smail_bool_t *, struct attr_table *, struct attr_table *, char *));
extern struct attr_table *find_config_attribute __P((char *));
extern void add_config_stat __P((char *, struct stat *));
extern int is_newconf __P((void));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
