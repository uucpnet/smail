/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:config.c,v 1.80 2005/07/25 05:10:53 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * config.c:
 *	global variables which depend upon configuration
 *
 *	most of these values can be changed by the config file.
 *
 *	no external functions.
 */

#include "defs.h"

#include <sys/types.h>
#include <stdio.h>

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#include "smail.h"
#include "alloc.h"
#include "list.h"
#include "main.h"
#include "addr.h"
#include "smailstring.h"
#include "dys.h"
#include "config.h"
#include "extern.h"

smail_bool_t allow_one_mx_target_cname_hack = ALLOW_ONE_MX_TARGET_CNAME_HACK; /* relax DNS MX checks a bit */
char *listen_name = NULL; 		/* DNS name of interface to listen on */
char *sending_name = NULL; 		/* DNS name of interface to send mail from */
char *visible_name = VISIBLE_NAME;	/* hostname used in outgoing addrs */
char *visible_domains = VISIBLE_DOMAINS; /* domains this host is in */
char *uucp_name = UUCP_NAME;		/* hostname used in !-routes */
char *hostnames = HOSTNAMES;		/* list of other local host names */
char *more_hostnames = MORE_HOSTNAMES;	/* additional list of hostnames */
long max_message_size = MAX_MESSAGE_SIZE; /* max size of message body */
char *grades = GRADES;			/* mapping of precedence to grade */
smail_bool_t ignore_user_case = IGNORE_USER_CASE; /* TRUE to do caseless username lookups */
uid_t nobody_uid = NOBODY_UID;		/* user id for access permission */
gid_t nobody_gid = NOBODY_GID;		/* group id for access permission */
char *nobody = SMAIL_NOBODY;		/* name of nobody user */
char *log_fn = LOGFILE;			/* name of information log file */
char *panic_fn = PANIC_LOG;		/* name of panic log file */
char *cons_fn = CONSOLE;		/* name of console device file */
char *spool_dirs = SPOOL_DIRS;		/* names of spooling directories */
int spool_mode = SPOOL_MODE;		/* mode for spool files */
int lock_mode = LOCK_MODE;		/* mode for lock files */
unsigned int log_mode = LOG_MODE;	/* mode for system log files */
unsigned int message_log_mode = MESSAGE_LOG_MODE; /* mode for per-message log files */
int spool_grade = SPOOL_GRADE;		/* default spool grade character */
int open_retries = OPEN_RETRIES;	/* max open retries on config files */
int open_interval = OPEN_INTERVAL;	/* sleep between open retries */
int min_runq_grade;			/* minimum grade to process in queue run */
int max_runq_grade;			/* maximum grade to process in queue run */
int min_delivery_grade;			/* minimum grade to deliver */
int max_delivery_grade;			/* maximum grade to deliver */
char *config_file = CONFIG_FILE;	/* config file name */
char *director_file = DIRECTOR_FILE;	/* directors file name */
char *router_file = ROUTER_FILE;	/* routers file name */
char *method_dir = METHOD_DIR;		/* directory for non-/ method files */
char *transport_file = TRANSPORT_FILE;	/* transports file name */
char *qualify_file = QUALIFY_FILE;	/* domain qualification file name */
char *retry_file = RETRY_FILE;		/* address retry control file name */
char *smail_lib_dir = SMAIL_LIB_DIR;	/* default config file directory */
char *smail_util_dir = SMAIL_UTIL_DIR;	/* default smail utility directory */
char *received_field = RECEIVED_FIELD;	/* Received: field string */
char *message_id_field = MESSAGE_ID_FIELD; /* Message-Id: field string */
char *date_field = DATE_FIELD;		/* Date: field string */
char *from_field = FROM_FIELD;		/* From: field string */
char *return_path_field = NULL;		/* DEPRECATED and unused */
char *smail = SMAIL;			/* location of the smail program */
char *daemon_pidfile = DAEMON_PIDFILE;	/* location of the PID file */
double max_load_ave = MAX_LOAD_AVE;	/* spool mail > this load agerage */
char *trusted_users = TRUSTED_USERS;	/* : list of trusted users */
char *trusted_groups = TRUSTED_GROUPS;	/* : list of trusted groups */
size_t message_buf_size = MESSAGE_BUF_SIZE; /* size of message buffers */
unsigned int hit_table_len = HIT_TABLE_LEN; /* #entries in address hit table */
smail_bool_t flock_mailbox = FLOCK_MAILBOX;	/* TRUE to use lock_fd_wait() macro */
int fnlock_retries = FNLOCK_RETRIES;	/* retries for lock_file() creat */
unsigned int fnlock_interval = FNLOCK_INTERVAL;	/* retry intervals for lock_file() */
int fnlock_mode = FNLOCK_MODE;		/* mode for lock_file() lockfiles */
smail_bool_t lock_by_name = LOCK_BY_NAME; /* TRUE to use spool lockfiles */
smail_bool_t queue_only = FALSE;	/* TRUE to default to -Q flag */
int max_hop_count = MAX_HOP_COUNT;	/* fail if hop_count exceeds this */
char *delivery_mode_string = DELIVERY_MODE; /* string naming delivery mode */
char *delivery_grades = DELIVERY_GRADES;/* the msg grade range to be delivered */
char *runq_grades = RUNQ_GRADES;	/* the msg grade range in runq */
char *smart_user = CONFIG_SMART_USER;	/* default user for smartuser */
char *smart_path = CONFIG_SMART_PATH;	/* default path for smarthost */
char *smart_transport = CONFIG_SMART_TRANSPORT;	/* transport for smarthost */
char *second_config_file = SECOND_CONFIG_FILE; /* secondary config file */
char *copying_file = COPYING_FILE;	/* pathname to COPYING file */
smail_bool_t auto_mkdir = AUTO_MKDIR;	/* TRUE to auto create directories */
unsigned int auto_mkdir_mode = AUTO_MKDIR_MODE;	/* the mode for auto directories */
smail_bool_t require_configs = REQUIRE_CONFIGS;	/* TRUE to require config files */
char *postmaster_address = POSTMASTER_ADDRESS; /* default addr of postmaster */
char *bypass_content_filtering_recipients = BYPASS_CONTENT_FILTERING_RECIPIENTS; /* bypass *_checks if a recipient is in this list */
char *body_checks = BODY_CHECKS;	/* list of EREs to reject if matched in body */
voidplist_t *body_checks_list = NULL; 	/* compiled body_checks */
char *body_checks_always = BODY_CHECKS_ALWAYS; /* list of EREs to reject if matched in body with no bypass */
voidplist_t *body_checks_always_list = NULL; 	/* compiled body_checks_always */
char *header_checks = HEADER_CHECKS;	/* list of EREs to reject if matched in headers */
voidplist_t *header_checks_list = NULL;	/* compiled header_checks */
char *header_checks_always = HEADER_CHECKS_ALWAYS; /* list of EREs to reject if matched in headers with no bypass */
voidplist_t *header_checks_always_list = NULL;	/* compiled header_checks_always */
int runq_max = RUNQ_MAX;		/* max simultaneous runqs to start */
int smtp_accept_max = SMTP_ACCEPT_MAX;	/* max simultaneous SMTPs to accept */
int smtp_accept_queue = SMTP_ACCEPT_QUEUE; /* simultaneous SMTPs to queueonly */
smail_bool_t smtp_allow_debug = SMTP_ALLOW_DEBUG; /* TRUE to allow DEBUG command in SMTP */
smail_bool_t smtp_allow_expn = SMTP_ALLOW_EXPN;	/* TRUE to allow EXPN commanda in SMTP */
char *smtp_bad_mx_except = SMTP_BAD_MX_EXCEPT; /* hostname list of bad MX exceptions */
char *smtp_bad_mx_targets = SMTP_BAD_MX_TARGETS; /* ip list of undesirable MX targets */
char *smtp_banner = SMTP_BANNER;	/* smtp startup banner message */
unsigned int smtp_debug_pause = 0;	/* seconds to pause after child fork for debugger connect */
unsigned int smtp_greeting_delay = 1;	/* seconds to pause before sending $smtp_banner */
smail_bool_t smtp_hello_verify = SMTP_HELLO_VERIFY; /* verify HELO/EHLO host has A RR matching sockaddr */
char *smtp_hello_broken_allow = SMTP_HELLO_BROKEN_ALLOW; /* ip list that can avoid hello verification */
char *smtp_hello_dnsbl_domains = SMTP_HELLO_DNSBL_DOMAINS; /* list of DNSBL domains to check sender_host */
char *smtp_hello_dnsbl_except = SMTP_HELLO_DNSBL_EXCEPT; /* list of exceptions for smtp_hello_dnsbl_domains */
smail_bool_t smtp_hello_reject_broken_ptr = SMTP_HELLO_REJECT_BROKEN_PTR; /* like TCP Wrappers PARANOID check */
char *smtp_hello_reject_hostnames = SMTP_HELLO_REJECT_HOSTNAMES; /* list of hostnames to match sender_host */
smail_bool_t smtp_hello_verify_literal = SMTP_HELLO_VERIFY_LITERAL; /* TRUE to verify HELO/EHLO domain literal has PTR RR */
smail_bool_t smtp_hello_verify_ptr = SMTP_HELLO_VERIFY_PTR; /* TRUE to verify HELO/EHLO matches PTR RR for sockaddr */
char *smtp_host_dnsbl_domains = SMTP_HOST_DNSBL_DOMAINS; /* list of DNSBL domains to check sender_host_really */
char *smtp_host_dnsbl_except = SMTP_HOST_DNSBL_EXCEPT; /* list of exceptions for smtp_host_dnsbl_domains */
char *smtp_host_reject_hostnames = SMTP_HOST_REJECT_HOSTNAMES; /* list of hostname REs to match sender_host_really */
char *smtp_local_sender_allow = SMTP_LOCAL_SENDER_ALLOW;/* locally deliverable mailboxes which might come back to us */
smail_bool_t smtp_local_sender_restrict = SMTP_LOCAL_SENDER_RESTRICT; /* TRUE to verify locally deliverable sender clients match smtp_remote_allow */
unsigned int smtp_max_recipients = SMTP_MAX_RECIPIENTS; /* maximum recipients per SMTP connect */
unsigned int smtp_max_bounce_recipients = SMTP_MAX_BOUNCE_RECIPIENTS; /* maximum recipients per bounce message */
char *smtp_mua_only_hosts = SMTP_MUA_ONLY_HOSTS; /* list of hosts which are not allowed to send bounces */
char *smtp_permit_mx_backup = SMTP_PERMIT_MX_BACKUP; /* list of domains we will allow for secondary MX */
char *smtp_rbl_domains = SMTP_RBL_DOMAINS; /* list of RBL domains */
char *smtp_rbl_except = SMTP_RBL_EXCEPT; /* list of exceptions to RBL lookups */
char *smtp_recipient_no_verify = SMTP_RECIPIENT_NO_VERIFY; /* ip list for not verifying RCPT TO: */
char *smtp_reject_hosts = SMTP_REJECT_HOSTS; /* list of IP/Nets to match client address */
char *smtp_remote_allow = SMTP_REMOTE_ALLOW; /* ip list that can send remote smtp */
char *smtp_sender_no_verify = SMTP_SENDER_NO_VERIFY; /* ip list for not verifying MAIL FROM: */
char *smtp_sender_reject = SMTP_SENDER_REJECT; /* list of REs to match whole sender addresses */
char *smtp_sender_reject_db = SMTP_SENDER_REJECT_DB; /* database to match whole sender addresses */
char *smtp_sender_reject_db_proto = SMTP_SENDER_REJECT_DB_PROTO; /* smtp_sender_reject_db access protocol */
char *smtp_sender_reject_hostnames = SMTP_SENDER_REJECT_HOSTNAMES; /* list of hostname REs to match sender addresses */
char *smtp_sender_rhsbl_domains = SMTP_SENDER_RHSBL_DOMAINS; /* list of RHSBL domains for sender addresses */
char *smtp_sender_rhsbl_except = SMTP_SENDER_RHSBL_EXCEPT; /* list of exceptions to sender RHSBL lookups */
smail_bool_t smtp_sender_verify_mx_only = SMTP_SENDER_VERIFY_MX_ONLY; /* TRUE to require MX RRs for MAIL FROM: hosts */
unsigned int smtp_expn_delay = SMTP_EXPN_DELAY;	/* seconds to delay after an EXPN command */
unsigned int smtp_vrfy_delay = SMTP_VRFY_DELAY;	/* seconds to delay after a VRFY command */
unsigned int smtp_invalid_recipient_error_delay = SMTP_INVALID_RECIPIENT_ERROR_DELAY; /* seconds to delay after RCPT results in ERR_100 */
unsigned int smtp_error_delay = SMTP_ERROR_DELAY; /* seconds to delay after any other SMTP error */
char *sender_env_variable = SENDER_ENV_VARIABLE; /* env variable naming user */
smail_bool_t switch_percent_and_bang = SWITCH_PERCENT_AND_BANG; /* TRUE to switch precedence of % and ! */
smail_bool_t error_copy_postmaster = ERROR_COPY_POSTMASTER; /* TRUE to copy postmaster on errors */
long retry_interval = RETRY_INTERVAL;	/* default delivery retry interval */
long retry_duration = RETRY_DURATION;	/* default delivery retry duration */
long host_lock_timeout = HOST_LOCK_TIMEOUT; /* timeout for host lock */
long smtp_receive_command_timeout = SMTP_RECEIVE_COMMAND_TIMEOUT; /* timeout for smtp command reads */
long smtp_receive_message_timeout = SMTP_RECEIVE_MESSAGE_TIMEOUT; /* timeout for smtp message */
char *auth_domains = AUTH_DOMAINS;	/* authoritative domain list */
long rfc1413_query_timeout = -1;	/* Timeout on RFC1413 queries - initially disabled */
time_t resolve_timeout = RESOLVE_TIMEOUT; /* timeout on directors/routers */

#ifdef USE_CYRUS_IMAPD
char *cyrus_deliver_path = CYRUS_DELIVER_PATH; /* path to Cyrus 'deliver' program */
char *cyrus_group = CYRUS_GROUP;	/* The username of the Cyrus group */
char *cyrus_noquota_passphrase = CYRUS_NOQUOTA_PASSPHRASE; /* secret to bypass quotas */
char *cyrus_user = CYRUS_USER;		/* The username of the Cyrus user */
#endif

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
