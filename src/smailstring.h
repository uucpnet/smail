/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:smailstring.h,v 1.15 2005/11/14 01:28:14 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smailstring.h:
 */

/* XXX all the contents of dys.h should probably be in here... */

/* data type for a dynamic string region */
struct str {
    char *p;				/* xmalloc'd text region */
    size_t i;				/* index into text region */
    size_t a;				/* current allocation size */
};

/* external functions defined in string.c */
extern char *str2lower __P((char *));
extern int strcmpic __P((char *, char *));
extern int strncmpic __P((char *, char *, size_t));
extern char *chop __P((char *));
extern int strip __P((char *));
extern char *strcolon __P((char *));
extern int is_string_in_list __P((char *, char *));
extern int is_number_in_list __P((unsigned int, char *));
extern char *quote __P((char *, int, int));
extern char *quote_args __P((char **));
extern void str_c_quote __P((struct str *, char *, int, int));
extern char *c_dequote __P((char *, int *));
extern void str_printf __P((struct str *, char *, ...));
extern void str_printf_va __P((struct str *, char *, va_list));
extern char *xprintf __P((char *, ...));
extern long c_atol __P((char *, char **));
extern char *base62 __P((unsigned long));
extern char *read_line __P((FILE *));
extern void str_cat __P((struct str *, const char *));
extern void str_ncat __P((struct str *, const char *, size_t));
extern time_t ivaltol __P((char *));
extern char *ltoival __P((time_t));
extern char *copy __P((char *));
extern char *rcopy __P((char *, char *));
extern char *make_lib_fn __P((char *));
extern char *make_util_fn __P((char *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
