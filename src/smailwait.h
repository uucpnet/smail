/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:smailwait.h,v 1.3 2003/12/14 22:42:39 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smailwait.h -- machinations for <wait.h>, <sys/wait.h>, et al
 */

#if !defined(POSIX_OS) && ((defined(UNIX_BSD) || defined(WAIT_USE_UNION)) && !defined(NO_WAIT_USE_UNION))
# define STATUS_TYPE	union wait
#else
# define STATUS_TYPE	int
#endif

#ifdef POSIX_OS
# define SMAIL_WAITPID(st)	waitpid(-1, st, WNOHANG)
#else
# ifdef WNOHANG
#  define SMAIL_WAITPID(st)	wait3(st, WNOHANG, (void /* struct rusage * */ *) 0)
# else
#  include "ERROR: something is wrong with the conf/os definition!"
# endif
#endif

/* XXX maybe these should each be wrapped with #ifndef ? */
#if !defined(UNIX_BSD) && !defined(WAIT_USE_UNION) && !defined(POSIX_OS)
# ifdef UNIX_AIX3				/* XXX is this true for non-rs6k? */
#  define WIFSTOPPED(w)	((w) & 0100)		/* borrowed from GNU GDB */
# else
#  define WIFSTOPPED(w)	(((w) & 0377) == 0177)
# endif
# define WIFSIGNALED(w)	(((w) & 0377) != 0177 && ((w) & ~0377) == 0)
# define WIFEXITED(w)	(((w) & 0377) == 0)

# define WEXITSTATUS(w)	(((w) >> 8) & 0377)
# define WRETCODE(w)	WEXITSTATUS(w)
# define WSTOPSIG(w)	(((w) >> 8) & 0377)
# define WTERMSIG(w)	((w) & 0x177)
#endif
#if !defined(WCOREDUMP)
# ifndef WCOREFLAG
#  define WCOREFLAG	0200
# endif
# if !defined(UNIX_BSD) && !defined(WAIT_USE_UNION) && !defined(POSIX_OS)
#  define WCOREDUMP(w)	(((w) & WCOREFLAG) != 0)	/* not required by POSIX.1 */
# else
/* there's also the bitfield variant:    (((union __wait *) &(x))->__w_coredump) */
#  define WCOREDUMP(w)  (((* (int *) (void *) &(w)) & WCOREFLAG) != 0)
# endif
#endif

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
