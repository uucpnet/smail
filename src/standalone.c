/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:standalone.c,v 1.9 2004/01/08 22:53:45 woods Exp"
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef ANSI_C
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#include "smail.h"
#include "smailsock.h"
#include "config.h"
#include "alloc.h"
#include "list.h"
#include "smailstring.h"
#include "dys.h"
#include "parse.h"
#include "addr.h"
#include "hash.h"
#include "main.h"
#include "log.h"
#include "direct.h"
#include "route.h"
#include "transport.h"
#include "child.h"
#include "exitcodes.h"
#include "smailconf.h"
#include "extern.h"
#include "debug.h"
#include "error.h"
#include "smailport.h"

/*
 * get_local_year stub for get_local_year in sysdep.c
 */
int
get_local_year()
{
      return 1915;
}

/*
 * This "stub" is actually stolen verbatim from sysdep.c....
 */
#ifdef HAVE_BSD_NETWORKING
/*
 * get_inet_addr - call inet_addr()
 *
 * believe it or not, but inet_addr() is not portable!!!!  :-(
 */
unsigned long
get_inet_addr(ipaddr)
    char *ipaddr;
{
#if defined(INET_ADDR_USE_STRUCT)
    struct in_addr s_inet;		/* internet address */
#endif
    unsigned long inet;			/* internet address */

#ifdef INET_ADDR_USE_STRUCT
    s_inet = inet_addr(ipaddr);
    inet = s_inet.s_addr;
#else
    inet = inet_addr(ipaddr);
#endif

    return inet;
}
#endif


/*
 * define panic, fatal and write_log here, rather than
 * using the external routines.  We are testing and just want
 * the information displayed, not logged.
 */
/*VARARGS2*/
#ifdef ANSI_C
void
panic(int exitcode, char *fmt, ...)
#else
void
panic(exitcode, fmt, va_alist)
    int exitcode;			/* call exit(exitcode) */
    char *fmt;				/* printf(3) format */
    va_dcl                              /* arguments for printf */
#endif
{
    static struct str sp;
    static int initialised = FALSE;
    va_list ap;

    if (!initialised) {
	STR_INIT(&sp);
	initialised = TRUE;
    } else {
	STR_CLEAR(&sp);
	STR_CHECK(&sp);
    }

#ifdef ANSI_C
    va_start(ap, fmt);
#else
    va_start(ap);
#endif
    (void) fprintf(stderr, "PANIC(%d): ", exitcode);
    str_printf_va(sp, fmt, ap);
    va_end(ap);
    (void) fprintf(stderr, "%s\n", STR(&sp));	/* fatal messages are not \n terminated */

    return_to_sender = TRUE;
    exit(exitcode);
    /* NOTREACHED */
}

/*VARARGS2*/
#ifdef ANSI_C
void
write_log(int log, char *fmt, ...)
#else
void
write_log(log, fmt, va_alist)
    int log;				/* TRUE if to write global log file */
    char *fmt;				/* printf(3) format */
    va_dcl                              /* arguments for printf */
#endif
{
    static struct str sp;
    static int initialised = FALSE;
    va_list ap;

    if (!initialised) {
	STR_INIT(&sp);
	initialised = TRUE;
    } else {
	STR_CLEAR(&sp);
	STR_CHECK(&sp);
    }

#ifdef ANSI_C
    va_start(ap, fmt);
#else
    va_start(ap);
#endif
    str_printf_va(sp, fmt, ap);
    va_end(ap);
    (void) fprintf(stderr, "%s\n", STR(&sp));	/* log messages are not \n terminated */

    return;
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
