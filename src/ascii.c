/*
#ident "@(#)smail/src:RELEASE-3_2_0_121:ascii.c,v 1.8 2003/12/14 22:42:42 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * ascii.c: deprecated -- use <ctype.h>
 */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
