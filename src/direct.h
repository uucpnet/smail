/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:direct.h,v 1.12 2003/12/14 22:42:41 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

#ifndef DIRECT_H
#define DIRECT_H

/*
 * direct.h:
 *	interface file for direct.c.  Also, types and macros for
 *	use by director drivers.
 */

/* structure of a director, as read from the director file */
struct director {
    char *name;				/* name of director */
    char *driver;			/* name of driver */
    struct director *succ;		/* next director in the list */
    unsigned long flags;		/* boolean flag values */
    char *owner;			/* string to expand into alias owner */
    char *default_user;			/* default user name for permissions */
    char *default_group;		/* default group name */
    char *default_home;			/* default home directory */
    char *domains;                      /* domains for this director */
    char *set_user;			/* set user for permissions */
    char *set_group;			/* set group for permissions */
    char *set_home;			/* set home directory */
    char *private;			/* private data storage */
    uid_t default_uid;			/* cache - default uid for perms */
    gid_t default_gid;			/* cache - default gid for perms */
    char *x_default_home;		/* cache - expanded default home */
    uid_t set_uid;			/* cache - set uid for perms */
    gid_t set_gid;			/* cache - set gid for perms */
    char *x_set_home;			/* cache - expanded set home */
};

/* compiled in director drivers */
struct direct_driver {
    char *name;					/* name of director driver */
    void (*cache) __P((struct director *));	/* function to cache director info */
    struct addr *(*driver) __P((struct director *,
				struct addr *,
				struct addr **,
				struct addr **,
				struct addr **,
				struct addr **)); /* function to perform directing */
    void (*finish) __P((struct director *));	/* function to free resources */
    char *(*builder) __P((struct director *,
			  struct attribute *)); /* fun to read from director file */
    void (*dumper) __P((FILE *, struct director *)); /* fun to dump configuration */
};

/* flags for the director.flags field */
#define CAUTION_DIRECTOR	0x0001	/* secure source of addresses */
#define NOBODY_DIRECTOR		0x0002	/* use "nobody" if unsecure */
#define CACHED_DIRECTOR		0x0004	/* read in cache data */
#define SENDER_OKAY		0x0008	/* assumes me_too operation */
#define IGNORE_ALIAS_MATCH	0x0010	/* ignore aliases that match parent */

/* external functions defined in direct.c */
extern void direct_local_addrs __P((struct addr *, struct addr **, struct addr **, struct addr **, struct addr **));
extern void director_user_info __P((struct addr *));
extern struct director *find_director __P((char *));
extern struct direct_driver *find_direct_driver __P((char *));
extern void cache_directors __P((void));
extern void finish_directors __P((void));
extern char *read_director_file __P((void));
extern void dump_director_config __P((FILE *));

#endif	/* DIRECT_H */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
