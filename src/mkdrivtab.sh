#! /bin/sh
:
#ident	"@(#)smail/src:RELEASE-3_2_0_121:mkdrivtab.sh,v 1.25 2005/07/06 23:24:10 woods Exp"

# Script to create drivertab.[ch] and the driver directory makefiles
# from a driver configuration file.  This script makes use of the AWK
# script mkdriv.awk to actually process the contents of the
# configuration file.  The AWK script produces output tokens which
# tell this script how to operate.
#
# The format of a driver file is a collection of one line records
# describing the drivers which are to be linked to the smail binary.
# Additional source and header files can be specified for use in a
# particular library file using the special driver name "library".
#
# Drivers come in four types:  director, router, transport, and lookup
# drivers (note "lookup" drivers are not yet used or supported).
#
# A line describing a driver is of the following form:
#
#	type name [modifiers ...]
#
# where type is one of "director", "router", "transport", or "lookup";
# and where name is the symbolic name for the driver, for reference
# from run-time configuration files.
#
# There can be modifiers applied to an entry.  These modifiers can be
# one of the following:
#
#	source=name.c	This modifier declares that the source for the
#			driver is in the file "name.c" in the source
#			directory for the specific driver type.  The
#			default source file name is the symbolic name
#			of the driver with a suffix of ".c".
#
#	header=name	This modifier declares that the header file
#			associated with the driver is in the file
#			"name" in the source directory for the
#			specified driver type.  By default lookup
#			drivers do not have header files, while
#			director, router and transport drivers have
#			header files with the basename of the source
#			file and a suffix of ".h".
#
#	nocache		This modifier declares that the driver does
#			not have a "cache" entry point.  This does not
#			apply to lookup drivers.
#
#	nofinish	This modifier declares that the driver does
#			not have a "finish" entry point.  This does
#			not apply to lookup drivers.
#
#	nobuilder	This modifier declares that the driver does
#			not have a "builder" entry point.  This does
#			not apply to lookup drivers.
#
# Additional source and header files for a particular driver library
# can be specified using a line of the form:
#
#	type library [name.c | name.h] ...
#
# where type specifies the driver library, library is the keyword
# "library", and where name.c specifies a source file in the driver
# source directory, and where name.h specifies a header file in the
# driver source directory.
#
# Blanks lines are okay, and comments can be started with a '#'
# character and continue until the end of the line.
#
# The following is a simple example of a driver file:
#
#	# director drivers
#	director  aliasfile
#	director  forwardfile nocache nofinish source=fwdfile.c
#	director  user	      nocache nofinish
#	director  library     dtlib.c dtlib.h
#
#	# router drivers
#	router    pathalias
#	router	  uuname      # the command output may be cached
#	router	  smarthost   nocache nofinish
#	router	  library     rtlib.c rtlib.h
#
#	# transport drivers
#	transport pipe	      nocache nofinish
#	transport appendfile  nocache nofinish
#	transport library     tplib.c tplib.h
#	transport library     bsmtp.c bsmtp.h

if [ $# -gt 0 ]; then
	driver_file=$1
	exec < $driver_file
else
	driver_file="<standard-input>"
fi

outfile=drivertab.c
backup=.drivertab.c
if [ -f $outfile ]; then
	echo "Building $outfile, backup copy left in $backup ..." 1>&2
	mv -f $outfile $backup
else
	echo "Building $outfile ..." 1>&2
fi
cat <<__EOF__ > $outfile
/*
 * $outfile:
 *	define the available director, router, transport and lookup
 *	drivers for use by smail.
 *
 * #     #                                                   ###
 * #  #  #    ##    #####   #    #     #    #    #   ####    ###
 * #  #  #   #  #   #    #  ##   #     #    ##   #  #    #   ###
 * #  #  #  #    #  #    #  # #  #     #    # #  #  #         #
 * #  #  #  ######  #####   #  # #     #    #  # #  #  ###
 * #  #  #  #    #  #   #   #   ##     #    #   ##  #    #   ###
 *  ## ##   #    #  #    #  #    #     #    #    #   ####    ###
 *
 * THIS FILE IS GENERATED AUTOMATICALLY BY THE SCRIPT $0 FROM
 * THE DRIVER CONFIGURATION FILE $driver_file
 *
 * YOU MUST MAKE CHANGES TO THE DRIVER CONF FILE AND REBUILD RATHER THAN
 * EDITING THIS FILE DIRECTLY.
 */

#include "defs.h"

#include <sys/types.h>
#include <stdio.h>

#include "smail.h"
#include "list.h"
#include "parse.h"
#include "addr.h"
#include "direct.h"
#include "route.h"
#include "transport.h"

__EOF__

sed -e 's/#.*//' -e '/^[ 	]*$/d' |
    ${AWK:-awk} -f mkdriv.awk |
    (
	while read tag rest; do
		case "$tag" in
		%header_start)
			#
			# get ready to build the drivertab.h file...
			#

			# first restore drivertab.c if it hasn't changed....
			#
			if [ -f $backup ] ; then
				if cmp $backup $outfile ; then
					echo "Restoring identical backup copy of $outfile." 1>&2
					mv -f $backup $outfile
				fi
			fi

			outfile=drivertab.h
			backup=.$outfile
			if [ -f $outfile ]; then
			    echo "Building $outfile, backup copy left in $backup ..." 1>&2
			    mv -f $outfile $backup
			else
			    echo "Build $outfile ..." 1>&2
			fi
			cat <<__EOF__ > $outfile
/*
 * $outfile:
 *	define the names of the available director, router, transport and
 *	lookup drivers for use by smail.
 *
 * #     #                                                   ###
 * #  #  #    ##    #####   #    #     #    #    #   ####    ###
 * #  #  #   #  #   #    #  ##   #     #    ##   #  #    #   ###
 * #  #  #  #    #  #    #  # #  #     #    # #  #  #         #
 * #  #  #  ######  #####   #  # #     #    #  # #  #  ###
 * #  #  #  #    #  #   #   #   ##     #    #   ##  #    #   ###
 *  ## ##   #    #  #    #  #    #     #    #    #   ####    ###
 *
 * THIS FILE IS GENERATED AUTOMATICALLY BY THE SCRIPT $0 FROM
 * THE DRIVER CONFIGURATION FILE $driver_file
 *
 * YOU MUST MAKE CHANGES TO THE DRIVER CONF FILE AND REBUILD RATHER THAN
 * EDITING THIS FILE DIRECTLY.
 */

__EOF__
			;;
		%header_end)
			# we're done creating the drivertab.h file
			#
			if [ -f $backup ] ; then
				if cmp $backup $outfile ; then
					echo "Restoring identical backup copy of $outfile." 1>&2
					mv -f $backup $outfile
				fi
			fi
			;;
		%makefile_start)
			# get ready to create a driver library makefile
			#
			# the library directory name is the second parameter
			#
			outfile=${rest}/Makefile
			if [ -f $outfile ]; then
				dir=`dirname $outfile`
				backup=$dir/.`basename $outfile`
				echo "Building $outfile, backup copy left in $backup ..." 1>&2
				mv -f $outfile $backup
			else
			    echo "Build $outfile ..." 1>&2
			fi
			cat <<__EOF__ > $outfile
#
#	Makefile for the $dir driver library in smail
#

#     #                                                   ###
#  #  #    ##    #####   #    #     #    #    #   ####    ###
#  #  #   #  #   #    #  ##   #     #    ##   #  #    #   ###
#  #  #  #    #  #    #  # #  #     #    # #  #  #         #
#  #  #  ######  #####   #  # #     #    #  # #  #  ###
#  #  #  #    #  #   #   #   ##     #    #   ##  #    #   ###
 ## ##   #    #  #    #  #    #     #    #    #   ####    ###

# THIS FILE IS GENERATED AUTOMATICALLY BY THE SCRIPT $0 FROM
# THE DRIVER CONFIGURATION FILE $driver_file
#
# YOU MUST MAKE CHANGES TO THE DRIVER CONF FILE AND REBUILD RATHER THAN
# EDITING THIS FILE DIRECTLY.

ROOT=../..
THIS_DIR=src/$dir

include \${ROOT}/conf/Make.local

MKDEPEND=\${ROOT}/conf/lib/mkdepend.sh
MKDEFS=\${ROOT}/conf/lib/mkdefs.sh
CHECKDEFS=\${ROOT}/conf/lib/checkdefs.sh
XEXEC=\${SHELL} \${ROOT}/conf/lib/xexec.sh
DEFS_SH=defs.sh
DEFS_H=defs.h
DEFS_SED=defs.sed

__EOF__
			;;
		%makefile_end)
			# we're nearly done creating the driver library makefile
			#
			cat <<\__EOF__ >> $outfile
SRC=${CSRC} ${HSRC}

.c.o:
	@. ./${DEFS_SH}; ${XEXEC} $$CC $$CFLAGS $$INCLUDES -c $*.c

all:	${TARGET_LIB}

lint:	${CSRC} ${DEFS_SH}
	@. ./${DEFS_SH}; ${XEXEC} ${LINT} ${LINTFLAGS} ${CSRC}

${TARGET_LIB}: ${OBJ} Makefile ${DEFS_SH}
	rm -f ${TARGET_LIB}
	${AR} cr ${TARGET_LIB} ${OBJ}
	@. ./${DEFS_SH}; ${XEXEC} $$RANLIB ${TARGET_LIB}

csrc:
	@echo ${CSRC}

hsrc:
	@echo ${HSRC}

sources: ${SRC}

mkdefs defs ${DEFS_H} ${DEFS_SH} ${DEFS_SED}: ${ROOT}/conf/EDITME
	ROOT=${ROOT} ${SHELL} ${MKDEFS}

../drivertab.h: ../mkdrivtab.sh ../mkdriv.awk ${DEFS_SH}
	cd .. && ${MAKE} drivertab.h

${OBJ}:	${DEFS_SH}

depend:	${SRC} check_defs
	@rm -f .depend
	@. ./${DEFS_SH}; ${XEXEC} ${SHELL} ${MKDEPEND} $$CPPFLAGS $$INCLUDES ${CSRC}
	. ./${DEFS_SH}; echo "$$DEFS_DEPEND" >> .depend

check_defs:
	SHELL=${SHELL} ROOT=${ROOT} ${SHELL} ${CHECKDEFS}

.PRECIOUS: ${ROOT}/conf/EDITME
${ROOT}/conf/EDITME: ${DOT_MAKE}
	cd ${ROOT}/conf && ${MAKE} EDITME

clean:
	rm -f .${DEFS_SH} .${DEFS_H} .${DEFS_SED} .Makefile
	rm -f a.out core ${OBJ}
	rm -f ${TARGET_LIB}

clobber: clean
	rm -f .depend
	rm -f ${DEFS_SH} ${DEFS_H} ${DEFS_SED}
	rm -f Makefile

.PHONY: all install local-install clean local-clean clobber local-clobber depend local-depend lint mkdefs defs check_defs tags TAGS
.PHONY: csrc hsrc sources
__EOF__
			if [ -f $backup ] ; then
				if cmp $backup $outfile ; then
					echo "Restoring identical backup copy of $outfile." 1>&2
					mv -f $backup $outfile
				fi
			fi
			;;
		%indent)
			# this is a special hack to "preserve"
			# indentation as otherwise 'read' will just
			# skip it all...
			#
			echo "    $rest" >> $outfile
			;;
		*)
			# just copy the input to the output
			#
			echo "$tag $rest" >> $outfile
			;;
		esac
	done
    )

exit 0
