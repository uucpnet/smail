/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:extern.h,v 1.132 2005/07/12 18:49:33 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * extern.h:
 *	externals used by the smail program
 */
#ifdef NEED_ERRNO_DECL
extern int errno;			/* previous system error value */
#endif

/*
 * External variables used in the smail program
 */

/* external variables defined in config.c */
extern smail_bool_t allow_one_mx_target_cname_hack;	/* relax DNS MX checks a bit */
extern char *listen_name;		/* DNS name of interface to listen on */
extern char *sending_name;		/* DNS name of interface to send mail from */
extern char *visible_name;		/* hostname used in outgoing addrs */
extern char *visible_domains;		/* domains this host is in */
extern char *uucp_name;			/* hostname used in !-routes */
extern char *hostnames;			/* list of other local host names */
extern char *more_hostnames;		/* additional list of hostnames */
extern long max_message_size;		/* max size of message body */
extern char *grades;			/* mapping of precedence to grade */
extern smail_bool_t ignore_user_case;	/* TRUE to do caseless username lookups */
extern uid_t nobody_uid;		/* user id for access permission */
extern gid_t nobody_gid;		/* group id for access permission */
extern char *nobody;			/* name of nobody user */
extern char *log_fn;			/* name of information log file */
extern char *panic_fn;			/* name of panic log file */
extern char *cons_fn;			/* name of console device file */
extern char *spool_dirs;		/* names of spooling directories */
extern int spool_mode;			/* mode for spool files */
extern int lock_mode;			/* mode for lock files */
extern unsigned int log_mode;		/* mode for system log files */
extern unsigned int message_log_mode;	/* mode for per-message log files */
extern int spool_grade;			/* default spool grade character */
extern int open_retries;		/* max open retries on startup file */
extern int open_interval;		/* sleep between open retries */
extern int min_runq_grade;		/* minimum grade to process in queue run */
extern int max_runq_grade;		/* maximum grade to process in queue run */
extern int min_delivery_grade;		/* minimum grade to deliver */
extern int max_delivery_grade;		/* maximum grade to deliver */
extern char *config_file;		/* config file name */
extern char *director_file;		/* directors file name */
extern char *router_file;		/* routers file name */
extern char *method_dir;		/* directory for non-/ method files */
extern char *transport_file;		/* transports file name */
extern char *qualify_file;		/* domain qualification file name */
extern char *retry_file;		/* address retry control file name */
extern char *smail_lib_dir;		/* default config file directory */
extern char *smail_util_dir;		/* default smail utility directory */
extern char *received_field;		/* Received: field string */
extern char *message_id_field;		/* Message_Id: field string */
extern char *date_field;		/* Date: field string */
extern char *from_field;		/* From: field string */
extern char *return_path_field;		/* Return-Path: field string */
extern char *smail;			/* location of the smail program */
extern char *daemon_pidfile;		/* location of the PID file */
extern double max_load_ave;		/* spool mail > this load agerage */
extern int sender_is_trusted;		/* TRUE if sender address and Sender: are trusted */
extern char *trusted_users;		/* : list of trusted users */
extern char *trusted_groups;		/* : list of trusted groups */
extern size_t message_buf_size;		/* size of message buffers */
extern unsigned int hit_table_len;	/* #entries in address hit table */
extern smail_bool_t flock_mailbox;	/* TRUE to use lock_fd_wait() macro */
extern int fnlock_retries;		/* retries for lock_file() creat */
extern unsigned int fnlock_interval;	/* retry intervals for lock_file() */
extern int fnlock_mode;			/* mode for lock_file() lockfiles */
extern smail_bool_t lock_by_name;	/* TRUE to use spool lockfiles */
extern smail_bool_t queue_only;		/* TRUE to queue but not deliver */
extern int max_hop_count;		/* fail if hop_count exceeds this */
extern char *delivery_mode_string;	/* string naming delivery mode */
extern char *delivery_grades;		/* the msg grade range to be delivered */
extern int runq_max;			/* max simultaneous runqs to start */
extern char *runq_grades;		/* the msg grade range in runq */
extern char *smart_user;		/* default user for smartuser driver */
extern char *smart_path;		/* default path for smarthost driver */
extern char *smart_transport;		/* transport for smarthost driver */
extern char *second_config_file;	/* secondary configuration file */
extern char *copying_file;		/* pathname to COPYING file */
extern smail_bool_t auto_mkdir;		/* TRUE to auto create directories */
extern unsigned int auto_mkdir_mode;	/* the mode for auto directories */
extern smail_bool_t require_configs;	/* TRUE to require config files */
extern char *postmaster_address;	/* default addr of postmaster */
extern char *bypass_content_filtering_recipients; /* bypass *_checks if a recipient is in this list */
extern char *body_checks; 		/* list of EREs to reject if matched in body */
extern voidplist_t *body_checks_list; 	/* compiled body_checks */
extern char *body_checks_always; 	/* list of EREs to reject if matched in body with no bypass */
extern voidplist_t *body_checks_always_list; 	/* compiled body_checks_always */
extern char *header_checks; 		/* list of EREs to reject if matched in headers */
extern voidplist_t *header_checks_list;	/* compiled header_checks */
extern char *header_checks_always;	/* list of EREs to reject if matched in headers with no bypass */
extern voidplist_t *header_checks_always_list;	/* compiled header_checks_always */
extern int smtp_accept_max;		/* max simultaneous SMTPs to accept */
extern int smtp_accept_queue;		/* simultaneous SMTPs until queueonly */
extern smail_bool_t smtp_allow_debug;	/* TRUE to allow DEBUG command in SMTP */
extern smail_bool_t smtp_allow_expn;	/* TRUE to allow EXPN command in SMTP */
extern char *smtp_bad_mx_except;	/* hostname list of bad MX exceptions */
extern char *smtp_bad_mx_targets;	/* ip list of undesirable MX targets */
extern char *smtp_banner;		/* smtp startup banner message */
extern unsigned int smtp_debug_pause;	/* seconds to pause after child fork for debugger connect */
extern unsigned int smtp_greeting_delay;/* seconds to pause before sending $smtp_banner */
extern smail_bool_t smtp_hello_verify;	/* TRUE to verify HELO/EHLO host has A RR matching sockaddr */
extern char *smtp_hello_broken_allow;	/* ip list that can avoid hello verification */
extern char *smtp_hello_dnsbl_domains;	/* list of DNSBL domains to check sender_host */
extern char *smtp_hello_dnsbl_except;	/* list of exceptions for smtp_hello_dnsbl_domains */
extern smail_bool_t smtp_hello_reject_broken_ptr; /* like TCP Wrappers PARANOID check */
extern char *smtp_hello_reject_hostnames; /* list of hostnames to match sender_host */
extern smail_bool_t smtp_hello_verify_literal;	/* TRUE to verify HELO/EHLO domain literal has PTR RR */
extern smail_bool_t smtp_hello_verify_ptr;	/* TRUE to verify HELO/EHLO matches PTR RR for sockaddr */
extern char *smtp_host_dnsbl_domains;	/* list of DNSBL domains to check sender_host_really */
extern char *smtp_host_dnsbl_except;	/* list of exceptions for smtp_host_dnsbl_domains */
extern char *smtp_host_reject_hostnames; /* list of hostname REs to match sender_host_really */
extern char *smtp_local_net;		/* the local network calculated from src. addr */
extern char *smtp_local_sender_allow;	/* locally deliverable mailboxes which might come back to us */
extern smail_bool_t smtp_local_sender_restrict;	/* TRUE to verify locally deliverable sender clients match smtp_remote_allow */
extern unsigned int smtp_max_recipients; /* maximum recipients per SMTP connect */
extern unsigned int smtp_max_bounce_recipients; /* maximum recipients per bounce message */
extern char *smtp_mua_only_hosts;	/* list of hosts which are not allowed to send bounces */
extern char *smtp_permit_mx_backup;	/* list of domains we will allow for secondary MX */
extern char *smtp_rbl_domains;		/* list of RBL domains */
extern char *smtp_rbl_except;		/* list of exceptions to RBL lookups */
extern char *smtp_recipient_no_verify;	/* ip list for not verifying RCPT TO: */
extern char *smtp_reject_hosts;		/* list of IP/Nets to match client address */
extern char *smtp_remote_allow;		/* ip list that can send remote smtp */
extern char *smtp_sender_no_verify;	/* ip list for not verifying MAIL FROM: */
extern char *smtp_sender_reject;	/* list of REs to match whole sender addresses */
extern char *smtp_sender_reject_db;	/* database to match whole sender addresses */
extern char *smtp_sender_reject_db_proto; /* smtp_sender_reject_db access protocol */
extern char *smtp_sender_reject_hostnames; /* list of hostname REs to match sender addresses */
extern char *smtp_sender_rhsbl_domains;	/* list of RHSBL domains for sender addresses */
extern char *smtp_sender_rhsbl_except;	/* list of exceptions to sender RHSBL lookups */
extern smail_bool_t smtp_sender_verify_mx_only;	/* TRUE to require MX RRs for MAIL FROM: hosts */
extern unsigned int smtp_expn_delay;	/* seconds to delay after an EXPN command */
extern unsigned int smtp_vrfy_delay;	/* seconds to delay after a VRFY command */
extern unsigned int smtp_invalid_recipient_error_delay;	/* seconds to delay after RCPT results in ERR_100 */
extern unsigned int smtp_error_delay;	/* seconds to delay after any other SMTP error */
extern char *sender_env_variable;	/* env variable naming user */
extern smail_bool_t switch_percent_and_bang;	/* TRUE to switch precedence of % and ! */
extern smail_bool_t error_copy_postmaster;	/* TRUE to copy postmaster on errors */
extern long retry_interval;		/* default delivery retry interval */
extern long retry_duration;		/* default delivery retry duration */
extern long host_lock_timeout;		/* timeout for host exclusivity lock */
extern long smtp_receive_command_timeout; /* timeout for receiving SMTP cmds */
extern long smtp_receive_message_timeout; /* timeout for receiving SMTP msg */
extern char *auth_domains;		/* authoritative domain list */
extern long rfc1413_query_timeout;	/* Timeout on RFC1413 queries - initially disabled */
extern time_t resolve_timeout;		/* timeout on directors/routers */
extern int scan_frozen;			/* should mailq look in the error queue? */

#ifdef USE_CYRUS_IMAPD
extern char *cyrus_deliver_path;	/* path to Cyrus 'deliver' program */
extern char *cyrus_group;		/* The username of the Cyrus group */
extern char *cyrus_noquota_passphrase;	/* secret to bypass quotas */
extern char *cyrus_user;		/* The username of the Cyrus user */
#endif

/* external variables defined in copyright.c */
extern char copyright[];		/* the program copyright notice */

/* external variables defined in default.c */
extern struct director *directors;	/* configured directors */
extern struct router *routers;		/* configured routers */
extern struct transport *transports;	/* configured transports */
extern struct transport *builtin_transports; /* builtin transports */

/* external variables defined in direct.c */
extern int cached_directors;		/* TRUE if cache_directors() called */

/* external variables defined in drivertab.c */
#ifdef DIRECT_H
extern struct direct_driver direct_drivers[]; /* compiled director drivers */
#endif
#ifdef ROUTE_H
extern struct route_driver route_drivers[]; /* compiled router drivers */
#endif
#ifdef TRANSPORT_H
extern struct transport_driver transport_drivers[]; /* transport drivers */
#endif

/* external variables defined in ldinfo.c */
extern int compile_num;
extern char *compile_date;

/* external variables defined in log.c */
extern FILE *panicfile;			/* open stream to panic log file */
extern FILE *logfile;			/* open stream to system log file */
extern FILE *msg_logfile;		/* open stream to per-message log */

/* external variables defined in modes.c */
extern pid_t daemon_pid;		/* pid of daemon process */
extern int mailq_summary_only;		/* does mailq print only the summary tally? */

/* external variables defined in notify.c */
extern int send_to_postmaster;		/* set TRUE to mail to postmaster */
extern int return_to_sender;		/* set TRUE to mail log to sender */

/* external variables defined in parse.c */
extern char *off;			/* boolean off attribute value */
extern char *on;			/* boolean on attribute value */

/* external variables defined in queue.c */
extern int msg_grade;			/* grade level for this message */
extern unsigned long msg_body_size;	/* size of message body */
extern off_t start_msg_headers;		/* where the message headers begin */

/* external variables defined in resolve.c */
extern struct hash_table *hit_table;	/* table to recognize address hits */
extern struct block *hit_table_block;

/* external variables defined in route.c */
extern int cached_routers;		/* TRUE if cache_routers() called */

/* external variables defined in spool.c */
extern char *message_id;		/* unique string ID for message */
extern char *spool_dir;			/* directory used to spool message */
extern char *spool_fn;			/* basename of spool file */
extern char *input_spool_fn;		/* name in input/ directory */
extern int spoolfile;			/* open spool file */
extern char *lock_fn;			/* name of lock file for spool_fn */
extern char *msg_buf;			/* i/o buffer for spool file */
extern char *msg_ptr;			/* read placeholder in msg_buf */
extern char *msg_max;			/* last valid char in msg_buf */
extern char *end_msg_buf;		/* end of msg_buf */
extern off_t msg_foffset;		/* file offset for msg_buf contents */
extern off_t msg_size;			/* size of spool file */
extern time_t msg_mtime;		/* last modified time of spool file */

/* external variables defined in version.c */
extern char *version_number;		/* string defining version number */
extern char *release_date;		/* the date for this release */
extern char *patch_number;		/* most recent patch number */
extern char *patch_date;		/* patch date */
extern char *bat;			/* the proper bat for this release */

/* external variables defined in transports.c */
extern char *path_to_sender;		/* uucp-style route to sender */
extern int cached_transports;		/* TRUE if cache_transports() called */

/* external variables defined in smtprecv.c */
extern char *ident_sender;		/* The calculated identity of the sender */
extern char *ident_method;		/* Method used to get identity */
extern char *sender_host_invalid;	/* short error text if sender_host is not actually valid */
extern char *sender_host_really;	/* result of PTR lookup */
extern char *sender_host_really_invalid;/* short error text if sender_host_really is not actually valid */
extern char *smtp_local_addr;		/* ascii representation of addr from getsockname() */
extern char *smtp_local_port;		/* ascii representation of port from getsockname() */
extern int peer_is_localhost;		/* endpoint addresses are the same? */
extern char *smtp_local_net;		/* classic (non-CIDR) guess of the local network */
extern off_t accepted_msg_size;		/* size we're currently willing to accept */


/*
 * External functions used in the smail program
 */

/* external functions defined in silly.c */
extern void silly __P((void));			/* a very silly function */

/* external functions defined in expand.c */
extern char *expand_string __P((char *, struct addr *, char *, char *));
extern char **build_cmd_line __P((char *, struct addr *, char *, char **));

/* external functions defined in header.c */
extern char *process_header __P((struct addr **));
extern int read_header __P((void));
extern int write_header __P((FILE *, struct addr *));
extern int parse_precedence __P((char *));

/* external functions defined in modes.c */
extern void build_host_strings __P((void));
extern void compute_nobody __P((void));
extern void input_signals __P((void));
extern void processing_signals __P((void));
extern void delivery_signals __P((void));
extern void test_addresses __P((void));
extern void test_dnsbls __P((void));
extern void perform_deliver_mail __P((void));
extern void deliver_mail __P((void));
extern void daemon_mode __P((void));
extern void noop_mode __P((void));
extern void verify_addresses __P((void));
extern void print_version __P((void));
extern void print_copying_file __P((void));
extern void print_variables __P((void));
extern void print_queue __P((void));
extern void smtp_mode __P((FILE *, FILE *, void *));
extern int process_spool_file __P((char *));
extern int fork_wait __P((void));
extern void run_queue __P((int));

/* external functions defined in notify.c */
extern void fail_delivery __P((struct addr *));
extern void defer_delivery __P((struct addr *));
extern void succeed_delivery __P((struct addr *));
extern void error_delivery __P((struct addr *));
extern void notify __P((struct addr *, struct addr *));

/* external functions defined in pathto.c */
extern void pathto __P((int, char **));
extern void optto __P((int, char **));
extern void uupath __P((int, char **));

/* external functions defined in pwcache.c */
extern struct passwd *getpwbyname __P((int, char *));
extern struct group *getgrbyname __P((char *));
extern struct passwd *getpwbyuid __P((uid_t));
extern struct group *getgrbygid __P((gid_t));

/* external functions defined in queue.c */
extern int queue_message __P((FILE *, enum dot_usage, struct addr *, char **));
extern char **read_message __P((void));
extern int write_body __P((FILE *, unsigned long));
extern void check_grade __P((void));
extern void log_incoming __P((void));
extern char **scan_spool_dirs __P((int));
extern int swallow_smtp __P((FILE *));

/* external functions defined in resolve.c */
extern void resolve_addr_list __P((struct addr *, struct addr **, struct addr **, struct addr **, int));
extern int islocalhost __P((char *));

/* external functions defined in qualify.c */
extern char *read_qualify_file __P((void));
extern char *qualify_domain __P((char *));
extern void dump_qualify_config __P((FILE *));

/* external functions defined in retry.c */
extern char *read_retry_file __P((void));
extern void dump_retry_config __P((FILE *));
extern struct addr *retry_addr_before __P((struct addr *, struct addr **, struct addr **));
extern struct addr *retry_addr_after __P((time_t, struct addr *, struct addr **));
extern void retry_addr_finished __P((struct addr *));
extern int retry_host_lock __P((struct transport *, char *, int *, struct error **));
extern void retry_host_unlock __P((time_t, struct error *));

/* external functions defined in smtprecv.c */
extern char **receive_smtp __P((FILE *, FILE *, void *));

/* external functions defined in sysdep.c */
extern char *time_stamp __P((void));
extern char *get_arpa_date __P((time_t));
extern int get_local_year __P((void));
extern char *unix_date __P((void));
extern void compute_local_sender __P((void));
extern void getfullname __P((void));
#ifdef HAVE_BSD_NETWORKING
extern unsigned long get_inet_addr __P((char *));
#endif
extern FILE *fopen_as_user __P((char *, char *, uid_t, gid_t,  /* XXX mode_t */unsigned int));
extern int lock_file __P((char *, FILE *));
extern void unlock_file __P((char *, FILE *));
#ifdef	USE_FCNTL_RD_LOCK
extern int fcntl_rd_lock __P((int));
extern int fcntl_rd_lock_wait __P((int));
#endif
extern char *compute_hostname __P((void));
extern char *compute_domain __P((char *));
extern pid_t open_child __P((char **, char **, FILE **, FILE **, volatile int,  unsigned int, uid_t, gid_t));
extern int close_child __P((FILE *, FILE *, pid_t));
extern void close_all __P((void));
extern char *scan_dir __P((char *));
extern int touch __P((char *, time_t, time_t));
extern int fsetsize __P((int, off_t, off_t));
#ifndef HAVE_VFPRINTF
extern int vfprintf __P((FILE *, char *, va_list));
#endif
#ifndef HAVE_STRERROR
extern char *strerror __P((int));
#endif
extern char *strsysexit __P((int));
#ifndef HAVE_STRSIGNAL
extern char *strsignal __P((int));
#endif
#ifndef HAVE_SIG2STR
extern int sig2str __P((int, char *));
#endif
extern int set_sigchld_handler __P((void (*)(int)));

/* external functions defined in libcompat XXX should have compat.h */
int dummy_reference __P((void));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
