/*
#ident	"@(#)smail/directors:RELEASE-3_2_0_121:smartuser.c,v 1.35 2005/08/26 19:59:39 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smartuser.c:
 *	direct mail to another address.  This is useful for directing mail
 *	that did not resolve to a user on the local machine to a user on
 *	a remote machine.
 *
 * Specifications for the smartuser directing driver:
 *
 *	private attribute data:
 *	    new_user: (a string) specifies a new address.  expand_string()
 *		is used to build the actual address, with the address'
 *		remainder being substituted for $user.
 *
 *	private attribute flags:
 *	    well_formed_only: if set, only match names which contain letters
 *		numbers, `.', `-', `_', and '+'.   White spaces and dots
 *		are folded into single occurances of the character `.'.
 *
 * NOTE:  If no new_user attribute is specified, the smart_user
 *	  config file attribute will be used.  If neither of these exist,
 *	  then the smart_user driver does not match anything.  This makes
 *	  it possible to have a compiled-in configuration that does not
 *	  match anything, while making it possible to enable the smartuser
 *	  driver using only a config file.
 *
 * TODO:  add a "cmd" attribute (which alternately uses global smart_user_cmd) which
 *	  which will allow dynamic lookups to be done.  E.g. the following
 *	  little script could be used to query a set of internal mailbox
 *	  server on a private network from a gateway:
 *
 *		# In order to use the traditional "vrfy" and support multiple
 *		# domains the internal DNS must point the MXs for all those
 *		# domains at the internal mailboxes server and it would be
 *		# invoked with the following setting:
 *		#
 *		# smart_user_cmd = "/local/sbin/vrfy_addr ${shquote:user}@${shquote:target_domain}"
 *		#
 *		if vrfy "$1" >/dev/null 2>&1; then
 *			echo "<@mailboxes.private:$1>"
 *		else
 *			echo ":error:The address <$1> is not valid."
 *		fi
 *
 *	  The stdout of the command will become the new_user value.
 *
 *	  It would be a configuration error for the command to fail, thus for
 *	  incoming SMTP transactions this would cause a temporary recipient
 *	  reject.
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <ctype.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include <pcre.h>

#include "../smail.h"
#include "../alloc.h"
#include "../list.h"
#include "../smailstring.h"
#include "../dys.h"
#include "../main.h"
#include "../parse.h"
#include "../addr.h"
#include "../field.h"
#include "../lookup.h"
#include "../match.h"
#include "../log.h"
#include "../exitcodes.h"
#include "../direct.h"
#include "../transport.h"
#include "../smailconf.h"
#include "smartuser.h"
#include "../extern.h"
#include "../debug.h"
#include "../error.h"
#include "../smailport.h"

/* functions local to this file */
static char *make_well_formed __P((char *));
static char *make_passable __P((char *));

static struct attr_table smartuser_attributes[] = {
    { "new_user", t_string, 0, NULL, NULL,
	  OFFSET(smartuser_private, new_user) },
    { "transport", t_string, 0, NULL, NULL,
	  OFFSET(smartuser_private, transport) },
    { "prefix", t_string, 0, NULL, NULL,
	  OFFSET(smartuser_private, prefix) },
    { "suffix", t_string, 0, NULL, NULL,
	  OFFSET(smartuser_private, suffix) },
    { "well_formed_only", t_boolean, 0, NULL, NULL, SMARTUSER_WELLFORMED },
};
static struct attr_table *end_smartuser_attributes =
	ENDTABLE(smartuser_attributes);
static struct smartuser_private smartuser_template = {
    NULL,				/* new_user */
    NULL,				/* transport */
    NULL,				/* prefix */
    NULL,				/* suffix */
    NULL,				/* transport_ptr */
};


/*
 * dtd_smartuser - direct to another address
 */
/*ARGSUSED*/
struct addr *
dtd_smartuser(dp, in, out, new, defer, fail)
    struct director *dp;		/* director entry */
    struct addr *in;			/* input local-form addrs */
    struct addr **out;			/* output resolved addrs */
    struct addr **new;			/* output new addrs to resolve */
    struct addr **defer;		/* addrs to defer to a later time */
    struct addr **fail __attribute__((unused));	/* unresolvable addrs */
{
    register struct addr *cur;		/* temp for processing input */
    struct addr *new2 = NULL;		/* addr created if prefix or suffix was stripped */
    struct addr *next;			/* next value for cur */
    struct addr *pass = NULL;		/* addrs to pass to next director */
    char *error = NULL;
    char *temp;
    struct smartuser_private *priv;

    if (!(priv = (struct smartuser_private *)dp->private)) {
	priv = &smartuser_template;
    }

    DEBUG1(DBG_DRIVER_HI, "dtd_smartuser called for director %v\n", dp->name);

    if (priv->new_user == NULL && smart_user == NULL && priv->transport == NULL) {
	/* there is no smart user information, don't match anything */
	DEBUG1(DBG_DRIVER_MID, "director %v: no new_user, smart_user or transport, not matching anything\n",
	       dp->name);
	return in;
    }

    for (cur = in; cur; cur = next) {
	char *new_user;			/* expanded new_user attribute */
	char *remainder;		/* remainder from addr structure */
	struct addr *new_addr;		/* new addr structure */

	next = cur->succ;
	if (cur->flags & ADDR_SMARTUSER) {
	    /* do not match addresses which smart_user already rerouted */
	    DEBUG2(DBG_DRIVER_MID, "director %v: <%v> already re-routed by a smartuser director, pass\n",
		   dp->name, cur->remainder);
	    cur->succ = pass;
	    pass = cur;
	    continue;
	}

	if (dp->flags & SMARTUSER_WELLFORMED) {
	    /*
	     * if required, copy the remainder, making it "well-formed"
	     * in the process
	     */
	    remainder = make_well_formed(cur->remainder);
	    if (remainder == NULL) {
		/* not a sufficiently well-formed, pass on the address */
		DEBUG2(DBG_DRIVER_MID, "director %v: <%v> not well formed, pass\n",
		       dp->name, cur->remainder);
		cur->succ = pass;
		pass = cur;
		continue;
	    }
	} else {
	    remainder = make_passable(cur->remainder);
	}

	if (priv->prefix) {
	    char *anchored_re;
	    char *start = NULL;
	    char *beg = NULL;
	    char *end = NULL;
	    char *last = NULL;

	    /*
	     * if we are testing against a prefix, strip the prefix,
	     * remembering it in rem_prefix
	     */
	    anchored_re = xprintf("^%s", priv->prefix);
	    if (match_re(cur->remainder, anchored_re, TRUE, &start, &beg, &end, &last) != MATCH_MATCHED) {
		DEBUG3(DBG_DRIVER_MID, "director %v: did not match '%s' prefixed name to <%v>, pass\n",
		       dp->name, anchored_re, cur->remainder);
		cur->succ = pass;
		pass = cur;
		xfree(anchored_re);
		continue;		/* did not start with prefix */
	    }
	    xfree(anchored_re);
	    new2 = alloc_addr();
	    new2->remainder = COPY_STRING(last);
	    new2->rem_prefix = rcopy(beg, end);
	    DEBUG2(DBG_DRIVER_HI, "dtd_smartuser(): stripped address: %v, prefix: %v.\n", new2->remainder, new2->rem_prefix);
	}
	if (priv->suffix) {
	    char *newrem;
	    char *anchored_re;
	    char *start = NULL;
	    char *beg = NULL;
	    char *end = NULL;
	    char *last = NULL;

	    /*
	     * if we are testing against a suffix, strip the suffix,
	     * remembering it in rem_suffix
	     */
	    if (!new2) {
		newrem = cur->remainder;
	    } else {
		newrem = new2->remainder; /* use new value without prefix */
	    }
	    anchored_re = xprintf("%s$", priv->suffix);
	    if (match_re(newrem, anchored_re, TRUE, &start, &beg, &end, &last) != MATCH_MATCHED) {
		DEBUG3(DBG_DRIVER_MID, "director %v: did not match '%s' suffixed user name to <%v>, pass\n",
		       dp->name, anchored_re, cur->remainder);
		cur->succ = pass;
		pass = cur;
		xfree(anchored_re);
		continue;		/* did not end with suffix */
	    }
	    xfree(anchored_re);
	    if (!new2) {
		new2 = alloc_addr();
	    } else {
		xfree(new2->remainder);	/* don't leak copy just made above! */
	    }
	    new2->remainder = rcopy(newrem, start);
	    new2->rem_prefix = rcopy(beg, end);
	    DEBUG2(DBG_DRIVER_HI, "dtd_smartuser(): stripped address: %v, suffix: %v.\n", new2->remainder, new2->rem_suffix);
	}
	/*
	 * If we stripped a prefix and/or suffix string then substitute the new
	 * fully resolved addr structure in place of the original, leaving the
	 * original now as its parent.
	 */
	if (new2 && !(cur->flags & ADDR_VRFY_ONLY)) {
	    DEBUG2(DBG_DRIVER_MID, "dtd_smartuser: re-writing addr %v to %v.\n", cur->in_addr, new2->remainder);
	    new2->in_addr = COPY_STRING(new2->remainder);
	    new2->director = dp;
	    new2->parent = cur;
	    cur = new2;
	} else {
	    DEBUG3(DBG_DRIVER_LO, "director %v _NOT_ re-writing addr %v to %v because of VRFY_ONLY\n",
		   dp->name, cur->in_addr, new2->remainder);
	}

	cur->director = dp;		/* match anything, at this point */

	/*
	 * if we are only verifying deliverability then we don't want to
	 * actually transform anything just yet so just push this one onto the
	 * "resolved addresses" list and go on to the next...
	 */
	if (cur->flags & ADDR_VRFY_ONLY) {
	    cur->succ = *out;
	    *out = cur;
	    continue;
	}

	if ((priv->new_user != NULL) || (smart_user != NULL)) {
	    new_user = expand_string(priv->new_user ? priv->new_user : smart_user,
				     (struct addr *) NULL, (char *) NULL, remainder);
	} else {
	    new_user = remainder;
	}
	if (new_user == NULL) {
	    /*
	     * ERR_120 - smartuser expansion failed
	     *
	     * DESCRIPTION
	     *      expand_string() failed to expand the new_user attribute
	     *      for a smartuser director.
	     *
	     * ACTIONS
	     *      Defer the message with a configuration error.
	     *
	     * RESOLUTION
	     *      Check the entry in the director file and fix the
	     *      new_user attribute.
	     */
	    cur->error = note_error(ERR_CONFERR|ERR_120,
				    xprintf("director %v: new_user expansion failed",
					    dp->name));
	    cur->succ = *defer;
	    *defer = cur;
	    continue;
	}

	temp = preparse_address(new_user, &error);
	if (temp == NULL) {
	    /*
	     * ERR_121 - smartuser parse error
	     *
	     * DESCRIPTION
	     *      preparse_address() found an error while parsing the
	     *      expanded new_user attribute for a smartuser director.
	     *      The actual error was returned in `error'.
	     *
	     * ACTIONS
	     *      Defer the message with a configuration error.
	     *
	     * RESOLUTION
	     *      Check the entry in the director file and fix the
	     *      new_user attribute.
	     */
	    cur->error = note_error(ERR_CONFERR|ERR_121,
				    xprintf(
				      "director %v: smartuser parse error: %v",
					    dp->name, error));
	    cur->succ = *defer;
	    *defer = cur;
	    continue;
	}
	new_addr = alloc_addr();
	new_addr->parent = cur;
	new_addr->work_addr = temp;
	new_addr->in_addr = COPY_STRING(new_user);
	new_addr->flags = ADDR_SMARTUSER;
	new_addr->director = dp;
	new_addr->transport = priv->transport_ptr;
	if (priv->transport == NULL) {
	    /* 
	     * We are just rewriting the address (no transport), so we
	     * put it on the reprocess pile
	     */
	    new_addr->succ = *new;
	    *new = new_addr;
	    DEBUG3(DBG_DRIVER_LO, "director %v: matched %v, aliased to <%v>\n",
		   dp->name, cur->remainder, new_user);
	} else {
	    /* 
	     * We have a transport, so we can mark this one as done and
	     * put it on the resolved pile.  We also need to set a couple
	     * more things....
	     */
	    new_addr->remainder = COPY_STRING(new_user);
	    new_addr->next_addr = COPY_STRING(new_user);
	    new_addr->succ = *out;
	    *out = new_addr;
	    DEBUG4(DBG_DRIVER_LO, "director %v: matched %v, aliased to <%v>, transport %v\n",
		   dp->name, cur->remainder, new_user, priv->transport);
	}
    }

    return pass;			/* return addrs for next director */
}

/*
 * dtb_smartuser - read the configuration file attributes
 */
char *
dtb_smartuser(dp, attrs)
    struct director *dp;		/* director entry being defined */
    struct attribute *attrs;		/* list of per-driver attributes */
{
    char *error;
    struct smartuser_private *priv;	/* new smartuser_private structure */
    char *fn = xprintf("smartuser director: %v", dp->name);

    /* copy the template private data */
    priv = (struct smartuser_private *)xmalloc(sizeof(*priv));
    (void) memcpy((char *)priv, (char *)&smartuser_template, sizeof(*priv));

    dp->private = (char *)priv;
    /* fill in the attributes of the private data */
    error = fill_attributes((char *)priv,
			    attrs,
			    &dp->flags,
			    smartuser_attributes,
			    end_smartuser_attributes,
			    fn);
    if (error) {
	return error;
    }
    /* There should be a check here to make sure that at least one
     * of the transport or new_user attributes are specified, but
     * this would cause problems with a default config - ie no
     * directors file
     */
    if ((priv->transport != NULL) 
	&& ((priv->transport_ptr = find_transport(priv->transport)) == NULL)) {
	    return xprintf("%v: director %v: unknown transport: %v", fn, dp->name, priv->transport);
    }
    xfree(fn);

    return NULL;
}


/*
 * dtp_smartuser - dump smartuser config
 */
void
dtp_smartuser(f, dp)
     FILE * f;
     struct director *dp;
{
    (void) dump_standard_config(f,
				(dp->private) ? dp->private : (char *)&smartuser_template,
				dp->name,
				dp->flags,
				smartuser_attributes,
				end_smartuser_attributes);
}


/*
 * make_well_formed - check for well-formedness of a local address
 *
 * Check to make sure that characters in the input are in the set of
 * letters, numbers and the characters `.' and `-'.  Also,  any set
 * of 1 or more white space characters and `.' are transformed into
 * exactly one `.' character.
 *
 * returns NULL or a well-formed string.  The value returned points
 * to storage which may be reused on subsequent calls.
 */
static char *
make_well_formed(name)
    register char *name;		/* input string */
{
    static char *result = NULL;		/* region for forming result */
    static size_t result_size;		/* allocated region length */
    register char *p;			/* point to result */
    register int c;			/* char from name string */

    /* get a region at least big enough to store result */
    if (result == NULL) {
	result = xmalloc(result_size = strlen(name) + 1);
    } else {
	register size_t new_size = strlen(name) + 1;

	if (new_size > result_size) {
	    result = xrealloc(result, new_size);
	}
    }

    /*
     * copy name to result, performing any necessary transformations.
     * Stop if any characters are found which would make the input
     * not well-formed.
     *
     * Don't allow it to begin with `-'.
     */
    p = result;
    if (*name == '-') {
	return NULL;
    }
    while ((c = *name++)) {
	if (isalnum(c) || c == '-' || c == '_' || c == '+') {
	    *p++ = c;
	} else if (isspace((int) c) || c == '.') {
	    *p++ = '.';
	    while (isspace((int) (c = *name)) || c == '.') {
		name++;
	    }
	} else {
	    return NULL;
	}
    }
    *p++ = '\0';

    return result;
}

/*
 * make_passable - build a legal local-part address string from an input
 *
 * given an input string, surround that string in quotes, if required
 * to make a valid local-part address, according to the RFC822 rules.
 * If the address is put in quotes, the text within quotes is guarranteed
 * to be properly \-escaped where required.
 *
 * return value may point to storage which can be reused in subsequent
 * calls.  The value should be copied if it is to be retained.
 */
static char *
make_passable(name)
    char *name;				/* input local-part address */
{
    int need_quotes = FALSE;		/* true if we should put in quotes */
    int esc_ct = 0;			/* count of \'s required */
    static char *result = NULL;		/* region for building result */
    static size_t result_size;		/* size of result region */
    size_t new_size;			/* minimum size required */
    register char *p;			/* temp for building result */
    register char *q;			/* temp for scanning name */
    register int c;			/* char from name */

    for (q = name; (c = *q); q++) {
	if (isalnum(c)) {
	    continue;
	} else if (c == '.' || c == '-') {
	    if (q == name || q[1] == '\0' ||
		q[-1] == '.' || q[-1] == '-')
	    {
		need_quotes = TRUE;
	    }
	} else {
	    need_quotes = TRUE;
	    if (c == '"' || c == '\\' || !isprint((int) c)) {
		esc_ct++;
	    }
	}
    }

    if (need_quotes) {
	new_size = strlen(name) + esc_ct + sizeof("\"\"");
	if (result == NULL) {
	    result = xmalloc(result_size = new_size);
	} else if (result_size < new_size) {
	    result = xrealloc(result, new_size);
	    result_size = new_size;
	}

	/* copy name to result, inserting \ where appropriate */
	p = result;
	*p++ = '"';
	for (q = name; (c = *q); q++) {
	    if (c == '"' || c == '\\' || !isprint((int) c)) {
		*p++ = '\\';
		*p++ = c;
	    } else {
		*p++ = c;
	    }
	}
	*p++ = '"';
	*p = '\0';

	return result;
    }

    return name;			/* the original string was fine */
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
