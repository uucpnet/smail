/*
#ident	"@(#)smail/src/directors:RELEASE-3_2_0_121:error.h,v 1.3 2004/12/14 00:24:16 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * error.h:
 *	interface file for error driver.
 */

/* optional private information from director file entry */
struct error_private {
    char *defaulttext;			/* default error text */
};

extern struct addr *dtd_error __P((struct director *,
				   struct addr *,
				   struct addr **,
				   struct addr **,
				   struct addr **,
				   struct addr **));
extern char *dtb_error __P((struct director *, struct attribute *));
extern void dtp_error __P((FILE *, struct director *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
