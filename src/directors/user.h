/*
#ident	"@(#)smail/src/directors:RELEASE-3_2_0_121:user.h,v 1.12 2004/03/18 20:24:31 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * user.h:
 *	interface file for driver code in user.c
 */

/* boolean attributes for director.flags field */
#define USER_IGNORE_CASE 0x0001000	/* XXX deprecated */

/* private information from director file entry */
struct user_private {
    char *transport;			/* name of the transport */
    char *prefix;			/* prefix, e.g., "real-" */
    char *suffix;			/* suffix, e.g., "-.*" */
#ifdef HAVE_FGETPWENT
    char *pwfile;			/* name of password file */
#endif
};

extern struct addr *dtd_user __P((struct director *,
				  struct addr *,
				  struct addr **,
				  struct addr **,
				  struct addr **,
				  struct addr **));
extern char *dtb_user __P((struct director *, struct attribute *));
extern void dtp_user __P((FILE *, struct director *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
