/*
#ident	"@(#)smail/src/directors:RELEASE-3_2_0_121:smartuser.h,v 1.7 2003/12/14 22:42:37 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smartuser.h:
 *	interface file for the smartuser driver.
 */

/* boolean attributes for director.flags field */
#define SMARTUSER_WELLFORMED	0x00010000

/* private information from director file entry */
struct smartuser_private {
    char *new_user;			/* template for new address */
    char *transport;			/* transport if routing to "smart" transport */
    char *prefix;			/* prefix, e.g., "owner-" */
    char *suffix;			/* suffix */
    struct transport *transport_ptr;	/* transport structure */
};

extern struct addr *dtd_smartuser __P((struct director *,
				       struct addr *,
				       struct addr **,
				       struct addr **,
				       struct addr **,
				       struct addr **));
extern char *dtb_smartuser __P((struct director *, struct attribute *));
extern void dtp_smartuser __P((FILE *, struct director *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
