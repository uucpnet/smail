/*
#ident	"@(#)smail/src/directors:RELEASE-3_2_0_121:error.c,v 1.14 2005/08/26 19:59:39 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * error.c:
 *	Generate permanent or temporary errors with specified text.
 *
 * Specifications for the "error" directing driver:
 *
 *	private attribute data:
 *
 *	    defaulttext (string):  default text to use if none is specified
 *		by the administrator.
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <ctype.h>
#include <assert.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include "../smail.h"
#include "../alloc.h"
#include "../list.h"
#include "../main.h"
#include "../parse.h"
#include "../addr.h"
#include "../field.h"
#include "../log.h"
#include "../exitcodes.h"
#include "../lookup.h"
#include "../smailstring.h"
#include "../dys.h"
#include "../direct.h"
#include "../transport.h"
#include "../smailconf.h"
#include "error.h"
#include "../extern.h"
#include "../debug.h"
#include "../error.h"
#include "../smailport.h"

static struct attr_table error_attributes[] = {
    { "defaulttext", t_string, 0, NULL, NULL, OFFSET(error_private, defaulttext) },
};
static struct attr_table *end_error_attributes = ENDTABLE(error_attributes);

static struct error_private error_template = {
    NULL,				/* defaulttext */
};

/*
 * dtb_error - read the configuration file attributes
 */
char *
dtb_error(dp, attrs)
    struct director *dp;		/* director entry being defined */
    struct attribute *attrs;		/* list of per-driver attributes */
{
    char *error;
    struct error_private *priv;	/* new error_private structure */
    char *fn = xprintf("error director: %v", dp->name);

    /* copy the template private data */
    priv = (struct error_private *) xmalloc(sizeof(*priv));
    (void) memcpy((char *) priv, (char *) &error_template, sizeof(*priv));

    dp->private = (char *) priv;
    /* fill in the attributes of the private data */
    error = fill_attributes((char *)priv,
			    attrs,
			    &dp->flags,
			    error_attributes,
			    end_error_attributes,
			    fn);
    xfree(fn);
    if (error) {
	return error;
    }

    return NULL;
}


/*
 * dtp_error - dump error config
 */
void
dtp_error(fp, dp)
     FILE *fp;
     struct director *dp;
{
    (void) dump_standard_config(fp,
				(dp->private) ? dp->private : (char *) &error_template,
				dp->name,
				dp->flags,
				error_attributes,
				end_error_attributes);
}


/*
 * dtd_error - error driver
 */
/*ARGSUSED*/
struct addr *
dtd_error(dp, in, out, new, defer, fail)
    struct director *dp;		/* director entry */
    struct addr *in;			/* input local-form addrs */
    struct addr **out __attribute__((unused));	/* output resolved addrs */
    struct addr **new __attribute__((unused));	/* output new addrs to resolve */
    struct addr **defer;		/* addrs to defer to a later time */
    struct addr **fail;			/* unresolvable addrs */
{
    struct error_private *priv;		/* private storage */
    struct addr *pass = NULL;		/* addr structures to return */
    struct addr *cur;			/* addr being processed */
    struct addr *next;			/* next addr to process */
    unsigned int do_defer = FALSE;	/* defer instead of fail? */

    DEBUG1(DBG_DRIVER_HI, "dtd_error(%d) called\n", dp->name);

    if (!(priv = (struct error_private *) dp->private)) {
	priv = &error_template;
    }
    for (cur = in; cur; cur = next) {
	next = cur->succ;
	if (strncmpic(cur->remainder, ":defer:", sizeof(":defer:") - 1) == 0) {
	    do_defer = TRUE;
	}
	if (do_defer ||
	    (strncmpic(cur->remainder, ":error:", sizeof(":error:") - 1) == 0) ||
	    (strncmpic(cur->remainder, ":fail:", sizeof(":fail:") - 1) == 0)) {
	    struct addr *top;
	    char *in_addr;		/* pointer to either original input addr, or nothing */
	    char *errtxt;		/* pointer to original error text */
	    char *exptxt;		/* pointer to expanded error text */

	    cur->director = dp;		/* matched if we reached this point */

	    /* find the top parent to log the original in_addr */
	    for (top = cur; top->parent && top->parent->in_addr; top = top->parent) {
		;
	    }
	    /* but don't bother if it's the same as the immediate parent, or cur */
	    if (top == cur->parent || top == cur) {
		top = NULL;
	    }
	    errtxt = strchr(cur->remainder + 1, ':') + 1;
	    assert(errtxt);
	    while (isspace((int) *errtxt)) {
		errtxt++;
	    }
	    if (! *errtxt && priv->defaulttext) {
		errtxt = priv->defaulttext;
	    }
	    exptxt = expand_string(errtxt, in->parent, (char *) NULL, (char *) NULL);
	    if (exptxt == NULL) {
		/*
		 * ERR_192 - error text expansion failed
		 *
		 * DESCRIPTION
		 *      The error text part of a :defer:text or :fail:text
		 *      address failed to be expanded by expand_string().
		 *
		 * ACTIONS
		 *      Fail the address and send an error to the address owner
		 *      or to the postmaster.  Report the parent address which
		 *      produced the :defer:text or :fail:text address in the
		 *      error message.
		 *
		 * RESOLUTION
		 *      The postmaster or address owner should correct the address.
		 */
		cur->error = note_error(ERR_NPOWNER | ERR_192,
					xprintf("director %v: error message evaluation failed for address <%v>%s%v%s.",
						dp->name,
						cur->in_addr,
						top ? " (derived from <" : "",
						top ? top->in_addr : "",
						top ? ">)" : ""));
		cur->succ = *fail;
		*fail = cur;
		continue;
	    }
	    /*
	     * chop of the exptext on any input addr generated by something
	     * like the rewrite router....
	     */
	    in_addr = cur->parent ? cur->parent->in_addr : cur->in_addr;
	    if ((strncmpic(in_addr, ":defer:", sizeof(":defer:")-1) == 0) ||
		(strncmpic(in_addr, ":error:", sizeof(":error:")-1) == 0) ||
		(strncmpic(in_addr, ":fail:", sizeof(":fail:")-1) == 0)) {
		in_addr = NULL;
	    }
	    /*
	     * Note this ugly mess does result in duplication of the original
	     * address in the log messages, but in bounce messages this is
	     * essential for the "failed addresses" section of the report as
	     * otherwise the sender cannot match the failure to an address that
	     * was actually sent to.  It also helps in SMTP rejects.
	     *
	     * NOTE:  To test this expression fully and properly there are at
	     * least four forms to try:
	     *
	     * 1. a direct error address:
	     *
	     *    $ smail -bv ':fail:"This is a bad address"'
	     *    <:fail:> ... not deliverable: address failed: This is a bad address
	     *
	     * 2. an alias to an error address
	     *
	     *	  aliases:
	     *	  --------
	     *	  test-fail:	:fail:"this address is designed to fail"
	     *
	     *    $ smail -bv test-fail
	     *    <:fail:> (orig-to <test-fail@building.weird.com>) ... not deliverable: address <test-fail@building.weird.com> failed: This address is designed to fail.
	     *
	     * 3. an alias to an alias to an error address:
	     *
	     *	  aliases:
	     *	  --------
	     *	  test-to-fail:	test-fail
	     *	  test-fail:	:fail:"this address is designed to fail"
	     *
	     *    $ smail -bv test-to-fail
	     *    <:fail:> (orig-to <test-to-fail@building.weird.com>) ... not deliverable: address <test-fail> (derived from <test-to-fail@building.weird.com>) failed: This address is designed to fail.
	     *
	     * 4. an error address generated by a rewrite router:
	     *
	     *	  rewrite:
	     *	  --------
	     *		error.example.com + ${lookup:user:lsearch{/etc/mail/aliases} then {$value}
	     *			else {:fail:"The mailbox '$user' does not exist for the domain '$host'."}}
	     *
	     *    $ smail -bv no-user@error.example.com
	     *    <:fail:> (orig-to <no-user@error.example.com>) ... not deliverable: address derived from <no-user@error.example.com> failed: The mailbox 'no-user' does not exist for the domain 'error.example.com'.
	     */
	    cur->error = note_error(ERR_NSOWNER | ERR_193,
				    xprintf("address%s%v%s%s%v%s%s: %s",
					    in_addr ? " <" : "",
					    in_addr ? in_addr : "",
					    in_addr ? (top ? "> (" : "> ") : " ",
					    top ? "derived from <" : "",
					    top ? top->in_addr : "",
					    top && in_addr ? ">) " : (top ? "> " : ""),
					    do_defer ? "deferred" : "failed",
					    exptxt));
	    if (do_defer) {
		cur->succ = *defer;
		*defer = cur;
	    } else {
		cur->succ = *fail;
		*fail = cur;
	    }
	} else {
	    DEBUG2(DBG_DRIVER_HI, "director %v: ignoring non-matching address <%v>, pass\n",
		   dp->name, cur->remainder);
	    cur->succ = pass;
	    pass = cur;
	    continue;
	}
    }

    return pass;
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
