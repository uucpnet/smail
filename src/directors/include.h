/*
#ident	"@(#)smail/src/directors:RELEASE-3_2_0_121:include.h,v 1.6 2003/12/14 22:42:37 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * include.h:
 *	interface file for aliasinclude and forwardinclude drivers.
 */

/*
 * the private structures and boolean attributes are the same for
 * both of these drivers.
 */

/* boolean attribute flags for director.flags field */
#define COPY_SECURE	0x00010000	/* security flags from parent */
#define COPY_OWNERS	0x00020000	/* ownership restricts. from parent */

/* optional private information from director file entry */
struct include_private {
    int modemask;			/* unsecure permissions ala umask(2) */
    char *owners;			/* ownership restrictions */
    char *owngroups;			/* group ownership restrictions */
    char *matchdirector;		/* director to match against */
    int retries;			/* retries on opens */
    unsigned int interval;		/* interval between retries */
};

extern struct addr *dtd_forwardinclude __P((struct director *,
					    struct addr *,
					    struct addr **,
					    struct addr **,
					    struct addr **,
					    struct addr **));
extern char *dtb_forwardinclude __P((struct director *, struct attribute *));
extern void dtp_forwardinclude __P((FILE *, struct director *));

extern struct addr *dtd_aliasinclude __P((struct director *,
					    struct addr *,
					    struct addr **,
					    struct addr **,
					    struct addr **,
					    struct addr **));
extern char *dtb_aliasinclude __P((struct director *, struct attribute *));
extern void dtp_aliasinclude __P((FILE *, struct director *));

extern struct addr *dtd_genericinclude __P((struct director *,
					    struct addr *,
					    struct addr **,
					    struct addr **,
					    struct addr **,
					    struct addr **));
extern char *dtb_genericinclude __P((struct director *, struct attribute *));
extern void dtp_genericinclude __P((FILE *, struct director *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
