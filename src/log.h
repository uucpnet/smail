/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:log.h,v 1.12 2005/04/02 05:34:48 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * log.h:
 *	interface file for routines in log.c
 */

/* bit-flag macros used in log.c */
#define WRITE_LOG_NONE	0x0000		/* write_log sends to no log */
#define WRITE_LOG_SYS	0x0001		/* write_log sends to system log */
#define WRITE_LOG_CONS	0x0002		/* write_log sends to console */
#define WRITE_LOG_MLOG	0x0004		/* write_log sends to per-msg log */
#define WRITE_LOG_PANIC	0x0008		/* write_log sends to panic log */
#define WRITE_LOG_TTY	0x0010		/* write_log sends to stderr */

#define WRITE_LOG_LAST	WRITE_LOG_TTY	/* the one with the highest bit set */

/* external variables defined in log.c */
extern struct str logstr;		/* STR for write_log_va() */

/* external functions defined in log.c */
extern void open_system_logs __P((void));
extern void close_system_logs __P((void));
extern void open_msg_log __P((void));
extern void close_msg_log __P((void));
extern void unlink_msg_log __P((void));
extern void panic __P((int, char *, ...));
extern void write_log __P((int, char *, ...));
extern void send_log __P((FILE *, char *));
extern char *scan_msg_log __P((int));
extern struct addr *process_msg_log __P((struct addr *, struct identify_addr **));
extern char *decode_x_line __P((char *, char **, char **, unsigned long int *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
