/*
#ident "@(#)smail/src:RELEASE-3_2_0_121:smailport.h,v 1.10 2005/06/27 22:56:18 woods Exp"
 */

/*
 *	smailport.h -- define any missing common-use macros....
 *
 * Source files include this one last....
 */

#ifndef VA_START
# ifdef __STDC__
#  define VA_START(args, lastarg)       va_start(args, lastarg)
# else
#  define VA_START(args, lastarg)       va_start(args)
# endif
#endif

#ifndef SIG_ERR
# define SIG_ERR	((void (*) __P((int))) -1)
#endif

#ifndef SIG2STR_MAX
# define SIG2STR_MAX	32	/* This is just the sunos-5 default value.... */
#endif

#ifndef STDIN_FILENO
# define STDIN_FILENO	0	/* standard input file descriptor */
#endif
#ifndef STDOUT_FILENO
# define STDOUT_FILENO	1	/* standard output file descriptor */
#endif
#ifndef STDERR_FILENO
# define STDERR_FILENO	2	/* standard error file descriptor */
#endif

/* whence values for lseek(2) */
#ifndef SEEK_SET
# define SEEK_SET	0	/* set file offset to offset */
#endif
#ifndef SEEK_CUR
# define SEEK_CUR	1	/* set file offset to current plus offset */
#endif
#ifndef SEEK_END
# define SEEK_END	2	/* set file offset to EOF plus offset */
#endif

/* trap undefined open flags */
#ifndef	O_RDONLY
# define O_RDONLY	0
#endif
#ifndef O_WRONLY
# define O_WRONLY	1
#endif
#ifndef O_RDWR
# define O_RDWR		2
#endif

/* mode values for access(2) */
#ifndef F_OK
# define F_OK		0	/* does file exist */
#endif
#ifndef X_OK
# define X_OK		1	/* is it executable by caller */
#endif
#ifndef W_OK
# define W_OK		2	/* is it writable by caller */
#endif
#ifndef R_OK
# define R_OK		4	/* is it readable by caller */
#endif

#ifndef _PATH_DEVNULL
# define _PATH_DEVNULL	"/dev/null"
#endif

/*
 * in <sys/param.h> on some systems, <sys/sysmacros.h> on others...
 */
#ifndef MIN
# define MIN(a, b)	(((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
# define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#endif

#if !defined(PATH_MAX) && defined(_POSIX_PATH_MAX)
# define PATH_MAX	_POSIX_PATH_MAX
#elif !defined(PATH_MAX)
# define PATH_MAX	255		/* maybe conservative, but safe. */
#endif

/*
 * My man page says that 255 chars (plus NUL byte) is the limit on length of
 * a host name.  There appears to be no #define for it in 4.2BSD.
 */
#ifndef MAXHOSTNAMELEN
# define MAXHOSTNAMELEN	256
#endif

/*
 * ancient systems
 */
#ifndef AF_LOCAL
# define AF_LOCAL	AF_UNIX
#endif

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
