/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:route.h,v 1.13 2005/07/25 05:12:37 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

#ifndef	ROUTE_H
#define ROUTE_H

/*
 * route.h:
 *	interface file for route.c.  Also, types and macros for
 *	use by router drivers.
 */

/* structure of a router, as read from the configuration file */
struct router {
    char *name;				/* name of router */
    char *driver;			/* name of driver */
    struct router *succ;		/* next router in the list */
    unsigned long flags;		/* boolean flag values */
    struct method *method;		/* table of host/tport associations */
    char *default_transport;		/* name of default transport */
    char *private;			/* private data storage */
};

/* method - table associating hosts and transports */
struct method {
    char *host;				/* host name */
    int mingrade;			/* min grading for this entry */
    int maxgrade;			/* max grading for this entry */
    char *transport;			/* transport name */
    char *filename;			/* filename was read from */
};

/* compiled in route drivers */
struct route_driver {
    char *name;					/* name of route driver */
    void (*cache) __P((struct router *));	/* function to cache routing info */
    void (*driver) __P((struct router *,
			struct addr *,
			struct addr **,
			struct addr **,
			struct addr **));	/* function to perform routing */
    void (*finish) __P((struct router *));	/* function to free resources */
    char *(*builder) __P((struct router *, struct attribute *)); /* func to read from router file */
    void (*dumper) __P((FILE *, struct router *)); /* function to dump config */
};

/*
 * structure for route information passed between the rt[dv]_standard
 * routines and the various router drivers that use them; also used by
 * bindlib, which is why it's here instead of routers/rtlib.h.
 */
struct rt_info {
    char *next_host;			/* next-hop host string */
    char *route;			/* route from next_host to target */
    int matchlen;			/* length of match on target */
    struct transport *transport;	/* optional transport */
    struct transport_hints *tphint_list; /* options transport hints */
};

/* values for router.flags field */
#define USE_ALWAYS	0x0001		/* if match, don't use next router */
#define RT_AFFECTS_USER 0x0002		/* This router can affect the user part */

/* external functions defined in route.c */
extern void route_remote_addrs __P((struct addr *, struct addr **, struct addr **, struct addr **, struct addr **));
extern char *match_end_domain __P((char *, char *));
extern char *is_suffix __P((char *, char *, int));
extern void route_driver_finish __P((struct router *, struct addr *, int, char *, char *, struct transport *, struct transport_hints *));
extern char *uucp_to_route_addr __P((char *));
extern struct router *find_router __P((char *));
extern struct route_driver *find_route_driver __P((char *));
extern void cache_routers __P((void));
extern void finish_routers __P((void));
extern char *read_router_file __P((void));
extern void dump_router_config __P((FILE *));
extern struct method *read_method_file __P((char *, char **));

#endif	/* ROUTE_H */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
