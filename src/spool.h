/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:spool.h,v 1.12 2005/05/02 23:32:56 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * spool.h:
 *	interface file for routines in spool.c
 */

/* macros used in spool.c */
#define READ_FAIL	-2		/* spool file read failed */
#define WRITE_FAIL	-1		/* general-purpose write failed */
/* size of spool file basename */
#define SPOOL_FN_LEN	(sizeof("tttttt-iiiiiig") - 1)

/*
 * GETSPOOL fetches characters from the spool buffer
 * and calls read_spool to read more characters when the
 * end of the buffer is reached.
 * returns a char or EOF on end-of-file or READ_FAILED on read error
 */
#define GETSPOOL()	(msg_ptr < msg_max ?				\
			    (0xff & (*msg_ptr++)) :			\
			    ((msg_foffset + (msg_max - msg_buf) >= msg_size) ? \
				EOF :					\
				(read_spool() == FAIL) ?		\
				    READ_FAIL :				\
				    (0xff & (*msg_ptr++))))

/*
 * PUTSPOOL(c) writes a character to the spool file buffer and
 * flushes the buffer when it is full.
 */
#define PUTSPOOL(c, erp) (msg_max < end_msg_buf ?			\
			    (0xff &(*msg_max++ = (c))) :		\
			    write_spool(erp) == FAIL ?			\
				EOF :					\
				(msg_foffset += msg_max - msg_buf,	\
				 msg_max = msg_buf,			\
				 (0xff & (*msg_max++ = (c)))))

/* external functions defined in spool.c */
extern int creat_spool __P((void));
extern int lock_message __P((void));
extern void unlock_message __P((void));
extern int write_spool __P((char **));
extern int egrep_spool_headers __P((voidplist_t *, char **));
extern int egrep_spool_body __P((voidplist_t *, char **));
extern int open_spool __P((char *, int, int));
extern void close_spool __P((void));
extern void unlink_spool __P((void));
extern int seek_spool __P((off_t));
extern off_t tell_spool __P((void));
extern int send_spool __P((FILE *, unsigned long));
extern int read_spool __P((void));
extern void log_spool_errors __P((void));
extern int new_grade __P((int));
extern void freeze_message __P((void));
extern time_t message_date __P((void));
extern long spool_max_free_space __P((void));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
