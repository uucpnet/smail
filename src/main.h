/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:main.h,v 1.22 2005/10/26 16:27:28 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * main.h:
 *	interface file for functions in main.c.
 */

#ifndef MAIN_H
#define MAIN_H

/* macros used in main.c */

/* values for operation_mode */
enum op_mode {					/* XXX op_mode_t */
    MODE_DEFAULT,		/* default operation */
    PRINT_VERSION,		/* print the current smail version */
    DAEMON_MODE,		/* operate as a daemon */
    DELIVER_MAIL,		/* deliver mail normally */
    FREEZE_CONFIG,		/* freeze a config file */
    PRINT_QUEUE,		/* print the spool directories */
    REBUILD_ALIASES,		/* rebuild the system alias file */
    SMTP_MODE,			/* read smtp on standard input */
    BATCH_SMTP_MODE,		/* batch smtp on standard input */
    TEST_MODE,			/* addressing parsing tests */
    VERIFY_ADDRS,		/* verify use of addresses */
    NOOP_MODE,			/* do nothing (run queue, if applicable) */
    PATHTO_MODE,		/* invoked as pathto, display routing path */
    UUPATH_MODE,		/* invoked as uupath, display uucp path */
    OPTTO_MODE,			/* invoked as optto, not yet supported */
    ROGUE_MODE,			/* display the rogue tombstone mode */
    COPYING_MODE,		/* print the COPYING file */
    PRINT_VARS_MODE,		/* print configuration variables */
    TEST_DNSBL_MODE		/* test DNSBLs */
};

/* values for error_processing */
enum er_proc {					/* XXX er_proc_t */
    ERROR_DEFAULT,		/* use defaults */
    DEV_NULL,			/* don't say anything on errors */
    MAIL_BACK,			/* send mail back to sender */
    TERMINAL,			/* write to stderr */
    WRITE_BACK			/* write using write(1) */
};

/* mode used in delivering mail */
enum deliver_mode {				/* deliver_mode_t */
    DELIVER_DEFAULT,		/* use defaults */
    FOREGROUND,			/* deliver mail in foreground */
    BACKGROUND,			/* deliver mail in background */
    QUEUE_MESSAGE		/* don't perform delivery, just queue */
};

/* types of operation based on program name */
enum prog_type {				/* prog_type_t */
    PROG_SMAIL,			/* normal operation */
    PROG_RMAIL,			/* invoked as "rmail" */
    PROG_PATHTO,		/* invoked as "pathto" */
    PROG_OPTTO,			/* invoked as "optto" */
    PROG_UUPATH,		/* invoked as "uupath" */
    PROG_NEWALIASES,		/* invoked as "newaliases" */
    PROG_SMAILCONF,		/* invoked as "smailconf" */
    PROG_MAILQ,			/* invoked as "mailq" */
    PROG_RUNQUEUE,		/* invoked as "runqueue" */
    PROG_RSMTP,			/* invoked as "rsmtp" */
    PROG_SMTPD,			/* invoked as "smtpd" */
    PROG_ROGUE,			/* invoked as "rogue" */
    PROG_EXECMAIL		/* invoked as "execmail" (for SCO XENIX) */
};

/* how dots are treated on incoming messages */
enum dot_usage {				/* dot_usage_t */
    NO_DOT_PROTOCOL,		/* don't do anything special with dots */
    HIDDEN_DOTS,		/* use the hidden-dot algorithm on input */
    DOT_ENDS_MESSAGE,		/* a dot on a line by itself ends message */
    SMTP_DOTS			/* like HIDDEN_DOTS, though EOF is error */
};

/* external variables defined in main.c */
extern int islocal;			/* TRUE if mail originated locally */
extern int exitvalue;			/* call exit with this value */
extern char *program;			/* argv[0] from main */
extern char *sender;			/* sender of message */
extern char *local_sender;		/* local sender of message */
extern int error_sender;		/* TRUE if special sender <> given */
extern char *sender_name;		/* full name of sender */
extern int dont_deliver;		/* if TRUE, don't actually deliver */
extern int process_queue;		/* process spooled files */
extern unsigned int queue_interval;	/* process queues at this interval */
extern int hop_count;			/* hop count so far for message */
extern int do_aliasing;			/* do aliasing for local addresses */
extern int extract_addresses;		/* get recipients from header */
extern enum op_mode operation_mode;	/* mode of operation */
extern enum dot_usage dot_usage;	/* how do we treat . on input */
extern enum deliver_mode deliver_mode;	/* foreground, background or queued */
extern enum er_proc error_processing;	/* method of displaying errors */
extern enum prog_type prog_type;	/* type of program we are running as */
extern int me_too;			/* sender allowed in aliases */
extern struct addr *recipients;		/* list of cmd-line recipient addresses */
extern struct addr *parameters;		/* fancy argv[] list */
extern int num_recipients;		/* number of entries in 'recipients' */
extern char *primary_name;		/* primary local name from hostnames */
extern char *arg_debug_file;		/* debug filename if no to stderr */
extern int only_testing;		/* avoid writing sys/panic logs */
extern char *dnsbl_to_test;		/* for test_dnsbls() */
extern uid_t real_uid;			/* saved real uid before ruid setup */
extern gid_t real_gid;			/* saved real gid before rgid setup */
extern char **save_argv;		/* saved pointer to arguments */
extern int some_deferred_addrs;		/* don't unlink spool file */
					/* as some addrs were deferred */
extern uid_t prog_euid;			/* effective uid of program */
extern gid_t prog_egid;			/* effective gid of program */
extern int force_zero_exitvalue;	/* if TRUE always exit with status 0 */
extern int call_freeze_message;		/* if TRUE must call freeze_message() */
extern char *sender_host;		/* name of sender's host */
extern char *sender_host_addr;		/* inet source address of sender's host */
extern char *sender_host_port;		/* inet source port of sender's host */
extern char *sender_proto;		/* name of sender's sending protocol */
extern char *sender_program;		/* name of program that spooled msg */
extern char *ident_sender;		/* The calculated identity of the sender */
extern char *ident_method;		/* Method used to determine ident_sender */
extern char *smtp_service_name;

/* external functions defined in main.c */
extern int main __P((int, char **));
extern void initialize_state __P((void));
extern void reset_hit_table __P((void));
extern void process_args __P((char **, int));
extern void check_sender __P((int));
extern void process_recipients __P((int));

#endif	/* MAIN_H */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
