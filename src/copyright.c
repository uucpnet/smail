/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:copyright.c,v 1.7 2003/12/14 22:42:42 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * copyright.c:
 *	include a copyright notice in the smail binary.
 */
#ifndef lint
char copyright[] = "\n\
Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll\n\
\n\
SMAIL is free software and you are welcome to distribute copies of it\n\
under certain conditions.  Consult a file COPYING, distributed in the\n\
source for SMAIL to see the conditions\n";
#endif

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
