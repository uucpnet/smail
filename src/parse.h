/*
#ident "@(#)smail/src:$Name $:parse.h,v 1.8 2005/07/06 23:47:57 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 by Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 *
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * parse.h:
 *	interface file for routines in parse.c.
 */

/* types used in parse.c */
/*
 * attributes returned by parse_entry(), parse_config(), and parse_table() are
 * a linked lists of the following structure.
 *
 * Note that both pointers are always pointing into the same allocated storage
 * block (except when the attribute is of a boolean type, in which case the
 * "value" field points to one of two fixed global storage spaces also pointed
 * to by the variables "on" and "off").  The dynamically allocated block can be
 * freed by passing just the "name" pointer to xfree().
 */
struct attribute {
    struct attribute *succ;		/* next attribute */
    char *name;				/* name of attribute */
    char *value;			/* value of the attribute */
};

/* external functions defined in parse.c */
extern char *parse_entry __P((char *, struct attribute **, struct attribute **, char **));
extern struct attribute *parse_config __P((char *, char **));
extern struct attribute *parse_table __P((char *, char **));
extern char *read_entry __P((FILE *, char *));
extern char *skip_space __P((char *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
