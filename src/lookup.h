/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:lookup.h,v 1.14 2005/04/22 18:34:41 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * lookup.h:
 *	Interface file for lookup.c
 */

/* generic form of structure returned by database open calls */
struct generic_db {
    struct db_proto *proto;		/* access method (defined only in lookup.c!) */
    char *name;				/* name of db */
};

/* macros used in communicating with functions in lookup.c */
#define DB_FAIL		(-1)		/* unrecoverable failure */
#define DB_AGAIN	(-2)		/* retry operation at a later time */
#define DB_NOMATCH	(-3)		/* no match was found */
#define DB_SUCCEED	0		/* operation was successful */
#define FILE_SUCCEED	0		/* operation on file was successful */
#define FILE_FAIL	(-4)		/* unrecoverable database failure */
#define FILE_AGAIN	(-5)		/* try using database later */
#define FILE_NOMATCH	(-6)		/* no such file */

/* WARNING: bindlib.c defines DB_NEGMATCH as (-8) */

#define LOOKUP_DBG_NAME(val)	((val) == DB_FAIL ? "DB_FAIL" :		\
				 (val) == DB_AGAIN ? "DB_AGAIN" :	\
				 (val) == DB_NOMATCH ? "DB_NOMATCH" :	\
				 (val) == DB_SUCCEED ? "DB_SUCCEED" :	\
				 (val) == FILE_FAIL ? "FILE_FAIL" :	\
				 (val) == FILE_AGAIN ? "FILE_AGAIN" :	\
				 (val) == FILE_NOMATCH ? "FILE_NOMATCH" : "!!!INVALID_LOOKUP_RESULT!!!")

/* external functions defined in lookup.c */
extern int open_database __P((char *, char *, int, unsigned int, struct stat *, char **, char **));
extern void close_database __P((char *));
extern int lookup_database __P((char *, char *, char **, char **));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
