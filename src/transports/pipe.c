/*
#ident	"@(#)smail/src/transports:RELEASE-3_2_0_121:pipe.c,v 1.58 2005/08/31 17:57:25 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * pipe.c:
 *	deliver mail over a pipe to a program exec'd in a child
 *	process.  Be very careful to keep things secure by setting
 *	the uid and gid in the child process to something appropriate.
 *
 * Specifications for the pipe transport driver:
 *
 *	private attribute data:
 *	    cmd (string):  a string which will be parsed for vectors
 *		to pass to execv.  Some variable expansion will be done,
 *		using the build_cmd_line function.
 *	    user (name):  the name of the user to setuid to in the child
 *		process.  If not specified then the addr structure is
 *		searched for a uid.  If none is found there, use the
 *		nobody_uid.
 *	    group (name):  the name of the group to setgid to in the child
 *		process.  The algorithm used for uid is used here as well.
 *	    umask (int):  umask for child process.
 *	    handle_write_errors (char):  If set to "(i)gnore" then
 *		ignore write errors encountered when writing the message to the
 *		pipe.  If set to "(d)efer" then defer message delivery if a write
 *		error occurs.  If set to "(b)ounce" then return an error to the
 *		sender when a write error occurs.  (may also be "unset")
 *	    defer_child_exitcodes (string):  this attribute may be set to a
 *		colon separated list of one or more specific exit codes which
 *		should result in a defer.  This can be used to handle truly
 *		temporary errors from the LDA, such as with Cyrus-2.x where
 *		"deliver" uses LMTP and will exit(65) when the LMTP daemon is
 *		not reachable.
 *	    bounce_child_exitcodes (string):  this attribute may be set to a
 *		colon separated list of one or more specific exit codes which
 *		should result in a bounce.  By default this list is set to the
 *		range "1-127" to include all the possible non-zero exitcodes.
 *	    ignore_child_exitcodes (string):  this attribute may be set to a
 *		colon separated list of one or more specific exit codes which
 *		should be ignored.  By default this list contains only the one
 *		value "0" but that value is really only decorative since
 *		EXIT_SUCCESS values are always silently ignored.
 *
 *	private attribute flags:
 *	    pipe_as_user:  if set, become an appropriate user in the
 *		child process.  Otherwise, become the nobody user.
 *	    ignore_status:  DEPRECATED.
 *		(If none of the *_child_exitcodes attributes are set, and this
 *		is set, then ignore the exit status of the child process.
 *		Otherwise complain if it is non-zero.)
 *	    pipe_as_sender:  become the sender, based on the saved
 *		sender's real uid.
 *	    log_output:  log the stdout and stderr of the child process
 *		to the message log file.  This output will also then be
 *		returned back to the sender of the message if status_to_sender
 *		is set.
 *	    ignore_write_errors:  DEPRECATED.
 *		(If handle_write_errors is not set, and this is set, then treat
 *		handle_write_errors as if it were set to "ignore")
 *	    defer_child_errors:  DEPRECATED. generally, only child failures
 *		from the signal SIGTERM are retried.  If this is set, then
 *		retries are performed if the exit code is non-zero, _OR_ if the
 *		write failed on the pipe.  You generally don't want to use this
 *		feature.  For example if your local delivery agent might return
 *		errors when the mailbox is over quota then normally you want to
 *		bounce the message to the sender immediately.  (Ignored if
 *		either handle_write_errors or defer_child_exitcodes are set.
 *		Also, ingore_child_exitcodes and bounce_child_exitcodes, if
 *		set, take precedence.)
 *          status_to_sender: if set and the exit status of the child process
 *		is non-zero, and the ignore_status flag is NOT set, then
 *		report back to the sender instead of the postmaster.
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <pwd.h>
#include <grp.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# define _GNU_SOURCE			/* to see decl. of strsignal() */
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#if defined(POSIX_OS) || defined(UNIX_BSD) || defined(WAIT_USE_UNION)
# include <sys/wait.h>
#endif

#include "../smail.h"
#include "../alloc.h"
#include "../list.h"
#include "../main.h"
#include "../parse.h"
#include "../addr.h"
#include "../log.h"
#include "../spool.h"
#include "../smailwait.h"
#include "../child.h"
#include "../exitcodes.h"
#include "../smailstring.h"
#include "../dys.h"
#include "../transport.h"
#include "../smailconf.h"
#include "pipe.h"
#include "../extern.h"
#include "../debug.h"
#include "../error.h"
#include "../smailport.h"

/* functions local to this file */
static char **get_pipe_env __P((struct addr *));
static void get_pipe_ugid __P((struct addr *, uid_t *, gid_t *));

static struct attr_table pipe_attributes[] = {
    { "cmd", t_string, 0, NULL, NULL, OFFSET(pipe_private, cmd) },
    { "user", t_string, 0, NULL, NULL, OFFSET(pipe_private, user) },
    { "group", t_string, 0, NULL, NULL, OFFSET(pipe_private, group) },
    { "umask", t_mode, 0, NULL, NULL, OFFSET(pipe_private, umask) },
    { "handle_write_errors", t_char, 0, NULL, NULL, OFFSET(pipe_private, handle_write_errors) },
    { "bounce_child_exitcodes", t_string, 0, NULL, NULL, OFFSET(pipe_private, bounce_child_exitcodes) },
    { "defer_child_exitcodes", t_string, 0, NULL, NULL, OFFSET(pipe_private, defer_child_exitcodes) },
    { "ignore_child_exitcodes", t_string, 0, NULL, NULL, OFFSET(pipe_private, ignore_child_exitcodes) },
    { "pipe_as_user", t_boolean, 0, NULL, NULL, PIPE_AS_USER },
    { "pipe_as_sender", t_boolean, 0, NULL, NULL, PIPE_AS_SENDER },
    { "ignore_status", t_boolean, 0, NULL, NULL, PIPE_IGNORE_STATUS }, /* DEPRECATED */
    { "log_output", t_boolean, 0, NULL, NULL, PIPE_LOG_OUTPUT },
    { "parent_env", t_boolean, 0, NULL, NULL, PIPE_PARENT_ENV },
    { "ignore_write_errors", t_boolean, 0, NULL, NULL, PIPE_IGNORE_WRERRS }, /* DEPRECATED */
    { "defer_child_errors", t_boolean, 0, NULL, NULL, PIPE_DEFER_ERRORS }, /* DEPRECATED */
    { "status_to_sender", t_boolean, 0, NULL, NULL, PIPE_STATUS_2SENDER },
};
static struct attr_table *end_pipe_attributes = ENDTABLE(pipe_attributes);


/*
 * tdp_pipe - pipe transport driver
 */
void
tpd_pipe(addr, succeed, defer, fail)
    struct addr *addr;			/* recipient addresses for transport */
    struct addr **succeed;		/* successful deliveries */
    struct addr **defer;		/* defer until a later queue run */
    struct addr **fail;			/* failed deliveries */
{
    register struct pipe_private *priv;	/* pipe driver's private data */
    struct transport *tp = addr->transport;
    char *ourcmd;			/* possibly expanded command-line */
    char **argv;			/* args to pass to open_child */
    FILE *child;			/* child process' stdin */
    int status;				/* exit status from child process */
    pid_t pid;				/* pid of child process */
    char **pipe_env;			/* environment to give to children */
    uid_t uid;				/* uid for child process */
    gid_t gid;				/* gid for child process */
    int errfd;				/* child process output file */
    char *error;			/* error from build_cmd_line() */
    char *errstr = NULL;		/* error after close_child() */
    struct error *write_defer = NULL;	/* pipe write error message for defer */
    struct error *write_error = NULL;	/* pipe write error message for bounce */
    unsigned int save_umask;		/* saved value from umask() */

    DEBUG1(DBG_DRIVER_HI, "tpd_pipe called: addr = %s\n", addr->in_addr);
    priv = (struct pipe_private *)tp->private;

    /* subject addresses to retry interval and duration limits */
    addr = retry_addr_before(addr, defer, fail);
    if (addr == NULL) {
	return;
    }

    if (priv->cmd == NULL) {
	/*
	 * ERR_137 - no cmd attribute for pipe
	 *
	 * DESCRIPTION
	 *      No cmd attribute was specified for a pipe transport.  This
	 *      attribute is required.
	 *
	 * ACTIONS
	 *      The message is deferred with a configuration error.
	 *
	 * RESOLUTION
	 *      Correct the entry in the transport file.
	 */
	register struct error *er;

	er = note_error(ERR_CONFERR|ERR_137,
			xprintf("transport %s: no cmd attribute for pipe",
				tp->name));
	insert_addr_list(addr, defer, er);
	return;
    }
    /*
     * This is a special trick because if we are not wrapping the command with
     * /bin/sh (as is preferrable on hosts which support #! since the exit code
     * won't get lost), then we have to expand the $user string *before* we
     * start hacking on it in build_cmd_line() else it will not be broken into
     * separate arguments and execv() will fail.  In theory build_cmd_line()
     * should probably always do this double expansion itself, but for now this
     * hack will have to suffice.  In either case quoting rules are messy.
     */
    if (*(priv->cmd) == '$') {
	/* XXX perhaps we should just use substitute()???? */
	ourcmd = expand_string(priv->cmd, addr, (char *) NULL, (char *) NULL);
    } else {
	ourcmd = priv->cmd;		/* without HAVE_HASH_BANG should be ``/bin/sh -c ${shquote:user}'' */
    }
    /* get the argument vectors for the execv() call */
    argv = build_cmd_line(ourcmd, addr, "", &error);
    if (argv == NULL) {
	/*
	 * ERR_138 - error in cmd attribute
	 *
	 * DESCRIPTION
	 *      build_cmd_line() encountered an error while parsing the cmd
	 *      attribute.  The specific error was returned in `error'.
	 *
	 * ACTIONS
	 *      Defer the message with a configuration error.
	 *
	 * RESOLUTION
	 *      Correct the entry in the transport file to have a valid,
	 *      expandable cmd attribute.
	 */
	struct error *er;

	er = note_error(ERR_CONFERR|ERR_138,
			xprintf("transport %s: error in cmd attribute: %s",
				tp->name, error));
	insert_addr_list(addr, defer, er);
	return;
    }
    if (argv[0][0] != '/') {
	/*
	 * ERR_139 - absolute path for cmd required
	 *
	 * DESCRIPTION
	 *      The first vector from the cmd attribute must be an absolute
	 *      pathname.  Search paths are not used and relative paths do
	 *      not make sense in smail.
	 *
	 * ACTIONS
	 *      Defer the message with a configuration error.
	 *
	 * RESOLUTION
	 *      Correct the entry in the transport file to ensure that the
	 *      first part of the cmd attribute represents an abolute
	 *      pathname.
	 */
	register struct error *er;

	er = note_error(ERR_CONFERR|ERR_139,
			xprintf("transport %s: absolute path for cmd required",
				tp->name));
	insert_addr_list(addr, defer, er);
	return;
    }

    /* get the environment for the child process */
    pipe_env = get_pipe_env(addr);

    /* get the user id and group id */
    get_pipe_ugid(addr, &uid, &gid);

    /*
     * if we are logging output, tie the output to the per-message log file
     */
    if (tp->flags & PIPE_LOG_OUTPUT) {
	if (! msg_logfile) {
	    open_msg_log();
	}
	fflush(msg_logfile);
	errfd = fileno(msg_logfile);
    } else {
	errfd = -1;
    }

    if (dont_deliver) {
	/* succeed everything */
	insert_addr_list(addr, succeed, (struct error *)NULL);
	return;
    }

    if (errfd >= 0) {
	struct addr *cur;
	int first = TRUE;
	char **argp;
	char *cp;
	int c;

	/*
	 * Insert a proper msglog 'X' header opening line which, along with a
	 * trailing timestamp line we'll add at the end, will wrap any stderr
	 * output from the pipe to make a proper msglog entry.
	 *
	 * Note we can't use write_log() as it closes the entry with a
	 * timestamp line immediately.
	 *
	 * XXX using the comma-separated "<in_addr> orig-to: <parent>" format
	 * is a bit of a hack and it isn't well supported by process_msg_log(),
	 * but it's the best I can think to do for the cases where the pipe
	 * driver might be handed multiple addresses.
	 */
	fputs("Xstderr: ", msg_logfile);
	for (cur = addr; cur; cur = cur->succ) {
	    struct addr *top;

	    /* find the top parent to log the original in_addr */
	    for (top = cur; top->parent && top->parent->in_addr; top = top->parent) {
		;
	    }
	    if (top == cur) {
		top = NULL;
	    }
	    dprintf(msg_logfile,
		    "%s<%s>%s%v%s ",
		    first ? "" : ", ",
		    cur->in_addr,
		    top ? " orig-to: <" : "",
		    top ? top->in_addr : "",
		    top ? ">" : "");
	    first = FALSE;
	}
	fputs(" stderr output from pipe:", msg_logfile);
	for (argp = argv; *argp; argp++) {
	    /* XXX we could/should probably try to use quote() here */
	    fputs(" \"", msg_logfile);
	    for (cp = *argp; (c = *cp++); ) {
		if (c == '\\' || c == '"') {
		    fprintf(msg_logfile, "\\%c", c);
		} else if (isprint((int) c)) {
		    putc(c, msg_logfile);
		} else {
		    fprintf(msg_logfile, "\\%03o", c & 0xff);
		}
	    }
	    putc('"', msg_logfile);
	}
	putc('\n', msg_logfile);
	(void) fflush(msg_logfile);
    }
    /*
     * create the child process with the set environment.
     * allow writes to process' stdin, redirect stdout/stderr to errfd,
     * or to DEV_NULL.
     */
#ifndef NODEBUG
    if (debug >= DBG_DRIVER_LO && errfile) {
	char **argp;
	char *cp;
	int c;

	fputs("  pipe: execv:", errfile);
	for (argp = argv; *argp; argp++) {
	    /* XXX we could/should probably try to use quote() here */
	    fputs(" \"", errfile);
	    for (cp = *argp; (c = *cp++); ) {
		if (c == '\\' || c == '"') {
		    fprintf(errfile, "\\%c", c);
		} else if (isprint((int) c)) {
		    putc(c, errfile);
		} else {
		    fprintf(errfile, "\\%03o", c & 0xff);
		}
	    }
	    putc('"', errfile);
	}
	putc('\n', errfile);
    }
#endif	/* NODEBUG */
    save_umask = umask(priv->umask);
    pid = open_child(argv, pipe_env, &child, (FILE **)NULL, errfd,
		     CHILD_DEVNULL, uid, gid);
    (void) umask(save_umask);
    if (pid < 0) {
	/*
	 * ERR_140 - could not create process
	 *
	 * DESCRIPTION
	 *      open_child() failed to create a child process.  This is most
	 *      likely a result of fork() failing due to a lack of available
	 *      process slots, or pipe() failing due to a lack of available
	 *      file descriptors.  The specific error should be available in
	 *      errno.
	 *
	 * ACTIONS
	 *      It will probably be possible for fork() or pipe() to succeed
	 *      sometime in the future, unless a configuration error has
	 *      caused too many file descriptors to remain open, which will
	 *      require a configuration change.  Thus, defer the input
	 *      addresses.
	 *
	 * RESOLUTION
	 *      Hopefully delivery will exceed on a later attempt.
	 */
	register struct error *er;

	er = note_error(ERR_140,
			xprintf("transport %s: could not create process: %s",
				tp->name, strerror(errno)));
	insert_addr_list(addr, defer, er);
	if (errfd >= 0) {
	    /*
	     * remember we're not using write_log(WRITE_LOG_MSG) because it
	     * does the timestamping itself
	     */
	    (void) fputs(er->message, msg_logfile);
	    /* append a timestamp and PID to close the msglog entry */
	    (void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
	    (void) fflush(msg_logfile);
	}
	return;
    }

    /* write out the message */
    if (write_message(child, addr) ||
	fflush(child) == EOF)
    {
	if (priv->handle_write_errors == 'd') {
	    /*
	     * prepare to defer each input address....
	     *
	     * (we can't just move the input addresses to the defer list yet as
	     * we first have to call close_child() and check it's status.)
	     */
	    write_defer = note_error(ERR_141,
				     xprintf("transport %s: write error on pipe (deferring delivery): %s",
					     tp->name, strerror(errno)));
	} else if (priv->handle_write_errors == 'i' || tp->flags & PIPE_IGNORE_WRERRS) {
	    /*
	     * this is a warning only, but do log for each input address.
	     */
	    struct addr *cur;
	    int oerrno = errno;

	    for (cur = addr; cur; cur = cur->succ) {
		write_log(WRITE_LOG_SYS,
			 "note: %s ... transport %s: write on pipe to child PID# [%ld] failed: %s",
			  addr->in_addr, tp->name, (long int) pid, strerror(oerrno));
	    }
	} else {		/* assume (tp->handle_write_errors == 'd') */
	    /*
	     * ERR_141 - write on pipe failed
	     *
	     * DESCRIPTION
	     *      A write to a process created with open_child() failed, and
	     *      the handle_write_errors attribute in the transport instance
	     *      is not set to `i'(gnore) (and the deprecated
	     *      ignore_write_errors attribute is not set either) .
	     *
	     *      This is generally caused by a process exiting before
	     *      reading up to an EOF on standard input.  Sometimes shell
	     *      commands run directly will have side effects but will
	     *      not actually read its stdin.  For example, a forward
	     *      file containing:
	     *
	     *		"|mailx -s \"gone fishing\" $sender < $HOME/.fishing"
	     *
	     *      will send a message to the originator of a message
	     *      stating that the recipient is on vacation.
	     *
	     *      However, as the program (mailx in this case) does not
	     *      actually read the message, but rather reads from a user
	     *      specified file ($HOME/.fishing in this case), a write error
	     *      will occur when Smail attempts to write the file to the
	     *      pipe.
	     *
	     *      The above problem COULD be dealt with by setting the
	     *      appropriate attribute for the "pipe" transport instance in
	     *      question such that write errors are ignored.
	     *
	     *      However a MUCH better solution is to change the forward
	     *      file to call upon a user-defined script:
	     *
	     *		"|$HOME/bin/gone-fishing"
	     *
	     *      That script ($HOME/bin/gone-fishing in this case) should
	     *      something like the following:
	     *
	     *		#! /bin/sh
	     *		:
	     *		# first throw away the incoming message...
	     *		cat > /dev/null
	     *		# then send a reply to the sender
	     *		mailx -s "gone fishing" $SENDER < $HOME/.fishing
	     *
	     *      This script will read and ignore the standard input and
	     *      thus Smail can write the message to the pipe without
	     *      enountering an error.
	     *
	     *      The last possibility is that a command failed before
	     *      reading all of stdin, or the execv() failed in
	     *      open_child(), or the child shell failed to fork() or
	     *      execv(), etc.
	     *
	     * ACTIONS
	     *      Fail the input addresses and send an error to the address
	     *      owner or to the postmaster.  However, if close_child()
	     *      returns a non-zero exit status, such an error has
	     *      precedence over a write error.  The exception is that if
	     *      the status code matches one in the ignore_child_exitcodes
	     *      list, (or the deprecated ignore_status attribute is set),
	     *      then the write_error message is still returned.
	     *
	     *	    If handle_write_errors is set to 'd' (or the deprecated
	     *	    defer_child_errors is set), then defer the message delivery
	     *	    rather than failing it.
	     *
	     * RESOLUTION
	     *      Either scold the user who has written the offending script,
	     *      or fix it yourself!
	     */
	    write_error =
		note_error(ERR_NPOWNER|ERR_141,
			   xprintf("transport %s: write error on pipe: %s",
				   tp->name, strerror(errno)));
	}
    }

    status = close_child(child, (FILE *) NULL, pid);
    if (status == -1 && !(tp->flags & PIPE_IGNORE_STATUS)) {
	/*
	 * ERR_142 - failed to reap child process
	 *
	 * DESCRIPTION
	 *      For some reason, close_pipe() failed to find the child
	 *      process.  This should never happen.
	 *
	 * ACTIONS
	 *	Fail the input addresses and Notify the postmaster of the
	 *	error.  Note: write errors have precedence over this error.
	 *
	 * RESOLUTION
	 *      This is most likely a bug in either Smail or the UNIX
	 *      kernel.
	 */
	if (!write_defer && !write_error) {
	    register struct error *er;

	    er = note_error(ERR_NPOSTMAST|ERR_142,
		      xprintf("transport %s: failed to reap child process: %s",
			      tp->name, strerror(errno)));
	    insert_addr_list(addr, fail, er);
	    if (errfd >= 0) {
		/*
		 * remember we're not using write_log(WRITE_LOG_MSG) because it
		 * does the timestamping itself
		 */
		(void) fputs(er->message, msg_logfile);
		/* append a timestamp and PID to close the msglog entry */
		(void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
		(void) fflush(msg_logfile);
	    }
	    return;
	}
    }
    if (WIFEXITED(status)) {
	if (WEXITSTATUS(status) != 0) {
	    /*
	     * ERR_144 - process returned non-zero status
	     *
	     * DESCRIPTION
	     *      close_child() reaped a non-zero exit status from the
	     *      child process.  This could be okay or it could be bad,
	     *      it is difficult to tell.
	     *
	     * ACTIONS
	     *      If the ignore_status attribute is set, then this situation
	     *      is ignored, except that it is logged in the system file.
	     *      Otherwise, the addresses are failed and the message is
	     *      returned to either the address owner (if valid), or if the
	     *      status_to_sender flag is set then the message is returned
	     *      to the sender, or as a last resort the message is
	     *      "returned" to the postmaster.
	     *
	     * RESOLUTION
	     *      The program run by the transport should be checked to
	     *      determine what the various exit codes mean, and if the
	     *      status is significant.  If the exit status is not
	     *      significant, then ignore_status should be set for the
	     *      transport.
	     *
	     *      Note that on systems without #! support the pipe driver
	     *      wraps the command in /bin/sh which will likely try to
	     *      fork() again to exec() the final pipe command.  The exit
	     *      status therefore may be from the shell and not the final
	     *      pipe command if, for example, the user is out of processes.
	     *      This can be avoided if the pipe command is a simple program
	     *      and is prefixed with the shell builtin "exec" which will
	     *      avoid subsequent fork().  If your OS has #! support
	     *      (i.e. it can exec() scripts) the default pipe driver
	     *      configuration uses just "$user", which will be expanded and
	     *      then made into an argv[] for exec().  In this case the
	     *      shell will not be involved and the exit code will most
	     *      definitely be from the command given.
	     */
	    errstr = xprintf("transport %s: child PID# [%ld] returned status %s (%d)",
			     tp->name, (long int) pid, strsysexit(WEXITSTATUS(status)), WEXITSTATUS(status));

	    /* XXX this first "if" goes away once ignore_status is finally removed */
	    if (!priv->defer_child_exitcodes &&
		!priv->bounce_child_exitcodes &&
		!priv->ignore_child_exitcodes &&
		(tp->flags & (PIPE_IGNORE_STATUS | PIPE_STATUS_2SENDER)) == PIPE_IGNORE_STATUS) {
		/*
		 * if ignore_status is set and status_to_sender is not set then
		 * log errors in the system log file, but don't do anything
		 * about them.
		 */
		struct addr *cur;

		for (cur = addr; cur; cur = cur->succ) {
		    write_log(WRITE_LOG_SYS, "note: %s ... %s", cur->in_addr, errstr);
		}
	    } else {
		if (is_number_in_list((unsigned int) WEXITSTATUS(status), priv->ignore_child_exitcodes)) {
		    /*
		     * this is a warning only, but do log for each input address.
		     */
		    struct addr *cur;
		    
		    for (cur = addr; cur; cur = cur->succ) {
			write_log(WRITE_LOG_SYS, "note: %s ... %s", cur->in_addr, errstr);
		    }
		    if (errfd >= 0) {
			/*
			 * remember we're not using write_log(WRITE_LOG_MSG) because it
			 * does the timestamping itself
			 */
			(void) fprintf(msg_logfile, "note: %s\n", errstr);
			/* append a timestamp and PID to close the msglog entry */
			(void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
			(void) fflush(msg_logfile);
		    }
		    return;
		} else if (is_number_in_list((unsigned int) WEXITSTATUS(status), priv->defer_child_exitcodes)) {
		    insert_addr_list(addr, defer, note_error(ERR_144, errstr));
		    if (errfd >= 0) {
			/*
			 * remember we're not using write_log(WRITE_LOG_MSG) because it
			 * does the timestamping itself
			 */
			(void) fprintf(msg_logfile, "warning: %s\n", errstr);
			/* append a timestamp and PID to close the msglog entry */
			(void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
			(void) fflush(msg_logfile);
		    }
		    return;
		} else if (is_number_in_list((unsigned int) WEXITSTATUS(status), priv->bounce_child_exitcodes)) {
		    insert_addr_list(addr, fail,
				     note_error(((tp->flags & PIPE_STATUS_2SENDER) ? ERR_NSOWNER : ERR_NPOWNER) | ERR_144, errstr));
		    if (errfd >= 0) {
			/*
			 * remember we're not using write_log(WRITE_LOG_MSG) because it
			 * does the timestamping itself
			 */
			(void) fprintf(msg_logfile, "error: %s\n", errstr);
			/* append a timestamp and PID to close the msglog entry */
			(void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
			(void) fflush(msg_logfile);
		    }
		    return;
		} else {
		    /*
		     * XXX once defer_child_errors is finally removed then this
		     * should probably become a panic() since a non-zero exit
		     * code should always match in at least one of the lists.
		     */
		    insert_addr_list(addr,
				     (!priv->defer_child_exitcodes &&
				      (tp->flags & PIPE_DEFER_ERRORS)) ? defer : fail,
				     note_error(((tp->flags & PIPE_STATUS_2SENDER) ? ERR_NSOWNER : ERR_NPOWNER) | ERR_144, errstr));
		    if (errfd >= 0) {
			/*
			 * remember we're not using write_log(WRITE_LOG_MSG) because it
			 * does the timestamping itself
			 */
			(void) fprintf(msg_logfile, "fatal: %s\n", errstr);
			/* append a timestamp and PID to close the msglog entry */
			(void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
			(void) fflush(msg_logfile);
		    }
		    return;
		}
	    }
	}
    } else if (WIFSIGNALED(status)) {
	char signm[SIG2STR_MAX];

	if (sig2str(WTERMSIG(status), signm) == -1) {
	    sprintf(signm, " %d", WTERMSIG(status));
	}
	errstr = xprintf("transport %s: pipe reader PID# [%ld] killed by signal SIG%s %s: %s",
			 tp->name, (long int) pid, signm,
			 WCOREDUMP(status) ? "and dumped core" : "(no core)",
			 strsignal(WTERMSIG(status)));
    } else if (WIFSTOPPED(status)) {
	char signm[SIG2STR_MAX];

	/*
	 * in theory we'll hopefully never see stopped processes...
	 *
	 * in theory we should SIGCONT stopped processes, but maybe someone
	 * will eventually do that for us....  In any case we'll set an error
	 * string and then bounce the message.
	 */
	if (sig2str(WTERMSIG(status), signm) == -1) {
	    sprintf(signm, " %d", WSTOPSIG(status));
	}
	errstr = xprintf("transport %s: pipe reader PID# [%ld] stopped unexpectedly by signal SIG%s: %s",
			 tp->name, (long int) pid, signm, strsignal(WSTOPSIG(status)));
    }
    if (!WIFEXITED(status) && (WIFSIGNALED(status) || WIFSTOPPED(status))) {
	if (errfd >= 0) {
	    /*
	     * remember we're not using write_log(WRITE_LOG_MSG) because it
	     * does the timestamping itself
	     */
	    (void) fputs(errstr, msg_logfile);
	    /* append a timestamp and PID to close the msglog entry */
	    (void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
	    (void) fflush(msg_logfile);
	}
	/*
	 * ERR_143 - process killed/stopped by signal
	 *
	 * DESCRIPTION
	 *      The exit status returned by close_child() revealed that
	 *      the child process was killed by a signal.  This could be
	 *      due to the machine being brought down, if the signal is
	 *      SIGTERM.  Other signals may represent genuine problems.
	 *
	 * ACTIONS
	 *      If the child was killed with SIGTERM, defer the input
	 *      addresses, otherwise fail the addresses and notify the
	 *      postmaster.  This is probably not a problem that address
	 *      owners should have to deal with.  If ignore_status is
	 *      set, then the problem is ignored (except for SIGTERM).
	 *
	 * RESOLUTION
	 *      Time to use your skill in tracking down unusual
	 *      problems, exept of course in the case of SIGTERM....
	 */
	if (WIFSIGNALED(status) && WTERMSIG(status) == SIGTERM) {
	    insert_addr_list(addr, defer, note_error(ERR_143, errstr));
	    return;
	}
	insert_addr_list(addr, fail, note_error(ERR_NPOSTMAST | ERR_143, errstr));
	return;
    }

    if (write_defer) {
	/* no higher-precedence error was found so defer all messages */
	insert_addr_list(addr, defer, write_defer);
    } else if (write_error) {
	/* no higher-precedence error was found so defer or fail as appropriate */
	/* XXX once defer_child_errors is finally removed then this is just "fail"  */
	insert_addr_list(addr,
			 (!priv->handle_write_errors &&
			  !priv->defer_child_exitcodes &&
			  (tp->flags & PIPE_DEFER_ERRORS)) ? defer : fail,
			 write_error);
    } else {
	/* everything went okay, link into the succeed list */
	insert_addr_list(addr, succeed, (struct error *) NULL);
    }
    if (errfd >= 0) {
	/* append a timestamp and PID to close the msglog entry */
	(void) fprintf(msg_logfile, "[%s: [%ld]]\n", time_stamp(), (long int) getpid());
	(void) fflush(msg_logfile);
    }
    return;
}

/*
 * tpb_pipe - read the configuration file attributes for a transport instance
 *            using the "pipe" driver
 */
char *
tpb_pipe(tp, attrs)
    struct transport *tp;		/* transport entry being defined */
    struct attribute *attrs;		/* list of per-driver attributes */
{
    char *error;
    static struct pipe_private pipe_template = {
	NULL,				/* cmd */
	NULL,				/* user */
	NULL,				/* group */
	0,				/* umask */
	'\0',				/* handle_write_errors (XXX set to 'b' when ignore_write_errors is finally gone) */
	NULL,				/* bounce_child_exitcodes (XXX set to "1-127" when ignore_status is finally gone) */
	NULL,				/* defer_child_exitcodes */
	"0",				/* ignore_child_exitcodes */
    };
    struct pipe_private *priv;		/* new pipe_private structure */
    char *fn = xprintf("pipe transport: %s", tp->name);

    /* copy the template private data */
    priv = (struct pipe_private *)xmalloc(sizeof(*priv));
    (void) memcpy((char *)priv, (char *)&pipe_template, sizeof(*priv));

    /* set default flags */
    tp->flags |= PIPE_AS_USER | PIPE_LOG_OUTPUT;

    tp->private = (char *)priv;
    /* fill in the attributes of the private data */
    if ((error = fill_attributes((char *)priv,
				 attrs,
				 &tp->flags,
				 pipe_attributes,
				 end_pipe_attributes,
				 fn))) {
	return error;
    }
    switch (priv->handle_write_errors) {
    case '\0':					/* unset */
    case 'd':					/* 'd'efer */
    case 'b':					/* 'b'ounce */
    case 'i':					/* 'i'gnore */
	break;
    default:
	return xprintf("%s: invalid value for handle_write_errors: '%c'", fn, priv->handle_write_errors);
    }
    xfree(fn);

    return NULL;
}



/*
 * tpp_pipe - dump the configuration attributes for a transport instance using
 *            the "pipe" driver
 */
void
tpp_pipe(f, tp)
     FILE * f;
     struct transport *tp;
{
    (void) dump_standard_config(f,
				tp->private,
				tp->name,
				tp->flags,
				pipe_attributes,
				end_pipe_attributes);
}



/*
 * get_pipe_env - return an environment suitable for the child process
 */
static char **
get_pipe_env(addr)
    struct addr *addr;			/* addrs being delivered to */
{
    struct transport *tp = addr->transport;
    static char *pipe_env[50];		/* lots of space for lots of variables */
    static int inited = FALSE;		/* true if pipe_env is set up */
    char **next_env;			/* for stepping through pipe_env */
    static char **per_address_env;	/* start of per-address environment */
    char *p;
    struct addr *cur;			/* temp */
    struct addr *top;

    if (! inited) {
	char *return_address;

	inited = TRUE;

	/* load environment variables which vary per-message, not per addr */

	next_env = pipe_env;
#ifdef SHELL_EXEC_PATH
	*next_env++ = xprintf("SHELL=%s", SHELL_EXEC_PATH);
#else
	*next_env++ = COPY_STRING("SHELL=/bin/sh");
#endif

#ifdef SECURE_PATH
	*next_env++ = xprintf("PATH=%s", SECURE_PATH);
#else
	*next_env++ = "PATH=/bin:/usr/bin";
#endif

	p = getenv("TZ");
	if (p) {
	    *next_env++ = xprintf("TZ=%s", p);
	}
	return_address = get_return_addr(addr);
	*next_env++ = xprintf("SENDER=<%s>", return_address ? return_address : "MAILER-DAEMON");
	if (return_address) {
	    xfree(return_address);
	}
	*next_env++ = xprintf("MESSAGE_ID=%s", message_id);
	*next_env++ = xprintf("GRADE=%c", msg_grade);
	*next_env++ = xprintf("UUCP_NAME=%s", uucp_name);
	*next_env++ = xprintf("PRIMARY_NAME=%s", primary_name);
	*next_env++ = xprintf("VISIBLE_NAME=%s", visible_name);
	*next_env++ = xprintf("BASENAME=%s", spool_fn);
	*next_env++ = xprintf("SPOOL_FILE=%s/%s", spool_dir, input_spool_fn);

	per_address_env = next_env;
    } else {
	/* free up per-invocation storage used previously */
	char **pp;

	for (pp = per_address_env; *pp; pp++) {
	    xfree(*pp);
	    *pp = NULL;
	}
    }

    /* load environment variables which change per addr */
    next_env = per_address_env;

    /*
     * XXX by default we get our info from the first addr in the list.
     *
     * However strictly speaking we should either forcibly restrict all pipe
     * transports to using max_addrs=1, or else we should invent some syntax
     * (RFC 822 for addresses, colon separated entries for hostnames, etc.?)
     * for combining multiple values into each variable.
     */

    top = addr;
    while (top->parent) {
	top = top->parent;
    }
    if (top->in_addr) {
	*next_env++ = xprintf("ORIG_ADDR=%s", top->in_addr);
    }
    /* XXX this is not usually all that useful -- less so than top->in_addr */
    if (addr->in_addr) {
	*next_env++ = xprintf("INPUT_ADDR=%s", addr->in_addr);
    }
    /*
     * TARGET_DOMAIN should always be the matched domain from hostnames or
     * more_mostnames -- i.e. the domain that ADDR was specified to be in.
     * This is the same as ${target_domain}.
     */
    if (addr->local_name) {
	*next_env++ = xprintf("TARGET_DOMAIN=%s", addr->local_name);
    } else if (addr->parent->local_name) {
	*next_env++ = xprintf("TARGET_DOMAIN=%s", addr->parent->local_name);
    } else {
	*next_env++ = xprintf("TARGET_DOMAIN=%s", primary_name);
    }
    /*
     * The PARENT_ENV feature may be useful for the default transport instance
     * named "pipe" since this will result in the ADDR value being the address
     * which produced the shell command, rather than the shell-command itself.
     */
    if ((tp->flags & PIPE_PARENT_ENV) && addr->parent) {
	/* use parent addr structure for information stuffed in environment */
	if (addr->parent->remainder) {
	    *next_env++ = xprintf("ADDR=%s", addr->parent->remainder);
	}
    } else {
	if (addr->next_addr) {
	    *next_env++ = xprintf("ADDR=%s", addr->next_addr);
	}
	if (addr->next_host) {
	    *next_env++ = xprintf("HOST=%s", addr->next_host);
	}

	/* the following are probably only useful if !PIPE_PARENT_ENV */
	if (addr->rem_prefix) {
	    *next_env++ = xprintf("MBOX_PREFIX=%s", addr->rem_prefix);
	}
	if (addr->rem_suffix) {
	    *next_env++ = xprintf("MBOX_SUFFIX=%s", addr->rem_suffix);
	}
    }
    if (addr->route) {
	*next_env++ = xprintf("ROUTE=%s", addr->route);
    }

    /* is there a suitable username to go into the environment? */
    for (cur = addr; cur && ! (cur->flags & ADDR_ISUSER); cur = cur->parent) {
	;
    }
    if (cur) {	/* yes */
	/* XXX is remainder guaranteed to be set when ADDR_ISUSER is set? */
	*next_env++ = xprintf("USER=%s", cur->remainder);
	*next_env++ = xprintf("LOGNAME=%s", cur->remainder);
	*next_env++ = xprintf("HOME=%s", cur->home ? cur->home : "/");
    }

    /* end of environment */
    *next_env = NULL;

    return pipe_env;
}

/*
 * get_pipe_ugid - return the uid and gid to use for the child process
 */
static void
get_pipe_ugid(addr, uid, gid)
    struct addr *addr;			/* associated addr structures */
    uid_t *uid;				/* store uid here */
    gid_t *gid;				/* store gid here */
{
    struct transport *tp = addr->transport;
    struct pipe_private *priv = (struct pipe_private *)tp->private;

    /*
     * determine the uid to use for delivery
     */
    if (priv->user == NULL) {
	if ((tp->flags & PIPE_AS_SENDER)) {
	    *uid = real_uid;
	} else if ((tp->flags & PIPE_AS_USER) && addr->uid != BOGUS_USER) {
	    *uid = addr->uid;
	} else {
	    *uid = nobody_uid;
	}
    } else {
	struct passwd *pw;

	pw = getpwbyname(FALSE,
			 expand_string(priv->user, (struct addr *) NULL, (char *) NULL, (char *) NULL));
	if (pw == NULL) {
	    write_log(WRITE_LOG_PANIC,
		      "transport %s: warning: user %s unknown, using nobody",
		      tp->name, priv->user);
	    priv->user = NULL;
	    *uid = nobody_uid;
	} else {
	    *uid = pw->pw_uid;
	    *gid = pw->pw_gid;		/* set in case "group" attribute not given */
	}
    }

    /*
     * determine the gid for use for delivery (if not set above)
     */
    if (priv->group == NULL) {
	if (priv->user == NULL) {
	    if ((tp->flags & PIPE_AS_SENDER)) {
		*gid = prog_egid;
	    } else if ((tp->flags & PIPE_AS_USER) && addr->gid != BOGUS_GROUP) {
		*gid = addr->gid;
	    } else {
		*gid = nobody_gid;
	    }
	}
    } else {
	struct group *gr;

	gr = getgrbyname(expand_string(priv->group, (struct addr *) NULL, (char *) NULL, (char *) NULL));
	if (gr == NULL) {
	    write_log(WRITE_LOG_PANIC,
		      "transport %s: warning: group %s unknown, ignored",
		      tp->name, priv->group);
	    priv->group = NULL;
	} else {
	    *gid = gr->gr_gid;
	}
    }
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
