/*
#ident	"@(#)smail/src/transports:RELEASE-3_2_0_121:pipe.h,v 1.8 2004/01/18 02:31:51 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * pipe.h:
 *	interface file for transport driver in pipe.c.
 */

/* structure for pipe driver's private data */
struct pipe_private {
    char *cmd;
    char *user;				/* run as this user */
    char *group;			/* run as this group */
    unsigned int umask;			/* umask for child process */
    int handle_write_errors;		/* how to handle write errors */
    char *bounce_child_exitcodes;	/* bounce if child exit code matches in list */
    char *defer_child_exitcodes;	/* defer if child exit code matches in list */
    char *ignore_child_exitcodes;	/* ignore if child exit code matches in list */
};

/* transport flags private to pipe.c */
#define PIPE_AS_USER	    0x00010000	/* use uid/gid from addr structure */
#define PIPE_IGNORE_STATUS  0x00020000	/* ignore exit status of program (DEPRECATED) */
#define PIPE_AS_SENDER	    0x00040000	/* use uid of sender */
#define PIPE_LOG_OUTPUT	    0x00080000	/* log program output */
#define PIPE_PARENT_ENV     0x00100000	/* stuff env from parent addr */
#define PIPE_IGNORE_WRERRS  0x00200000	/* ignore write errors (DEPRECATED) */
#define PIPE_DEFER_ERRORS   0x00400000	/* defer rather than fail on errors (DEPRECATED) */
#define PIPE_STATUS_2SENDER 0x00800000	/* report exit status to sender */

extern void tpd_pipe __P((struct addr *, struct addr **, struct addr **, struct addr **));
extern char *tpb_pipe __P((struct transport *, struct attribute *));
extern void tpp_pipe __P((FILE *, struct transport *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
