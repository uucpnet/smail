/*
#ident	"@(#)smail/src/transports:RELEASE-3_2_0_121:tcpsmtp.h,v 1.7 2004/01/12 05:44:33 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * tcpsmtp.h:
 *	Interface file for transport driver in tcpsmtp.c.
 */

#ifdef HAVE_BIND
#include "../bindlib.h"
#endif

/* structure for tcpsmtp driver's private data */
struct tcpsmtp_private {
    time_t short_timeout;		/* timeout for short SMTP commands */
    time_t long_timeout;		/* normal SMTP read timeout */
    char *service;			/* service port for SMTP */
#ifdef HAVE_BIND
    struct bindlib_private bindlib_attr;
#endif
};

/* transport flags private to tcpsmtp.c */
#define TCPSMTP_USE_BIND    0x00010000	/* look up next_host with bind */

extern void tpd_tcpsmtp __P((struct addr *, struct addr **, struct addr **, struct addr **));
extern char *tpb_tcpsmtp __P((struct transport *, struct attribute *));
extern void tpp_tcpsmtp __P((FILE *, struct transport *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
