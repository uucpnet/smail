/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:smtplib.h,v 1.19 2005/08/31 17:36:38 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 by Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 *
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smtplib.h:
 *	interface file for routines in smtplib.c.
 */

/* values returned by functions in smtplib.c */
#define SMTP_SUCCEED	0
#define SMTP_NOCONNECT	(-1)
#define SMTP_FAIL	(-2)
#define SMTP_AGAIN	(-3)
#define SMTP_EHLO_FAIL	(-4)

/*
 * NOTE: #define sucks for bit-flags, enum is only slightly better and still
 * requires that we be able to count in binary and understand binary logic
 * idioms.  Bit-fields are the best way to represent flags (except for when you
 * want to mass-assign them all to one value! :-).
 *
 * The current official list of SMTP Service Extension keywords is here:
 *
 * 	http://www.iana.org/assignments/mail-parameters
 */
typedef struct esmtpflags {
    unsigned int ESMTP_basic		: 1; /* got EHLO 250 reply */
    unsigned int ESMTP_8bitmime		: 1; /* rfc1652 */
    unsigned int ESMTP_atrn		: 1; /* rfc2645 */
    unsigned int ESMTP_auth		: 1; /* rfc2645 */
    unsigned int ESMTP_binarymime	: 1; /* rfc3030 */
    unsigned int ESMTP_checkpoint	: 1; /* rfc1845 */
    unsigned int ESMTP_chunking		: 1; /* rfc3030 */
    unsigned int ESMTP_deliverby	: 1; /* rfc2852 */
    unsigned int ESMTP_dsn		: 1; /* rfc1891 */
    unsigned int ESMTP_enhancedstatuscodes : 1;	/* rfc2034 */
    unsigned int ESMTP_etrn		: 1; /* rfc1985 */
    unsigned int ESMTP_expn		: 1; /* optional cmd, as per rfc[2]821+rfc1869 */
    unsigned int ESMTP_help		: 1; /* optional cmd, as per rfc[2]821+rfc1869 */
    unsigned int ESMTP_onex		: 1; /* One message transaction only [Eric Allman] */
    unsigned int ESMTP_pipelining	: 1; /* rfc2920 */
    unsigned int ESMTP_saml		: 1; /* optional cmd, as per rfc[2]821+rfc1869 */
    unsigned int ESMTP_send		: 1; /* optional cmd , as per rfc[2]821+rfc1869 */
    unsigned int ESMTP_size		: 1; /* rfc1870 */
    unsigned int ESMTP_soml		: 1; /* optional cmd, as per rfc[2]821+rfc1869 */
    unsigned int ESMTP_starttls		: 1; /* rfc3207 */
    unsigned int ESMTP_turn		: 1; /* optional cmd, as per rfc[2]821+rfc1869 */
    unsigned int ESMTP_verb		: 1; /* Verbose [Eric Allman] */
    unsigned int ESMTP_vrfy		: 1; /* not optional, but some think it is */
    unsigned int ESMTP_x		: 1; /* unspported, non-standard, unregistered */
} esmtpf_t;

/*
 * the following structure is passed around between SMTP functions and
 * should be initialized as necessary to describe the SMTP connection
 * characteristics.
 *
 * NOTE:  If "in" is set to NULL, then we will be producing batch SMTP.
 */
struct smtp {
    FILE *in;				/* input channel from remote server */
    FILE *out;				/* output channel to remote server */
    char *server_name;			/* name of the remote server */
    unsigned short_timeout;		/* timeout period for short commands */
    unsigned long_timeout;		/* normal SMTP read timeout period */
    char *nl;				/* line terminator string */
    struct transport *tp;		/* associated transport */
    esmtpf_t esmtp_flags;		/* ESMTP extensions supported by remote */
    unsigned long max_size;		/* message size limit of remote */
};

/* functions defined in smtplib.c */
extern int smtp_startup __P((struct smtp *, struct error **, int));
extern int smtp_send __P((struct smtp *, struct addr *, struct addr **, struct addr **, struct addr **, int, struct error **));
extern void smtp_shutdown __P((struct smtp *));
extern int rfc821_is_dot_string __P((char *));
extern int rfc821_is_quoted_string __P((char *));
extern char *rfc821_make_quoted_string __P((char *, char *));
extern char *rfc821_quote_the_local_part __P((char *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
