/*
#ident	"@(#)smail/src/transports:RELEASE-3_2_0_121:smtplib.c,v 1.85 2005/11/15 01:17:03 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smtplib.c:
 *	Send mail using the SMTP protocol.  This soure file is a set of library
 *	routines that can be used by transport drivers that can handle creating
 *	the virtual circuit connections.  Some of these routines are also used
 *	in ../smtprecv.c.
 *
 * A NOTE ABOUT SMTP ERROR LOGGING:
 *
 * Note that the reply from the remote server should be logged (with
 * write_log() and/or note_error(), as appropriate) starting with a preceding
 * newline so that entire mult-line replies will remain intact and properly
 * formatted in the log files.  There is code in log.c send_log(), for example,
 * which properly parses these messages in such a way that they will be treated
 * as the single entry that they are.
 */

#define NEED_SOCKETS
#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <setjmp.h>
#include <signal.h>
#include <ctype.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include "../smail.h"
#include "../alloc.h"
#include "../list.h"
#include "../smailsock.h"
#include "../main.h"
#include "../jump.h"
#include "../parse.h"
#include "../addr.h"
#include "../exitcodes.h"
#include "../log.h"
#include "../spool.h"
#include "../smailstring.h"
#include "../dys.h"
#include "../transport.h"
#include "smtplib.h"
#include "../extern.h"
#include "../error.h"
#include "../debug.h"
#include "../smailport.h"

/* reply code groups, encoded in hex */
#define POSITIVE_DEBUG		0x000	/* debugging messages */
#define POSITIVE_PRELIM		0x100	/* positive preliminary replies */
#define POSITIVE_COMPLETE	0x200	/* positive completion replies */
#define POSITIVE_INTERMEDIATE	0x300	/* positive intermediate replies */
#define NEGATIVE_TRY_AGAIN	0x400	/* transient negative completion */
#define NEGATIVE_FAILED		0x500	/* permanent negative completion */
#define PSEUDO_TRY_AGAIN	0x800	/* pseudo transient neg completion */
#define PSEUDO_FAILED		0x900	/* pseudo permanent neg completion */

#define REPLY_GROUP(c)	   ((c)&0xf00)	/* mask out reply code group */

/* specific reply codes */
#define REPLY_READY		0x220	/* SMTP service ready */
#define REPLY_FINISHED		0x221	/* SMTP service finished, closing */
#define REPLY_OK		0x250	/* successful command completion */
#define REPLY_WILLFORWARD	0x251	/* user address will be forwarded */
#define REPLY_START_DATA	0x354	/* okay to start sending message */
#define REPLY_DOWN		0x421	/* remote SMTP closing down */
#define REPLY_MAILBOX_UNAVAIL	0x450	/* mailbox busy */
#define REPLY_REMOTE_FAILURE	0x451	/* remote error in processing */
#define REPLY_STORAGE_FULL	0x452	/* remote system storage full */
#define REPLY_COMMAND_ERROR	0x500	/* Syntax error in command */
#define REPLY_PARAMETER_ERROR	0x501	/* Syntax error in parameter(s) */
#define REPLY_CMD_NOT_IMPL	0x502	/* Command recognized but not implemented */
#define REPLY_SEQUENCE_ERROR	0x503	/* Bad sequence of commands */
#define REPLY_BAD_PARAMETER	0x504	/* Parameter recognized but not implemented */
#define REPLY_UNKNOWN_USER	0x550	/* remote says mailbox unknown */
#define REPLY_USER_NOT_LOCAL	0x551	/* remote says mailbox is not local */
#define REPLY_OVER_QUOTA	0x552	/* remote says mailbox exceeded storage quota */
#define REPLY_ILLEGAL_USER	0x553	/* remote says mailbox name not allowed */
#define REPLY_TRANS_FAILURE	0x554	/* transaction failed (no SMTP service avail, no valid recipients) */
#define REPLY_ESMTP_PARAM_ERR	0x555	/* unsupported or invalid ESMTP RCPT_TO or MAIL_FROM parameter */

/* pseudo-reply codes */
#define REPLY_EOF		0x851	/* EOF on read */
#define REPLY_TIMEOUT		0x852	/* timeout on read */
#define REPLY_PROTO_ERROR	0x903	/* SMTP protocol error on read */

/* variables local to this file */
static struct str smtp_out;		/* region for outgoing commands */
static struct str smtp_in;		/* region from incoming responses */
static char *greeting_name;		/* intialized with the HELO/EHLO parameter */
static int smtp_init_flag = FALSE;	/* TRUE if SMTP system initialized */
static JUMP_ENVBUF timeout_buf;		/* timeouts jump here */

/* functions local to this file */
static char *skip_to_eol __P((char *));
static void log_malformed_ehlo_reply __P((char *, char *));
static void do_smtp_shutdown __P((struct smtp *, int));
static int write_command_maybe_wait __P((struct smtp *, unsigned int, char *, unsigned int, char **));
static int wait_write_command __P((struct smtp *, unsigned int, char *, unsigned int, char **));
static int write_command_nowait __P((struct smtp *, char *, unsigned int));
static int flush_command_stream __P((struct smtp *, char **));
static int wait_read_response __P((struct smtp *, unsigned int, char **));
static int read_response_internal __P((struct smtp *, unsigned int, char **));
static void catch_timeout __P((int));
static struct error * no_remote __P((struct transport *, char *));
static struct error * try_again __P((struct transport *, char *));
static struct error * fatal_error __P((struct transport *, char *));
static struct error * remote_storage_full __P((struct transport *, char *));
static struct error * remote_mailbox_unavail __P((struct transport *, char *));
static struct error * remote_failure __P((struct transport *, char *));
static struct error * remote_unknown_user __P((struct transport *, char *));
static struct error * remote_user_not_local __P((struct transport *, char *));
static struct error * remote_over_quota __P((struct transport *, char *));
static struct error * remote_illegal_user __P((struct transport *, char *));
static struct error * write_failed __P((struct transport *));
static struct error * read_failed __P((struct transport *)); 

/*
 * smtp_startup - initiate contact on an SMTP connection
 *
 * given input and output channels to a remote SMTP server, initiate
 * the session for future mail commands.  Once the startup has been
 * acomplished, smtp_send() can be used to send individual messages.
 *
 * If try_ehlo is non-zero and ESMTP support is configured, perform
 * ESMTP negotiaition using the EHLO greeting command.
 *
 * return:
 *	SMTP_SUCCEED on successful startup
 *	SMTP_FAIL if the connection should not be retried
 *	SMTP_NOCONNECT if another MX host should be tried
 *	SMTP_AGAIN if the connection should be retried later
 *	SMTP_EHLO_FAIL if the receiver hung up in response
 *		to an EHLO command.  This can only occur when
 *		try_ehlo is set (and HAVE_EHLO is defined).
 *
 * For SMTP_FAIL, SMTP_NOCONNECT, and SMTP_AGAIN (but not SMTP_EHLO_FAIL),
 * return a filled-in error structure.
 */
int
smtp_startup(smtpb, error_p, try_ehlo)
    struct smtp *smtpb;			/* SMTP description block */
    struct error **error_p;		/* error description */
    int try_ehlo;			/* whether to use ESMTP */
{
    int reply = REPLY_OK;		/* If we're in batch mode, always
					 * return success. */
    char *reply_text;
    char *p;

    if (! smtp_init_flag) {
	STR_INIT(&smtp_in);
	STR_INIT(&smtp_out);
	smtp_init_flag = TRUE;
# ifdef HAVE_BSD_NETWORKING
	if (sending_name && get_inet_addr(sending_name) != INADDR_NONE) {
	    greeting_name = xprintf("[%s]", sending_name);
	} else
# endif
	{
	    greeting_name = xprintf("%v", sending_name ? sending_name : primary_name);
	}
    }
    /*
     * wait for the server to say it is ready.
     *
     * Possible reponses:
     *	220 - service ready (continue conversation)
     *  421 - closing down  (try again later)
     *	5xx - service closed (bounce!)
     *
     *  852 - internal error representing a read timeout/disconnect
     *  903 - internal error for something other than an SMTP reply!
     */
    if (smtpb->in) {
	DEBUG(DBG_DRIVER_MID, "\n");	/* we're about to get more debug! */
	reply = wait_read_response(smtpb, smtpb->short_timeout, &reply_text);
    }
    if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	/* read/proto error, try another MX or try again later */
	*error_p = remote_failure(smtpb->tp, reply_text);
	return SMTP_NOCONNECT;		/* skip this host... */
    }
    if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	/* what a rude thing to say right off the top! */
	*error_p = fatal_error(smtpb->tp, reply_text);
	return SMTP_FAIL;		/* bouncey... bouncey... */
    }
    if (reply != REPLY_READY) {
	/* didn't get a legal "OK" reponse, try again later */
	*error_p = no_remote(smtpb->tp, reply_text);
	return SMTP_AGAIN;		/* retry later... */
    }
    if ((p = strchr(&reply_text[4], ' '))) {	/* only a space is a legal separator in RFC2821 */
	*p = '\0';
    }
    if (strncmpic(&reply_text[4], greeting_name, strlen(greeting_name)) == 0) {
	char *msg = xprintf("server '%v' greeted me with my own name: %s",
			    smtpb->server_name, reply_text);

	if (p) {
	    *p = ' ';
	}
	*error_p = fatal_error(smtpb->tp, msg);
	xfree(msg);
	return SMTP_FAIL;		/* bouncy... bouncey... */
    }
    if (p) {
	*p = ' ';
    }

    smtpb->esmtp_flags.ESMTP_basic = 0;
    smtpb->esmtp_flags.ESMTP_8bitmime = 0;
    smtpb->esmtp_flags.ESMTP_atrn = 0;
    smtpb->esmtp_flags.ESMTP_auth = 0;
    smtpb->esmtp_flags.ESMTP_binarymime = 0;
    smtpb->esmtp_flags.ESMTP_checkpoint = 0;
    smtpb->esmtp_flags.ESMTP_chunking = 0;
    smtpb->esmtp_flags.ESMTP_deliverby = 0;
    smtpb->esmtp_flags.ESMTP_dsn = 0;
    smtpb->esmtp_flags.ESMTP_enhancedstatuscodes = 0;
    smtpb->esmtp_flags.ESMTP_etrn = 0;
    smtpb->esmtp_flags.ESMTP_expn = 0;
    smtpb->esmtp_flags.ESMTP_help = 0;
    smtpb->esmtp_flags.ESMTP_onex = 0;
    smtpb->esmtp_flags.ESMTP_pipelining = 0;
    smtpb->esmtp_flags.ESMTP_saml = 0;
    smtpb->esmtp_flags.ESMTP_send = 0;
    smtpb->esmtp_flags.ESMTP_size = 0;
    smtpb->esmtp_flags.ESMTP_soml = 0;
    smtpb->esmtp_flags.ESMTP_starttls = 0;
    smtpb->esmtp_flags.ESMTP_turn = 0;
    smtpb->esmtp_flags.ESMTP_verb = 0;
    smtpb->esmtp_flags.ESMTP_vrfy = 0;
    smtpb->esmtp_flags.ESMTP_x = 0;

#ifdef HAVE_EHLO
    if (try_ehlo) {
	/*
	 * say who we are the new fangled way....
	 * Possible responses:
	 *	250 - okay	    (continue conversation)
	 *	421 - closing down  (try again later)
	 *	5xx - fatal error   (try HELO, do not bounce!)
	 */
	STR_CLEAR(&smtp_out);
	(void) str_printf(&smtp_out, "EHLO %s", greeting_name);

	reply = wait_write_command(smtpb, smtpb->short_timeout,
				   STR(&smtp_out), (unsigned int) STR_LEN(&smtp_out),
				   &reply_text);

	if (reply == REPLY_TIMEOUT || reply == REPLY_EOF) {
	    /*
	     * Some broken servers just terminate the conection when they
	     * receive an EHLO.  As per RFC 1869 4.7 we handle this case by
	     * returning a special value here that can optionally be used to
	     * force re-connecting to the buggy host and only issuing a HELO.
	     */
	    return SMTP_EHLO_FAIL;
	}
	if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    /* some other error (REPLY_PROTO_ERROR) */
	    *error_p = no_remote(smtpb->tp, reply_text);
	    return SMTP_NOCONNECT;	/* skip this host... */
	}
	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN) {
	    /* remote SMTP closing....  try again later */
	    /*
	     * Some mis-configured Postfix servers have been known to return
	     * "402 Error: command not implemented" in response to unknown
	     * commands.  If such a server lives behind a mis-configured Cisco
	     * PIX firewall this will happen when we send EHLO and we will
	     * never be able to establish a successful SMTP session.
	     *
	     * The proper fix is to issue a "no fixup protocol smtp 25" command
	     * to the Cisco (and save the config to make it permanent, of
	     * course) _AND_ then to also turn off 'soft_bounce' in the Postfix
	     * server.  If the remote server's administrator(s) only do the
	     * latter and do not also fix their firewall the then a slightly
	     * more correct "502 Error:  command not implemented" will be
	     * returned by Postfix forcing us to forego ESMTP and fall back to
	     * sending a HELO.
	     * 
	     * This is all because the PIX fails to implement ESMTP, and in
	     * doing so it "helpfully" translates the EHLO command into an
	     * "XXXX" command.  In combination with the Postfix 'soft_bounce'
	     * testing feature this causes an irreconcilable deadlock.  The
	     * ESMTP specification is very nearly a decade old.  The latest
	     * SMTP RFC (2821) includes EHLO as a mandatory feature by default.
	     *
	     * Unfortunately this is not the only SMTP violation committed by
	     * the PIX.  You can recognize it because it translates all
	     * characters except '2', '0', and ' ' in the 220 greeting to
	     * asterisks, thus obliterating the mandatory 220 host name
	     * parameter as well as any explicit indication of ESMTP support.
	     *
	     * The Cisco Firewall Team appear to be stuck back in 1992 or
	     * earlier....
	     *
	     * Postfix postmasters who are forced to live behind a broken PIX
	     * by lame-brained idiot security officers are never going to be
	     * able to successfully use 'soft_bounce' for testing (not that
	     * it's a good idea to use this so-called feature, even just for
	     * testing -- test the real behaviour, not some broken imitation).
	     *
	     * In any case don't ask me to break Smail!  Smail's not the only
	     * mailer which will refuse to deal with this situation.  At least
	     * this way you won't have to un-freeze any messages destined to
	     * the broken site and they'll be delivered almost as soon as the
	     * remote configurations are fixed.
	     */
	    *error_p = try_again(smtpb->tp, reply_text);
	    return SMTP_AGAIN;		/* he says we should come back later... */
	}
	if (reply == REPLY_OK) {
	    char * cp = reply_text;
	    int on_greet_line = 1;	/* we start on the greeting line */

	    smtpb->esmtp_flags.ESMTP_basic = 1;
	    /*
	     * Parse the EHLO reply to find out what ESMTP options the remote
	     * server supports....
	     */
	    while (*cp) {
		char *pp;

		/*
		 * every line must begin with the three digits "250", followed
		 * by a '-' for continued lines, and a ' ' for the final line
		 */
		if (strncmp(cp, "250", (size_t) 3) != 0) {
		    log_malformed_ehlo_reply("Not a 250 reply!", reply_text);
		    return SMTP_SUCCEED;
		}
		cp += 3;
		if (*cp != ' ' && *cp != '-') {
		    log_malformed_ehlo_reply("250 not followed by space or dash", reply_text);
		    return SMTP_SUCCEED;
		}
		cp++;			/* skip the ' ' or '-' */
		if (on_greet_line) {
		    /*
		     * Ignore greeting on first line
		     */
		    on_greet_line = 0;
		    cp = skip_to_eol(cp);
		    continue;
		}
		/*
		 * search for whitespace (or newline) after the keyword
		 */
		for (pp = cp; *(pp) && !isspace((int) *pp); ++pp) {
		     ;
		}
		/*
		 * in the ESMTP greeting the parameter values are separated
		 * by whitespace, not an equal sign!  We'll generously skip
		 * any whitespace before a newline too...
		 */
		while (isspace((int) *pp)) {
		    ++pp;
		}
		/*
		 * decipher which keyword it is...
		 */
		if (strncmpic(cp, "8BITMIME", sizeof("8BITMIME")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_8bitmime = 1;
		} else if (strncmpic(cp, "ATRN", sizeof("ATRN")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_atrn = 1;
		} else if (strncmpic(cp, "AUTH", sizeof("AUTH")-1) == 0 && cp[sizeof("AUTH")-1] == ' ') {
		    /* XXX followed by a space separated list of the names of supported SASL mechanisms */
		    smtpb->esmtp_flags.ESMTP_auth = 1;
		} else if (strncmpic(cp, "BINARYMIME", sizeof("BINARYMIME")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_binarymime = 1;
		} else if (strncmpic(cp, "CHECKPOINT", sizeof("CHECKPOINT")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_binarymime = 1;
		} else if (strncmpic(cp, "CHUNKING", sizeof("CHUNKING")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_chunking = 1;
		} else if (strncmpic(cp, "DELIVERBY", sizeof("DELIVERBY")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_deliverby = 1;
		} else if (strncmpic(cp, "DSN", sizeof("DSN")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_dsn = 1;
		} else if (strncmpic(cp, "ENHANCEDSTATUSCODES", sizeof("ENHANCEDSTATUSCODES")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_enhancedstatuscodes = 1;
		} else if (strncmpic(cp, "ETRN", sizeof("ETRN")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_etrn = 1;
		} else if (strncmpic(cp, "EXPN", sizeof("EXPN")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_expn = 1;
		} else if (strncmpic(cp, "HELP", sizeof("HELP")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_help = 1;
		} else if (strncmpic(cp, "ONEX", sizeof("ONEX")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_onex = 1;
		} else if (strncmpic(cp, "PIPELINING", sizeof("PIPELINING")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_pipelining = 1;
		} else if (strncmpic(cp, "SAML", sizeof("SAML")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_saml = 1;
		} else if (strncmpic(cp, "SEND", sizeof("SEND")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_send = 1;
		} else if (strncmpic(cp, "SIZE", sizeof("SIZE")-1) == 0) {
		    char *errbuf = NULL;
		    char *ep = strchr(cp, '\n');

		    if (ep+1 == pp) {	/* pp points at next line if no parameter */
			cp = skip_to_eol(cp);
			continue;
		    }
		    if (ep) {
			*ep = '\0';
		    }
		    smtpb->esmtp_flags.ESMTP_size = 1;
		    smtpb->max_size = c_atol(pp, &errbuf); /* XXX this is very generous! */
		    if (ep) {
			*ep = '\n';
		    }
		    if (errbuf) {
			log_malformed_ehlo_reply(errbuf, reply_text);
			cp = skip_to_eol(cp);
			continue;
		    }
		} else if (strncmpic(cp, "SOML", sizeof("SOML")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_soml = 1;
		} else if (strncmpic(cp, "STARTTLS", sizeof("STARTTLS")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_starttls = 1;
		} else if (strncmpic(cp, "TURN", sizeof("TURN")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_turn = 1;
		} else if (strncmpic(cp, "VERB", sizeof("VERB")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_verb = 1;
		} else if (strncmpic(cp, "VRFY", sizeof("VRFY")-1) == 0) {
		    smtpb->esmtp_flags.ESMTP_vrfy = 1;
		} else if (*cp == 'X' || *cp == 'x') {
		    smtpb->esmtp_flags.ESMTP_x = 1;
		} else {
		    char *ep = strchr(cp, '\n');

		    if (ep) {
			*ep = '\0';
		    }
# ifdef NO_LOG_EHLO
		    DEBUG1(DBG_DRIVER_LO, "smtp_startup(): destination offers unsupported ESMTP option '%v'.\n", cp);
# else
		    write_log(WRITE_LOG_SYS, "destination offers unsupported ESMTP option '%v'.", cp);
# endif
		    if (ep) {
			*ep = '\n';
		    }
		}
		cp = skip_to_eol(cp);
	    }
	    DEBUG9(DBG_DRIVER_MID, "destination supports %s%s%s%s%s%s%s%s%s",
		   smtpb->esmtp_flags.ESMTP_basic ? "ESMTP" : "[INTERNAL ERROR!]",
		   smtpb->esmtp_flags.ESMTP_8bitmime ? " 8BITMIME" : "",
		   smtpb->esmtp_flags.ESMTP_atrn ? " ATRN" : "",
		   smtpb->esmtp_flags.ESMTP_auth ? " AUTH" : "",
		   smtpb->esmtp_flags.ESMTP_binarymime ? " BINARYMIME" : "",
		   smtpb->esmtp_flags.ESMTP_checkpoint ? " CHECKPOINT" : "",
		   smtpb->esmtp_flags.ESMTP_chunking ? " CHUNKING" : "",
		   smtpb->esmtp_flags.ESMTP_deliverby ? " DELIVERBY" : "",
		   smtpb->esmtp_flags.ESMTP_dsn ? " DSN" : "");
	    DEBUG9(DBG_DRIVER_MID, "%s%s%s%s%s%s%s%s%s",
		   smtpb->esmtp_flags.ESMTP_enhancedstatuscodes ? " ENHANCEDSTATUSCODES" : "",
		   smtpb->esmtp_flags.ESMTP_etrn ? " ETRN" : "",
		   smtpb->esmtp_flags.ESMTP_expn ? " EXPN" : "",
		   smtpb->esmtp_flags.ESMTP_help ? " HELP" : "",
		   smtpb->esmtp_flags.ESMTP_onex ? " ONEX" : "",
		   smtpb->esmtp_flags.ESMTP_pipelining ? " PIPELINING" : "",
		   smtpb->esmtp_flags.ESMTP_saml ? " SAML" : "",
		   smtpb->esmtp_flags.ESMTP_send ? " SEND" : "",
		   smtpb->esmtp_flags.ESMTP_size ? " SIZE" : "");
	    DEBUG6(DBG_DRIVER_MID, "%s%s%s%s%s.\n",
		   smtpb->esmtp_flags.ESMTP_soml ? " SOML" : "",
		   smtpb->esmtp_flags.ESMTP_starttls ? " STARTTLS" : "",
		   smtpb->esmtp_flags.ESMTP_turn ? " TURN" : "",
		   smtpb->esmtp_flags.ESMTP_verb ? " VERB" : "",
		   smtpb->esmtp_flags.ESMTP_vrfy ? " VRFY" : "",
		   smtpb->esmtp_flags.ESMTP_x ? " X*(some-vendor-feature)" : "");
	    /*
	     * If debugging is turned on high enough, and if the remote server
	     * advertised "VERB" then ask the remote server for more verbose
	     * replies.  This often doesn't do much with modern Internet
	     * mailers, but it shouldn't hurt if they advertised it!
	     */
	    if (smtpb->esmtp_flags.ESMTP_verb && debug >= DBG_DRIVER_MID) {
		reply = wait_write_command(smtpb, smtpb->short_timeout,
					   "VERB", (unsigned int) sizeof("VERB") - 1,
					   &reply_text);
		if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
		    /* some other error ... */
		    *error_p = no_remote(smtpb->tp, reply_text);
		    return SMTP_NOCONNECT;	/* dropped, skip this host... */
		}
		if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN) {
		    /* remote SMTP closed, try again later */
		    *error_p = try_again(smtpb->tp, reply_text);
		    return SMTP_AGAIN;
		}
		if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
		    /* very very very rude! */
		    *error_p = fatal_error(smtpb->tp, reply_text);
		    return SMTP_FAIL;	/* bouncey... bouncey... */
		}
	    }
	    return SMTP_SUCCEED;
	}
	/*
	 * Fall through and try an old-fashioned HELO
	 */
    }
#endif /* HAVE_EHLO */
    /*
     * say who we are the old fashioned way....
     * Possible responses:
     *	250 - okay	    (continue conversation)
     *	421 - closing down  (try again later)
     *  5xx - fatal error   (return message to sender)
     */
    STR_CLEAR(&smtp_out);
    (void) str_printf(&smtp_out, "HELO %s", greeting_name);

    reply = wait_write_command(smtpb, smtpb->short_timeout,
			       STR(&smtp_out), (unsigned int) STR_LEN(&smtp_out),
			       &reply_text);
    if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	/* some other error ... */
	*error_p = no_remote(smtpb->tp, reply_text);
	return SMTP_NOCONNECT;	/* skip this host... */
    }
    if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN) {
	/* remote SMTP closed, try again later */
	*error_p = try_again(smtpb->tp, reply_text);
	return SMTP_AGAIN;
    }
#ifndef HAVE_EHLO
    if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	/* what a rude thing to say right off the top! */
	*error_p = fatal_error(smtpb->tp, reply_text);
	return SMTP_FAIL;	/* bouncey... bouncey... */
    }
#else /* HAVE_EHLO */
    /*
     * RFC 1869 4.7 warns that some really broken implementations may not
     * accept HELO after they have issued a failure response to EHLO, and we
     * may may try to compensate by sending an RSET before sending the HELO,
     * and ignore any error response from the RSET.
     */
    if (reply == REPLY_SEQUENCE_ERROR) {
	/* log 503 Bad sequence of commands errors */
	write_log(WRITE_LOG_SYS, "%x after bad EHLO followed by HELO:\n%s", reply, reply_text);
    } else if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	/* what a rude thing to say right off the top! */
	*error_p = fatal_error(smtpb->tp, reply_text);
	return SMTP_FAIL;	/* bouncey... bouncey... */
    }
    if (reply != REPLY_OK) {	/* try RSET and then HELO one more time */
	reply = wait_write_command(smtpb, smtpb->short_timeout,
				   "RSET", (unsigned int) sizeof("RSET") - 1,
				   &reply_text);
	if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    /* some other error ... */
	    *error_p = no_remote(smtpb->tp, reply_text);
	    return SMTP_NOCONNECT;	/* skip this very broken host... */
	}
	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN) {
	    /* remote SMTP closed, try again later */
	    *error_p = try_again(smtpb->tp, reply_text);
	    return SMTP_AGAIN;
	} else if (reply != REPLY_OK) {
	    /*
	     * Some extremely buggy servers don't accept any commands before
	     * they get a HELO, not even RSET.  Fortunately they are usually so
	     * buggy that it is safe to just ignore their error messages and
	     * simply carry on....
	     */
	    write_log(WRITE_LOG_SYS, "unexpected response to RSET:\n%s", reply_text);
	}
	/*
	 * the RSET command has been sent; try the old HELO again...
	 */
	STR_CLEAR(&smtp_out);
	(void) str_printf(&smtp_out, "HELO %s", greeting_name);

	reply = wait_write_command(smtpb, smtpb->short_timeout,
				   STR(&smtp_out), (unsigned int) STR_LEN(&smtp_out),
				   &reply_text);
	if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    /* some other error */
	    *error_p = no_remote(smtpb->tp, reply_text);
	    return SMTP_NOCONNECT;	/* skip this host... */
	}
	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN) {
	    /* remote SMTP closed, try again later */
	    *error_p = try_again(smtpb->tp, reply_text);
	    return SMTP_AGAIN;
	}
	if (reply == REPLY_SEQUENCE_ERROR) {
	    write_log(WRITE_LOG_SYS, "%x after EHLO/HELO/RSET/HELO:\n%s", reply, reply_text);
	}
    }
#endif
    if (reply != REPLY_OK) {
	/* some other fatal error, return message to sender */
	*error_p = fatal_error(smtpb->tp, reply_text);
	smtp_shutdown(smtpb);	/* be nice and say goodbye! */
	return SMTP_FAIL;	/* bouncey... bouncey.... */
    }

    /* connection established */
    return SMTP_SUCCEED;
}

/*
 * custom code to safely skip to the end of a line.
 */
static char *
skip_to_eol(cp)
    char *cp;
{
	while (*cp && *cp != '\n') {
	    ++cp;
	}
	if (*cp == '\n') {
	    ++cp;
	}
	return cp;
}

/*
 * This seems to be a reasonable way to handle malformed EHLO replies....
 */
static void
log_malformed_ehlo_reply(error_msg, reply_text)
    char *error_msg;
    char *reply_text;
{
#ifdef NO_LOG_EHLO
    DEBUG2(DBG_DRIVER_LO, "destination supports esmtp, but is buggy: %v;\n%s\n",
	   error_msg, reply_text);
#else
    /* XXX this is really ugly though as it leaves the newlines in place,
     * though it should always be with normal SMTP continuation lines so
     * shouldn't be too hard to parse....
     */
    write_log(WRITE_LOG_SYS, "destination supports esmtp, but is buggy: %s;\n%s",
	      error_msg, reply_text);
#endif /* not NO_LOG_EHLO */
    return;
}



/*
 * smtp_send - mail a message to a remote SMTP server
 *
 * Using a virtual circuit connection of some kind, transmit
 * the current spooled message to the given set of recipient
 * addresses.  If the circuit has only a write channel, and no read
 * channel, then transmit batch SMTP.
 *
 * Return:
 *	SUCCEED	- more smtp messages can be sent.
 *	FAIL	- don't try to send any more messages.
 *
 * For FAIL, always returns a filled-in error structure.
 */
int
smtp_send(smtpb, addr, succeed, defer, fail, defer_failure, error_p)
    struct smtp *smtpb;			/* SMTP description block */
    struct addr *addr;			/* list of recipient addresses */
    struct addr **succeed;		/* successful addresses */
    struct addr **defer;		/* addresses to be retried */
    struct addr **fail;			/* failed addresses */
    int defer_failure;			/* should we keep this one? */
    struct error **error_p;		/* error description */
{
    register int reply;			/* reply code from SMTP commands */
    char *reply_text = NULL;		/* text of reply */
    int success;			/* success value from calls */
    struct addr *cur;			/* current address being sent */
    struct addr *next;			/* next addr to send */
    struct addr *okay = NULL;		/* partially successful addrs */
    struct transport *tp = smtpb->tp;
    char *return_address;

    if (! smtp_init_flag) {
	STR_INIT(&smtp_in);
	STR_INIT(&smtp_out);
	smtp_init_flag = TRUE;
    }
    STR_CLEAR(&smtp_out);

    /*
     * send MAIL command
     *
     * signal that we are sending a new message,
     * and give the sender address for it.
     */
    return_address = get_return_addr(addr);
    if (!return_address) {
	*error_p = try_again(tp, "error getting return address");
	insert_addr_list(addr, defer_failure ? defer : fail, *error_p);
	do_smtp_shutdown(smtpb, REPLY_OK);
	return FAIL;
    }
    str_printf(&smtp_out, "MAIL FROM:<%s>", rfc821_quote_the_local_part(return_address));
    xfree(return_address);

    /*
     * If the remote-SMTP supports the ESMTP size option then send (a guess of)
     * the size of the message to be transported.  Of course the guess could be
     * a bit more educated than it currently is, but usually it doesn't matter
     * if it is slightly incorrect.  We simply add 2% in order to account for
     * \n -> \r\n conversion.
     */
    if (smtpb->esmtp_flags.ESMTP_size) {
	str_printf(&smtp_out, " SIZE=%lu",
		   (unsigned long) ((msg_size * 102L) / 100L));
    }
#if 0
    /*
     * This is commented out because we are not supposed to send a non-MIME
     * message on 8BITMIME mode and we don't have any way to either detect if
     * the message is already in MIME format or to force a MIME conversion if
     * necessary.  Sending a non-7bit clean message in 7BIT mode isn't a good
     * idea either, but is a common practice.  Sigh.  We'll just avoid saying
     * what we're sending....
     */
    /*
     * XXX in theory we should only set the body type to 8BITMIME if that's how
     * we received it.  We'd need a command-line flag to store in the queue
     * file to do this though.  Similary we should only claim 7BIT if we know
     * there are no high-bit chars in the body.
     */
    if (smtpb->esmtp_flags.ESMTP_8bitmime) {
	str_printf(&smtp_out, " BODY=8BITMIME");
    } else {
	str_printf(&smtp_out, " BODY=7BIT");
    }
#endif

    reply = write_command_maybe_wait(smtpb, smtpb->long_timeout,
				     STR(&smtp_out), (unsigned int) STR_LEN(&smtp_out),
				     &reply_text);
    /*
     * Possible responses:
     *	250 - okay	    (continue conversation)
     *	4xx - temporary error (try message again later)
     *  5xx - fatal error   (return message to sender)
     */
    if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN || REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	*error_p = try_again(tp, reply_text);
	insert_addr_list(addr, defer_failure ? defer : fail, *error_p);
	do_smtp_shutdown(smtpb, reply);
	return FAIL;
    }
    if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	*error_p = fatal_error(tp, reply_text);
	insert_addr_list(addr, fail, *error_p);
	do_smtp_shutdown(smtpb, reply);
	return FAIL;
    }

    /* give all of the recipient addresses to the remote SMTP */
    for (cur = addr; cur; cur = next) {
	next = cur->succ;

	/*
	 * each recipient specified individually.
	 * Possible responses:
	 *  250, 251 - okay, or forwarded (continue conversation)
	 *  421 - connection closing   (try again later)
	 *  451 - remote error         (try again later)
	 *  452 - remote storage full  (try this addr again later)
	 *  550 - unknown user         (return this addr to sender)
	 *  5xx - failure	       (return message to sender)
	 */
	STR_CLEAR(&smtp_out);
	str_printf(&smtp_out, "RCPT TO:<%s>", rfc821_quote_the_local_part(cur->next_addr));

	reply = write_command_maybe_wait(smtpb, smtpb->long_timeout,
					 STR(&smtp_out), (unsigned int) STR_LEN(&smtp_out),
					 &reply_text);

	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN || REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    /*
	     * NOTE: keep these in sync with the pipeline handling below
	     */
	    switch (reply) {
	    case REPLY_MAILBOX_UNAVAIL:
		cur->error = remote_mailbox_unavail(tp, reply_text);
		if (defer_failure) {
		    cur->succ = *defer;	/* defer only the current recipient */
		    *defer = cur;
		} else {
		    cur->succ = *fail;	/* time's up for current recipient? */
		    *fail = cur;
		}
		break;
	    case REPLY_REMOTE_FAILURE:
		cur->error = remote_failure(tp, reply_text);
		if (defer_failure) {
		    cur->succ = *defer;	/* defer only the current recipient */
		    *defer = cur;
		} else {
		    cur->succ = *fail;	/* time's up for current recipient? */
		    *fail = cur;
		}
		break;
	    case REPLY_STORAGE_FULL:
		cur->error = remote_storage_full(tp, reply_text);
		if (defer_failure) {
		    cur->succ = *defer;	/* defer only the current recipient */
		    *defer = cur;
		} else {
		    cur->succ = *fail;	/* time's up for current recipient? */
		    *fail = cur;
		}
		break;
	    default:
		*error_p = try_again(tp, reply_text);
		/*
		 * unknown codes defer all recipients (current and sent!)
		 * XXX is this sane? (probably not! :-)
		 */
		insert_addr_list(cur, defer_failure ? defer : fail, *error_p);
		insert_addr_list(okay, defer_failure ? defer : fail, *error_p);
		do_smtp_shutdown(smtpb, reply);
		return FAIL;
	    }
	} else if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	    /*
	     * NOTE: keep these in sync with the pipeline handling below
	     */
	    switch (reply) {
	    case REPLY_UNKNOWN_USER:
		cur->error = remote_unknown_user(tp, reply_text);
		cur->succ = *fail;	/* fail only the current recipient */
		*fail = cur;
		break;
	    case REPLY_USER_NOT_LOCAL:
		cur->error = remote_user_not_local(tp, reply_text);
		cur->succ = *fail;	/* fail only the current recipient */
		*fail = cur;
		break;
	    case REPLY_OVER_QUOTA:
		cur->error = remote_over_quota(tp, reply_text);
		cur->succ = *fail;	/* fail only the current recipient */
		*fail = cur;
		break;
	    case REPLY_PARAMETER_ERROR:	/* not strictly allowed by RFC [2]821, but used by the likes of DMail */
	    case REPLY_ILLEGAL_USER:
		cur->error = remote_illegal_user(tp, reply_text);
		cur->succ = *fail;	/* fail only the current recipient */
		*fail = cur;
		break;
	    case REPLY_SEQUENCE_ERROR:
	    default:
		/* XXX need to explain that this error applies to all recipients */
		*error_p = fatal_error(tp, reply_text);
		/*
		 * unknown codes are fatal for all recipients; i.e. don't send
		 * to anyone at a mis-behaving remote-SMTP site.
		 */
		insert_addr_list(cur, fail, *error_p);
		insert_addr_list(okay, fail, *error_p);
		do_smtp_shutdown(smtpb, reply);
		return FAIL;
	    }
	} else if (!smtpb->esmtp_flags.ESMTP_pipelining) {
	    /* successful thus far.... */
	    cur->succ = okay;
	    okay = cur;
	}
    }
    if (!smtpb->esmtp_flags.ESMTP_pipelining && !okay) {
	*error_p = fatal_error(tp, reply_text);
	do_smtp_shutdown(smtpb, REPLY_TRANS_FAILURE);
	return FAIL;
    }

    if (dont_deliver) {
#ifndef NDEBUG
	if (debug < DBG_DRIVER_MID) {
	    DEBUG(DBG_DRIVER_LO, "\n");
	}
#endif
	DEBUG(DBG_DRIVER_LO, "    DEBUGGING -- delivery FAKED, not sending DATA command...");
	DEBUG(DBG_DRIVER_MID, "\n");
    } else {
	/*
	 * say that we will next be sending the actual data
	 * Possible responses:
	 *  354 - go ahead with the message (continue conversation)
	 *  421 - closing connection (try all recipients again later)
	 *  4xx - remote error       (try all recipients again later)
	 *  5xx - fatal error        (return message to sender)
	 */
	reply = write_command_maybe_wait(smtpb, smtpb->long_timeout,
					 "DATA", (unsigned int) sizeof("DATA") - 1,
					 &reply_text);
    }

    /*
     * we only "pipeline" the commands up to the "DATA" so now we must read all
     * the replies to the commands given so far....
     */
    if (smtpb->esmtp_flags.ESMTP_pipelining) {
	char *data_reply_text;

	if (flush_command_stream(smtpb, &reply_text) != REPLY_OK) {
	    *error_p = try_again(tp, reply_text);
	    insert_addr_list(addr, defer_failure ? defer : fail, *error_p);
	    /* XXX no need to shut down? -- this indicates a write failed */
	    return FAIL;
	}

	/* Now read all the responses.  First the MAIL FROM: reply */
	DEBUG(DBG_DRIVER_MID, "SMTP-pipeline: expecting reply from 'MAIL FROM:'\n");
	reply = wait_read_response(smtpb, smtpb->long_timeout, &reply_text);

	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN || REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    *error_p = try_again(tp, reply_text);
	    insert_addr_list(addr, defer_failure ? defer : fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	} else if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	    *error_p = fatal_error(tp, reply_text);
	    insert_addr_list(addr, fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	}
	reply_text = NULL;

	/* ... next the reply from each RCPT TO: command */
	for (cur = addr; cur; cur = next) {
	    next = cur->succ;

	    DEBUG1(DBG_DRIVER_MID, "SMTP-pipeline: expecting reply from 'RCPT TO:<%s>'\n", cur->next_addr);
	    reply = wait_read_response(smtpb, smtpb->long_timeout,
				       &reply_text);

	    /*
	     * here we should only defer/fail individual recipients else we'll
	     * leave un-read replies on the input channel.
	     */
	    if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN || REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
		/*
		 * NOTE: keep these in sync with the non-pipeline handling above
		 */
		switch (reply) {
		case REPLY_MAILBOX_UNAVAIL:
		    cur->error = remote_mailbox_unavail(tp, reply_text);
		    break;
		case REPLY_REMOTE_FAILURE:
		    cur->error = remote_failure(tp, reply_text);
		    break;
		case REPLY_STORAGE_FULL:
		    cur->error = remote_storage_full(tp, reply_text);
		    break;
		default:
		    cur->error = try_again(tp, reply_text);
		    break;
		}
		if (defer_failure) {
		    cur->succ = *defer;	/* defer only the current recipient */
		    *defer = cur;
		} else {
		    cur->succ = *fail;	/* time's up for current recipient? */
		    *fail = cur;
		}
	    } else if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
		/*
		 * NOTE: keep these in sync with the non-pipeline handling above
		 */
		switch (reply) {
		case REPLY_UNKNOWN_USER:
		    cur->error = remote_unknown_user(tp, reply_text);
		    break;
		case REPLY_USER_NOT_LOCAL:
		    cur->error = remote_user_not_local(tp, reply_text);
		    break;
		case REPLY_OVER_QUOTA:
		    cur->error = remote_over_quota(tp, reply_text);
		    break;
		case REPLY_ILLEGAL_USER:
		    cur->error = remote_illegal_user(tp, reply_text);
		    break;
		default:
		    cur->error = fatal_error(tp, reply_text);
		    break;
		}
		cur->succ = *fail;	/* fail only the current recipient */
		*fail = cur;
	    } else {
		/* successful thus far */
		cur->succ = okay;
		okay = cur;
		reply_text = NULL;
	    }
	}
	if (dont_deliver) {
	    DEBUG(DBG_DRIVER_MID, "DEBUGGING -- delivery FAKED, no pending DATA reply...\n");
	} else {
	    /* ... and finally read the reply from the "DATA" command */
	    DEBUG(DBG_DRIVER_MID, "SMTP-pipeline: expecting reply from 'DATA'\n");
	    reply = wait_read_response(smtpb, smtpb->long_timeout,
				       &data_reply_text);
	    /*
	     * just ignore this if we already have a more meaningful reply.  This
	     * isn't terribly critical because the correct reply should be
	     * squirreled away in the per-recipient error pointer and already be on
	     * the fail list, but just to be sure...
	     */
	    reply_text = reply_text ? reply_text : data_reply_text;
	}
    }
    if (dont_deliver) {
	DEBUG(DBG_DRIVER_HI, "DEBUGGING -- delivery FAKED, no DATA reply to process...\n");
    } else {
	/*
	 * now process the reply to the 'DATA' command....
	 */
	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN || REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    /* temporary error, defer message */
	    *error_p = try_again(tp, reply_text);
	    insert_addr_list(okay, defer_failure ? defer : fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	} else if (REPLY_GROUP(reply) == NEGATIVE_FAILED || REPLY_GROUP(reply) == PSEUDO_FAILED) {
	    /* fatal error, return message to sender */
	    *error_p = fatal_error(tp, reply_text);
	    insert_addr_list(okay, fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	} else if (reply != REPLY_START_DATA) {
	    /* XXX note that if reply text is not a valid SMTP error this may screw
	     * up the logs a bit...
	     */
	    *error_p = fatal_error(tp,
				   xprintf("%x remote protocol error: expecting 354 (or failure), got:\n%s",
					   REPLY_PROTO_ERROR,
					   reply_text));
	    insert_addr_list(okay, fail, *error_p);
	    do_smtp_shutdown(smtpb, REPLY_PROTO_ERROR);
	    return FAIL;
	}
    }

    if (dont_deliver) {
	DEBUG(DBG_DRIVER_HI, "DEBUGGING -- delivery FAKED, not sending message content...\n");
    } else {
	tp->flags |= PUT_DOTS;		/* use the hidden dot protocol. */
	DEBUG(DBG_DRIVER_LO, "\n      sending message...");
	success = write_message(smtpb->out, okay);
	if (success == WRITE_FAIL) {
	    DEBUG(DBG_DRIVER_LO, " failed while writing body.\n");
	    *error_p = write_failed(tp);
	    insert_addr_list(okay, defer_failure ? defer : fail, *error_p);

	    /* assume connection has gone away */
	    return FAIL;
	}
	if (success == READ_FAIL) {
	    DEBUG(DBG_DRIVER_LO, " failed while reading message from spool.\n");
	    *error_p = read_failed(tp);
	    insert_addr_list(okay, defer_failure ? defer : fail, *error_p);

	    /* okay to advance to the next message */
	    return SUCCEED;
	}
    }

    if (dont_deliver) {
	DEBUG(DBG_DRIVER_HI, "DEBUGGING -- delivery FAKED, not sending end-of-DATA command...\n");
    } else {
	/*
	 * Finish by sending the final "." (closing part of "DATA")
	 *
	 * If we are in ESMTP PIPELINING mode and the server SMTP does not check
	 * for at least one valid recipient prior to accepting the DATA command,
	 * and we had no recipients left to send to then we have to send the '.'
	 * anyway, just with no intervening message text (see the last example in
	 * RFC2197).
	 *
	 * Possible responses:
	 *  250 - message accepted
	 *  451 - local error in processing	(try all recipients again later)
	 *  452 - insufficient system storage	(try all recipients again later)
	 *  4xx - other temporary error		(try all recipients again later)
	 *  552 - exceeded storage allocation	(return message to sender)
	 *  554 - Transaction failed		(return message to sender
	 *  5xx - other fatal error		(return message to sender)
	 */
	reply = wait_write_command(smtpb, smtpb->long_timeout,
				   ".", 1, &reply_text);

	if (REPLY_GROUP(reply) == NEGATIVE_TRY_AGAIN) {
	    /* temporary error, defer message */
	    *error_p = try_again(tp, reply_text);
	    insert_addr_list(okay, defer_failure ? defer : fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	}
	if (REPLY_GROUP(reply) == PSEUDO_TRY_AGAIN) {
	    /* possible error, defer, but note possible duplicate */
	    *error_p = try_again(tp,
				 xprintf("%x-error from remote SMTP server after sending '.'.  Actual error:\n%x-    %s\n%x NOTE: duplicate delivery may occur!",
					 reply, reply, reply_text, reply));
	    insert_addr_list(okay, defer_failure ? defer : fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	}
	if (reply != REPLY_OK) {
	    /* fatal error, return message to sender */
	    *error_p = fatal_error(tp, reply_text);
	    insert_addr_list(okay, fail, *error_p);
	    do_smtp_shutdown(smtpb, reply);
	    return FAIL;
	}
    }

    insert_addr_list(okay, succeed, (struct error *) NULL);
    return SUCCEED;
}


/*
 * smtp_shutdown - end an interactive SMTP conversation
 */
void
smtp_shutdown(smtpb)
    struct smtp *smtpb;			/* SMTP description block */
{
    char *reply_text;			/* where to store reply text */

    (void) wait_write_command(smtpb, smtpb->short_timeout,
			      "QUIT", (unsigned int) sizeof("QUIT") - 1, &reply_text);
}

/*
 * do_smtp_shutdown - call shutdown if the connection wasn't dropped
 */
static void
do_smtp_shutdown(smtpb, reply)
    struct smtp *smtpb;			/* SMTP description block */
    int reply;				/* response code from command */
{
    switch (reply) {
    case REPLY_DOWN:
    case REPLY_PROTO_ERROR:
    case REPLY_TIMEOUT:
    case REPLY_EOF:
	break;

    default:
	smtp_shutdown(smtpb);
	break;
    }
}

static int
write_command_maybe_wait (smtpb, timeout, text, len, reply_text)
    struct smtp *smtpb;			/* SMTP description block */
    unsigned int timeout;		/* read timeout */
    char *text;				/* text of command */
    register unsigned int len;		/* length of command */
    char **reply_text;			/* text of response from remote */
{
    if (smtpb->esmtp_flags.ESMTP_pipelining) {
	return write_command_nowait(smtpb, text, len);
    } else {
	return wait_write_command(smtpb, timeout, text, len, reply_text);
    }
}

/*
 * wait_write_command - send a command, then wait for the response
 *
 * For batched SMTP, return a response code of REPLY_OK, unless there
 * was a write error.
 */
static int
wait_write_command(smtpb, timeout, text, len, reply_text)
    struct smtp *smtpb;			/* SMTP description block */
    unsigned timeout;			/* read timeout */
    char *text;				/* text of command */
    register unsigned int len;		/* length of command */
    char **reply_text;			/* text of response from remote */
{
    int reply;

    reply = write_command_nowait(smtpb, text, len);
    if (reply != REPLY_OK) {
	return reply;
    }
    reply = flush_command_stream(smtpb, reply_text);
    if (reply != REPLY_OK) {
	return reply;
    }
    /* wait for the response to come back */
    reply = wait_read_response(smtpb, timeout, reply_text);

    return reply;
}

/*
 * write_command_nowait - send a command
 */
static int
write_command_nowait(smtpb, text, len)
    struct smtp *smtpb;			/* SMTP description block */
    char *text;				/* text of command */
    register unsigned int len;		/* length of command */
{
    register FILE *f = smtpb->out;
    register char *cp;

    DEBUG2(DBG_DRIVER_MID, "SMTP-send:\n%S\n", (size_t) len, text);

    /* send out the command */
    for (cp = text; len > 0; cp++, --len) {
	putc(*cp, f);
    }

    /* terminate the command line */
    for (cp = smtpb->nl; *cp; cp++) {
	putc(*cp, f);
    }

    return REPLY_OK;
}

static int
flush_command_stream(smtpb, reply_text)
    struct smtp *smtpb;			/* SMTP description block */
    char **reply_text;			/* text of response from remote */
{
    register FILE *f = smtpb->out;

    (void) fflush(f);
    if (ferror(f)) {
	*reply_text = xprintf("%x write error, remote probably down", REPLY_EOF);
	DEBUG1(DBG_DRIVER_MID, "SMTP-reply:\n%s\n", *reply_text);
	return REPLY_EOF;
    }

    return REPLY_OK;
}

/*
 * wait_read_response - wait for a response from the remote SMTP server
 *
 * return the response code, and the response text.  Abort on timeouts
 * or end-of-file or protocol errors.
 */
static int
wait_read_response(smtpb, timeout, reply_text)
    struct smtp *smtpb;			/* SMTP description block */
    unsigned timeout;			/* read timeout */
    char **reply_text;			/* return text of response here */
{
    int result;

    /* If we're in batch mode, always return success. */
    if (! smtpb->in) {
	return REPLY_OK;
    }
    do {
	result = read_response_internal(smtpb, timeout, reply_text);
	DEBUG1(DBG_DRIVER_MID, "SMTP-reply:\n%s\n", *reply_text);
    } while (REPLY_GROUP(result) == POSITIVE_DEBUG);

    return result;
}

#if ((__STDC__ - 0) <= 0)
/* most traditional compilers won't optimize global variables */
JUMPSIG save_sig;			/* previous alarm signal catcher */
unsigned save_alarm;			/* previous alarm value */
#else
static volatile JUMPSIG save_sig;	/* previous alarm signal catcher */
static volatile unsigned save_alarm;	/* previous alarm value */
#endif

/*
 * WARNING: do not put newlines in any faked reply_text messgaes!!!!
 */
static int
read_response_internal(smtpb, timeout, reply_text)
    struct smtp *smtpb;			/* SMTP description block */
    unsigned int timeout;			/* read timeout */
    char **reply_text;			/* return text of response here */
{
    register int lstart;		/* offset to start of an input line */
    int reply_code;			/* SMTP reply code value */
    register int c;
    register char *rp;

    /* reset the input buffer */
    STR_CLEAR(&smtp_in);

    save_alarm = alarm(0);
    JUMP_SETSIG(SIGALRM, catch_timeout, &save_sig);

    /* if we timeout, say so */
    if (JUMP_SETJMP(timeout_buf)) {
	(void) alarm(0);
	JUMP_CLEARSIG(SIGALRM, &save_sig);
	if (save_alarm) {
	    (void) alarm(save_alarm);
	}
	*reply_text = xprintf("%x timeout on read from remote SMTP server", REPLY_TIMEOUT);
	return REPLY_TIMEOUT;
    }

    /* don't let reads block forever */
    (void) alarm(timeout);

    /* loop until the response is completed */
    do {
	lstart = STR_LEN(&smtp_in);	/* remember the beginning of this line */
	while ((c = getc(smtpb->in)) != EOF && c != '\n') {
	    STR_NEXT(&smtp_in, c);
	}
	rp = STR(&smtp_in);		/* get current pointer */
	if (STR_LEN(&smtp_in) > 0 && rp[STR_LEN(&smtp_in) - 1] == '\r') {
	    STR_PREV(&smtp_in);
	}
	STR_NEXT(&smtp_in, '\n');
	rp = STR(&smtp_in);		/* get current pointer */
    } while (c != EOF &&
	     isdigit((int) rp[lstart]) &&
	     isdigit((int) rp[lstart + 1]) &&
	     isdigit((int) rp[lstart + 2]) &&
	     rp[lstart + 3] == '-');

    /* replace last newline with a NUL byte */
    /* XXX or should we? */
    if (STR_LEN(&smtp_in) > 0) {
	rp[STR_LEN(&smtp_in) - 1] = '\0';
    }

    /* restore previous alarm catcher and setting */
    (void) alarm(0);
    JUMP_CLEARSIG(SIGALRM, &save_sig);
    if (save_alarm) {
	(void) alarm(save_alarm);
    }

    /* 
     * finally it's safe to see if there was a read error....
     */
    if (c == EOF) {
	*reply_text = xprintf("%x read error from remote SMTP server (connection closed unexpectedly)", REPLY_EOF);
	return REPLY_EOF;
    }
    /* .... or other protocol error */
    rp = STR(&smtp_in);
    if (! isdigit((int) rp[lstart]) ||
	! isdigit((int) rp[lstart + 1]) ||
	! isdigit((int) rp[lstart + 2]) ||
	! isspace((int) rp[lstart + 3]))
    {
	*reply_text = xprintf("%x protocol error (no response code) in reply from remote SMTP server", REPLY_PROTO_ERROR);
	return REPLY_PROTO_ERROR;
    }

    *reply_text = rp;

    /* snarf out the reply code for our return value.... */
    (void) sscanf(rp, "%3x", (unsigned int *) &reply_code);	/* XXX better to hand parse? */

    return reply_code;
}


/*
 *	rfc821_is_dot_string() - is a string compliant with RFC 821 dot-string?
 *
 * in RFC 821 a dot-string must not contain any <special> or <SP>, other than
 * "." of course, unless it's escaped with a "\", and with no double "."s and
 * no trailing "." either:
 *
 *          <dot-string> ::= <string> | <string> "." <dot-string>
 *
 *          <string> ::= <char> | <char> <string>
 *
 *          <char> ::= <c> | "\" <x>
 *
 *          <SP> ::= the space character (ASCII code 32)
 *
 *          <c> ::= any one of the 128 ASCII characters, but not any
 *                    <special> or <SP>
 *
 *          <x> ::= any one of the 128 ASCII characters (no exceptions)
 *
 *          <special> ::= "<" | ">" | "(" | ")" | "[" | "]" | "\" | "."
 *                    | "," | ";" | ":" | "@"  """ | the control
 *                    characters (ASCII codes 0 through 31 inclusive and
 *                    127)
 *
 * Note that we don't care about finding quoted-strings inside strings because
 * RFC 821 doesn't allow that, at least not for local-part (which is the only
 * place it allows quoted-string anyway).
 *
 * Note the new RFC 2821 (which replaces 821) seems to use the RFC 2822
 * definition of "atext" and in turn defines a "Dot-string" as just:
 *
 *	Dot-string = Atom *("." Atom)
 *	Atom = 1*atext
 *
 * I.e. the use of backslash to escape any ASCII char seems to be gone.
 */
int
rfc821_is_dot_string(s)
    char *s;
{
    int c;

    while ((c = *s++)) {
	if (c == '\\') {
	    if (!isascii((int) *s)) {
		return FALSE;
	    }
	    s++;				/* anything else goes! */
	    continue;
	}
	if (c == '<' || c == '>' || c == '(' || c == ')' || c == '[' || c == ']' ||
	    c == '\\' || c == ',' || c == ';' || c == ':' || c == '@' || c == '"' ||
	    c == ' ' || (c == '.' && *s == '.') || iscntrl((int) c)) {
	    return FALSE;
	}
	if (!isascii(c)) {
	    return FALSE;
	}
    }
    if (*(s-2) == '.') {			/* trailing-dot? */
	return FALSE;
    }

    return TRUE;
}

/*
 *	rfc821_is_quoted_string() - is a string compliant with RFC 821 quoted-string?
 *
 * in RFC-821 a quoted local part may be any one of the 128 ASCII
 * characters except <CR>, <LF>, quote ("), or backslash (\);
 * unless it's escaped with a "\":
 *
 *          <quoted-string> ::=  """ <qtext> """
 *
 *          <qtext> ::=  "\" <x> | "\" <x> <qtext> | <q> | <q> <qtext>
 *
 *          <CR> ::= the carriage return character (ASCII code 13)
 *
 *          <LF> ::= the line feed character (ASCII code 10)
 *
 *          <q> ::= any one of the 128 ASCII characters except <CR>,
 *                    <LF>, quote ("), or backslash (\)
 *
 *          <x> ::= any one of the 128 ASCII characters (no exceptions)
 *
 * Note the new RFC 2821 (which replaces 821) seems to use the RFC 2822
 * definition of "qcontent".  The confusing part is that in the RFC 2822
 * definition the backslash is _not_ allowed to escape <CR> or <LF>.
 */
int
rfc821_is_quoted_string(s)
    char *s;
{
    int c;

    if (*s++ != '"') {
	return FALSE;
    }
    while ((c = *s++)) {
	/* double-quote ends the string */
	if (c == '"') {
	    break;
	}
	if (c == '\\') {
	    if (!isascii((int) *s)) {
		return FALSE;
	    }
	    s++;				/* anything else goes! */
	    continue;
	}
	if (c == '\r' || c == '\n' || c == '\\') {
	    return FALSE;
	}
	if (!isascii(c)) {
	    return FALSE;
	}
    }
    /* must end with a double-quote */
    if (c != '"') {
	return FALSE;
    }
    /* nothing can be after the double-quote */
    if (*s) {
	return FALSE;
    }

    return TRUE;
}

/*
 *	rfc821_make_quoted_string() - make quoted-string from local-part
 *
 * Put quotes around `s', and insert a backslash before characters that need
 * quoting when they part of a quoted-string.
 *
 * `at' might point to a domain-part, and if so we tack that onto the end.
 */
char *
rfc821_make_quoted_string(s, domain)
    char *s;
    char *domain;
{
    static struct str dst;
    static int dst_inited = FALSE;		/* TRUE if str inited */
    char *cp;
    int ch;

    if (! dst_inited) {
	STR_INIT(&dst);
	dst_inited = TRUE;
    } else {
	STR_CHECK(&dst);
	STR_CLEAR(&dst);
    }
    STR_NEXT(&dst, '"');
    for (cp = s; (ch = *cp); cp++) {
	if (ch > 127 || ch == '\r' || ch == '\n' || ch == '"' || ch == '\\') {
	    STR_NEXT(&dst, '\\');
	}
	STR_NEXT(&dst, ch);
    }
    STR_NEXT(&dst, '"');
    if (domain) {
	STR_NEXT(&dst, '@');
	STR_CAT(&dst, domain);			/* append domain, and NUL */
    } else {
	STR_NEXT(&dst, '\0');			/* just NUL-terminate */
    }
    STR_DONE(&dst);

    return STR(&dst);
}


/*
 * rfc821_quote_the_local_part() - quote just the local part of an address
 *
 * According to RFC 821, a local-part is a dot-string or a quoted-string.
 *
 * `address' might be just a mailbox local-part, or might be a fully qualified
 * address with a domain....
 *
 * So, first we see if this is a full addr, and if so then we temporarly
 * terminate just the local-part by replacing the '@' with a NUL byte.  Then we
 * check to see if the local-part is a dot-string or a quoted-string.  If it is
 * not either we just turn it into a quoted-string and be done with it.  (and
 * then we re-attach the domain, if any, by putting the '@' back in place)
 *
 * We assume the domain part, if any, is already legal as otherwise it wouldn't
 * have been found in the DNS and be routed and get this far.
 *
 * The return value will either be identical to the input `address', or it will
 * be new storage pointing to a copy of the address string which has had its
 * local-part quoted.
 */
char *
rfc821_quote_the_local_part(address)
    char *address;
{
    char *at = NULL;			/* pointer to any '@' char... */

    /*
     * XXX NOTE: by searching for the last `@' we don't allow route-addrs with
     * routes or other nonsense!  The route will get quoted and become the new
     * local part.  This is messy, but people SHOULD NOT be using routes in
     * SMTP address parameters anyway.  This is also contrary to RFC 2821 which
     * suggests stripping off the route and just using the final destination
     * under the assumption that it is to a fully routed and reachable domain.
     *
     * However any stripping of routes should be done long before now, either
     * in get_return_addr() for the MAIL parameter, or in the routers for RCPT
     * parameters.
     */
    if ((at = strrchr(address, '@'))) {
	/*
	 * XXX we should copy the local part to new storage instead of
	 * modifying the caller's storage
	 */
	*at = '\0';
    }
    if (!rfc821_is_dot_string(address) && !rfc821_is_quoted_string(address)) {
	address = rfc821_make_quoted_string(address, at ? at++ : (char *) NULL);
    }
    if (at) {
	*at = '@';			/* put it back, in the original string! */
    }

    return address;
}


/* catch_timeout - longjmp after an alarm */
/* ARGSUSED */
static void
catch_timeout(signo)
    int signo __attribute__((unused));
{
    JUMP_LONGJMP(timeout_buf, 1);
}

static struct error *
no_remote(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_172 - no connection to remote SMTP server
     *
     * DESCRIPTION
     *	    No 220 reply was received from the remote SMTP server over
     *	    the virtual circuit, assume that the remote host was not
     *	    really reachable.
     *
     * ACTIONS
     *	    Try again later.
     *
     * RESOLUTION
     *	    Retries should eventually take care of the problem, but we set
     *	    ERR_NSOWNER in case the retry_duration has been exceeded in which
     *	    case the message should be bounced.
     */
    error_text = xprintf("%v transport failure: no connection to remote SMTP server:\n%s", tp->name, reply_text);
    return note_error(ERR_172 | ERR_NSOWNER, error_text);
}

static struct error *
try_again(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_151 - temporary SMTP error
     *
     * DESCRIPTION
     *	    wait_write_command() received a temporary error response
     *      while conversing with the remote host.  These can be read
     *	    errors or read timeouts, or they can be negative responses
     *	    from the remote side.
     *
     * ACTIONS
     *      Defer the input address(es).
     *
     * RESOLUTION
     *	    Retries should eventually take care of the problem, but we set
     *	    ERR_NSOWNER in case the retry_duration has been exceeded in which
     *	    case the message should be bounced.
     */
    error_text = xprintf("temporary failure from %v transport:\n%s", tp->name, reply_text);
    return note_error(ERR_151 | ERR_NSOWNER, error_text);
}

static struct error *
fatal_error(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_152 - permanent SMTP error
     *
     * DESCRIPTION
     *	    wait_write_command() received a permanent error response
     *      while conversing with the remote host.  These are generally
     *	    permanent negative response codes from the remote side.
     *
     * ACTIONS
     *      Fail the input addresses, and return to the address owner
     *	    or the message originator.
     *
     * RESOLUTION
     *      The resolution depends upon the error message.  See RFC821
     *	    for details.
     */
    error_text = xprintf("fatal error from %v transport:\n%s", tp->name, reply_text);
    return note_error(ERR_152 | ERR_NSOWNER, error_text);
}

static struct error *
remote_mailbox_unavail(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_187 - Remote mailbox is busy (SMTP 450)
     *
     * DESCRIPTION
     *      The remote host returned status indicating that it
     *      cannot accept mail for this mailbox at this time.
     *
     * ACTIONS
     *      Defer this recipient.
     *
     * RESOLUTION
     *	    Retries should eventually take care of the problem, but we set
     *	    ERR_NSOWNER in case the retry_duration has been exceeded in which
     *	    case the message should be bounced.
     */
    error_text = xprintf("remote mailbox busy, reported by %v transport:\n%s", tp->name, reply_text);
    return note_error(ERR_187 | ERR_NSOWNER, error_text);
}

static struct error *
remote_failure(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_188 - Remote failure in processing
     *
     * DESCRIPTION
     *      The remote host returned status indicating that it
     *      is experiencing temporary problems processing this
     *      recipient address.
     *
     * ACTIONS
     *      Defer this recipient.
     *
     * RESOLUTION
     *	    Retries should eventually take care of the problem, but we set
     *	    ERR_NSOWNER in case the retry_duration has been exceeded in which
     *	    case the message should be bounced.
     */
    error_text = xprintf("remote processing error reported by %v transport:\n%s", tp->name, reply_text);
    return note_error(ERR_188 | ERR_NSOWNER, error_text);
}

static struct error *
remote_storage_full(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_153 - Remote host's storage is full (SMTP 452)
     *
     * DESCRIPTION
     *      The remote host returned status indicating that he
     *      cannot take more recipient addresses.
     *
     * ACTIONS
     *      Defer the remaining addresses in hopes that other
     *      storage is not affected.
     *
     * RESOLUTION
     *	    Hopefully by sending a few addresses now and a few later the
     *	    message will eventually be delivered to all recipients, but we set
     *	    ERR_NSOWNER in case the retry_duration has been exceeded in which
     *	    case the message should be bounced.
     */
    error_text = xprintf("remote storage full error reported by %v transport:\n%s", tp->name, reply_text);
    return note_error(ERR_153 | ERR_NSOWNER, error_text);
}

static struct error *
remote_unknown_user(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_156 - remote host returned bad address status (SMTP 550)
     *
     * DESCRIPTION
     *      Status from the remote host indicates an error in the
     *      recipient address just sent to it.
     *
     * ACTIONS
     *      Return mail to the address owner or to the sender.
     *
     * RESOLUTION
     *      An alternate address should be attempted or the
     *      postmaster at the site that generated the error message
     *      should be consulted.
     */
    error_text = xprintf("%v transport reports unknown user:\n%s", tp->name, reply_text);
    return note_error(ERR_156 | ERR_NSOWNER, error_text);
}

static struct error *
remote_user_not_local(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_189 - remote host says user not local (SMTP 551)
     *
     * DESCRIPTION
     *      Status from the remote host indicates this user is not local.
     *
     * ACTIONS
     *      Return mail to the address owner or to the sender.
     *
     * RESOLUTION
     *      An alternate address should be attempted or the
     *      postmaster at the site that generated the error message
     *      should be consulted.
     */
    error_text = xprintf("%v transport reports user not local:\n%s", tp->name, reply_text);
    return note_error(ERR_189 | ERR_NSOWNER, error_text);
}

static struct error *
remote_over_quota(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_190 - remote host says user over quota (SMTP 552)
     *
     * DESCRIPTION
     *      Status from the remote host indicates this user has exceeded
     *      their maximum allowed mailbox size.
     *
     * ACTIONS
     *      Return mail to the address owner or to the sender.
     *
     * RESOLUTION
     *      Out-of-band communications should be attempted or the
     *      postmaster at the site that generated the error message
     *      should be consulted.
     */
    error_text = xprintf("%v transport reports user over quota:\n%s", tp->name, reply_text);
    return note_error(ERR_190 | ERR_NSOWNER, error_text);
}

static struct error *
remote_illegal_user(tp, reply_text)
    struct transport *tp;
    char *reply_text;
{
    char *error_text;

    /*
     * ERR_191 - remote host says mailbox is invalid (SMTP 553)
     *
     * DESCRIPTION
     *      Status from the remote host indicates this user account is not
     *      allowed to receive mail, or the syntax of the mailbox is
     *      incorrect, etc.
     *
     * ACTIONS
     *      Return mail to the address owner or to the sender.
     *
     * RESOLUTION
     *      Out-of-band communications should be attempted or the
     *      postmaster at the site that generated the error message
     *      should be consulted.
     */
    error_text = xprintf("%v transport reports invalid mailbox:\n%s", tp->name, reply_text);
    return note_error(ERR_191 | ERR_NSOWNER, error_text);
}

static struct error *
write_failed(tp)
    struct transport *tp;
{
    char *error_text;

    /*
     * ERR_154 - Error writing to remote host
     *
     * DESCRIPTION
     *      An error occured when transmitting the message text to the
     *      remote host.
     *
     * ACTIONS
     *      Defer all of the remaining input addresses.
     *
     * RESOLUTION
     *	    Retries should eventually take care of the problem, but we set
     *	    ERR_NSOWNER in case the retry_duration has been exceeded in which
     *	    case the message should be bounced.
     */
    error_text = xprintf("%v transport failure: Error writing to remote host", tp->name);
    return note_error(ERR_154 | ERR_NSOWNER, error_text);
}

static struct error *
read_failed(tp)
    struct transport *tp;
{
    char *error_text;

    /*
     * ERR_155 - Failed to read spooled message
     *
     * DESCRIPTION
     *      We failed to read the spooled message on our side while
     *      sending the message text to a remote host.
     *
     * ACTIONS
     *      Defer the message with a configuration error.  If we are
     *      unable to read the spool file, there is little we can do.
     *
     * RESOLUTION
     *      "This should never happen."
     */
    error_text = xprintf("%v transport failure: Error reading from spool file", tp->name);
    return note_error(ERR_155 | ERR_CONFERR, error_text);
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
