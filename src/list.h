/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:list.h,v 1.5 2005/07/03 05:57:45 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * list.h:
 *	list storage management
 */

/*
 * list structure types for general use
 */
typedef struct int_list {		/* general purpose list of ints entry */
    struct int_list *succ;		/* single forward link */
    int i_used;				/* flag to indicate if node is valid */
    long int i_val;			/* data associated with entry */
} intlist_t;

typedef struct charp_list {		/* general purpose list of C strings entry */
    struct charp_list *succ;		/* single forward link */
    char *text;				/* pointer to xmalloc()'ed storage for a C string */
} charplist_t;

typedef struct voidp_list {		/* general purpose list of generic pointers */
    struct voidp_list *succ;		/* single forward link */
    void *storage;			/* data associated with entry */
    void (*freefn) __P((void *));	/* pointer to fn that frees storage */
} voidplist_t;

/*
 * functions exported by list.c
 */
extern intlist_t *add_intlist __P((intlist_t *, long int));
extern int remove_intlist_matching __P((intlist_t *, long int));
extern int remove_intlist_at __P((intlist_t *, size_t));
extern void free_intlist __P((intlist_t *));

extern charplist_t *add_charplist __P((charplist_t *, char *));
extern void free_charplist __P((charplist_t *));

extern voidplist_t *add_voidplist __P((voidplist_t *, char *, void (*) __P((void *))));
extern void free_voidplist __P((voidplist_t *));

#ifndef NDEBUG
extern size_t count_intlist __P((intlist_t *));
extern void print_intlist __P((intlist_t *));
extern size_t count_charplist __P((charplist_t *));
extern void print_charplist __P((charplist_t *));
extern size_t count_voidplist __P((voidplist_t *));
extern void print_voidplist __P((voidplist_t *));
#endif

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
