/*
#ident	"@(#)smail/src/routers:RELEASE-3_2_0_121:smarthost.h,v 1.5 2003/12/14 22:42:35 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * smarthost.h:
 *	interface file for smarthost driver.
 */

/* private information stored per router file entry */
struct smarthost_private {
    char *path;				/* path from the localhost */
    char *host;				/* next_host to get to smarthost */
    char *route;			/* route from next_host to smarthost */
};

extern void rtd_smarthost __P((struct router *,
			       struct addr *,
			       struct addr **,
			       struct addr **,
			       struct addr **));
extern char *rtb_smarthost __P((struct router *, struct attribute *));
extern void rtp_smarthost __P((FILE *, struct router *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
