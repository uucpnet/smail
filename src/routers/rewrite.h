/*
#ident 	"@(#)smail/src/routers:RELEASE-3_2_0_121:rewrite.h,v 1.4 2003/12/14 22:42:35 woods Exp"
*/

/*
 *
 * rewrite.h:
 *	interface file for rewrite driver.
 */

/* macros local to the reroute driver */

/* flag attributes */
#define RW_REOPEN	0x00010000	/* always reopen database to search */
#define RW_OPTIONAL	0x00020000	/* the paths file is optional */
#define RW_TRYAGAIN	0x00040000	/* defer address on open failure */

/* private information stored per router file entry */
struct rewrite_private {
    char *file;				/* file attribute */
    char *proto;			/* protocol name */
    char *domain;			/* optional domain names */
    char *required;			/* required domain names */
    int retries;			/* max count of retries */
    unsigned int interval;		/* sleep interval between retries */
    char *database;			/* internal - open database */
    char *error_text;			/* internal - error text from open */
};

extern void rtd_rewrite __P((struct router *,
			     struct addr *,
			     struct addr **,
			     struct addr **,
			     struct addr **));
extern char *rtb_rewrite __P((struct router *, struct attribute *));
extern void rtp_rewrite __P((FILE *, struct router *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
