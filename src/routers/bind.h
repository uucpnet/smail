/*
#ident	"@(#)smail/src/routers:RELEASE-3_2_0_121:bind.h,v 1.11 2003/12/14 22:42:36 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * bind.h:
 *	interface file for bind driver.
 */

#include "../bindlib.h"

/* private information stored per router file entry */
struct bind_private {
    struct bindlib_private bindlib_attr;
};

extern void rtd_bind __P((struct router *,
			  struct addr *,
			  struct addr **,
			  struct addr **,
			  struct addr **));
extern char *rtb_bind __P((struct router *, struct attribute *));
extern void rtp_bind __P((FILE *, struct router *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
