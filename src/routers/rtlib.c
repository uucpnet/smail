/*
#ident	"@(#)smail/src/routers:RELEASE-3_2_0_121:rtlib.c,v 1.26 2005/06/22 21:23:52 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * rtlib.c:
 *	Support routines for smail routing drivers.  Simple routers
 *	can depend on these routines rather than having to duplicate
 *	this intelligence.
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif

#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include "../smail.h"
#include "../alloc.h"
#include "../list.h"
#include "../main.h"
#include "../parse.h"
#include "../addr.h"
#include "../route.h"
#include "../lookup.h"
#include "../smailstring.h"
#include "../dys.h"
#include "../exitcodes.h"
#include "../transport.h"
#include "rtlib.h"
#include "../extern.h"
#include "../debug.h"
#include "../error.h"
#include "../smailport.h"

/*
 * rtd_standard - standard function for routing to a target
 *
 * These routines calls a lookup function to route individual target
 * names.  This lookup routine will be called as follows:
 *
 *	result = (*lookup)(rp, addr, fl, &rt_info, &error);
 *
 *	    int result;			- output: success value
 *
 *	    struct router *rp;		- input: router entry
 *	    struct addr *addr;		- input: addr to route
 *	    int fl;			- input: flags affecting operation
 *
 *	    struct rt_info rt_info;	- output: routing information
 *	    struct error *error;	- output: routing error
 *	    int len;			- output: target match length
 *
 * The lookup routine should route to the target specified in the
 * single stucture passed in 'addr'.  If routing was successful and a
 * match was found, the 'next_host', 'route' and 'len' values should
 * always be set as appropriate.  If routing was not successful due to
 * an error other than the host not being found, error should be set
 * to a structure defining the type of the error.
 *
 * The 'success' value should be a number from lookup.h corresponding
 * to one of:
 *
 *	DB_SUCCEED	The target was matched by the router.  The
 *			addr structure will be processed by the
 *			route_driver_finish() function with the
 *			values from the 'rt_info' structure.
 *
 *	DB_NOMATCH	The target was not matched by the router.  The
 *			addr structure will be linked to the 'out'
 *			list without modification.
 *
 *	DB_FAIL		An unrecoverable error in routing to the
 *			target.  This will cause the addr structure to
 *			be linked into the 'fail' output list, and tagged
 *			with the 'error' structure.
 *
 *	DB_AGAIN	A temporary error in routing to the target.
 *			This will cause the addr structure to be
 *			linked into the 'defer' output list, and tagged
 *			with the 'error' structure.
 *
 *	FILE_NOMATCH	The router considers itself optional and
 *			disabled (its database doesn't exist, for
 *			example).  This and all subsequent unfinished
 *			addr structures will be linked into the 'out'
 *			list unchanged.
 *
 *	FILE_FAIL	There was an unrecoverable error in the
 *			router.  This is treated as a configuration
 *			error, the ERR_CONFERR flag will be set in the
 *			'error' structure.  This and all subsequent
 *			unfinished addrs will be linked into the
 *			'defer' list, and tagged with the 'error'
 *			structure.
 *
 *	FILE_AGAIN	There was a temporary error in the router.
 *			This and all subsequent unfinished addrs will
 *			be linked into the 'defer' list, tagged with
 *			the 'error' structure.
 *
 * The 'fl' value is a bit-wise or of the following values from
 * rtlib.h:
 *
 *	RT_VERIFY	Performing verification only.  In this case
 *			we are only interested in the success value,
 *			not in the 'rt_info' structure.  A router can
 *			optimize its operation accordingly.  For
 *			example, a router need only verify that any
 *			match exists, it need not find the best match.
 *
 * The 'rt_info' structure defines the next_host, targe match length
 * and, optionally, the route and transport determined by the router.
 */
void
rtd_standard(rp, in, out, defer, fail, lookup)
    struct router *rp;			/* router table entry */
    struct addr *in;			/* input addr structures */
    struct addr **out;			/* non-failed addr structures */
    struct addr **defer;		/* addrs to defer to a later time */
    struct addr **fail;			/* unresolvable addrs */
    int (*lookup) __P((struct router *,
		  struct addr *,
		  int,
		  struct rt_info *,
		  struct error **));
{
    register struct addr *cur;		/* current addr being processed */
    struct addr *next;			/* next addr to process */
    struct error *error = NULL;		/* error from last lookup */
    char *last_target = NULL;		/* last target that was checked */
    int result = DB_NOMATCH;		/* results of last lookup */
					/* XXX un-initialized! */
    int file_result = 0;		/* non-zero means router error */
    struct rt_info rt_info;		/* routing info from router */

    DEBUG1(DBG_DRIVER_HI, "rtd_standard() called from rtd_%s()\n", rp->driver);

    for (cur = in; cur; cur = next) {

	next = cur->succ;

	if (cur->flags & ADDR_FINISHED) {
	    /* this addr has already been completed, skip it */
	    DEBUG1(DBG_DRIVER_HI, "%v: already finished routing, moving to out list.\n", cur->in_addr);
	    cur->succ = *out;
	    *out = cur;
	    continue;
	}

	if (file_result) {
	    switch (file_result) {

	    case FILE_NOMATCH:
		DEBUG1(DBG_DRIVER_HI, "%v: router disabled, moving to out list.\n", cur->in_addr);
		cur->succ = *out;
		*out = cur;
		continue;

	    case FILE_FAIL:
	    case FILE_AGAIN:
		DEBUG1(DBG_DRIVER_HI, "%v: router failed, moving to both error & defer lists.\n", cur->in_addr);
		cur->error = error;	/* use error from last lookup */
		cur->succ = *defer;
		*defer = cur;
		continue;
	    }
	}

	if ((rp->flags & RT_AFFECTS_USER) || /* This means routing may change on a per user basis */
	    (last_target == NULL) || 	/* and this means we have not done a lookup yet... */
	    (! EQIC(last_target, cur->target))) {
	    /*
	     * The current target differs from the previous target, so
	     * look it up.
	     */
	    DEBUG1(DBG_DRIVER_HI, "%v: clearing rt_info and preparing to call lookup().\n", cur->in_addr);
	    rt_info.matchlen = -1;
	    rt_info.next_host = NULL;
	    rt_info.route = NULL;
	    rt_info.transport = NULL;
	    rt_info.tphint_list = (struct transport_hints *) NULL;
	    error = NULL;
	    last_target = cur->target;
	    /* no flags to pass */
	    result = (*lookup)(rp, cur, 0, &rt_info, &error);
	} else {
	    DEBUG2(DBG_DRIVER_HI, "%v: addr has same target, re-using result (%s) from last lookup().\n", cur->in_addr, LOOKUP_DBG_NAME(result));
	}

	/* Finish up routing, based on lookup response code */
	switch (result) {
	case DB_SUCCEED:
	    /* found a successful match */
	    route_driver_finish(rp, cur, rt_info.matchlen, rt_info.next_host,
				rt_info.route, rt_info.transport,
				rt_info.tphint_list);
	    cur->succ = *out;
	    *out = cur;
	    break;

	case DB_NOMATCH:
	    /* No match was found.  Pass the address onto the next router. */
	    cur->succ = *out;
	    *out = cur;
	    break;

	case DB_FAIL:
	    /* The address should be failed, with an error of some kind. */
	    exitvalue = EX_NOHOST;	/* set exit status */
	    cur->error = error;
	    cur->succ = *fail;
	    *fail = cur;
	    break;

	case DB_AGAIN:
	    /* Routing for this address should be reattempted later. */
	    exitvalue = EX_TEMPFAIL;	/* set exit status */
	    cur->error = error;
	    cur->succ = *defer;
	    *defer = cur;
	    break;

	case FILE_NOMATCH:
	    /* The file was not found, don't match any addresses. */
	    cur->succ = *out;
	    *out = cur;
	    file_result = FILE_NOMATCH;
	    break;

	case FILE_FAIL:
	    /* Permanent router error, this is a configuration error. */
	    exitvalue = EX_UNAVAILABLE;	/* set exit status */
	    error->info |= ERR_CONFERR;
	    cur->error = error;
	    cur->succ = *defer;
	    *defer = cur;
	    file_result = FILE_FAIL;
	    break;

	case FILE_AGAIN:
	    /* Temporary router database error, retry all addresses. */
	    exitvalue = EX_TEMPFAIL;	/* set exit status */
	    cur->error = error;
	    cur->succ = *defer;
	    *defer = cur;
	    file_result = FILE_AGAIN;
	    break;
	}
    }
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
