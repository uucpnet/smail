/*
#ident	"@(#)smail/src/routers:RELEASE-3_2_0_121:pathalias.h,v 1.6 2003/12/14 22:42:35 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * pathalias.h:
 *	interface file for pathalias driver.
 */

/* macros local to the pathalias driver */

/* flag attributes */
#define PA_REOPEN	0x00010000	/* always reopen database to search */
#define PA_OPTIONAL	0x00020000	/* the paths file is optional */
#define PA_TRYAGAIN	0x00040000	/* defer address on open failure */

/* private information stored per router file entry */
struct pathalias_private {
    char *file;				/* file attribute */
    char *proto;			/* protocol name */
    char *domain;			/* optional domain names */
    char *required;			/* required domain names */
    int retries;			/* max count of retries */
    unsigned int interval;		/* sleep interval between retries */
    char *database;			/* internal - open database */
    char *error_text;			/* internal - error text from open */
};

extern void rtd_pathalias __P((struct router *,
			       struct addr *,
			       struct addr **,
			       struct addr **,
			       struct addr **));
extern char *rtb_pathalias __P((struct router *, struct attribute *));
extern void rtp_pathalias __P((FILE *, struct router *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
