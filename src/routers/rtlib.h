/*
#ident	"@(#)smail/src/routers:RELEASE-3_2_0_121:rtlib.h,v 1.8 2003/12/14 22:42:35 woods Exp"
 */

/*
 * rtlib.h - interface file for routines in rtlib.c
 */

/* flag values passed from rt[dv]_standard to the driver routines */
#define RT_VERIFY	0x0001		/* Verify only */

/* external functions in the rtlib.c file */
void rtd_standard __P((struct router *,
		       struct addr *,
		       struct addr **,
		       struct addr **,
		       struct addr **,
		       int (*lookup)(struct router *,
				     struct addr *,
				     int,
				     struct rt_info *,
				     struct error **)));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
