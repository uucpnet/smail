/*
#ident	"@(#)smail/src/routers:RELEASE-3_2_0_121:reroute.h,v 1.4 2003/12/14 22:42:35 woods Exp"
 */

/*
 *    Copyright (C) 1992 Uwe Doering
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * reroute.h:
 *	interface file for reroute driver.
 */

/* macros local to the reroute driver */

/* flag attributes */
#define RR_REOPEN	0x00010000	/* always reopen database to search */
#define RR_OPTIONAL	0x00020000	/* the reroute file is optional */
#define RR_TRYAGAIN	0x00040000	/* defer address on open failure */
#define RR_MATCHALL	0x00080000	/* reroute all bang path addresses */
#define RR_MATCHLOCAL	0x00100000	/* match against local host names */
#define RR_MATCHDB	0x00200000	/* match against reroute database */
#define RR_BOUNCEONLY	0x00400000	/* restrict matchlocal to bounces */

/* private information stored per router file entry */
struct reroute_private {
    char *file;				/* file attribute */
    char *proto;			/* protocol name */
    char *domain;			/* optional domain names */
    char *required;			/* required domain names */
    int retries;			/* max count of retries */
    unsigned int interval;		/* sleep interval between retries */
    char *database;			/* internal - open database */
    char *error_text;			/* internal - error text from open */
};

extern void rtd_reroute __P((struct router *,
			     struct addr *,
			     struct addr **,
			     struct addr **,
			     struct addr **));
extern char *rtb_reroute __P((struct router *, struct attribute *));
extern void rtp_reroute __P((FILE *, struct router *));

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
