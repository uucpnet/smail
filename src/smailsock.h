/*
#ident	"@(#)smail/conf/lib:RELEASE-3_2_0_121:smailsock.h,v 1.6 2004/12/14 03:45:44 woods Exp"
*/

/*
 * smailsock.h -  for Smail's HAVE_BSD_NETWORKING code.
 *
 *   default set of header files that must be included to use BSD-style
 *   sockets, and BIND, as well as various related portability fixups...
 *
 * This should be included after "defs.h" and <sys/types> have both been included.
 *
 * NOTE:  This is the only #include file in which nested #includes are allowed.
 */

#ifdef HAVE_BSD_NETWORKING

# ifdef HAVE_SYS_TYPES_TCP_H	/* XXX define not yet supported */
#  include <sys/types.tcp.h>	/* suggested by Taylor UUCP */
# endif

# include <sys/socket.h>

# if defined(HAVE_WOLLONGONG_TCP)
#   include <sys/in.h>
#   include <sys/inet.h>
# else
#  include <netinet/in.h>	/* *BSD networking */
#  ifndef SV68R3V6		/* Motorola System V/68 R3V6 (with NSE Network Services Ext.) (XXX define not yet supported) */
#    include <netinet/tcp.h>
#  endif
# endif

# include <arpa/inet.h>

# include <netdb.h>
# if defined(__SVR4) || defined(__svr4__) || defined(DECLARE_H_ERRNO)
extern int h_errno;
# endif

/* XXX I'm no longer sure if this is needed -- it shouldn't be, but... */
# if defined(server) && defined(unix)
#  undef server			/* remove conflict with <sys/sysmacros.h> from AT&T System V Distributed UNIX (RFS) */
# endif

# ifndef INADDR_NONE
/*
 * XXX this should be 64-bit clean, but 64-bit platforms should not need it
 */
#  define INADDR_NONE		((unsigned long) (-1))
# endif

# ifndef INADDR_LOOPBACK
/*
 * XXX this may not be 64-bit clean, but 64-bit platforms should not need it
 */
#  define INADDR_LOOPBACK	((unsigned long) 0x7f000001)
# endif

# if defined(HAVE_NET_ERRNO_H) /* ISC System V/i386 R3 Networking (XXX define not yet supported) */
#  include <net/errno.h>
/* XXX should this be #else for the h_errno declaration? */
# endif

# ifdef HAVE_BIND
#  if defined(NOERROR) && (defined(__SVR4) || defined(__svr4__))
#   undef NOERROR		/* remove conflict  <sys/stream.h> from AT&T System V R4 */
#  endif
#  include <arpa/nameser.h>
#  include <resolv.h>
#  if !defined(NS_PACKETSZ)
#   if defined(PACKETSZ)
#    define NS_PACKETSZ		PACKETSZ	/* reverse BIND-4 compat */
#   else
#    include "ERROR: NS_PACKETSZ and PACKETSZ both undefined in <resolv.h>"
#   endif
#  endif
#  ifndef _PATH_RESCONF
#   define _PATH_RESCONF	"/etc/resolv.conf"
#  endif
# endif

# ifndef INET_ADDRSTRLEN
#  define INET_ADDRSTRLEN	((4 * 3) + 3 + 1) /* sizeof("255.255.255.255") */
# endif

/*
 * This function is in libresolv.so on SunOS-5.9, but not declared.  Oddly
 * inet_net_pton() _is_ declared in <arpa/inet.h>, right where it should be.
 */
#if defined(sun) && (defined(__SVR4) || defined(__svr4__))
extern char * inet_net_ntop __P((int, const void *, int, char *, size_t));
#endif

#endif /* HAVE_BSD_NETWORKING */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
