/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:silly.c,v 1.18 2004/02/02 04:43:55 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * silly:
 *	silly is a silly file which is linked in last to overcome a
 *	fencepost error in gdb
 *
 * NOTE: this function is left in for hisorical reasons
 *
 *	external functions: silly
 */

#include "defs.h"

#include <sys/types.h>
#include <stdio.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include "smail.h"
#include "alloc.h"
#include "list.h"
#include "main.h"
#include "addr.h"
#include "smailstring.h"
#include "dys.h"
#include "smailport.h"

#ifdef STANDALONE
char *local_sender;
#else /* !STANDALONE */
# include "extern.h"
extern char *local_sender;
#endif /* !STANDALONE */

/* local defines */
#define TOMBNAME (17)			/* max length of a tombsone name */

/*
 * silly - a very silly function
 *
 * display a silly message
 */
void
silly()
{
	char name[TOMBNAME+1];		/* name to put on tombstone */
	size_t sender_len;		/* length of the sender string */
	int pad_len;			/* spaces on left for centering */
	int i;				/* index */

	/*
	 * form the name in the center of the 'name' string
	 */
	compute_local_sender();
	sender_len = strlen(local_sender);
	sender_len = (sender_len > TOMBNAME) ? TOMBNAME : sender_len;
	pad_len = (TOMBNAME-sender_len)/2;
	for (i=0; i < pad_len; ++i) {
		name[i] = ' ';
	}
	strncpy(&name[pad_len], local_sender, sender_len);
	for (i=pad_len+sender_len; i < TOMBNAME; ++i) {
		name[i] = ' ';
	}
	name[TOMBNAME] = '\0';

	/*
	 * display a silly message
	 */
	printf("\n\nThe route-addr hits...");
	fflush(stdout);
	sleep(1);
	printf("\rYour mailer feels weaker");
	fflush(stdout);
	sleep(1);
	printf("\rThe route-addr hits...     ");
	fflush(stdout);
	sleep(1);
	printf("\n\n");
	printf("             ___________\n");
	printf("            /           \\\n");
	printf("           /    R I P    \\\n");
	printf("          /               \\\n");
	printf("         /                 \\\n");
	printf("        | %s |\n", name);
	printf("        |                   |\n");
	printf("        |    Eaten by a     |\n");
	printf("        |   chain letter    |\n");
	printf("        |    on level 1     |\n");
	printf("        |                   |\n");
	printf("        |       %4d        |\n", get_local_year());
	printf("        |                   |\n");
	printf("       *|      *  *  *      | *\n");
	printf(" ______)/\\/\\_//(\\/(/\\)/\\//\\/|_)______\n\n\n\n");

	return;
}

#ifdef STANDALONE

#ifndef HASH_STANDALONE
/*
 * main - see what the silly program does
 */
void
main()
{
	/* print the silly mssage */
	silly();
	exit(0);
}
#endif /* HASH_STANDALONE */

#endif /* STANDALONE */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
