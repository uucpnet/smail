/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:version.c,v 1.21 2004/02/02 03:20:17 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * version:
 *	return the current smail version
 */

#include "defs.h"

#include <sys/types.h>
#include <stdio.h>

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#include "smail.h"
#include "alloc.h"
#include "list.h"
#include "main.h"
#include "addr.h"
#include "dys.h"
#include "smailstring.h"
#include "version.h"
#include "extern.h"
#include "smailport.h"

/*
 * version - keep track of the smail version number
 *
 * external functions:
 *	version
 */
char *version_number = VERSION;
char *release_date = RELEASE_DATE;

static char *fmtver = NULL;	/* no version in the beginning */

/* XXX TODO ???:
 *
 * This could take an argument (it's passed the parameter name when invoked for
 * t_infoproc variables in format_attribute()) that would tell it what
 * information to supply, and thus it could be used to access the copyright,
 * and individual elements of what it now returns, compile_date, etc.  All that
 * would be required is the variable being added to conf_attributes with the
 * type t_infoproc, and a string compare lookup for the name supplied in here.
 */
char *
version()
{
    /*
     * form the version string for the first time if needed
     */
    if (!fmtver) {
	fmtver = xprintf("Smail-%s %s #%d", version_number, release_date, compile_num);
    }

    /*
     * return the formatted version string
     */
    return fmtver;
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
