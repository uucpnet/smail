/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:transport.c,v 1.68 2005/11/15 01:16:38 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * transport.c:
 *	process and deliver remote addresses.
 *
 *	The routines in this file are used after an address has been
 *	routed and resolved and associated with a particular transport.
 *	This file provides some support routines for transport drivers.
 *
 *	external functions:  assign_transports, call_transports,
 *			     write_message, get_return_addr, remote_from_line,
 *			     local_from_line, find_transport,
 *			     find_transport_driver
 */

#include "defs.h"

#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>

#ifdef STDC_HEADERS
# include <stdlib.h>
# include <stddef.h>
#else
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#ifdef HAVE_STRING_H
# if !defined(STDC_HEADERS) && defined(HAVE_MEMORY_H)
#  include <memory.h>
# endif
# include <string.h>
#endif
#ifdef HAVE_STRINGS_H
# include <strings.h>
#endif

#ifdef __STDC__
# include <stdarg.h>
#else
# include <varargs.h>
#endif

#ifdef TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# ifdef HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif

#if defined(HAVE_UNISTD_H)
# include <unistd.h>
#endif

#include "smail.h"
#include "alloc.h"
#include "list.h"
#include "main.h"
#include "parse.h"
#include "addr.h"
#include "hash.h"
#include "log.h"
#include "spool.h"
#include "smailstring.h"
#include "dys.h"
#include "exitcodes.h"
#include "route.h"
#include "transport.h"
#include "transports/smtplib.h"	/* for rfc821_quote_the_local_part() */
#include "smailconf.h"
#include "extern.h"
#include "debug.h"
#include "error.h"
#include "smailport.h"

/* variables exported from this file */
char *path_to_sender = NULL;		/* uucp-style route to sender */
int cached_transports = FALSE;		/* TRUE if cache_transports() called */

/* functions local to this file */
static int assign_compare __P((const void *, const void *));
static int write_bsmtp_prologue __P((FILE *, struct addr *));
static int write_bsmtp_epilogue __P((FILE *, struct transport *));
static void build_path_to_sender __P((void));
static char *tp_remove_header __P((char *, struct attribute *));
static char *tp_insert_header __P((char *, struct attribute *));
static char *tp_append_header __P((char *, struct attribute *));
static char *transport_driv_function __P((char *, struct attribute *));

static char *inet_from_line __P((struct addr *));


static struct attr_table transport_generic[] = {
    { "driver", t_string, 0, NULL, NULL, OFFSET(transport, driver) },
    { "max_addrs", t_int, 0, NULL, NULL, OFFSET(transport, max_addrs) },
    { "max_hosts", t_int, 0, NULL, NULL, OFFSET(transport, max_hosts) },
    { "max_chars", t_int, 0, NULL, NULL, OFFSET(transport, max_chars) },
    { "retry_dir", t_string, 0, NULL, NULL, OFFSET(transport, retry_dir) },
    { "shadow", t_string, 0, NULL, NULL, OFFSET(transport, shadow) },
    { "error_transport", t_string, 0, NULL, NULL,
	  OFFSET(transport, error_transport) },
    { "strict", t_boolean, 0, NULL, NULL, STRICT_TPORT },
    { "uucp_xform", t_boolean, 0, NULL, NULL, UUCP_XFORM },
    { "uucp", t_boolean, FA_FMT_DEPRECATED, NULL, NULL, UUCP_XFORM },
    { "inet_xform", t_boolean, 0, NULL, NULL, INET_XFORM },
    { "inet", t_boolean, FA_FMT_DEPRECATED, NULL, NULL, INET_XFORM },
    { "local_xform", t_boolean, 0, NULL, NULL, LOCAL_XFORM },
    { "received", t_boolean, 0, NULL, NULL, PUT_RECEIVED },
    { "return_path", t_boolean, 0, NULL, NULL, PUT_RETURNPATH },
    { "from", t_boolean, 0, NULL, NULL, PUT_FROM },
    { "local", t_boolean, 0, NULL, NULL, LOCAL_TPORT },
    { "crlf", t_boolean, 0, NULL, NULL, PUT_CRLF },
    { "bsmtp", t_boolean, 0, NULL, NULL, BSMTP_TPORT },
    { "hbsmtp", t_boolean, 0, NULL, NULL, HBSMTP_TPORT },
    { "dots", t_boolean, 0, NULL, NULL, PUT_DOTS },
    { "debug", t_boolean, 0, NULL, NULL, DEBUG_TPORT },
    { "unix_from_hack", t_boolean, 0, NULL, NULL, UNIX_FROM_HACK },
    { "uucp_from_hack", t_boolean, FA_FMT_DEPRECATED, NULL, NULL, UNIX_FROM_HACK }, /* XXX ancient deprecated alias */
    { "remove_header", t_proc, 0, NULL, (tup_t *)tp_remove_header, 0 },
    { "insert_header", t_proc, 0, NULL, (tup_t *)tp_insert_header, 0 },
    { "append_header", t_proc, 0, NULL, (tup_t *)tp_append_header, 0 },
};


/*
 * assign_transports - assign instances of transports for addresses
 *
 * given a list of addresses as input, produce a list assigning a list
 * of remote addresses to specific instances of a transport.  One
 * instance essentially maps onto exactly one call to the underlying
 * transport driver.
 *
 * NOTE:  This routine breaks the forward links between addr structures.
 */
struct assign_transport *
assign_transports(list)
    struct addr *list;			/* list of addresses */
{
    register struct addr *aq;		/* temp */
    register struct assign_transport *asn; /* list of assigned transports */
    register struct addr **aap;		/* points to array for sorting */
    register int i;			/* index */
    size_t ct;				/* count of items in input list */
    int host_ct = 0;			/* count of hosts in instance */
    int addr_ct = 0;			/* count of addrs in instance */
    int char_ct = 0;			/* count of chars in instance */
    struct transport *last_transport = NULL; /* last assigned transport */
    char *last_host;			/* last assigned host */
    char *last_owner;                   /* last assigned owner */

    /* count the number of input items */
    for (ct = 0, aq = list; aq; aq = aq->succ) {
	ct++;
    }
    if (ct == 0) {
	DEBUG(DBG_REMOTE_MID, "assign_transports() called with nothing to do....\n");
	return NULL;
    }
    DEBUG1(DBG_REMOTE_HI, "assign_transports() called with %d addrs...\n", ct);

    /* create an array and copy remote information to it for sorting */
    aap = (struct addr **) xmalloc((size_t) (ct * sizeof(*aap)));
    for (aq = list, i = 0; aq; aq = aq->succ, i++) {
	aap[i] = aq;
    }

    qsort((char *) aap, ct, sizeof(*aap), assign_compare);

    /*
     * we now have an array sorted by transport and host.
     * pass through that array and assign addr structures to
     * specific transport instances, taking into account
     * max_addr and max_hosts for the transports.
     */
    last_host = NULL;
    last_owner = (char *) (-1);		/* can't use 0 as owner can be unset */
    for (asn = NULL, i = ct-1; i >= 0; --i) {
	/* possible reasons to switch to a new instance */
	if (
	    /* we have a different transport */
	    aap[i]->transport != last_transport ||

            /* Same host, but different address owner */
            (aap[i]->next_host && last_host &&
              EQIC(aap[i]->next_host, last_host) &&
              aap[i]->owner != last_owner &&
              (aap[i]->owner == NULL || last_owner == NULL ||
              !EQIC(aap[i]->owner, last_owner))) ||

            /* maximum number of hosts per transport call exceeded */
	    (last_transport->max_hosts &&
	      aap[i]->next_host && last_host &&
	      !EQIC(aap[i]->next_host, last_host) &&
	      last_transport->max_hosts < ++host_ct) ||

	    /* maximum number of addrs per transport call exceeded */
	    (last_transport->max_addrs &&
	      last_transport->max_addrs < ++addr_ct) ||

	    /* maximum number of chars of addr per call exceeded */
	    (last_transport->max_chars &&
	      last_transport->max_chars <
		   (char_ct += (int)strlen(aap[i]->next_addr) + 3)))
	{
	    /* initialize for a new instance */
	    struct assign_transport *newasn;

	    DEBUG1(DBG_REMOTE_HI, "new instance ... transport=<%v>\n",
		   aap[i]->transport->name);
	    newasn = (struct assign_transport *)xmalloc(sizeof(*newasn));
	    last_transport = aap[i]->transport;
	    last_host = aap[i]->next_host;
            last_owner = aap[i]->owner;
	    host_ct = 1;
	    addr_ct = 1;
	    char_ct = strlen(aap[i]->next_addr) + 3;
	    /* fill in the first entry of the instance */
	    aap[i]->succ = NULL;
	    newasn->succ = asn;
	    asn = newasn;
	} else {
	    /* add the addr to the previous instance */
	    aap[i]->succ = asn->addr;
	}
	asn->addr = aap[i];
	DEBUG2(DBG_REMOTE_HI, "adding...host=<%v>, addr=<%v>\n",
	       asn->addr->next_host, asn->addr->next_addr);
    }

    /* all done, free temps and return the list */
    xfree((char *)aap);
    return asn;
}

/*
 * compare two struct addrs for transport and then hostname,
 * and then owner 
 *
 * Note that we are sorting an array of "struct addr *" not an array of "struct
 * addr", so we'll be called with a pair of "struct addr **".
 */
static int
assign_compare(x, y)
    const void *x;
    const void *y;
{
    const struct addr *a = *((const struct addr * const *) x);
    const struct addr *b = *((const struct addr * const *) y);
    register int ret = strcmp(a->transport->name, b->transport->name);

    if (ret) {
	/* transports are unequal, return value just based on that */
	return ret;
    }

    /* if next_host not defined, the addrs are equal */
    if (a->next_host == NULL || b->next_host == NULL) {
	ret = 0;
    } else {
	ret = strcmpic(a->next_host, b->next_host);
    }

    if (ret) {
        /* hosts are unequal, return value just based on that */
        return ret;
    }

    /* compare owner fields */
    if (a->owner == NULL) {
	    return (b->owner == NULL) ? 0 : (-1);
    }
    return (b->owner == NULL) ? 1 : strcmpic(a->owner, b->owner);
}


/*
 * call_transports - call transport drivers for assigned transports
 *
 * given a list produced by assign_transports, call the transport drivers
 * to actually perform delivery to addresses.
 */
void
call_transports(asn, defer, fail)
    register struct assign_transport *asn; /* list of assigned instances */
    struct addr **defer;		/* addrs to defer to a later time */
    struct addr **fail;			/* unresolvable addrs */
{
    struct addr *succeed;		/* succeeded addrs from drivers */
    struct addr *tp_defer;		/* defer addrs from drivers */
    struct addr *tp_fail;		/* fail addrs from drivers */
    struct addr *shadow_list = NULL;	/* shadow delivery list */
    struct assign_transport *sh_asn;	/* shadow transport instances */

    for (; asn; asn = asn->succ) {
	time_t started;
	struct transport_driver *driver;
	register struct transport *tp = asn->addr->transport;

	/*
	 * don't deliver to remote transports if the hop_count would
	 * pass beyond maximum.
	 */
	if (hop_count >= max_hop_count && !(tp->flags&LOCAL_TPORT)) {
	    /*
	     * ERR_145 - maximum hop count exceeded
	     *
	     * DESCRIPTION
	     *      If a remote transport were performed, the maximum hop
	     *      count, as set by the max_hop_cound attribute in the
	     *      config file, would be exceeded.  This condition is
	     *      assumed to be a result of a loop condition where two
	     *      machines are sending mail back and forth to each other.
	     *
	     * ACTIONS
	     *      An error message is sent back to the sender stating the
	     *      error.  Note that if one of the sites back to the sender
	     *      has a shorter value for max_hop_count, then the error
	     *      message will fail to reach the sender though, hopefully,
	     *      a message will be sent back to the postmaster at the
	     *      local site.
	     *
	     * RESOLUTION
	     *      Determine which two sites are causing the loop condition
	     *      and remove or correct the forward or alias from one of
	     *      those two sites.  If the problem is not actually a loop
	     *      condition but a long path, a shorter path should be
	     *      used.  It is unlikely that paths longer than 10 or 15
	     *      hops should ever be required.
	     */
	    struct error *loop_error =
		note_error(ERR_NSENDER|ERR_145,
			   "loop detection: maximum hop count exceeded");
	    register struct addr *cur;

	    /* NOTE:  the sequence of operations here IS correct */
	    for (cur = asn->addr; cur; cur = cur->succ) {
		cur->error = loop_error;
	    }
	    fail_delivery(asn->addr);
	    insert_addr_list(asn->addr, fail, loop_error);
	    continue;
	}

	/* lookup the driver */
	driver = find_transport_driver(tp->driver);

	if (driver == NULL) {
	    /* configuration error, the driver does not exist */
	    /*
	     * ERR_146 - transport driver not found
	     *
	     * DESCRIPTION
	     *      The driver name was not in the table of transport
	     *      drivers.
	     *
	     * ACTIONS
	     *      Defer addresses with configuration errors.
	     *
	     * RESOLUTION
	     *      The postmaster must check the transport configuration
	     *      before delivery can be performed.
	     */
	    insert_addr_list(asn->addr,
			     defer,
			     note_error(ERR_CONFERR|ERR_146,
					xprintf(
					   "transport %v: driver %s not found",
						tp->name, tp->driver)));
	    continue;
	}

	/* determine official time of attempt */
	(void) time(&started);

	/* call the transport */
	succeed = NULL;
	tp_defer = NULL;
	tp_fail = NULL;

	DEBUG2(DBG_REMOTE_MID, "transport %v calling driver %s...\n",
	       tp->name, tp->driver);
	(*driver->driver)(asn->addr, &succeed, &tp_defer, &tp_fail);

	/* subject transport-deferred addresses to retry duration limits */
	tp_defer = retry_addr_after(started, tp_defer, fail);

	/* look at successful deliveries */
	if (succeed) {
	    retry_addr_finished(succeed);
	    /* log successes */
	    succeed_delivery(succeed);
	    if (tp->shadow) {
		/* on successful delivery, call the shadow transport */
		struct transport *shadow_tp = find_transport(tp->shadow);

		if (shadow_tp == NULL) {
		    send_to_postmaster = TRUE;
		    write_log(WRITE_LOG_MLOG,
			      "Xnotice: <> transport %v: shadow transport %v not found",
			      tp->name, tp->shadow);
		    write_log(WRITE_LOG_PANIC,
			      "transport %v: shadow transport %v not found",
			      tp->name, tp->shadow);
		} else {
		    /* insert in shadow delivery list, with new transport */
		    register struct addr *cur;
		    register struct addr *next;

		    for (cur = succeed; cur; cur = next) {
			next = cur->succ;
			cur->flags |= ADDR_SHADOW;
			cur->transport = shadow_tp;
			cur->succ = shadow_list;
			shadow_list = cur;
		    }
		}
	    }
	}

	/* look at deferred deliveries */
	if (tp_defer) {
	    retry_addr_finished(tp_defer);
	    insert_addr_list(tp_defer, defer, (struct error *)NULL);
	}

	/* look at failed deliveries */
	if (tp_fail) {
	    retry_addr_finished(tp_fail);
	    fail_delivery(tp_fail);	/* log them */
	    if (tp->error_transport == NULL) {
		insert_addr_list(tp_fail, fail, (struct error *)NULL);
	    } else {
		/* on failed delivery, call the error transport */
		struct transport *error_tp =
		    find_transport(tp->error_transport);

		if (error_tp == NULL) {
		    send_to_postmaster = TRUE;
		    write_log(WRITE_LOG_MLOG,
			      "Xnotice: <> transport %v: error transport %v not found",
			      tp->name, tp->error_transport);
		    write_log(WRITE_LOG_PANIC,
			      "transport %v: error transport %v not found",
			      tp->name, tp->error_transport);
		} else {
		    /* insert in shadow delivery list, with new transport */
		    register struct addr *cur;
		    register struct addr *next;

		    for (cur = tp_fail; cur; cur = next) {
			next = cur->succ;
			cur->transport = error_tp;
			cur->succ = shadow_list;
			shadow_list = cur;
		    }
		}
	    }
	}
    }

    /*
     * log any delivery deferrals after all transports have had their say
     */
    if (defer) {
	defer_delivery(*defer);
    }

    /* if there are shadow deliveries to be made, assign them and transport */
    if (shadow_list == NULL) {
	return;				/* no shadows, just return */
    }

    /* assign shadow transports, if any, and perform deliveries */
    for (sh_asn = assign_transports(shadow_list);
	 sh_asn;
	 sh_asn = sh_asn->succ)
    {
	struct transport_driver *driver;
	register struct transport *tp = sh_asn->addr->transport;
	struct addr *dummy_defer;

	/* lookup the driver */
	driver = find_transport_driver(tp->driver);

	if (driver == NULL) {
	    /* configuration error: for shadows, send to postmaster */
	    send_to_postmaster = TRUE;
	    write_log(WRITE_LOG_MLOG,
		      "Xnotice: <> shadow transport %v: driver %s not found",
		      tp->name, tp->driver);
	    write_log(WRITE_LOG_SYS,
		      "shadow transport %v: driver %s not found",
		      tp->name, tp->driver);
	    continue;
	}

	/* call the shadow transport */
	DEBUG2(DBG_REMOTE_LO, "calling driver %s from shadow transport %v\n",
	       tp->driver, tp->name);
	succeed = NULL;
	tp_fail = NULL;
	(*driver->driver)(sh_asn->addr, &succeed, &dummy_defer, &tp_fail);

	/* log failed addresses */
	if (fail) {
	    struct addr *cur;
	    struct addr *next;

	    for (cur = tp_fail; cur; cur = next) {
		next = cur->succ;
		write_log(WRITE_LOG_SYS, "%v ... shadow transport %v failed: %v",
			  cur->in_addr, cur->transport->name, cur->next_addr);
		if ((cur->flags & ADDR_SHADOW) == 0) {
		    /*
		     * put failed deliveries from an error_transport on
		     * output fail list
		     */
		    cur->succ = *fail;
		    *fail = cur;
		}
	    }
	}
    }
}


/*
 * cache_transports - call cache entrypoints for all transports
 *
 * cache information used by transport drivers.  This can be called
 * when it is determined that there will be an attempt to deliver more
 * than one mail message, to increase the overall efficiency of the
 * mailer.
 *
 * Daemons can call this periodically to recache stale data.
 */
void
cache_transports()
{
    struct transport *tp;		/* temp for stepping thru transports */
    struct transport_driver *driver;

    for (tp = transports; tp; tp = tp->succ) {
	driver = find_transport_driver(tp->driver);
	if (driver && driver->cache) {
	    (*driver->cache)(tp);
	}
    }
    cached_transports = TRUE;
}

#ifdef notyet
/*
 * finish_transports - free resources used by all directors
 *
 * free information that was cached by transports or used by
 * transports in the process of delivering messages.  Transports can
 * cache data for efficiency, or can maintain state between
 * invocations.  This function is called when transports will no
 * longer be needed, allowing transports to free any resources that
 * they were using that will no longer be needed.  For example, it is
 * a good idea for transports to close any files that they opened, as
 * file descriptors are a precious resource in some machines.
 */
void
finish_transports()
{
    struct transports *tp;		/* temp for stepping thru transports */
    struct transport_driver *driver;

    for (tp = transports; tp; tp = tp->succ) {
	driver = find_transport_driver(tp->driver);
	if (driver->finish) {
	    (*driver->finish)(dp);
	}
    }
    cached_transports = FALSE;
}
#endif


/*
 * write_message - write out the current message to an open file
 *
 * this takes a transport file entry and a list of addr structures
 * and writes the message in the manner specified in the transport
 * file entry.  It is intended to be called from transport drivers.
 *
 * If BSMTP_TPORT is set in the transport flags, an SMTP envelope
 * is placed around the message.  However, result codes are not
 * read from the destination so this is not useful for interactive
 * SMTP.  The list of addr structures is only used for writing the
 * SMTP envelope.  HBSMTP_TPORT generates an SMTP envelope without
 * the initial HELO or the final QUIT commands.
 *
 * NOTE:  Everybody still needs to call fflush after calling
 *	  write_message().
 *
 * return READ_FAIL, WRITE_FAIL or SUCCEED
 */
int
write_message(f, addr)
    FILE *f;				/* write on this file */
    struct addr *addr;			/* list of addresses */
{
    struct transport *tp = addr->transport;
    int fail;				/* returned failure codes */

    /*
     * put out the initial smtp commands up to the DATA command
     */
    if (tp->flags & (BSMTP_TPORT | HBSMTP_TPORT)) {
	DEBUG(DBG_HEADER_LO, "BSMTP Envelope text...\n");
#ifndef NODEBUG
	if (debug >= DBG_HEADER_LO && errfile) {
	    (void) write_bsmtp_prologue(errfile, addr);
	}
#endif	/* NODEBUG */
	if (write_bsmtp_prologue(f, addr) == FAIL) {
	    return WRITE_FAIL;
	}
	tp->flags |= PUT_DOTS;		/* force use of SMTP dot protocol */
    }

    DEBUG(DBG_HEADER_LO, "... message header ...");
    /*
     * write out a From<space> line, if required
     */
    if (tp->flags & PUT_FROM) {
	/* but what form do we need? */
	if (tp->flags & INET_XFORM) {
#ifndef NODEBUG
	    if (debug >= DBG_HEADER_LO && errfile) {
		(void)fputs(inet_from_line(addr), errfile);
	    }
#endif	/* NODEBUG */
	    if (fputs(inet_from_line(addr), f) == EOF) {
		return WRITE_FAIL;
	    }
	} else if (tp->flags & LOCAL_XFORM) {
#ifndef NODEBUG
	    if (debug >= DBG_HEADER_LO && errfile) {
		(void)fputs(local_from_line(), errfile);
	    }
#endif	/* NODEBUG */
	    if (fputs(local_from_line(), f) == EOF) {
		return WRITE_FAIL;
	    }
	} else {
#ifndef NODEBUG
	    if (debug >= DBG_HEADER_LO && errfile) {
		(void)fputs(remote_from_line(), errfile);
	    }
#endif	/* NODEBUG */
	    if (fputs(remote_from_line(), f) == EOF) {
		return WRITE_FAIL;
	    }
	}
	DEBUG(DBG_HEADER_LO, "\n");
	/* put the end of line */
	if (tp->flags & PUT_CRLF) {
	    if (putc('\r', f) == EOF) {
		return WRITE_FAIL;
	    }
	}
	if (putc('\n', f) == EOF) {
	    return WRITE_FAIL;
	}
    }

    /*
     * write out the header according to the transport flags
     */
    if (write_header(f, addr) == FAIL) {
	return WRITE_FAIL;
    }
    if ((tp->flags & PUT_CRLF) && putc('\r', f) == EOF) {
	return WRITE_FAIL;
    }
    if (putc('\n', f) == EOF) {
	return WRITE_FAIL;
    }

    if (tp->flags & DEBUG_TPORT) {
	/* if we are debugging, don't dump the body, dump interesting
	 * debugging info instead */
	extern char **save_argv;	/* import this from main */
	char **av;

	(void) fprintf(f, "|---- original flags ----|\n");
	for (av = save_argv; *av; av++) {
	    dprintf(f, "%v\n", *av);
	}
	(void) fprintf(f, "|---- interesting info ----|\n");
	dprintf(f,"\
	message_id=%s\n\
	spool_dir=%s\n\
	spool_fn=%s\n\
	transport=%v\n\
	next_host=%v\n\
	msg_grade=%c\n",
		       message_id,
		       spool_dir,
		       spool_fn,
		       tp->name,
		       (addr && addr->next_host)? addr->next_host: "(null)",
		       msg_grade);

	send_log(f, "|---- per-message log ----|\n");
    } else {

	DEBUG(DBG_HEADER_LO, "... message body .");

	/*
	 * write out the body.
	 */
	if ((fail = write_body(f, tp->flags)) != SUCCEED) {
	    return fail;
	}
	DEBUG(DBG_HEADER_LO, "\n");
    }

    /*
     * finish off the SMTP envelope
     */
    if (tp->flags & (BSMTP_TPORT | HBSMTP_TPORT)) {
	DEBUG(DBG_HEADER_LO, "BSMTP epilogue...\n");
#ifndef NODEBUG
	if (debug >= DBG_HEADER_LO && errfile) {
	    (void) write_bsmtp_epilogue(errfile, tp);
	}
#endif	/* NODEBUG */
	if (write_bsmtp_epilogue(f, tp) == FAIL) {
	    return WRITE_FAIL;
	}
    }

    return SUCCEED;			/* everything went fine */
}

/*
 * write_bsmtp_prologue - write the initial SMTP commands for BSMTP transports
 *
 * write out the HELO, MAIL FROM, RCPT TO and DATA commands.
 */
static int
write_bsmtp_prologue(f, addr)
    FILE *f;				/* file to write commands to */
    struct addr *addr;			/* recipient addr structures */
{
    struct transport *tp = addr->transport;
    char *return_address;

    /*
     * how do we end a line?
     */
    char *eol = (tp->flags & PUT_CRLF)? "\r\n": "\n";

    if (! (tp->flags & HBSMTP_TPORT) &&
	dprintf(f, "HELO %v%s", primary_name, eol) == EOF)
    {
	return FAIL;
    }

    /*
     * determine which sender form is appropriate
     */
    return_address = get_return_addr(addr);

    if (!return_address) {
	return FAIL;
    }
    if (dprintf(f, "MAIL FROM:<%v>%s", rfc821_quote_the_local_part(return_address), eol) == EOF) {
	xfree(return_address);
	return FAIL;
    }
    xfree(return_address);
    return_address = NULL;

    /*
     * write out a RCPT TO: line for each recipient address
     */
    while (addr) {
	if (dprintf(f, "RCPT TO:<%v>%s", addr->next_addr, eol) == EOF) {
	    return FAIL;
	}
	addr = addr->succ;
    }

    if (fprintf(f, "DATA%s", eol) == EOF) {
	return FAIL;
    }

    return SUCCEED;
}

/*
 * get_return_addr - return a sender address in the right form for use by a
 *		     particular transport
 *
 * Returns a transformation of the sender address based on the various
 * transport attributes and flags.
 */
char *
get_return_addr(addr)
    struct addr *addr;
{
    struct transport *tp = addr->transport;
    char *sender_to_check;
    char *return_address;
    char *target;
    char *remainder;
    int form;
    int flags;

    DEBUG(DBG_REMOTE_MID, "\n");

    /*
     * the error_sender flag is set in main.c when sender is set itself so that
     * we don't have to keep comparing the string against the special and
     * standard null return path forms.
     *
     * note we don't use the owner address in this case either....
     */
    if (error_sender) {
	DEBUG3(DBG_REMOTE_MID, "transport %v: have the error sender address: '%v', returning as '%v'\n",
	       tp->name,
	       sender,
	       ((islocal && (tp->flags & LOCAL_XFORM)) ? "+": ""));
	return COPY_STRING(((islocal && (tp->flags & LOCAL_XFORM)) ? "+": ""));
    }

    if (addr->owner) {
	return_address = addr->owner;
    } else {
	return_address = sender;
    }
    DEBUG3(DBG_REMOTE_HI, "transport %v: starting with %sreturn_address: <%v>\n",
	   tp->name,
	   addr->owner ? "(owner) " : "",
	   return_address);

    /*
     * special handling for uucp_xform
     */
    if (tp->flags & UUCP_XFORM) {
	char *err;
	char *path;

	path = build_partial_uucp_route(return_address, &err, 0);
	if (path) {
	    return_address = xprintf("%s!%s", uucp_name, path);
	    xfree(path);
	} else {
	    /*
	     * bad sender path, prepend uucp_name! anyway.
	     *
	     * XXX or maybe we should return NULL?
	     */
	    write_log(WRITE_LOG_SYS | WRITE_LOG_PANIC,
		      "transport %v: uucp_xform generated invalid path for %s address <%v>: %s",
		      tp->name,
		      addr->owner ? "owner" : "sender",
		      sender,
		      err ? err : "(no error given)");
	    return_address = xprintf("%s!%s", uucp_name, return_address);
	}
	DEBUG3(DBG_REMOTE_MID, "transport %v: with uucp_xform using %s address: <%v>\n",
	       tp->name,
	       addr->owner ? "owner" : "sender",
	       return_address)

	return return_address;
    }

    /* XXX resolve_addr_list(), or the possible future (addr->owner_addr->form)... */
    sender_to_check = COPY_STRING(sender);
    form = parse_address(sender_to_check, &target, &remainder, &flags);
    switch (form) {
    case FAIL:				/* XXX should be impossible due to check_sender() */
    case PARSE_ERROR:			/* XXX should be impossible due to check_sender() */
	write_log(WRITE_LOG_SYS | WRITE_LOG_PANIC,
		  "transport %v: invalid %s address <%v>: %s",
		  tp->name,
		  addr->owner ? "owner" : "sender",
		  sender,
		  remainder ? remainder : "(no error given)");
	/* it'll usually end up being MAILER-DAEMON, but we let the caller decide */
	return_address = NULL;
	break;

    case BERKENET:
    case DECNET:
	write_log(WRITE_LOG_SYS | WRITE_LOG_PANIC,
		  "transport %v: deprecated form of %s address <%v>: %s",
		  tp->name,
		  addr->owner ? "owner" : "sender",
		  sender,
		  (form == BERKENET) ? "BERKENET" :
		  (form == DECNET) ? "DECNET" : "<INTERNAL_ERROR>");
	/* note we always rewrite these forms... */
	return_address = xprintf("%s@%s", remainder, target);
	break;
	
    case RFC_ROUTE:
    case RFC_ENDROUTE:
#if 0
	if (tp->flags & strict) {	/* XXX is this the right flag to use?  Probably not... */
	    /* then the routes are to be stripped off too.... */
	} else {
#endif
	    write_log(WRITE_LOG_SYS | WRITE_LOG_PANIC,
		      "transport %v: undesirable form of %s address <%v>: %s",
		      tp->name,
		      addr->owner ? "owner" : "sender",
		      sender,
		      (form == RFC_ROUTE) ? "RFC_ROUTE" :
		      (form == RFC_ENDROUTE) ? "RFC_ENDROUTE" : "<INTERNAL_ERROR>");
	    return_address = COPY_STRING(sender);
#if 0
	}
#endif
	break;

    case MAILBOX:
	/*
	 * note we never actually undo this form when local_xform is set, even
	 * if the target is the same as visible_name as the envelope address
	 * should really always stay in fully a qualified form.
	 */
	return_address = xprintf("%s@%s", remainder, target);
	break;

    case UUCP_ROUTE:
	if (tp->flags & INET_XFORM) {
	    if (strchr(target, '!') == NULL) {
		return_address = xprintf("%s@%s", remainder, target);
	    } else {
		char *r_route = uucp_to_route_addr(target);
		char *r_last;

		r_last = strrchr(r_route, ',');
		if (r_last) {
		    *r_last++ = '\0';
		    r_last++;		/* skip the @ */
		    return_address = xprintf("%s:%s@%s", r_route, remainder, r_last);
		} else {
		    /* XXX this doesn't look right. */
		    return_address = xprintf("%s@%s", remainder, r_route + 1);
		}
		xfree(r_route);
	    }
	} else {	/* local_xform */
	    return_address = COPY_STRING(sender); /* leave as it was */
	}
	break;

    case PCT_MAILBOX:
	write_log(WRITE_LOG_SYS | WRITE_LOG_PANIC,
		  "transport %v: deprecated form of %s address <%v>: PCT_MAILBOX",
		  tp->name,
		  addr->owner ? "owner" : "sender",
		  sender);
	/* note we always transform this to a plain MAILBOX form! */
	return_address = xprintf("%s@%s", remainder, target);
	break;

    case LOCAL:
	/* leave in local form only if local_xform */
	if (tp->flags & LOCAL_XFORM) {
	    return_address = COPY_STRING(remainder);
	} else {
	    return_address = xprintf("%s@%s", remainder, visible_name);
	}
	break;
    }
    xfree(sender_to_check);
    DEBUG3(DBG_REMOTE_MID, "transport %v: using %s address: <%v>\n",
	   tp->name,
	   addr->owner ? "owner" : "sender",
	   return_address);

    return return_address;
}

/*
 * write_bsmtp_epilogue - write tailing commands for BSMTP transports
 *
 * finish off the data command and write out the QUIT command.
 */
static int
write_bsmtp_epilogue(f, tp)
    FILE *f;
    struct transport *tp;
{
    if (tp->flags & HBSMTP_TPORT) {
	if (fprintf(f, (tp->flags&PUT_CRLF)? ".\r\n": ".\n") == EOF) {
	    return FAIL;
	}
	return SUCCEED;
    }
    if (tp->flags & PUT_CRLF) {
	if (fprintf(f, ".\r\nQUIT\r\n") == EOF) {
	    return FAIL;
	}
    } else {
	if (fprintf(f, ".\nQUIT\n") == EOF) {
	    return FAIL;
	}
    }

    return SUCCEED;
}


/*
 * remote_from_line - build a From_ line for remote transports
 */
char *
remote_from_line()
{
    static char *from_line = NULL;	/* saved From_ line */

    if (from_line) {
	return from_line;		/* exists, use it */
    }
    if (path_to_sender == NULL) {
	build_path_to_sender();
    }
    if (uucp_name) {
        from_line = xprintf("From %s %s remote from %s",
     			    path_to_sender, unix_date(), uucp_name);
    } else {
        from_line = xprintf("From %s %s", path_to_sender, unix_date());
    }

    return from_line;
}

/*
 * local_from_line - build a From_ line for local transports
 */
char *
local_from_line()
{
    static char *from_line = NULL;	/* saved From_ line */

    if (from_line) {
	return from_line;		/* exists, use it */
    }
    if (path_to_sender == NULL) {
	build_path_to_sender();
    }
    from_line = xprintf("From %s %s", path_to_sender, unix_date());
    return from_line;
}

/*
 * inet_from_line - build a From_ line for an inet-based transport
 */
static char *
inet_from_line(addr)
    struct addr *addr;
{
    static char *from_line = NULL;	/* saved From_ line */
    char *return_address;

    if (from_line) {
	return from_line;		/* exists, use it */
    }
    return_address = get_return_addr(addr);
    from_line = xprintf("From %s %s", return_address ? return_address : "MAILER-DAEMON", unix_date());
    if (return_address) {
	xfree(return_address);
    }
    return from_line;
}

/*
 * build_path_to_sender - build a !-route version of sender address
 */
static void
build_path_to_sender()
{
    char *error;

    path_to_sender = build_partial_uucp_route(sender, &error, 0);
    if (path_to_sender == NULL) {
	write_log(WRITE_LOG_MLOG,
		  "Xnotice: <> error building path to sender, sender=%v, error=%s",
		  sender, error);
	write_log(WRITE_LOG_PANIC,
		  "error building path to sender, sender=%v, error=%v",
		  sender, error);
	path_to_sender = "Postmaster";
    }
}


/*
 * find_transport - given a transport's name, return the transport structure
 *
 * return NULL if no transport of that name exists.
 */
struct transport *
find_transport(name)
    register char *name;		/* search key */
{
    register struct transport *tp;	/* temp for stepping thru transports */

    /* loop through all the transports */
    for (tp = transports; tp; tp = tp->succ) {
	if (EQ(tp->name, name)) {
	    /* found the transport in question */
	    return tp;
	}
    }

    return NULL;			/* transport not found */
}

/*
 * find_transport_driver - given a driver's name, return the driver structure
 *
 * return NULL if driver does not exist.
 */
struct transport_driver *
find_transport_driver(name)
    register char *name;		/* search key */
{
    register struct transport_driver *tdp; /* pointer to table of drivers */

    for (tdp = transport_drivers; tdp->name; tdp++) {
	if (EQ(tdp->name, name)) {
	    return tdp;			/* found the driver */
	}
    }

    return NULL;			/* driver not found */
}


/*
 * read_transport_file - read transport file
 *
 * read the transport file and build the transport list describing the
 * entries.  Return an error message or NULL.
 *
 * The entries from the transport file are prepended to the built-in
 * transports.
 */
char *
read_transport_file()
{
    FILE *f;				/* open transport file */
    char *error;			/* error from read_standard_file() */
    struct transport **tpp;
    struct stat statbuf;
    /* FIXME:  avoid this ugly stuff -- use null-entry termination semantics... */
    struct attr_table *end_transport_generic = ENDTABLE(transport_generic);
    static struct transport transport_template = {
	NULL,				/* name */
	"pipe",				/* driver, a reasonable default */
	NULL,				/* succ will be assigned */
	PUT_RECEIVED,			/* flags */
	1,				/* max_addrs */
	1,				/* max_hosts */
	2000,				/* max_chars, about half of NCARGS? */
	NULL,				/* headers to remove */
	NULL,				/* headers to insert */
	NULL,				/* headers to append */
	NULL,				/* name of directory for retry files */
	NULL,				/* shadow */
	NULL,				/* error_transport */
	NULL,				/* private */
    };

    /*
     * try to open the transport file, stat file if possible
     */
    if (transport_file == NULL || EQ(transport_file, "-")) {
	return NULL;
    }
    f = fopen(transport_file, "r");
    if (f == NULL) {
	if (require_configs) {
	    return xprintf("%v: %s", transport_file, strerror(errno));
	}

	add_config_stat(transport_file, (struct stat *)NULL);
	return NULL;
    }

    (void)fstat(fileno(f), &statbuf);
    add_config_stat(transport_file, &statbuf);

    /* call read_standard_file to do the real work */
    error = read_standard_file(f, transport_file,
			       (char *) &transport_template,
			       sizeof(struct transport),
			       OFFSET(transport, name),
			       OFFSET(transport, flags),
			       OFFSET(transport, succ),
			       transport_generic,
			       end_transport_generic,
			       transport_driv_function,
			       (char **) &transports);

    /* finish up */
    (void) fclose(f);

    for (tpp = &transports; *tpp; tpp = &(*tpp)->succ) {
	int unique_flags = (((*tpp)->flags & UUCP_XFORM) ? 1 : 0) +
			   (((*tpp)->flags & INET_XFORM) ? 1 : 0) +
			   (((*tpp)->flags & LOCAL_XFORM) ? 1 : 0);

	if (unique_flags > 1) {
	    error = xprintf("%s%stransport %v: only one of uucp_xform, inet_xform, or local_xform are allowed",
			    error ? error : "",
			    error ? "; " : "",
			    (*tpp)->name);
	}
    }
    *tpp = builtin_transports;

    /* return any error message */
    if (error) {
	return xprintf("%v: %s", transport_file, error);
    }
    return NULL;
}


void
dump_transport_config(f)
     FILE * f;
{
    struct transport *tp;		/* temp for stepping thru transports */
    /* FIXME:  avoid this ugly stuff -- use null-entry termination semantics... */
    struct attr_table *end_transport_generic = ENDTABLE(transport_generic);
    char * name;			/* name of transport */
    unsigned long flags;		/* flags for transport */
    struct transport_driver *driver;	/* drivers for transport */
    charplist_t *lp;			/* list pointer for header stuff */
    struct hash_table * tp_hash;	/* hash table to find multiple defs */

    DEBUG(DBG_ROUTE_HI, "dump transport configs called\n");
    fputs("#\n# -- transports configuration\n", f);

    tp_hash = new_hash_table(hit_table_len,
			     (struct block *) NULL,
			     HASH_DEFAULT);
    /*
     * work through each transport in turn.
     */
    for (tp = transports; tp; tp = tp->succ) {
	name = tp->name;
	flags = tp->flags;
	if (add_to_hash(name, (char *) NULL, (size_t) 0, tp_hash) == ALREADY_HASHED) {
	    if (debug) {
		fprintf(f, "#\n# %s: a custom transport with this name was already shown, ignoring old builtin\n",
			name);
	    }
	    continue;
	}
	fprintf(f, "#\n%s:\n", name);	/* not %v since reading back can't decode */
	(void) dump_standard_config(f,
				    (char *) tp,
				    name,
				    flags,
				    transport_generic,
				    end_transport_generic);
	/* The dump_standard_config() routine cannot handle lists, so we do them here */
	if (tp->hdrremove) {
	    for (lp = tp->hdrremove; (lp); lp = lp->succ) {
		fprintf(f, "\tremove_header=%s,\n", quote(lp->text, FALSE, TRUE));
	    }
	} else {
	    fputs("\t-remove_header,\n", f);
	}
	if (tp->hdrinsert) {
	    for (lp = tp->hdrinsert; (lp); lp = lp->succ) {
		fprintf(f, "\tinsert_header=%s,\n", quote(lp->text, FALSE, TRUE));
	    }
	} else {
	    fputs("\t-insert_header,\n", f);
	}
	if (tp->hdrappend) {
	    for (lp = tp->hdrappend; (lp); lp = lp->succ) {
		fprintf(f, "\tappend_header=%s,\n", quote(lp->text, FALSE, TRUE));
	    }
	} else {
	    fputs("\t-append_header,\n", f);
	}
	fputs("\t;\n", f);
	driver = find_transport_driver(tp->driver);
	if (driver) {
	    if (driver->dumper) {
		(*driver->dumper)(f, tp);
	    } else {
		fprintf(f, "\t#\n\t# -- the %s transport driver has no 'dumper' function!\n\t#\n", tp->driver);
	    }
	} else {
	    fprintf(f, "\t#\n\t# -- there is no %s transport driver!\n\t#\n", tp->driver);
	}
    }
    fputs("#\n# -- end of transports\n", f);
}


static char *
tp_remove_header(struct_p, attr)
    char *struct_p;			/* passed transport structure */
    struct attribute *attr;		/* attribute from transports file */
{
    struct transport *tp = (struct transport *) struct_p;

    if (EQ(attr->value, off)) {
	tp->hdrremove = NULL;
    } else if (EQ(attr->value, on)) {
	return xprintf("transport %v: %s: boolean form for non-boolean attribute",
		       tp->name, attr->name);
    } else {
	tp->hdrremove = add_charplist(tp->hdrremove, COPY_STRING(attr->value));
    }

    return NULL;
}

static char *
tp_insert_header(struct_p, attr)
    char *struct_p;			/* passed transport structure */
    struct attribute *attr;		/* attribute from transports file */
{
    struct transport *tp = (struct transport *) struct_p;

    if (EQ(attr->value, off)) {
	tp->hdrinsert = NULL;
    } else if (EQ(attr->value, on)) {
	return xprintf("transport %v: %s: boolean form for non-boolean attribute",
		       tp->name, attr->name);
    } else {
	tp->hdrinsert = add_charplist(tp->hdrinsert, COPY_STRING(attr->value));
    }

    return NULL;
}

static char *
tp_append_header(struct_p, attr)
    char *struct_p;			/* passed transport structure */
    struct attribute *attr;		/* attribute from transports file */
{
    struct transport *tp = (struct transport *) struct_p;

    if (EQ(attr->value, off)) {
	tp->hdrappend = NULL;
    } else if (EQ(attr->value, on)) {
	return xprintf("transport %v: %s: boolean form for non-boolean attribute",
		       tp->name, attr->name);
    } else {
	tp->hdrappend = add_charplist(tp->hdrappend, COPY_STRING(attr->value));
    }

    return NULL;
}

static char *
transport_driv_function(struct_p, driver_attrs)
    char *struct_p;			/* passed transport structure */
    struct attribute *driver_attrs;	/* driver-specific attributes */
{
    struct transport *tp = (struct transport *) struct_p;
    struct transport_driver *drv;

    if (tp->driver == NULL) {
	return xprintf("transport %v: no driver attribute", tp->name);
    }
    drv = find_transport_driver(tp->driver);
    if (drv == NULL) {
	return xprintf("transport %v: unknown driver: %s",
		       tp->name, tp->driver);
    }
    if (drv->builder) {
	return (*drv->builder)(tp, driver_attrs);
    }

    return NULL;
}

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
