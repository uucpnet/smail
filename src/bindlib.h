/*
#ident	"@(#)src/bindlib.h:RELEASE-3_2_0_121:bindlib.h,v 1.15 2004/07/25 17:10:00 woods Exp"
*/

/*
 *    Copyright (C) 1992  Ronald S. Karr
 *
 * BIND support common to the "bind" router and the "tcpsmtp" transport.
 * Created by Simon Leinen (simon@liasun6) on 9 July 1991.
 *
 * Converted to library form usable by tcpsmtp by Chip Salzenberg.
 * Modifications to work with rewritten bindlib.c by Nigel Metheringham.
 * Added mx_domains option - Philip Hazel, October 1993 
 *
 */

#ifndef BINDLIB_H
#define BINDLIB_H

/* configuration items shared by bind router and tcpip transport */

/* flag attributes */
#define BIND_DONT_FILTER     0x00800000 /* special: don't filter_mx_hints */
#define BIND_LOCAL_MX_OKAY   0x01000000 /* MX RR can point to local host */
#define BIND_DEFER_NO_CONN   0x02000000 /* defer if cannot connect to server */
#define BIND_DEFNAMES        0x04000000 /* append default domain name */
#define BIND_DOMAIN_REQUIRED 0x08000000 /* require two hostname parts */
#define BIND_MX_ONLY         0x10000000 /* require MX records */
#define BIND_DNS_SEARCH      0x20000000 /* search DNS names */

/* Levels of header rewriting */
#define BIND_REWRITE_NEVER      0       /* Never rewrite headers */
#define BIND_REWRITE_IFREQUIRED 1       /* Rewrite headers if necessary (inverted address) */
#define BIND_REWRITE_ALWAYS     2       /* Always rewrite headers */

#ifndef BIND_FALLBACK_PREFERENCE
#define BIND_FALLBACK_PREFERENCE 65535  /* Max 16 bit number */
#endif 

struct bindlib_private {
    char *match_domains;		/* domains to match */
    char *ignore_domains;               /* domains to ignore */
    char *required;                     /* domains to limit search to */
    char *widen_domains;                /* domains to try widening with */
    char *mx_domains;                   /* domains to use MX only with */ 
    char *gateways;                     /* known gateways and their domains */
    int  rewrite_headers;               /* Amount of header rewriting to do */
    int  cname_limit;                   /* Depth of CNAMES allowed */
    char *fallback_gateway;             /* Fallback gateway if physical connect fails */
};

/* configuration description */
#define BIND_ATTRIBUTES(STRUCT,BL_PRIV_MEM) \
    { "local_mx_okay", t_boolean, 0, NULL, NULL, BIND_LOCAL_MX_OKAY },	\
    { "defer_no_connect", t_boolean, 0, NULL, NULL, BIND_DEFER_NO_CONN }, \
    { "defnames", t_boolean, 0, NULL, NULL, BIND_DEFNAMES },		\
    { "dns_search", t_boolean, 0, NULL, NULL, BIND_DNS_SEARCH },	\
    { "domain_required", t_boolean, 0, NULL, NULL, BIND_DOMAIN_REQUIRED }, \
    { "mx_only", t_boolean, 0, NULL, NULL, BIND_MX_ONLY },		\
    { "match_domains", t_string, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.match_domains) },			\
    { "ignore_domains", t_string, 0, NULL, NULL,			\
	OFFSET(STRUCT, BL_PRIV_MEM.ignore_domains) },			\
    { "required", t_string, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.required) },				\
    { "widen_domains", t_string, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.widen_domains) },			\
    { "mx_domains", t_string, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.mx_domains) },			\
    { "gateways", t_string, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.gateways) },				\
    { "rewrite_headers", t_int, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.rewrite_headers) },			\
    { "cname_limit", t_int, 0, NULL, NULL,				\
	OFFSET(STRUCT, BL_PRIV_MEM.cname_limit) },			\
    { "fallback_gateway", t_string, 0, NULL, NULL,			\
	OFFSET(STRUCT, BL_PRIV_MEM.fallback_gateway) }

#define BIND_TEMPLATE_FLAGS 0

/* XXX Note that retries should be 2 ifndef HAVE_RENAME */
#define BIND_TEMPLATE_ATTRIBUTES \
   { NULL,                              /* char *match_domains */		\
     NULL,                              /* char *ignore_domains */		\
     NULL,                              /* char *required */			\
     NULL,                              /* char *widen_domains */		\
     NULL,                              /* char *mx_domains */			\
     NULL,                              /* char *gateways */			\
     0,                                 /* int  rewrite_headers */		\
     1,					/* int  cname_limit */			\
     NULL                               /* char *fallback_gateway */		\
   }

#ifdef HAVE_BIND
/* external functions defined in bindlib.c */
extern int bind_check_if_canonical_host __P((char *, in_addr_t, struct error **));
extern int bind_check_if_local_mxs __P((char *, int *, struct error **));
extern char *bind_lookup_txt_rr __P((char *, struct error **));
extern int bind_addr __P((char *, unsigned long, struct bindlib_private *, char *, struct rt_info *, struct error **));
extern char * bind_compute_domain __P((void));
#endif /* HAVE_BIND */

#endif /* not BINDLIB_H */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
