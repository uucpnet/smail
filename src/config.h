/*
#ident	"@(#)smail/src:RELEASE-3_2_0_121:config.h,v 1.162 2005/10/28 04:38:56 woods Exp"
 */

/*
 *    Copyright (C) 1987, 1988 Ronald S. Karr and Landon Curt Noll
 *    Copyright (C) 1992  Ronald S. Karr
 * 
 * See the file COPYING, distributed with smail, for restriction
 * and warranty information.
 */

/*
 * VERY IMPORTANT NOTE:
 *
 *	This file contains default values for a large number of
 *	configurable values in smail.  THIS FILE IS NOT INTENDED TO
 *	BE EDITED DIRECTLY.  Rather, users wishing to override
 *	default values should use the conf/EDITME file.  Either
 *	the MISC_DEFINES or the MISC_C_DEFINES list variable can
 *	be used to override values in this file.
 *
 *	This file contains two primary types of variables: variables
 *	which can be left undefined, and variables which must have a
 *	value.  For variables of the former type, two macros can be
 *	defined in the EDITME file: a value can be specified, or the
 *	variable can be disabled by using a macro prefix of NO_.  For
 *	example, to set LIST_FILENAME to /usr/local/lists/${strip:lc:user},
 *	use:
 *
 *	   MISC_C_DEFINES='#define LIST_FILENAME "/usr/local/lists/${strip:lc:user}"'
 *
 *	To disable the use of a list director:
 *
 *	   MISC_DEFINES=NO_LIST_FILENAME
 *
 *	The other type of variable (variables that must have values)
 *	obviously cannot be turned off with a NO_ macro.
 */


/*
 * SITENAME CONFIGURATION
 *
 * The following section defines the various possible names for the
 * local host which are recognized and produced implicitly by smail
 */

/*
 * For systems with neither gethostname() nor uname() system calls we can try
 * reading a hostname from a configuration file.  See compute_hostname() in
 * sysdep.c.
 *
 * # define SITENAME_FILE	"/etc/whoami"	(* default for HDB UUCP *)
 *
 * Don't define SITENAME_FILE except as a last resort.
 */

/*
 * Define the full domain name for the local site used in outgoing
 * addresses within header fields.  If VISIBLE_NAME is set to NULL, then
 * the the address used will be the first name in the list HOSTNAMES.  If
 * HOSTNAMES is also NULL, then a hostname will be computed in a system-
 * dependent fashion and VISIBLE_DOMAINS used to build HOSTNAMES and
 * VISIBLE_NAME in the form <hostname>.VISIBLE_DOMAINS.  VISIBLE_NAME
 * should be a domain name which which remote sites will understand.
 *
 * VISIBLE_NAME does not need to specifically specify the local host.  It
 * can be the domain name for a set of hosts which maintain a consistent
 * user database or automatically forward to the homes for all users
 * within the domain.
 *
 * NOTE: This can be set with the config file value "visible_name"
 *
 * # define VISIBLE_NAME	"uts.amdahl.com" (* Sample *)
 */
#ifndef	VISIBLE_NAME
# define VISIBLE_NAME NULL		/* Normal setting */
#endif

/*
 * The value of VISIBLE_DOMAINS may be used to create the HOSTNAMES and
 * VISIBLE_NAMES values.  VISIBLE_DOMAINS can be a list of colon-separated
 * values.  The first value will be used in generating VISIBLE_DOMAINS,
 * the first and remaining values will be used in generating HOSTNAMES.
 * For a host in the UUCP zone which is in the maps distributed over
 * USENET in comp.mail.maps, one of these values should be "uucp".
 * For a host in a registered domain, this should contain the domain in
 * which the host is registered.
 *
 * CAUTION:  for newly registered domains "uucp" should be first in this
 *	     list for a month or two, to allow for propogation of the
 *	     new domain name around the various networks.
 *
 * NOTE: This can be set with the config file value "visible_domains"
 *
 * # define VISIBLE_DOMAINS "amdahl.com:uucp" (* sample *)
 * # define VISIBLE_DOMAINS "Berkeley.EDU" (* sample site not in UUCP zone *)
 */
#ifndef	VISIBLE_DOMAINS
# define VISIBLE_DOMAINS "uucp"		/* for a host in the UUCP zone */
#endif

/*
 * A colon-separated list of names for the local host.  This list,
 * together with VISIBLE_NAME, UUCP_HOST and MORE_HOSTNAMES should
 * represent all possible names for the local host.  For a host that
 * is in more than one domain or that can gateway to more than one
 * level of domains, this should represent those names.  For a host in
 * a registered domain in the UUCP zone, which is also in the maps
 * distributed by USENET, <hostname>.uucp should be in this list.
 *
 * If HOSTNAMES is non-NULL then VISIBLE_DOMAINS is ignored.  If
 * HOSTNAMES is non-NULL and VISIBLE_NAME is NULL, then the first
 * value in HOSTNAMES is used to create the VISIBLE_NAME.
 *
 * Also, the first value in HOSTNAMES is used to specify the primary
 * domain name for the local host.  This name should uniquely
 * specify the local host and is used in tracing headers and in
 * forming the Message-Id.
 *
 * NOTE: This can be set with the config file value "hostnames"
 *
 * # define HOSTNAMES	"amdahl.uts.amdahl.com:amdahl.uucp" (* sample *)
 */
#ifndef	HOSTNAMES
# define HOSTNAMES	NULL		/* normal setting */
#endif

/*
 * A colon-separated list of yet more names for the local host.  This
 * list can be used to specify names in addition to those names computed
 * from the VISIBLE_DOMAINS values crossed with the real hostname.  Thus,
 * if a site (especially a domain gateway) wishes to have one set of
 * names computed automatically from the machines real hostname, while
 * also recognizing an additional set of names, HOSTNAMES can be set to
 * NULL (causing it to be computed) and MORE_HOSTNAMES can be set to the
 * list of additional hostnames.
 *
 * NOTE: This can be set with the config file value "more_hostnames"
 *
 * #define MORE_HOSTNAMES	"uts.amdahl.com:amdahl.com" (* sample *)
 */
#ifndef MORE_HOSTNAMES
# define MORE_HOSTNAMES	NULL		/* normal setting */
#endif

/*
 * The hostname used in !-routes in the `From_' lines before the header.
 * Set to NULL if this should be computed in a system-dependent fashion.
 *
 * NOTE: This can be set with the config file value "uucp_name"
 *
 * # define UUCP_NAME	"amdahl"	(* Example *)
 */
#ifndef	UUCP_NAME
# define UUCP_NAME NULL			/* Normal setting */
#endif

/*
 * A list of authoritative domains for this host can be configured
 * by setting this to a colon-separated list of domains.
 *
 * NOTE: This can be set with the config file value "auth_domains".
 */
#ifndef AUTH_DOMAINS
# define AUTH_DOMAINS NULL		/* Normal setting */
#endif


/*
 * STRONG SUGGESTIONS
 *
 * The following suggested values should probably not be changed.
 */

/*
 * maximum size for a message.  Messages longer than this are truncated.
 * If you do not wish messages to be truncated at all, set this to zero.
 *
 * NOTE: This can be set with the config file value "max_message_size"
 */
#ifndef	MAX_MESSAGE_SIZE
# define MAX_MESSAGE_SIZE (1024*1024)	/* RFC-1123 s.5.3.8 suggests 1mb */
#endif

/*
 * The maximum accepted length of an SMTP command line.
 *
 * 512 bytes was initially specified in RFC-821 section 4.5.3, page 42
 *
 * RFC-1869 section 4.1.2 (page 3) says :
 *
 *	This limit is hereby amended to only apply to command lines without any
 *	parameters.  Each specification that defines new MAIL FROM or RCPT TO
 *	parameters must also specify maximum parameter value lengths for each
 *	parameter so that implementors of some set of extensions know how much
 *	buffer space must be allocated. The maximum command length that must be
 *	supported by an SMTP implementation with extensions is 512 plus the sum
 *	of all the maximum parameter lengths for all the extensions supported.
 *
 * so we add a generous 512 additional bytes.....
 */
#ifndef MAX_SMTP_CMD_LINE_LEN
# define MAX_SMTP_CMD_LINE_LEN	1024
#endif

/*
 * This string defines the grade values which correspond to particular
 * names of the Precedence: field in the message header.  The parts of
 * the string are separated by `:' with alternating precedence name and
 * grade character.  Numbers are higher than upper case letters which are
 * higher than lower case letters in their grade value.  Grades in the
 * range [a-m] will only have an error message and header returned to the
 * sender on errors.  Grades in the range [n-z] will not have anything
 * returned to the sender on errors.
 *
 * The values recognized by many sendmail.cf files are: first-class,
 * special-delivery, list, bulk, and junk.  Others are useful mainly for
 * getting mail out of the local machine or for communication with other hosts
 * running smail in a similar configuration.
 *
 * NOTE:  This can be set with the config file value "grades"
 */
#ifndef	GRADES
# define GRADES "first-class:0:special-delivery:9:air-mail:A:list:L:bulk:b:occupant:o:junk:z"
#endif

/*
 * The maximum hop count allowed for remote delivery.  If a remote
 * transport is called and the current hop_count is equal to or greater
 * than this number, delivery fails.  This number does not affect local
 * delivery (i.e., transports with the `local' attribute turned on).
 *
 * NOTE: This can be set with the config file value "max_hop_count"
 *
 * WARNING: this number is not standardized across all networks, so
 * 	    return messages to senders will fail if an intermediate
 * 	    site in the return path has a lower maximum hop count
 * 	    then the sight initiating the "hop_count exeeded" error
 * 	    message.
 */
#ifndef MAX_HOP_COUNT
# define MAX_HOP_COUNT	20
#endif

/*
 * Whether or not we allow Smail to follow a single CNAME alias hop when
 * looking for the addresses of MX target hosts.
 *
 * Note that only DNS replies where the A RR for the CNAME target is also
 * included in the answers section will be allowed.  Smail does not (and never
 * ever did) actually go out and look for any missing A records (for CNAMEs).
 *
 * Note that no existing RFC gives even the slightest allowance for this
 * behaviour -- all specifications are, and always have been, very strict in
 * saying that the target of an MX record _MUST_ be a canonical hostname and
 * that the SMTP server _MUST_ report unusable MX records as errors.
 */
#ifndef ALLOW_ONE_MX_TARGET_CNAME_HACK
# define ALLOW_ONE_MX_TARGET_CNAME_HACK	0	/* All RFCs insist NO! */
#endif


/*
 * PREFERENCES
 *
 * The following values can be changed at the preference of the local
 * site administrator without affecting any remote sites, unless the
 * administrator gets them wrong and causes mail to fail.
 *
 * NOTE: many suggested pathnames are in /usr/lib/smail and
 *	 /usr/spool/smail for compatibility with current naming
 *	 schemes used by other mailers and by netnews.  An
 *	 alternate suggestion is to group these files under one
 *	 directory /usr/smail.
 */

/*
 * This pathname is where the file COPYING from the source directory
 * will be installed for reference by the -bc option to smail.  If
 * this does not begin with `/', it will be referenced relative to
 * smail_lib_dir.
 *
 * NOTE: This can be set with the config file value "copying_file"
 */
#ifndef COPYING_FILE
# define COPYING_FILE "COPYING"
#endif

/*
 * Set this to the name of the system-wide log file to which potentially
 * interesting, non-panic, messages are written.  This file should be
 * truncated periodically.
 *
 * NOTE: This can be set with the config file value "logfile"
 *
 * # define LOGFILE "/usr/smail/log/logfile"  (* Alternate suggestion *)
 */
#ifndef	LOGFILE
# define LOGFILE "/usr/spool/smail/log/logfile" /* Suggestion */
#endif

/*
 * Set this to the name of the system-wide smail panic log file.
 * This file should be periodically reviewed by the system
 * administrator to ensure that there are no major problems with
 * the mail software, files or configuration.
 *
 * NOTE: This can be set with the config file value "panic_log"
 *
 * # define PANIC_LOG	"/usr/smail/log/panic" (* alternate suggestion *)
 */
#ifndef	PANIC_LOG
# define PANIC_LOG	"/usr/spool/smail/log/paniclog" /* Suggestion */
#endif

/*
 * Set this to the name of the console device.  This is used as a last
 * resort in attempting to write panic messages.
 *
 * NOTE: This can be set with the config file value "console"
 */
#ifndef	CONSOLE
# define CONSOLE	"/dev/console"
#endif

/*
 * define the permission modes for the logfile and panic log file.
 * If smail is not going to be run as set uid or set gid, this will
 * have to be 0666.
 *
 * NOTE: This can be set with the config file value "log_mode"
 */
#ifndef LOG_MODE
# define LOG_MODE 0644
#endif

/*
 * Set this to the spool directories for mail messages, more than one
 * spool directory can be specified by separating names with the colon
 * ( `:' ) character.  Spooling directories are tried in sequence until
 * spooling succeeds completely in one of them.  Thus, alternative
 * directories are useful to prevent loss of mail when filesystems fill
 * up, or run out of inodes, or even when somebody accidentally changes
 * permissions on one spool directory or its associated locking directory.
 * In general, alternative directories should be on separate filesystems,
 * perhaps even separate disks, if you are really paranoid.
 *
 * Of particular use is having more than one spool directory after
 * an extended period of down-time to handle the suddenly increased
 * influx of mail traffic.
 *
 * NOTE: This can be set with the config file value "spool_dirs"
 *
 * # define SPOOL_DIRS	"/usr/smail/spool" (* alternate suggestion *)
 * # define SPOOL_DIRS	"/usr/smail/spool:/alt/smail/spool" (* Example *)
 */
#ifndef	SPOOL_DIRS
# define SPOOL_DIRS	"/usr/spool/smail" /* Suggestion */
#endif

/*
 * Define the permission mode for spool files.  If a file descriptor
 * locking protocol is used which requires that a file be writable to
 * lock it, then this mode must allow at least write by owner (or group
 * if the mailer is run as set group ID).  These should not allow for
 * global read, as the messages stored in spool files should be considered
 * private.
 *
 * NOTE: This can be set with the config file value "spool_mode"
 */
#ifndef	SPOOL_MODE
# ifdef	UNIX_SYS5			/* && !defined(UNIX_SUN_OS_4_1) ??? */
#  define SPOOL_MODE	0600
# else
#  define SPOOL_MODE	0400
# endif
#endif

/*
 * define the permission modes for lock files.  If the LOCK_BY_NAME
 * locking protocol is not used, then this is not of any value.
 *
 * NOTE: This can be set with the config file value "lock_mode" 
 */
#ifndef	LOCK_MODE
# define LOCK_MODE 0444
#endif

/*
 * define AUTO_MKDIR to be TRUE if spool and log directories should
 * be created automatically when smail fails to access them.  This
 * behavior allows smail to essentially automatically install itself
 * as long as the binaries are in place.  AUTO_MKDIR_MODE should be
 * set to the mode used for directory creation.
 */
#ifndef AUTO_MKDIR
# define AUTO_MKDIR  TRUE
#endif

#ifndef	AUTO_MKDIR_MODE
# define AUTO_MKDIR_MODE  0755
#endif

/*
 * define the permission modes for the per-message log files.
 *
 * NOTE: This can be set with the config file value "message_log_mode" 
 */
#ifndef	MESSAGE_LOG_MODE
# define MESSAGE_LOG_MODE 0644
#endif

/*
 * Name the user that is to receive mail to the Postmaster, by default.
 * This address is used, if no director matches Postmaster, to ensure
 * that this address will always reach somebody.
 */
#ifndef POSTMASTER_ADDRESS
# define POSTMASTER_ADDRESS "root"	/* should be good for all unix-like systems */
#endif

/*
 * Defining ERROR_COPY_POSTMASTER to TRUE will cause all error message
 * generated by smail to be copied to the postmaster.  Normally, bounces will
 * be copied to the postmaster only for selective errors that appear to be
 * related to local configuration errors.
 */
#if !defined(ERROR_COPY_POSTMASTER)
# define ERROR_COPY_POSTMASTER	FALSE	/* don't copy the postmaster */
#endif

/*
 * This defines the default grade for spool files. These grade
 * characters are used in a form similar to 4.3BSD UUCP grade
 * values.  Typically, this is `C'.  See the definition of the GRADES
 * attribute.
 *
 * NOTE: This can be set with the config file value "spool_grade"
 */
#ifndef	SPOOL_GRADE
# define SPOOL_GRADE	'C'
#endif

/*
 * On systems without an atomic rename system call, configuration files
 * are not guarranteed to always exist.  Thus, we must sleep and retry
 * open operations several times before determining that a file really
 * does not exist.  OPEN_INTERVAL defines the sleep interval between
 * retries (in seconds) and OPEN_RETRIES defines the number of retries
 * to be made in opening a file.
 *
 * For files that are optional, and which don't exist this is somewhat
 * expensive and introduces delays into the process, but there is nothing
 * that can be done.
 *
 * NOTE: These can be set with the config file values "open_interval" and
 *	 "open_retries"
 *
 * NOTES: for systems with an atomic rename, OPEN_RETRIES can be 0.
 *	  On systems without higher resolution timing, a sleep of
 *	  OPEN_INTERVAL is guarranteed to sleep at least
 *	  OPEN_INTERVAL-1 seconds.  Thus, this value should be at
 *	  least 2.
 */
#ifndef	OPEN_INTERVAL
# define OPEN_INTERVAL	2
#endif
#ifndef	OPEN_RETRIES
# ifdef HAVE_RENAME
#  define OPEN_RETRIES 0
# else
#  define OPEN_RETRIES	2
# endif
#endif

/*
 * The config file can be used to override some compiled in defaults of
 * smail.  If this does not begin with `/', it will be referenced
 * relative to the smail_lib_dir directory.
 *
 * NOTE: This cannot be set with the config file, for hopefully obvious
 *	 reasons.  This can be set on invocation of smail, however.
 *
 * # define CONFIG_FILE	"/usr/smail/lib/config" (* Alternate suggestion *)
 */
#ifndef	CONFIG_FILE
# define CONFIG_FILE	"config"	/* Suggestion */
#endif

/*
 * A secondary configuration file can be used which overrides both
 * internal configuration and configuration set in the primary configuration
 * file.  This is useful, for example, in an environment using shared
 * filesystems which may have a shared primary configuration and an
 * optional secondary configuration.  If this is NULL, then no secondary
 * configuration file used.
 *
 * NOTE: This can be set with the config file value "second_config_file"
 */
#ifndef SECOND_CONFIG_FILE
# define SECOND_CONFIG_FILE  NULL
#endif

/*
 * The director file contains the complete configuration for the director
 * subsystem.  It defines which director drivers are used, parameters
 * affecting driver operation and use, and the order in which directors
 * are called upon.  If this does not begin with `/', it will be
 * referenced relative to the smail_lib_dir directory.
 *
 * If this file does not exist, or DIRECTOR_FILE is NULL, the compiled
 * in defaults, defined in default.c, are used.
 *
 * NOTE: This can be set with the config file value "director_file"
 *
 * # define DIRECTOR_FILE "/usr/smail/lib/directors" (* alternate suggestion *)
 */
#ifndef	DIRECTOR_FILE
# define DIRECTOR_FILE	"directors"	/* Suggestion */
#endif

/*
 * The router file contains the complete configuration for the routing
 * subsystem.  It defines which route drivers are used, parameters
 * affecting driver operation and use, and the order in which routers
 * are called upon.  If this does not begin with `/', it will be
 * referenced relative to the smail_lib_dir directory.
 *
 * If this file does not exist, or ROUTER_FILE is NULL, the compiled
 * in defaults, defined in default.c, are used.
 *
 * NOTE: This can be set with the config file value "router_file"
 *
 * # define ROUTER_FILE	"/usr/smail/lib/routers" (* alternate suggestion *)
 */
#ifndef	ROUTER_FILE
# define ROUTER_FILE	"routers"	/* Suggestion */
#endif

/*
 * Method files indicate the transport method by which mail is delivered
 * to the next_hop site.  Method files can be indicated two ways in the
 * ROUTER_FILE: by explicit `/' or `~user' based paths, and by non-rooted
 * paths.  To find non-rooted method files, METHOD_DIR is pre-pended to
 * the path.  Set to NULL if you do not wish to have a method directory.
 * If this does not begin with `/', it will be referenced relative to
 * the smail_lib_dir directory.
 *
 * NOTE: This can be set with the config file value "method_dir"
 *
 * # define METHOD_DIR	"/usr/smail/lib/methods" (* alternate suggestion *)
 */
#ifndef	METHOD_DIR
# define METHOD_DIR	"methods"	/* Suggestion */
#endif

/*
 * The transport file contains the configuration for the transport
 * subsystem.  It defines which transport drivers are used, and parameters
 * affecting driver operation and use.  They are referenced from routers
 * and some directors.  Also, two transports are referenced implicitly be
 * smail's internal algorithms: "pipe" and "file" for delivery to
 * shell-command and file addresses.  If this does not begin with `/',
 * it will be referenced relative to the smail_lib_dir directory.
 *
 * If this file does not exist, or TRANSPORT_FILE is NULL, the compiled
 * in defaults, defined in default.c, are used.
 *
 * NOTE: This can be set with the config file value "transport_file"
 *
 * #define TRANSPORT_FILE "/usr/smail/lib/transports" (* alternate *)
 */
#ifndef	TRANSPORT_FILE
# define TRANSPORT_FILE   "transports"	/* Suggestion */
#endif

/*
 * The qualify file contains a list of hosts and the domains that they
 * live in.  It is searched sequentially.
 *
 * NOTE: This can be set with the config file value "qualify_file"
 *
 * #define QUALIFY_FILE "/usr/smail/lib/qualify" (* alternate *)
 */
#ifndef QUALIFY_FILE
# define QUALIFY_FILE   "qualify"  /* Suggestion */
#endif

/*
 * The retry file contains a list of hostname patterns, the time
 * to wait between retries, and the total time to attempt retries
 * before the address is considered undeliverable.
 *
 * NOTE: This can be set with the config file value "retry_file"
 *
 * #define RETRY_FILE "/usr/smail/lib/retry" (* alternate *)
 */
#ifndef RETRY_FILE
# define RETRY_FILE   "retry"  /* Suggestion */
#endif

/*
 * The smail library directory is the default directory for various
 * configuration files.  Also, files accessed through the routines in
 * lookup.c may be referenced relative to this directory by default.
 *
 * NOTE: This can be set with the config file value "smail_lib_dir",
 *	 though CONFIG_FILE is always referenced relative to the compiled
 *	 in lib directory, for hopefully obvious reasons.
 *
 * #define SMAIL_LIB_DIR "/usr/smail/lib"	(* Alternate Suggestion *)
 */
#ifndef SMAIL_LIB_DIR
# define SMAIL_LIB_DIR	"/usr/lib/smail"	/* Suggestion */
#endif

/*
 * The smail utility directory, which contains, among other programs,
 * mkdbm and mkaliases.
 *
 * This can be changed with the EDITME variable UTIL_BIN_DIR.
 */
#ifndef SMAIL_UTIL_DIR
# define SMAIL_UTIL_DIR	SMAIL_LIB_DIR
#endif

/*
 * Require that specified configuration files exist.  If this is
 * defined as TRUE then non-existent configuration files are generally
 * ignored.  If this defined as FALSE, non-existent configuration
 * files will generate a panic.
 */
#ifndef REQUIRE_CONFIGS
# define REQUIRE_CONFIGS FALSE
#endif

/*
 * Allow debugging to be used in SMTP dialogs.  This allows the remote end to
 * see the debugging output from smail, but does not have any other affects.
 * If this is TRUE, the DEBUG command in SMTP turns on debugging and sets its
 * level.  If this is FALSE, the DEBUG command only produces a cute message.
 * This is only really useful if NODEBUG is not defined.
 */
#ifndef SMTP_ALLOW_DEBUG
# define SMTP_ALLOW_DEBUG	TRUE	/* probably reasonable */
#endif

/*
 * Allow the information commands EXPN to be used in SMTP dialogs.  
 * This allows the remote end to get information about your site
 * so the paranoid may wish to disable this.
 */
#ifndef SMTP_ALLOW_EXPN
# define SMTP_ALLOW_EXPN	TRUE	/* probably reasonable */
#endif

/*
 * Set this to a string which will be expanded to form the Received: header
 * field.  This version is about as complete and correct as it can get....
 * Strictly speaking the "id" portion must include the "@$primary_name", but if
 * you read the verbose description of RECEIVED in RFC 822, it is stated "id"
 * may be "the internal message identifier."  Also, the "via" was intended to
 * "indicate what physical mechanism the message was sent over," but we replace
 * this with the virtual mechanism, i.e. the program that received the message.
 * We include the "ident" comment as well as the comments about the envelope
 * sender & owner and the director/router/transport information since this is
 * the easiest and most obvious place to log this information inside the msg.
 *
 * From-domain By-domain [Via-link] [With-protocol] [ID] [For-recip] ";" date-time
 *
 * XXX we really do need a way to to loop over all addrs in a list to expand
 * the 'for' part into the full (space separated?) list of known recipients.
 * Such a list is legal syntax, at least in RFC2821.
 *
 *	For = "FOR" FWS 1*( Path / Mailbox ) CFWS
 *
 * XXX the For-recip expansion should be avoided if the recipient was one from
 * the BCC field, but that won't help for SMTP where the sender has no way to
 * tell us which addresses might have been on his BCC list.
 *
 * NOTE: This can be changed with the config file value "received_field"
 *
 * XXX This probably shouldn't be a configurable setting.
 */
#ifndef	RECEIVED_FIELD
# define RECEIVED_FIELD		"Received: from \
${if def:sender_host_invalid\
	{(invalid client hostname: ${sender_host_invalid})}\
}\
${if def:sender_host\
	{$sender_host}\
  else\
	{${if origin:local\
		{localhost}\
	 else\
		{localhost(non-local sender)}\
	}}\
}\
${if def:sender_host_addr\
	{\n\t(${if def:sender_host_really_invalid\
		{($sender_host_really_invalid)}\
	}${if and{{def:sender_host_really}\
		   {!eqic{sender_host_really}{$sender_host}}}\
		{$sender_host_really}\
	    else\
		{${if !def:sender_host_really\
		    	{(no DNS PTR)}}}\
	}[$sender_host_addr]${if def:sender_host_port\
		{ port=$sender_host_port}})}\
}\
\n\tby $primary_name\
${if def:smtp_local_addr\
	{([$smtp_local_addr]${if def:smtp_local_port\
		{ port=$smtp_local_port}})}\
}\n\t\
${if def:sender_host_addr\
	{via TCP}\
 else\
	{${if def:sender_program\
		{${if eqic{sender_program}{rmail}\
			{via UUCP}\
		 else\
			{via $sender_program}\
		}}\
	 else\
		{via UNKNOWN}\
	}}\
} \
${if def:sender_proto\
	{with $sender_proto}\
 else\
	{with STDIO}\
}\
${if def:message_size\
	{ ($message_size bytes)}\
}\
\n\t(sender: <$return_address>)\
${if def:ident_sender\
	{\n\t(ident <$ident_sender> using $ident_method)}\
 else\
	{${if origin:local\
		{\n\t(ident <$local_sender> using UNIX)}\
	}}\
}\
\n\tid <$message_id@$primary_name>\n\tfor \
${if def{top:input_addr}\
	{<${top:quote:input_addr}>}\
 else\
	{unknown <>}\
};\
${if def{parent:mailbox}\
	{\n\t(next: <${if def:next_addr\
			{$next_addr}\
		     else\
			{${quote:input_addr}}\
		    }>)}\
}\
\n\t$spool_date\n\t\
($version_string built $compile_date)"
/* XXX stupid hacks for emacs " ( */
#endif

/*
 * Set this to a string which will be expanded to form the
 * Message-Id: header field.
 *
 * NOTE: This can be set with the config file value "message_id_field"
 *
 * XXX This probably shouldn't be a configurable setting.
 */
#ifndef	MESSAGE_ID_FIELD
# define MESSAGE_ID_FIELD "Message-Id: <${message_id}@${primary_name}>"
#endif

/*
 * Set this to a string which will be expanded to form the
 * Date: header field.
 *
 * NOTE: This can be set with the config file value "date_field"
 *
 * XXX This probably shouldn't be a configurable setting.
 */
#ifndef	DATE_FIELD
# define DATE_FIELD	"Date: ${spool_date}"
#endif

/*
 * Set this to a string which will be expanded to form the
 * From: header field (or the Sender: field).
 *
 * NOTE: This can be set with the config file value "from_field"
 *
 * NOTE: This _must_ begin with "From: ", but that will be replaced with
 * "Sender:" to generate a missing "Sender:" field.
 *
 * This version tries to take care not to accept blame for empty return paths
 * or remotely originated mail (i.e. does not put anything in the from field
 * resembling a local username or use any of the local domain or hostnames),
 * and for local users it tries to include the full $sender_name as the
 * display-name.
 *
 * XXX use of ${quote: in here isn't quite right because it ends up quoting
 * everything, even just plain spaces, and thus making Smail look just as
 * stupid as all the dumb PC clients that do it the same blind way too -- we
 * need a proper quoter for display-name phrases
 *
 * XXX This probably shouldn't be a configurable setting.
 */
#ifndef	FROM_FIELD
# define FROM_FIELD	"From: \
${if or{{eqic{from}{MAILER-DAEMON}}\
        {eq{from}{<>}}\
     }\
	{Delivery error returned from MAILER-DAEMON at ${visible_name} <>}\
  else\
	{${if eq{from}{<+>}\
		{Delivery error returned from PostMaster <PostMaster@${visible_name}>}\
	   else\
		{${if origin:local\
			{${if def{fullname}\
				${quote:fullname} }<${from}>}\
		   else\
			{Unknown Origin (Unverified <${from}>) <>}}}}\
	}\
}"
#endif

/*
 * recipients which get to bypass default content filters
 */
#ifndef BYPASS_CONTENT_FILTERING_RECIPIENTS
# define BYPASS_CONTENT_FILTERING_RECIPIENTS "(?i)<?postmaster(@.*)?>?:(?i)<?abuse(@.*)?>?"
#endif

/*
 * header content filters causing rejects for a messages not including
 * recipients matched in bypass_content_filtering_recipients.
 */
#ifndef	HEADER_CHECKS
# define HEADER_CHECKS	NULL
#endif

/*
 * header content filters causing rejects for a messages
 */
#ifndef	HEADER_CHECKS_ALWAYS
# define HEADER_CHECKS_ALWAYS	NULL
#endif

/*
 * body content filters causing rejects for a messages not including 
 * recipients matched in bypass_content_filtering_recipients.
 */
#ifndef	BODY_CHECKS
# define BODY_CHECKS	NULL
#endif

/*
 * body content filters causing rejects for a messages
 */
#ifndef	BODY_CHECKS_ALWAYS
# define BODY_CHECKS_ALWAYS	NULL
#endif

/*
 * A list of host domain names or RE patterns for MX domains which we allow to
 * have target hosts with addresses matching in smtp_bad_mx_targets
 *
 * For example on a gateway host this could be set to a list of MX domains
 * which are handled by servers on a private Intranet.
 */
#ifndef SMTP_BAD_MX_EXCEPT
# define SMTP_BAD_MX_EXCEPT	NULL
#endif

/*
 * A list of target addresses for MX records that we consider to be invalid.
 */
#ifndef SMTP_BAD_MX_TARGETS
# define SMTP_BAD_MX_TARGETS	"\
:10/8;RFC-1918 addresses are never valid for MX targets on the public Internet!\
:127/8;localhost is never valid for any MX target on the public Internet!\
:169.254/16;link local (DHCP) addresses are never valid for MX targets on the public Internet!\
:172.16/12;RFC-1918 addresses are never valid for MX targets on the public Internet!\
:192.0.2/24;Reserved addresses are never valid for MX targets on the public Internet!\
:192.88.99.0/24;Reserved addresses are never valid for MX targets on the public Internet!\
:192.168/16;RFC-1918 addresses are never valid for MX targets on the public Internet!\
:224/4;Multicast group addresses are never valid for MX targets on the public Internet!\
:240/5;Class-E addresses are reserved for IANA Special Use by RFC-3330 and are never valid for MX targets on the public Internet!"
#endif

/*
 * SMTP startup banner message
 *
 * Note that "$primary_name " is automatically prepended to be standards conforming.
 */
#ifndef SMTP_BANNER
# define SMTP_BANNER "Smail-$version (#$compile_num $compile_date)\nready at $date"
#endif

/*
 * Maximum number of allowed smtp connection processes forked by the
 * smtp daemon.
 *
 * NOTE: This can be set with the config file value "smtp_accept_max".
 */
#ifndef SMTP_ACCEPT_MAX
# define SMTP_ACCEPT_MAX	0	/* allow an infinite number */
#endif

/*
 * Maximum number of allowed smtp connection processes forked by the
 * smtp daemon, until only message queuing is allowed.
 *
 * NOTE: This can be set with the config file value "smtp_accept_queue".
 */
#ifndef SMTP_ACCEPT_QUEUE
# define SMTP_ACCEPT_QUEUE	0	/* allow an infinite number */
#endif

/*
 * The maimum number of abnormal errors to tolerate from accept(2)
 * (see modes.c)
 */
#ifndef MAX_ACCEPT_ERR_COUNT
# define MAX_ACCEPT_ERR_COUNT	100	/* old default was 10 */
#endif

/*
 * Number of seconds to wait for receipt of SMTP commands for interactive
 * SMTP receiver processes.  The default value is in accordance with the
 * recommendations of RFC1123.
 */
#ifndef SMTP_RECEIVE_COMMAND_TIMEOUT
# define SMTP_RECEIVE_COMMAND_TIMEOUT	(5*60)	/* five minutes */
#endif

/*
 * Number of seconds to wait for receipt of a message from a data
 * command within an SMTP receiver process.
 *
 * NOTE:  RFC1123 appears to be silent on a recommendation for this
 * value.
 */
#ifndef SMTP_RECEIVE_MESSAGE_TIMEOUT
# define SMTP_RECEIVE_MESSAGE_TIMEOUT	(2*60*60)	/* 2 hours */
#endif

/* 
 * The default list of networks from which to allow remote relay and
 * which do not need/want RCPT TO address to be verified.
 */
#ifndef SMTP_RECIPIENT_NO_VERIFY
# define SMTP_RECIPIENT_NO_VERIFY	NULL	/* always verify! */
#endif

/*
 * The default list of networks from which to allow remote relay.
 *
 * NOTE:  the keyword "localnet" is effectively replaced by the result of
 * inet_netof(local_sa.sin_addr) [where local_sa is filled by
 * getsockname(fileno(in)) for each connection].  This gives effectively a
 * wildcard pattern that matches a classic IP network for the local address of
 * the incoming connection.  Unfortunately with lots of old /8 space being
 * sub-delegated thanks to CIDR leaving "localnet" in the default can be a
 * security risk.
 *
 * NOTE: the keyword "whoson" causes a WHOSON lookup to be done and a
 * positive match is made if the WHOSON server lists the client address.
 *
 * #define SMTP_REMOTE_ALLOW		"whoson:localnet" (* special *)
 *
 */
#ifndef SMTP_REMOTE_ALLOW
# if defined(HAVE_LIBWHOSON)
#   define SMTP_REMOTE_ALLOW		"whoson:127/8" /* special */
# else
#  define SMTP_REMOTE_ALLOW		"127/8"	/* Just us... */
# endif
#endif

/*
 * Whether or not we verify HELO/EHLO host has A RR matching sockaddr
 */
#ifndef SMTP_HELLO_VERIFY
# define SMTP_HELLO_VERIFY		1 	/* 1123 says no, but... */
#endif

/*
 * A list of hosts/networks from which we allow broken HELO/EHLO
 *
 * The default value does not really need to contain 127/8 since connections
 * from any local interface (see peer_is_localhost in smtprecv.c) are allowed
 * by default.
 *
 * NOTE:  the keyword "localnet" is effectively replaced by the result of
 * inet_netof(local_sa.sin_addr) [where local_sa is filled by
 * getsockname(fileno(in)) for each connection].  This gives effectively a
 * wildcard pattern that matches a classic IP network for the local address of
 * the incoming connection.  Unfortunately with lots of old /8 space being
 * sub-delegated thanks to CIDR leaving "localnet" in the default can be a
 * security risk.
 *
 * NOTE: the keyword "whoson" causes a WHOSON lookup to be done and a
 * positive match is made if the WHOSON server lists the client address.
 */
#ifndef SMTP_HELLO_BROKEN_ALLOW
# if defined(HAVE_LIBWHOSON)
#  define SMTP_HELLO_BROKEN_ALLOW	"whoson:127/8"
# else
#  define SMTP_HELLO_BROKEN_ALLOW	"127/8"
# endif
#endif

/*
 * Whether or not we reject if PTR names don't have matching addresses
 *
 * This is very much like the TCP Wrappers PARANOID check.
 */
#ifndef SMTP_HELLO_REJECT_BROKEN_PTR
# define SMTP_HELLO_REJECT_BROKEN_PTR	1	/* definitely! */
#endif

/*
 * A list of hosts/networks which we deny outright!
 */
#ifndef SMTP_REJECT_HOSTS
# define SMTP_REJECT_HOSTS		NULL	/* none by default */
#endif

/*
 * A list of host domain names or RE patterns which we deny any if name/pattern
 * matches sender_host_really, i.e. the PTR for the connection source address.
 *
 * NOTE:  "localhost" and all the local hostnames (or anything else for that
 * matter) are all automatically allowed for connections from any local
 * interface (see peer_is_localhost in smtprecv.c).
 *
 * WARNING:  do not arbitrarily include $more_hostnames in this list!  Doing so
 * would block the hosts this server is primary MX if they used their own
 * hostnames in their PTRs and then tried to deliver mail to this server.
 *
 * Note iterating all the valid ISO 3166 2-letter country codes would be too
 * much effort to maintain so we just wildcard any two-letter top-level domain.
 * If spammers start regularly violating invalid 2-letter domains then we can
 * either reject the bogus ones explicitly or consider listing all of the valid
 * ones....
 *
 * Note "arpa" is not included in the list of valid domains as there are no
 * valid SMTP hosts possible in that domain and PTRs pointing at .ARPA are
 * just plain bogus.
 *
 * See <URL:http://www.iana.org/gtld/gtld.htm> for the current list of gTLDs.
 *
 * WARNING:  The negated expression must come second last because the more
 * specific messages need to match before it in order to be seen; and the
 * wildcard entry must come last.  Any localization of this setting should be
 * done by copying the original value and then inserting new entries at the
 * beginning.
 */
#ifndef SMTP_HOST_REJECT_HOSTNAMES
# define SMTP_HOST_REJECT_HOSTNAMES	"\
:.*\\.(?P<TLD>(test|example|invalid|localhost))\
;The top level domain '%{TLD}' is not valid on the public Internet -- it is reserved by IANA as per RFC 2606\
:(.*\\.)*(?P<DOM>(example\\.(com|net|org)))\
;The domain '%{DOM}' is not valid on the public Internet -- it is reserved by IANA as per RFC 2606\
:${foreach:hostname:${rxquote:value};Your PTR is forged!  That is my hostname!}\
:! .*\\.(AERO|BIZ|COM|COOP|EDU|GOV|INFO|INT|MIL|MUSEUM|NAME|NET|ORG|PRO|[A-Z][A-Z])\
;these are the valid top-level domains (this message will never be seen)\
:.*\\.(?P<TLD>([^.]+))\
;There is no such domain '%{TLD}' on the public Internet (your PTR is lying)!\
"
#endif

/*
 * A list of DNS black-list domains in which we look for sender_host_really,
 * rejecting the connection if any match is found.
 */
#ifndef SMTP_HOST_DNSBL_DOMAINS
# define SMTP_HOST_DNSBL_DOMAINS	NULL 	/* none by default */
#endif

/*
 * A list of IP/Nets execpted from lookups in smtp_host_dnsbl_domains
 */
#ifndef SMTP_HOST_DNSBL_EXCEPT
# if defined(HAVE_LIBWHOSON)
#  define SMTP_HOST_DNSBL_EXCEPT	"whoson:127/8" /* special */
# else
#  define SMTP_HOST_DNSBL_EXCEPT	"127/8"
# endif
#endif

/*
 * A list of HELO/EHLO greeting name REs which we deny outright!
 *
 * NOTE:  "localhost" and all the local hostnames (or anything else for that
 * matter) are all automatically allowed for connections from any local
 * interface (see peer_is_localhost in smtprecv.c).
 *
 * Note also that "localhost" and "localhost." and other unqualified domains
 * will all be trapped by the FQDN checks (unless the connection matches
 * peer_is_localhost), so need not be matched by these patterns.  Only names
 * already containing a '.' need to have their leading parts be matched
 * optionally.
 *
 * WARNING: do not arbitrarily include $more_hostnames in this list!  Doing so
 * would block the hosts this server MXs for from delivering to their own
 * domain names.
 *
 * Note the internal logic handles validation of domain literal address IP
 * numbers, etc., before this list is consulted.
 *
 * Note iterating all the valid ISO 3166 2-letter country codes would be too
 * much effort to maintain so we just wildcard any two-letter top-level domain.
 * If spammers start regularly violating invalid 2-letter domains then we can
 * either reject the bogus ones explicitly or consider listing all of the valid
 * ones....
 *
 * Note "arpa" is not included in this list as there are no valid SMTP hosts
 * possible in that domain.
 *
 * See <URL:http://www.iana.org/gtld/gtld.htm> for the current list of gTLDs.
 *
 * WARNING:  The negated expression must come second last because the more
 * specific messages need to match before them in order to be seen; and the
 * wildcard entry must come last.  Any localization of this setting should be
 * done by copying the original value and then inserting new entries at the
 * beginning.
 */
#ifndef SMTP_HELLO_REJECT_HOSTNAMES
# define SMTP_HELLO_REJECT_HOSTNAMES	"\
:.*\\.(?P<TLD>(test|example|invalid|localhost))\
;There is no such top level domain '%{TLD}' -- it is reserved by IANA as per RFC 2606\
:(.*\\.)*(?P<DOM>(example\\.(com|net|org)))\
;There is no such domain '%{DOM}' -- it is reserved by IANA as per RFC 2606\
:${foreach:hostname:${rxquote:value};That's my hostname!}\
:! .*\\.(AERO|BIZ|COM|COOP|EDU|GOV|INFO|INT|MIL|MUSEUM|NAME|NET|ORG|PRO|[A-Z][A-Z])\
;these are the valid top-level domains (this message will never be seen)\
:.*\\.(?P<TLD>([^.]+))\
;There is no such domain '%{TLD}' on the public Internet!\
"
#endif

/*
 * A list of DNS black-list domains in which we look for the HELO/EHLO name,
 * rejecting the connection if any match is found.
 */
#ifndef SMTP_HELLO_DNSBL_DOMAINS
# define SMTP_HELLO_DNSBL_DOMAINS	NULL 	/* none by default */
#endif

/*
 * A list of IP/Nets execpted from lookups in smtp_hello_dnsbl_domains
 */
#ifndef SMTP_HELLO_DNSBL_EXCEPT
# if defined(HAVE_LIBWHOSON)
#  define SMTP_HELLO_DNSBL_EXCEPT	"whoson:127/8"
# else
#  define SMTP_HELLO_DNSBL_EXCEPT	"127/8"
# endif
#endif

/*
 * Whether or not we verify HELO/EHLO domain literal has PTR RR
 */
#ifndef SMTP_HELLO_VERIFY_LITERAL
# define SMTP_HELLO_VERIFY_LITERAL	1 	/* it would be stupid not to */
#endif

/*
 * Whether or not we verify HELO/EHLO domain has matching PTR RR
 *
 * Some very large sites, including AOL, require all e-mail hosts to have PTR
 * records, but not so many actually require them to be valid.
 */
#ifndef SMTP_HELLO_VERIFY_PTR
# define SMTP_HELLO_VERIFY_PTR		0 	/* some sites do, but not yet all */
#endif

/*
 * Whether or not we verify that the client using a locally deliverable sender
 * address also matches smtp_remote_allow...
 *
 * WARNING: Even with the list below this has great potential for rejecting
 * legitimate mail on all but the most basic of leaf nodes.
 */
#ifndef SMTP_LOCAL_SENDER_RESTRICT
# define SMTP_LOCAL_SENDER_RESTRICT	0
#endif

/*
 * A list of mailbox REs that might just possibly come back to us....
 */
#ifndef SMTP_LOCAL_SENDER_ALLOW
# define SMTP_LOCAL_SENDER_ALLOW	"mailer-daemon:owner-.*:postmaster:real-.*:.*-request"
#endif

/*
 * a list of hosts/networks that host only (presumably local) MUAs and which
 * are not allowed to send bounces (i.e. use a null return path).
 */
#ifndef SMTP_MUA_ONLY_HOSTS
# define SMTP_MUA_ONLY_HOSTS		NULL
#endif

/*
 * The list of domains we will allow to secondary MX for.
 *
 * Set this to "!.*" to disable all relaying as a secondary MX.
 */
#ifndef SMTP_PERMIT_MX_BACKUP
# define SMTP_PERMIT_MX_BACKUP		NULL	/* secondary MXing is vastly over-rated! */
#endif

/*
 * The default list of Reverse (DNS) Black Lists.
 */
#ifndef SMTP_RBL_DOMAINS
# define SMTP_RBL_DOMAINS		NULL
#endif

/*
 * The default list of IP/Nets which will not trigger an RBL lookup.
 */
#ifndef SMTP_RBL_EXCEPT
# if defined(HAVE_LIBWHOSON)
#  define SMTP_RBL_EXCEPT		"whoson:127/8" /* special */
# else
#  define SMTP_RBL_EXCEPT		"127/8"
# endif
#endif

/*
 * The default list of networks from which do not need/want the MAIL FROM:
 * address to be verified.
 */
#ifndef SMTP_SENDER_NO_VERIFY
# define SMTP_SENDER_NO_VERIFY		NULL	/* always verify!!! */
#endif

/*
 * A list of REs which will cause the SMTP transaction to be denied if the
 * whole sender address matches any one of them.
 *
 * NOTE: this is more or less redundant with the default entries in the
 * SMTP_SENDER_REJECT_HOSTNAMES list, but it's here as well as an example.
 *
 * ":.+@(.*\\.)*(?P<TLD>(localhost|local|localdomain|prv|test|example|invalid(\\.)))\
 * ;Using '%{TLD}' in an SMTP envelope address is not permitted!"
 */
#ifndef SMTP_SENDER_REJECT
# define SMTP_SENDER_REJECT		NULL
#endif

/*
 * The name of a database (and its access protocol) containing a list of
 * addresses which will cause the SMTP transaction to be denied if the whole
 * sender address matches any one of them.
 */
#ifndef SMTP_SENDER_REJECT_DB
# define SMTP_SENDER_REJECT_DB		"dead-mail.senders"
#endif
#ifndef SMTP_SENDER_REJECT_DB_PROTO
# define SMTP_SENDER_REJECT_DB_PROTO	"lsearch"
#endif

/*
 * A list of host domain name REs which will cause the SMTP transaction to be
 * denied if the sender address domain matches any one of them.
 *
 * Note that smtp_bad_mx_targets will catch the invalid domain literals.
 *
 * XXX should (have) be(en) called SMTP_SENDER_REJECT_DOMAINS
 *
 * Note we use this more strict IP address matcher only as an example of how it
 * can be done.
 *
 * XXX are there any other totally invalid addresses that get abused?
 *
 * Note iterating all the valid ISO 3166 2-letter country codes would be too
 * much effort to maintain so we just wildcard any two-letter top-level domain.
 * If spammers start regularly violating invalid 2-letter domains then we can
 * either reject the bogus ones explicitly or consider listing all of the valid
 * ones....
 *
 * See <URL:http://www.iana.org/gtld/gtld.htm> for the current list of gTLDs.
 *
 * WARNING:  The negated expressions must come second last because we the more
 * specific messages need to match before them in order to be seen; and the
 * wildcard entry must come last.  Any localization of this setting should be
 * done by copying the original value and then inserting new entries at the
 * beginning.
 */
#ifndef SMTP_SENDER_REJECT_HOSTNAMES
# define SMTP_SENDER_REJECT_HOSTNAMES	"\
:.*\\.(?P<TLD>(test|example|invalid|localhost))\
;The domain '%{TLD}' is not valid -- it is reserved by IANA as per RFC 2606.\
:(.*\\.)*(?P<TLD>(example\\.(com|net|org)))\
;The domain 'example.%{TLD}' is not valid -- it is reserved by IANA as per RFC 2606.\
:\\[0+\\.0+\\.0+\\.0+\\]\
;The literal IP address [0.0.0.0] cannot ever be valid.\
:! \\[([0-9.]+|0[Xx][0-9a-fA-F]+)\\]\
;these are the valid IP addresses (this message will never be seen)\
:! .*\\.(AERO|BIZ|COM|COOP|EDU|GOV|INFO|INT|MIL|MUSEUM|NAME|NET|ORG|PRO|[A-Z][A-Z])\
;these are the valid top-level domains (this message will never be seen)\
:.*\\.(?P<TLD>([^.]+))\
;There is no such domain '%{TLD}' on the public Internet!"
#endif

/*
 * The default list of DNSBLs for the sender address domain
 */
#ifndef SMTP_SENDER_RHSBL_DOMAINS
# define SMTP_SENDER_RHSBL_DOMAINS	NULL
#endif

/*
 * The default list of hostnames which will not trigger an DNSBL lookup of
 * the sender address domain.
 */
#ifndef SMTP_SENDER_RHSBL_EXCEPT
# define SMTP_SENDER_RHSBL_EXCEPT	"${rxquote:hostnames}:${rxquote:more_hostnames}"
#endif

/*
 * Whether or not MX RRs are required for MAIL FROM: hosts
 */
#ifndef SMTP_SENDER_VERIFY_MX_ONLY
# define SMTP_SENDER_VERIFY_MX_ONLY	0 	/* continue on with RFC974 braindamage */
#endif

/*
 * maximum recipients per SMTP connect
 */
#ifndef SMTP_MAX_RECIPIENTS
# define SMTP_MAX_RECIPIENTS		100	/* suggested max in drums-smtpupd draft */
#endif

/*
 * maximum recipients per bounce message
 *
 * Normally only one recipient can ever be specified for a bounce message since
 * there's only ever one SMTP envelope sender address to which a bounce can be
 * returned to.  A common counter-example to this is bounces from mailing
 * lists, or indeed bounces to any mailbox which may be aliased to more than
 * one person at the same remote domain.  Of course this only matters if this
 * particular mailer instance is handling that remote domain and if the remote
 * mailer on the list host will attempt to send one message to multiple SMTP
 * recipients at a time.
 */
#ifndef SMTP_MAX_BOUNCE_RECIPIENTS
# define SMTP_MAX_BOUNCE_RECIPIENTS	1
#endif

/*
 * seconds to delay after an EXPN command
 *
 * The default value of ten seconds is an attempt to hinder dictionary attacks
 * but not make manual testing too much of a pain.
 */
#ifndef SMTP_EXPN_DELAY
# define SMTP_EXPN_DELAY		10
#endif

/*
 * seconds to delay after an VRFY command
 *
 * The default value of ten seconds is an attempt to hinder dictionary attacks
 * but not make manual testing too much of a pain.
 */
#ifndef SMTP_VRFY_DELAY
# define SMTP_VRFY_DELAY		10
#endif

/*
 * seconds to delay after RCPT results in ERR_100
 */
#ifndef SMTP_INVALID_RECIPIENT_ERROR_DELAY
# define SMTP_INVALID_RECIPIENT_ERROR_DELAY	60 /* stop dictionary searches */
#endif

/*
 * seconds to delay before the last line of any other SMTP "error" (4xx or 5xx response)
 *
 * RFC 2821 requires the client wait for at least five minutes for initial
 * greetings, MAIL, and RCPT command responses, but other delays may already
 * have been introduced by normal processing so stay well below this absolute
 * limit.
 *
 * Note there is also a hard-coded one-second inter-line delay implemented in
 * many responses.
 */
#ifndef SMTP_ERROR_DELAY
# define SMTP_ERROR_DELAY		60	/* prevent immediate reconnects */
#endif

/*
 * Set this to a string defining the mode to use for delivery of new
 * incoming mail messages.  It should be one of:
 *
 * "foreground"		- deliver without forking a new process
 * "background"		- fork a child to do delivery and don't wait for it
 * "queue"		- do not perform delivery, just write the spool file
 *
 * NOTE: This can be set with the config file value "delivery_mode"
 */
#ifndef DELIVERY_MODE
# define DELIVERY_MODE "background"
#endif

/*
 * Define the required effective user id for mail that is to be
 * delivered after being queued.  If the effective user ID when
 * smail is executed does not equal this number, then mail messages
 * can be queued, but will not be delivered until a later queue
 * run.
 *
 * This should normally be 0 (root), since smail is not likely to
 * be able to deliver mail messages successfully unless it is run
 * as root.
 *
 * If REQUIRED_EUID is not defined, then no check will be performed,
 * and any delivery mode will be allowed.
 */
#if !defined(REQUIRED_EUID) && !defined(NO_REQUIRED_UID)
# define REQUIRED_EUID		0	/* default, must be root to deliver */
#endif

/*
 * the mailbox file locking protocol requires a retry count and interval
 * for creating lock files.  This is used by the file driver called by
 * the "file" transport and, possibly, the "local" transport
 *
 * NOTE:  These can be changed in config file by setting "fnlock_retries"
 *	  and "fnlock_interval"
 */
#ifndef	FNLOCK_RETRIES
# define FNLOCK_RETRIES		5	/* up to five retries */
#endif

#ifndef	FNLOCK_INTERVAL
# define FNLOCK_INTERVAL	3	/* at three second intervals */
#endif

/*
 * This octal permission mode is used in creating mailbox lock files
 *
 * NOTE: This can be set with the config file value "fnlock_mode"
 */
#ifndef	FNLOCK_MODE
# define FNLOCK_MODE	0666		/* everybody can read and write */
#endif

/*
 * Define a nobody user ID and group ID for defining default access
 * permissions.  These IDs are often used by directors and routers and
 * transports that call external programs where no other uid/gid has been
 * given.
 *
 * If NOBODY_UID and NOBODY_GID are set to BOGUS_USER and BOGUS_GROUP
 * respectfully, but SMAIL_NOBODY is non-NULL, then getpwnam(3) is called to
 * obtain the value for nobody_uid (nobody_gid) (see compute_nobody()).
 *
 * NOTE: This can be set with the config file value "nobody"
 *
 * #define NOBODY_UID	11		(* uid "nobody" on namei *)
 * #define NOBODY_GID	90		(* gid "guest" on namei *)
 */
#ifndef	NOBODY_UID
# define NOBODY_UID	BOGUS_USER	/* compute the nobody_uid */
#endif

#ifndef	NOBODY_GID
# define NOBODY_GID	BOGUS_GROUP	/* compute the nobody_gid */
#endif

#ifndef	SMAIL_NOBODY
# if defined(UNIX_BSD4_3) || defined(sun) || defined(UNIX_AIX) || defined(UNIX_SYS5_4)
#  define SMAIL_NOBODY	"nobody"/* 4.3BSD+ & SysVr4 have a standard for this */
# else
#  if defined(UNIX_SCO)
#   define SMAIL_NOBODY	"nouser"/* the default for SCO 3.2v4 and 3.2v5 */
#  else
#   define SMAIL_NOBODY	"guest"	/* patch this with something reasonable */
#  endif
# endif
#endif

/*
 * Set SENDER_ENV_VARIABLE to name an environment variable that can
 * be used to name the calling user.  For System V systems this can be
 * set to LOGNAME.  For BSD systems, this can be set to USER.
 */
#ifndef SENDER_ENV_VARIABLE
/* # define SENDER_ENV_VARIABLE		"LOGNAME" (* for System V *) */
/* # define SENDER_ENV_VARIABLE		"USER" (* for BSD *) */
#define SENDER_ENV_VARIABLE		NULL	/* no default defined */
#endif

/*
 * Set RETRY_INTERVAL to the default minimum interval, in seconds, between a
 * deferred delivery and the subsequent retry.  Sites with lots of SMTP traffic
 * might want to consider setting this to be at least twice the value given to
 * queue_interval (-q) when that value is relatively small (eg. < 60min or so).
 * Setting this value to zero will have the effect of dynamically doing this at
 * runtime.
 */
#ifndef RETRY_INTERVAL
# define RETRY_INTERVAL (0)		/* default to 2 * queue_interval */
#endif

/*
 * Set RETRY_DURATION to the default maximum duration of retries for a given
 * next-hop address (i.e. once a message has been routed and a transport has
 * been selected).
 */
#ifndef RETRY_DURATION
# define RETRY_DURATION 	(4 * 24 * 60 * 60L) /* 4 days, long WE + one */
#endif

/*
 * Timeout of directors and routers for multiple defers
 *
 * This determines at what point a message that has been defered
 * in a router or director is bounced, otherwise can stick in a
 * defered state for ever
 */
#ifndef RESOLVE_TIMEOUT
#  define RESOLVE_TIMEOUT	(4 * 24 * 60 * 60L) /* 4 days, long WE + one */
#endif

/*
 * Delivery control by message grade
 *
 * runq_grades variable defines which messages are processed
 * during a queue run (selected by grades).  Only the grades in the
 * simple grade range (ie "a-z", no commas etc....)  will be processed.
 * The default value for this is NULL (or "-") - meaning all grades.
 *
 * The delivery_grades variable similarly defines a range of grades
 * which will be delivered immediately, rather than queued up for
 * later transmission.
 */
#ifndef RUNQ_GRADES
# define RUNQ_GRADES NULL		/*  */
#endif

#ifndef DELIVERY_GRADES
# define DELIVERY_GRADES NULL		/*  */
#endif

/*
 * The location of the PID file for the daemon process (-bd).
 */
#ifndef DAEMON_PIDFILE
# if defined(UNIX_BSD) || defined(UNIX_SUN_OS_5) || defined(UNIX_SYS_5_4) || defined(LINUX)
#  define DAEMON_PIDFILE	"/var/run/smail.pid"
# else
#  define DAEMON_PIDFILE	"/usr/spool/smail/smail.pid" /* i.e. in SPOOL_DIRS */
# endif
#endif

/*
 * Set HOST_LOCK_TIMEOUT to the default timeout, in seconds, for
 * use when trying to lock a retry file for exclusive delivery
 * to a given host.  Used only on the initial delivery, not by runq.
 */
#ifndef HOST_LOCK_TIMEOUT
# define HOST_LOCK_TIMEOUT	10
#endif

/*
 * Set SMAIL to the location of the `smail' program.  This filename is
 * used when smail wants to re-exec itself to completely reinitialize
 * its state.
 *
 * NOTE: This can be set with the config file value "smail"
 *
 * #define SMAIL "/usr/lib/smail/smail"	(* potentially useful setting *)
 * #define SMAIL "/usr/smail/lib/smail"	(* alternate suggestion *)
 */
#ifndef	SMAIL
# define SMAIL "/usr/lib/sendmail"	/* Suggestion */
#endif

/*
 * Maximum number of allowed runq processes to be forked by the main daemon.
 *
 * NOTE: This can be set with the config file value "runq_max".
 */
#ifndef RUNQ_MAX
# define RUNQ_MAX		0	/* allow an infinite number */
#endif

/*
 * We can directly deliver a mail message by forking a new smail process
 * each time we receive one when the load average is <= MAX_LOAD_AVE.
 * When the load average is > MAX_LOAD_AVE, all incoming messages are spooled
 * for later processing.  Spooling mail messages helps reduce the load on the
 * system while delaying mail delivery.
 *
 * The load average is taken from the 5 minute load average.  On systems
 * that do not compute load average, a value of 0 is always assumed.
 *
 * Set this value to 0 if you always wish to always with directly deliver
 * messages.  The MAX_LOAD_AVE on systems that compute the load average
 * should be set to a value where the response time becomes poor for
 * interactive users.
 *
 * 0 is a magic value which cause the load average not to be computed at
 * all.
 *
 * NOTE: This can be set with the config file value "max_load_ave"
 *
 * NOTE: Load-average computation is not currently supported.
 */
#ifndef	MAX_LOAD_AVE
# define MAX_LOAD_AVE	0
#endif

/*
 * Normal users should not be able to set the sender address for their mail (to
 * anything but their own address).  However, daemons need to be able to do
 * this, especially for mail from remote systems.  The TRUSTED_USERS string
 * lists which users are allowed to supply a sender address explicitly.
 *
 * If both this setting and TRUSTED_GROUPS are NULL, then anybody can supply a
 * any syntactically valid sender string.
 *
 * This string is a colon-separated list of user names.
 *
 * NOTE: This can be set with the config file value "trusted_users"
 *
 * Trusted users should include the user names with the real-user-ID under
 * which trusted mail submitting programs like rmail (via uux/uuxqt) or
 * sendmail (via inetd) execute.  Often this means root, uucp, and daemon.
 *
 * CAUTION:
 *
 *	Some versions of the uuxqt do not change the real UID when executing
 *	programs.  On such systems Smail has no way of knowing whether or not
 *	to trust the sender address from UUCP submitted mail since a local user
 *	can often initiate a UUCP transaction.  In such cases the
 *	TRUSTED_GROUPS attribute may be of some use since it uses the effective
 *	group-ID of the invoker for validation.
 *
 * Note this setting only affects a user's ability to forge sender headers,
 * which is pretty easy to do anyway (just connect to an SMTP socket and you
 * can forge all the mail you want, headers and all).
 */
#ifndef	TRUSTED_USERS
# define TRUSTED_USERS		"root:uucp:daemon"
#endif

/*
 * Smail will also use the effective gid under which the mailer was invoked
 * to determine if the sender address can be trusted.
 *
 * If both this setting and TRUSTED_USERS are NULL, then anybody can supply a
 * any syntactically valid sender string.
 *
 * This string is a colon-separated list of group names.
 *
 * NOTE: This can be set with the config file value "trusted_groups"
 *
 * Trusted groups should include the group names with the effective-group-ID
 * under which trusted mail submitting programs like rmail (via uux/uuxqt) or
 * sendmail (via inetd) might execute.
 *
 * This may be a better solution for programs that submit mail, but which do
 * not change their real uid (such as UUCP in many implementations).
 *
 * Be sure that all trusted pathways into the mailer have an entry in either
 * TRUSTED_USERS or TRUSTED_GROUPS, or the results may be worse than setting
 * TRUSTED_USERS and TRUSTED_GROUPS to NULL.
 *
 * Note this setting only affects a user's ability to forge sender headeers,
 * which is pretty easy to do anyway (just connect to an SMTP socket and you
 * can forge all the mail you want, headers and all).
 */
#ifndef TRUSTED_GROUPS
# define TRUSTED_GROUPS		NULL	/* safest off by default */
#endif

/*
 * if your machine handles large data areas efficiently, then making
 * MESSAGE_BUF_SIZE the maximum size of a message will minimize i/o
 * overhead for reading and writing the message.  Systems with a
 * small amount of memory or poor VM systems cannot afford large buffers.
 * Larger memory systems can set the buffer size to less than
 * MAX_MESSAGE_SIZE at the expense of more i/o on large mail messages.
 *
 * NOTE: This can be set with the config file value "message_buf_size"
 */
#ifndef	MESSAGE_BUF_SIZE
# ifdef	SMALL_MEMORY
#  define MESSAGE_BUF_SIZE	BUFSIZ
# else
#  if MAX_MESSAGE_SIZE == 0
#   define MESSAGE_BUF_SIZE	(256*1024)
#  else
#   define MESSAGE_BUF_SIZE	MAX_MESSAGE_SIZE
#  endif
# endif
#endif

/*
 * Number of entries in address hit table.  The larger the number the
 * greater the space but the greater the efficiency.
 *
 * NOTE: This can be set with the config file value "hit_table_len"
 */
#ifndef HIT_TABLE_LEN
# define HIT_TABLE_LEN	241		/* suitable for large mailing lists */
#endif

/*
 * If you wish to use the % operator in preference to the ! operator,
 * define SWITCH_PERCENT_AND_BANG to be TRUE.  This switch happens
 * only for addresses that have % and ! operators in the local-part of
 * a local-part@domain address.
 *
 * See the parse_address() function in addr.c for a complete description
 * of how this affects smail.
 *
 * SWITCH_PERCENT_AND_BANG should be defined as either TRUE or FALSE.
 */
#ifndef SWITCH_PERCENT_AND_BANG
#define SWITCH_PERCENT_AND_BANG	FALSE
#endif


/*
 * DEFAULT TRANSPORT CONFIGURATION
 *
 * This section adjusts the default transport configuration compiled into
 * smail.  See default.c for the complete default configuration source.
 *
 * NOTE:  The existence of a transport configuration file replaces all of
 *	  this configuration.
 */

/*
 * Default setup for the local transport
 *
 * Smail can perform local delivery either by appending to mailboxes
 * by itself (only useful if it is setuid or setgid to something that
 * can do this, or if all mail is queued and smail performs all delivery
 * as a daemon).  Alternately, it can send messages to another program,
 * such as /bin/mail, or /usr/libexec/mail.local and have that program
 * perform delivery.
 *
 * The method used depends upon what is set.  If LOCAL_MAIL_COMMAND
 * is set, it should be a command to be executed which will deliver
 * a message given on its standard input.  Otherwise LOCAL_MAIL_FILE
 * should be set to define which file should be appended to for
 * local mail delivery.  See the installation and operators guide
 * for information on what the command and filename strings look
 * like.
 *
 * Note that LOCAL_MAIL_FILE may be automatically defined in either the
 * system configuration or the local configuration by defining the
 * variable MAILBOX_DIR, in which case "/${strip:lc:user}" is appended.
 *
 * Note that the program called as LOCAL_MAIL_COMMAND must enforce all
 * of the local requirements of a ``unix'' mailbox format, which are
 * normally as follows (from the 4.4bsd mail.local(8) manual page):
 *
 *    Individual mail messages in the mailbox are delimited by an empty line
 *    followed by a line beginning with the string ``From ''.  A line contain-
 *    ing the string ``From '', the sender's name and a time stamp is prepended
 *    to each delivered mail message.  A blank line is appended to each mes-
 *    sage.  A greater-than character (``>'') is prepended to any line in the
 *     message which could be mistaken for a ``From '' delimiter line.
 *
 * #define LOCAL_MAIL_COMMAND "/path/prog $($user$)" (* custom program *)
 * #define LOCAL_MAIL_FILE "~/mbox"	(* Example: file in home directories *)
 */
#if defined(USE_LOCAL_MAIL_COMMAND) && !defined(NO_USE_LOCAL_MAIL_COMMAND)
# if !defined(LOCAL_MAIL_COMMAND)
#  if defined(UNIX_BSD4_4) || defined(BSDI)
#   define LOCAL_MAIL_COMMAND "/usr/libexec/mail.local -f <${return_address}> $(${user}$)"
#  else
#   define LOCAL_MAIL_COMMAND "/bin/mail -d $(${user}$)"
#  endif
# endif
#endif

#if !defined(LOCAL_MAIL_COMMAND) && !defined(LOCAL_MAIL_FILE)
# ifndef LOCAL_MAIL_COMMAND
/* NOTE: the ifdef logic here is used again for FORWARDTO_FILE below */
#  if defined(UNIX_SYS5)&& !defined(UNIX_SUN_OS_4_1) && !defined(UNIX_SYS5_4)
#   define LOCAL_MAIL_FILE "/usr/mail/${user}" /* traditional */
#  else
#   if defined(UNIX_BSD4_4) || defined(UNIX_BSD_4_3) || defined(UNIX_SYS5_4)
#    define LOCAL_MAIL_FILE "/var/mail/${user}" /* modern */
#   else
#    if defined(UNIX_SUN_OS_4_1)
#     define LOCAL_MAIL_FILE "/var/spool/mail/${user}"
#    else
#     define LOCAL_MAIL_FILE "/usr/spool/mail/${user}"
#    endif
#   endif
#  endif
# endif
#endif

#if !defined(IGNORE_USER_CASE)
# define IGNORE_USER_CASE	TRUE	/* do caseless username matching */
#endif

/*
 * if a program is used for local delivery, set LOCAL_MAIL_ADDRS to
 * the maximum number of addrs that the program can take.  Generally,
 * a program can either take one or can take many.
 */
#ifndef	LOCAL_MAIL_ADDRS
# define LOCAL_MAIL_ADDRS	0	/* rely solely on max_chars to limit */
#endif

/*
 * if smail is doing delivery to mailbox files directly, these files will
 * be created with this mode.
 */
#ifndef LOCAL_MAIL_MODE
# if defined(UNIX_BSD) || (defined(UNIX_SCO) && !defined(NO_MMDF_MAILBOXES)) || defined(MMDF_MAILBOXES)
#  define LOCAL_MAIL_MODE	0600	/* only user can read/write mail */
# else
#  define LOCAL_MAIL_MODE	0660	/* mailboxes in restricted group */
# endif
#endif

/*
 * if smail is doing delivery to mailbox files directly, these files will
 * be opened with this group.
 */
#ifndef LOCAL_MAIL_GROUP
# if defined(UNIX_SYS5) && !defined(UNIX_SUN_OS_4_1) && !defined(UNIX_BSD)
#  if (defined(UNIX_SCO) && !defined(NO_MMDF_MAILBOXES)) || defined(MMDF_MAILBOXES)
#   define LOCAL_MAIL_GROUP	"mmdf"
#  else
#   define LOCAL_MAIL_GROUP	"mail"	/* under System V use group "mail" */
#  endif
# else
#  define LOCAL_MAIL_GROUP	NULL	/* otherwise, no group in particular */
# endif
#endif

/*
 * Setup for the queued uux transport
 *
 * The following command should call uux and queue a mail message
 * for transmission to a remote host.  Check your man page to see
 * if uux supports the -amailpath option, and also check to see
 * if you have the -ggrade option.  Smail can support both of these
 * quite nicely.
 *
 * Note the $user variable is quoted in parentheses to make sure uux doesn't
 * interpret any exclamation marks in it.
 */
#ifndef	QUEUED_UUX_COMMAND
# ifdef HAVE_HDB_UUCP
#  define QUEUED_UUX_COMMAND \
	"/usr/bin/uux - -r -a${sender} -g${grade} ${host}!rmail $((${user})$)"
# else
#  define QUEUED_UUX_COMMAND \
	"/usr/bin/uux - -r ${host}!rmail $((${user})$)"
# endif
#endif

/*
 * Setup for the demand uux transport
 *
 * The following command should call uux and queue a mail message
 * for transmission to a remote host and immediately attempt delivery.
 * Check your man page to see if uux supports the -amailpath option, and
 * also check to see if you have the -ggrade option.  Smail can support
 * both of these quite nicely.
 *
 * Note the $user variable is quoted in parentheses to make sure uux doesn't
 * interpret any exclamation marks in it.
 */
#ifndef	DEMAND_UUX_COMMAND
# ifdef HAVE_HDB_UUCP
#  define DEMAND_UUX_COMMAND \
	"/usr/bin/uux - -a${sender} -g${grade} ${host}!rmail $((${user})$)"
# else
#  define DEMAND_UUX_COMMAND \
	"/usr/bin/uux - ${host}!rmail $((${user})$)"
# endif
#endif

/*
 * Setup for the queued uusmtp transport
 *
 * The following command should call uux and queue a mail message
 * for transmission to a remote host.  It should invoke a program
 * on the remote host that reads batched SMTP requests from its
 * standard input.
 */
#ifndef	QUEUED_UUSMTP_COMMAND
# ifdef HAVE_HDB_UUCP
#  define QUEUED_UUSMTP_COMMAND \
	"/usr/bin/uux - -r -a${sender} -g${grade} ${host}!rsmtp"
# else
#  define QUEUED_UUSMTP_COMMAND \
	"/usr/bin/uux - -r ${host}!rsmtp"
# endif
#endif

/*
 * Setup for the demand uusmtp transport
 *
 * The following command should call uux and queue a mail message
 * for immediate transmission to a remote host.  It should invoke
 * a program on the remote host that reads batched SMTP requests
 * from its standard input.
 */
#ifndef	DEMAND_UUSMTP_COMMAND
# ifdef HAVE_HDB_UUCP
#  define DEMAND_UUSMTP_COMMAND \
	"/usr/bin/uux - -a${sender} -g${grade} ${host}!rsmtp"
# else
#  define DEMAND_UUSMTP_COMMAND \
	"/usr/bin/uux - ${host}!rsmtp"
# endif
#endif

/*
 * Setup for the smtp transport
 *
 * If you wish to configure the SMTP over TCP/IP transports
 * then define this.  If defined, four transports will be configured:
 *
 *	smtp		- default smtp transport (either INET or UUCP zone)
 *	uucp_zone_smtp	- smtp transport for UUCP zone
 *	inet_zone_smtp	- smtp transport for the Internet
 *	local_smtp	- smtp transport for coordinated local networks
 *	uucp_zone_bind_smtp	- smtp transport for UUCP zone w/TCPSMTP_USE_BIND
 *	inet_zone_bind_smtp	- smtp transport for the Internet w/TCPSMTP_USE_BIND
 *	local_bind_smtp	- smtp transport for coordinated local networks w/TCPSMTP_USE_BIND
 */
#ifdef HAVE_BSD_NETWORKING
# if	!defined(USE_SMTP_TRANSPORT) && !defined(NO_USE_SMTP_TRANSPORT)
#  define USE_SMTP_TRANSPORT
# endif
#endif

#ifdef USE_CYRUS_IMAPD
/*
 * Setup for the cyrus_deliver transport
 *
 * NOTE:  The "-a postman" is magic that works with Cyrus-IMAPd-2.1.x and up to
 * allow users/admins to selectively control which sub-folders (-m mailbox)
 * "deliver" can post messages to.  Sub-folders without the "p" ACL for the
 * pseudo-user "postman" will be ignored and the message will only be posted to
 * the user's default "INBOX".  You can change this to "-a ${user}" if
 * you want to be able to deliver to any existing sub-folder.
 */

# ifndef CYRUS_USER
#  define CYRUS_USER		"cyrus"
# endif

# ifndef CYRUS_GROUP
#  define CYRUS_GROUP		"mail"
# endif

# ifndef CYRUS_DELIVER_PATH
#  define CYRUS_DELIVER_PATH	"/usr/cyrus/bin/deliver"
# endif

/*
 * note:  the leading slash is critical!  All commands must have an absolute
 * pathname, and the check is done prior to expansion of variable names.  Extra
 * slashes in pathnames will do no harm.
 */
# ifndef CYRUS_DELIVER_COMMAND
#   define CYRUS_DELIVER_COMMAND	"/${cyrus_deliver_path} -a ${cyrus_user} -e -f <${return_address}> ${if def:user_suffix:-m} ${if def:user_suffix:${user_suffix}} ${user}"
#   define CYRUS_DELIVER_NQ_COMMAND	"/${smail_util_dir}/cyrus-deliver-noquota -a ${cyrus_user} -e -f <${return_address}> ${if def:user_suffix:-m} ${if def:user_suffix:${user_suffix}} ${user}"
# endif

/*
 * Setup for the noquota transport
 *
 * Leaving this passphrase as the default is seriously insecure, at least
 * w.r.t. your quota settings.
 */
#ifndef CYRUS_NOQUOTA_PASSPHRASE
# define CYRUS_NOQUOTA_PASSPHRASE	"bypassquota" /* NOT A SUGGESTION!!! */
#endif

/*
 * Setup for the over_quota transport
 *
 * One can create the list of over-quota users with mkcyrusoqlist
 *
 * These are not currently configurable in the EDITME, nor at runtime.
 */
#ifndef OVER_QUOTA_FILE
# define OVER_QUOTA_FILE	"cyrus_over_quota.list"
#endif
#ifndef OVER_QUOTA_PROTO
# define OVER_QUOTA_PROTO	"lsearch" /* mkcyrusoqlist uses REs */
#endif

#endif /* USE_CYRUS_IMAPD */

/*
 * Setup for the default file transport
 */
#if !defined(FILE_TRANSPORT_NM)		/* && defined(APPENDFILE_TRANSPORT_NM)  XXX need drivertab.h */
# define FILE_TRANSPORT_NM	"file"
#endif

/*
 * Setup for the default pipe transport
 */
#if !defined(PIPE_TRANSPORT_NM)		/* && defined(PIPE_TRANSPORT_NM)  XXX need drivertab.h */
# define PIPE_TRANSPORT_NM	"pipe"
#endif

/*
 * If you wish to modify the default pipe transport command, define
 * PIPE_COMMAND in EDITME.  If you don't define it, it will default according
 * to whether your system supports interpreter file execution (i.e. "#!
 * /interpreter/path" at the beginning of an executable script) or not (under
 * the assumption that you'll want to allow scripts to be run from the pipe
 * transport).
 *
 * WARNING: whatever $user expands to will be passed as one parameter.
 */
#ifndef PIPE_COMMAND
# if defined(HAVE_HASH_BANG)
#  define PIPE_COMMAND "$user"
# else
   /*
    * NOTE: If your system does not support interpreter file execution then you
    * might wish to use something like sendmail's "smrsh" instead of the full
    * system shell to make the pipe transport somewhat more secure.
    * E.g. "/usr/libexec/smrsh -c ${user}"
    */
#  if defined(SHELL_EXEC_PATH) && (defined(__STDC__) || defined(__cplusplus))
#   define PIPE_COMMAND		SHELL_EXEC_PATH "-c ${user}"
#  else
#   if defined(SHELL_EXEC_PATH)
#    include "ERROR: PIPE_COMMAND must be defined explicitly for non STD-C compilers."
#   else
#    define PIPE_COMMAND	"/bin/sh -c ${user}"
#   endif
#  endif
# endif
#endif

/*
 * DEFAULT DIRECTOR CONFIGURATION
 *
 * This section adjusts the default director configuration compiled into
 * smail.  See default.c for the complete default configuration source.
 *
 * NOTE:  The existence of a direcor configuration file replaces all of
 *	  this configuration.
 */

/*
 * configuration for the default aliases director
 *
 * If you wish to have an aliases director by default, define
 * ALIASES_FILE to be the name of a sorted file, and ALIASES_PROTO
 * to be the database access protocol to use in searching this file
 * See the file lookup.c for a complete list of these protocols.  If
 * this does not begin with `/', it will be referenced relative to the
 * smail_lib_dir directory.
 *
 * A partial list is:
 *
 * lsearch - perform linear searches on raw alias files.  This is slow
 *	     unless the aliases file is small.
 * bsearch - perform binary searches on sorted alias files with one
 *	     alias rule per line.
 * dbm	   - use the Berkeley dbm(3X) or ndbm(3X) libraries to search
 *	     for aliases.
 * yp	   - use Sun's YP service to search for aliases.  This requires
 *	     the existence of the YP library routines.
 * aliasyp - use Sun's YP service in a manner compatible with standard
 *	     Sun mail.aliases YP databases.  These do not exactly match
 *	     the semantics of other YP databases in that they count an
 *	     the ending nul byte in the length of keys.  There is a
 *	     tool distributed with smail (mkline) that can be used to
 *	     create regular YP databases, for use with the `yp' proto.
 * nialias - use NeXTs netinfo mail alias table
 * nisplus - use Sun's NIS+ (the follow-on to YP) for remote table access.
 *
 * #define ALIASES_FILE	"/usr/lib/smail/aliases" (* Suggestion *)
 * #define ALIASES_FILE	"/usr/smail/lib/aliases" (* alternate *)
 */
#if !defined(ALIASES_FILE) && !defined(NO_ALIASES_FILE)
# define ALIASES_FILE	"/etc/aliases"		/* sendmail compatible name */
#endif

#if defined(ALIASES_FILE)

# if !defined(ALIASES_OWNER) && !defined(NO_ALIASES_OWNER)
#  define ALIASES_OWNER	"owner-${user}" /* sendmail compatible prefix */
# endif
/* NOTE: this is derrived from ALIASES_TYPE in conf/EDITME by conf/lib/mkdefs.sh */
# ifndef ALIASES_PROTO
#  define ALIASES_PROTO "lsearch"	/* This should work on any UN*X os */
# endif

/*
 * if the file does not exist, it is assumed to be empty.....
 *
 * # define ALIASES_OPTIONAL		(* never a good idea *)
 */

/*
 * In the aliases database does not exist we can consider treat it as a a
 * temporary failure which can be recovered from by retrying the open at a
 * later time.  If this flag is set the message delivery will be deferred until
 * the next queue run, and if it is not set the message is held by freezing it
 * into the error queue .  One common example is encountered on systems with
 * editors that lock files.  Since smail also tries to lock the aliases file,
 * the lock may fail if the file is being edited.  Another example would be use
 * of YP where the server host may be down.  To enable this behavior, define
 * the name below.
 */
# if !defined(ALIASES_TRYAGAIN) && !defined(NO_ALIASES_TRYAGAIN)
#  define ALIASES_TRYAGAIN		/* always a good idea! */
# endif

#endif /* ALIASES_FILE */

/*
 * configuration for the default dotforward director
 *
 * If you do not wish to support ~/.forward files, for some strange reason,
 * define NO_DOTFORWARD. dotforward files in smail are handled in a reasonable
 * manner which should prevent their use in trojan horse attacks.  For example,
 * any home directories known to be accessible from remote places can't be used
 * to write to any files or run any programs.
 *
 * #define NO_DOTFORWARD		(* you probably don't need this! *)
 */

/*
 * The following list should define all home directories that may be accessible
 * from remote hosts, so that care can be taken for .forward files found in
 * these places.  (used as the "unsecure" attribute of the "dotforward"
 * director)
 */
#ifndef REMOTE_HOMES
# define REMOTE_HOMES "~nuucp:~uucp:~ftp:/tmp:/usr/tmp:/var/tmp"
#endif

/*
 * configuration for the default forwardto director
 */
#ifndef ERROR_DIRECTOR_NM
# define ERROR_DIRECTOR_NM	"error"
#endif

/*
 * configuration for the default forwardto director
 *
 * If you wish to support users putting "Forward to " lines at the
 * beginning of their mailbox files to indicate forwarding, then
 * define how to find these mailbox files.  System V mailers currently
 * do not have any other means of specifying forwarding information
 * for users.  This setting maintains compatibility with that system.
 */
#if !defined(FORWARDTO_FILE) && !defined(NO_FORWARDTO_FILE)
# ifdef LOCAL_MAIL_FILE
#  define FORWARDTO_FILE LOCAL_MAIL_FILE
# else
/* NOTE: the ifdef logic here matches that for LOCAL_MAIL_FILE */
#  if defined(UNIX_SYS5)&& !defined(UNIX_SUN_OS_4_1) && !defined(UNIX_SYS5_4)
#   define FORWARDTO_FILE "/usr/mail/${user}" /* traditional */
#  else
#   if defined(UNIX_BSD4_4) || defined(UNIX_BSD_4_3) || defined(UNIX_SYS5_4)
#    define FORWARDTO_FILE "/var/mail/${user}" /* modern */
#   else
#    if defined(UNIX_SUN_OS_4_1)
#     define FORWARDTO_FILE "/var/spool/mail/${user}"
#    else
#     define FORWARDTO_FILE "/usr/spool/mail/${user}"
#    endif
#   endif
#  endif
# endif
#endif

/*
 * configuration for the default mailing list director ("lists")
 *
 * The forwardfile driver may also be used to define a directory which may
 * contain mailing list files.  By simply creating a file in a directory a
 * mailing list will have been defined.  aliases, forward files and local
 * usernames have precedence over these mailing list files, so it is generally
 * safe to allow general users access to this directory (e.g., users cannot use
 * this directory to steal mail from other users on the local host).
 *
 * In the spirit of allowing general users access to this directory, the
 * caution and nobody attributes are turned on.  This prevents worries about
 * users being able to put shell command and file addresses in these files and
 * accessing things that they shouldn't.  However, it still allows users the
 * convenience of specifying files and shell commands as destinations provided
 * they are able to arrange appropriate permissions for such files.
 *
 * Under a 4.3BSD system or a Sun running SunOS3.0 or higher, it is reasonable
 * to set the sticky bit on this directory.  In this case a user will be able
 * to create a file here with assurance that other users will not be able to
 * remove rename it.  Note that any site which is comfortable with normal UN*X
 * /tmp and /usr/tmp semantics, should also be comfortable with a globally
 * writable mailing list directory.
 *
 * If these definitions do not begin with `/', they will be referenced
 * relative to the smail_lib_dir directory.
 */
#if !defined(LIST_FILENAME) && !defined(NO_LIST_FILENAME)
# define LIST_FILENAME	"lists/${if def:ignore_user_case {${strip:lc:addr}} else {${strip:addr}}}"/* suggested pathname */
# define LIST_OWNER	"owner-${addr}"		/* standard list owner prefix */
#endif
#if defined(LIST_FILENAME) && !defined(LIST_OWNER)
# include "CONFIG_ERROR: LIST_OWNER must be defined with LIST_FILENAME."
#endif

/*
 * configuration for the default smartuser director
 *
 * Smail can be configured to send mail destined to an unknown user to
 * a remote site that supposedly understands more usernames than does
 * the local host.  By defining an address to which mail to unknown
 * users should be sent, this can be accomplished.  This address should
 * contain a $user where the local address is inserted into the address.
 *
 * If SMART_USER is set to NULL, then a smartuser director is configured
 * which will read the variable smart_user which can be set in the
 * config file.  The default value of the smart_user variable can be
 * specified as CONFIG_SMART_USER.  This enables a smart_user director
 * to be compiled in while the address for the smart_user director is
 * still modifiable through configuration.
 *
 * #define SMART_USER "$user@amdahl.uts.amdahl.com" (* sample setting *)
 */
#if !defined(SMART_USER) && !defined(NO_SMART_USER)
# define SMART_USER		NULL		/* normal setting */
#endif

#ifndef	CONFIG_SMART_USER
# define CONFIG_SMART_USER	NULL		/* normal setting */
#endif


/*
 * DEFAULT ROUTER CONFIGURATION
 *
 * This section adjusts the default router configuration compiled into
 * smail.  See default.c for the complete default configuration source.
 *
 * NOTE:  The existence of a router configuration file replaces all of
 *	  this configuration.
 */

/*
 * There is currently no support for method files from the compiled in smail
 * configuration.  However, a hardcoded internal method table can be used by
 * defining USE_INTERNAL_METHOD_TABLE (and by defining the table contents in
 * default.c).  If defined, all of the routers which use UUCP in one form or
 * another as a transport will use this table in addition to their default
 * associated transport.  Routers which use a direct SMTP transport won't use
 * this table.
 */
#if !defined(USE_INTERNAL_METHOD_TABLE) && !defined(NO_USE_INTERNAL_METHOD_TABLE)
/* # define USE_INTERNAL_METHOD_TABLE	(* you probably don't want this! *) */
#endif


/*
 * Default gethostbyaddr router setup
 *
 * If defined, a gethostbyaddr router will be configured which recognizes
 * hostnames such as [192.2.12.142] and delivers via SMTP to that IP
 * address.  This requires that the gethostbyaddr driver be configured
 * in by the conf/driver.cf configuration file.  This in turn requires
 * the a BSD-compatible networking library.
 */
#ifdef HAVE_BSD_NETWORKING
# if	!defined(USE_GETHOSTBYADDR) && !defined(NO_USE_GETHOSTBYADDR)
#  define USE_GETHOSTBYADDR		/*  */
# endif
#endif

/*
 * GETHOSTBYADDR_TRANSPORT defines the default transport used for hosts
 * matched with the pathalias router.
 *
 * #define GETHOSTBYADDR_TRANSPORT  "inet_zone_smtp" (* alternate suggestion *)
 */
#ifndef GETHOSTBYADDR_TRANSPORT
# define GETHOSTBYADDR_TRANSPORT "inet_zone_smtp"
#endif


/*
 * Default bind router setup
 *
 * If defined, a bind router will be configured which calls the
 * resolve(3N) to match DNS hosts accessible over TCP/IP.  This requires
 * that the gethostbyname driver be configured in by the conf/driver.cf
 * configuration file.  This in turn requires the a BSD-compatible
 * networking library, and BIND/DNS support.
 */
#if defined(HAVE_BSD_NETWORKING) && defined(HAVE_BIND)
# if	!defined(USE_BIND) && !defined(NO_USE_BIND)
#  define USE_BIND		/*  */
# endif
#endif

/*
 * BIND_TRANSPORT defines the default transport used for hosts
 * matched with the pathalias router.
 *
 * NOTE:  You probably want to ensure use of a *_bind_smtp transport...
 */
#ifndef BIND_TRANSPORT
# define BIND_TRANSPORT "inet_zone_bind_smtp"
#endif


/*
 * Default gethostbyname router setup
 *
 * If defined, a gethostbyname router will be configured which calls
 * gethostbyname(3N) to match hosts accessible over TCP/IP.  This requires
 * that the gethostbyaddr driver be configured in by the conf/driver.cf
 * configuration file.  This in turn requires the a BSD-compatible
 * networking library.
 */
#ifdef HAVE_BSD_NETWORKING
# if	!defined(USE_GETHOSTBYNAME) && !defined(NO_USE_GETHOSTBYNAME)
#  define USE_GETHOSTBYNAME		/*  */
# endif
#endif

/*
 * GETHOSTBYNAME_TRANSPORT defines the default transport used for hosts
 * matched with the pathalias router.
 *
 * #define GETHOSTBYNAME_TRANSPORT  "inet_zone_smtp" (* alternate suggestion *)
 */
#ifndef GETHOSTBYNAME_TRANSPORT
# define GETHOSTBYNAME_TRANSPORT "inet_zone_smtp"
#endif


/*
 * Default pathalias router setup
 *
 * If you wish to have a pathalias router by default, define PATHS_FILE
 * to be the name of a sorted paths file, and PATHS_PROTO to be the
 * database access protocol used for lookups.  See the file lookup.c
 * for a complete list of these protocols.    If this does not begin
 * with `/', it will be referenced relative to the smail_lib_dir
 * directory.  A current list is:
 *
 * lsearch - perform linear searches on raw path files.  This is slow
 *	     unless the path file is small.
 * bsearch - perform binary searches on sorted path files with one
 *	     path per line.
 * dbm	   - use the Berkeley dbm(3X) or ndbm(3X) libraries to search
 *	     for paths.
 * yp	   - use Sun's YP service to search for paths.  This requires
 *	     the existence of the YP library routines.
 *
 * #define PATHS_FILE	"/usr/smail/lib/paths" (* alternate suggestion *)
 */
#if !defined(PATHS_FILE) && !defined(NO_PATHS_FILE)
# define PATHS_FILE	"paths"		/* Suggestion */
#endif

#if defined(PATHS_FILE)

/* NOTE: this is derrived from PATHS_TYPE in conf/EDITME by conf/lib/mkdefs.sh */
# if !defined(PATHS_PROTO)
#  define PATHS_PROTO	"bsearch"	/* Compatible with smail1.0 and 2.0 */
# endif

/*
 * In some environments where smail is being integrated into new systems
 * it may be convenient to make the paths file optional, so that if
 * the file does not exist, it is assumed to be empty.  To enable this
 * behavior define the name below.
 */
# if !defined(PATHS_OPTIONAL) && !defined(NO_PATHS_OPTIONAL)
#  define PATHS_OPTIONAL		/*  */
# endif

/*
 * In some cases, failure to open an paths database can be considered a
 * temporary failure which can be recovered from by retrying the open at a
 * later time.  If this flag is set the message delivery will be deferred until
 * the next queue run, and if it is not set the message is held by freezing it
 * into the error queue.  One example would be use of YP where the server host
 * may be down.  To enable this behavior, define the name below.
 */
# if !defined(PATHS_TRYAGAIN) && !defined(NO_PATHS_TRYAGAIN)
#  define PATHS_TRYAGAIN		/* you probably don't want this! */
# endif

/*
 * PATHS_TRANSPORT defines the default transport used for hosts matched
 * with the pathalias router.
 *
 * #define PATHS_TRANSPORT  "demand"	(* alternate suggestion *)
 */
# if !defined(PATHS_TRANSPORT)
#  define PATHS_TRANSPORT	"uux"	/* queue remote rmail requests */
# endif

#endif /* PATHS_FILE */


/*
 * Default uuname router setup
 *
 * If you wish to obtain a list of neighbor sites from the UUCP programs,
 * define the command to extract the list of UUCP neighbor sites.
 */
#if !defined(UUNAME_COMMAND) && !defined(NO_UUNAME_COMMAND)
# define UUNAME_COMMAND	"/usr/bin/uuname" /* should work almost everywhere */
#endif

/*
 * UUNAME_TRANSPORT defines the default transport used for hosts matched
 * with the uuname router.
 *
 * #define UUNAME_TRANSPORT  "demand"	(* alternate suggestion *)
 */
#ifndef UUNAME_TRANSPORT
# define UUNAME_TRANSPORT  "uux"	/* queue remote rmail requests */
#endif


/*
 * Default setup for the smarthost router
 *
 * If you wish to send unknown addresses to a remote host for routing, define
 * the path/hostname used to send mail to that host.  Make sure you ask the
 * remote site's postmaster for permission before setting this up!
 *
 * If you leave SMART_PATH set to NULL then runtime value of the global
 * smart_path variable will be used as the default value for the 'path'
 * parameter for all smarthost router instances.  The default value for
 * the smart_path variable is set with CONFIG_SMART_PATH.
 * 
 * #define SMART_PATH  "namei!amdahl"	(* example path *)
 */
#if !defined(SMART_PATH) && !defined(NO_SMART_PATH)
# define SMART_PATH		NULL		/* using $smart_path is best */
#endif
#if !defined(CONFIG_SMART_PATH)
# define CONFIG_SMART_PATH	NULL		/* you never want to hard-code this */
#endif

/*
 * SMART_TRANSPORT defines the default transport used for hosts matched
 * with the uuname router.
 *
 * As well, CONFIG_SMART_TRANSPORT can be set to the default value for the
 * smart_transport global variable, which is the default value for the
 * 'transport' parameter for all smarthost router instances.
 *
 * #define SMART_TRANSPORT" inet_zone_smtp" (*  *)
 */
#if !defined(SMART_TRANSPORT)
# define SMART_TRANSPORT	NULL	/* best to allow $smart_transport */
#endif
#if !defined(CONFIG_SMART_TRANSPORT)
# ifdef HAVE_BSD_NETWORKING
#  define CONFIG_SMART_TRANSPORT "inet_zone_smtp"
# else
#  define CONFIG_SMART_TRANSPORT "uux"
# endif
#endif


/*
 * Optional force_paths router setup
 *
 * This router is not enabled by default -- use the rewrite router instead!
 *
 * If you wish to have a pathalias router by default, define FORCE_PATHS_FILE
 * to be the name of a sorted paths file, and FORCE_PATHS_PROTO to be the
 * database access protocol used for lookups.  See the file lookup.c for a
 * complete list of these protocols.  If this does not begin with `/', it will
 * be referenced relative to the smail_lib_dir directory.
 *
 * #define FORCE_PATHS_FILE	"forcepaths" (* suggestion *)
 */

#if defined(FORCE_PATHS_FILE)

/*
 * NOTE: FORCE_PATHS_PROTO is normally derrived from FORCE_PATHS_TYPE in
 * conf/EDITME by conf/lib/mkdefs.sh
 */
# if !defined(FORCE_PATHS_PROTO)
#  define FORCE_PATHS_PROTO	"lsearch"	/* Suggestion */
# endif

/*
 * The force_paths file is usually considered optional, so that if
 * the file does not exist, it is assumed to be empty.
 * 
 * # define FORCE_SMTP_OPTIONAL			(* not a good idea *)
 */

/*
 * In some cases, failure to open a forcepaths database can be considered a
 * temporary failure which can be recovered from by retrying the open at a
 * later time.  If this flag is set the message delivery will be deferred until
 * the next queue run, and if it is not set the message is held by freezing it
 * into the error queue.  One example would be use of YP where the server host
 * may be down.  To enable this behavior, define the name below.
 *
 */
# if !defined(FORCE_PATHS_TRYAGAIN) && !defined(NO_FORCE_PATHS_TRYAGAIN)
#  define FORCE_PATHS_TRYAGAIN			/*  */
# endif

/*
 * FORCE_PATHS_TRANSPORT defines the default transport used for hosts matched
 * with the pathalias driver for the force_paths router.
 *
 * #define FORCE_PATHS_TRANSPORT  "demand"	(* alternate suggestion *)
 */
# if !defined(FORCE_PATHS_TRANSPORT)
#  define FORCE_PATHS_TRANSPORT	"uux"		/* queue remote rmail requests */
# endif

#endif /* optional FORCE_PATHS_FILE configuration */


/*
 * Optional force_smtp router setup
 *
 * This router is not enabled by default -- use the rewrite router instead!
 *
 * If you wish to have a pathalias router by default, define FORCE_SMTP_FILE to
 * be the name of a sorted paths file, and FORCE_SMTP_PROTO to be the database
 * access protocol used for lookups.  See the file lookup.c for a complete list
 * of these protocols.  If this does not begin with `/', it will be referenced
 * relative to the smail_lib_dir directory.
 *
 * #define FORCE_SMTP_FILE	"forcesmtp"	(* alternate suggestion *)
 */

#if defined(FORCE_SMTP_FILE)

/*
 * NOTE: FORCE_SMTP_PROTO is normally derrived from FORCE_SMTP_TYPE in
 * conf/EDITME by conf/lib/mkdefs.sh
 */
# if !defined(FORCE_SMTP_PROTO)
#  define FORCE_SMTP_PROTO	"lsearch"	/* Suggestion */
# endif

/*
 * The force_smtp file is usually considered optional, so that if
 * the file does not exist, it is assumed to be empty.
 * 
 * # define FORCE_SMTP_OPTIONAL			(* not a good idea *)
 */

/*
 * In some cases, failure to open a forcesmtp database can be considered a
 * temporary failure which can be recovered from by retrying the open at a
 * later time.  If this flag is set the message delivery will be deferred until
 * the next queue run, and if it is not set the message is held by freezing it
 * into the error queue.  One example would be use of YP where the server host
 * may be down.  To enable this behavior, define the name below.
 */
# if !defined(FORCE_SMTP_TRYAGAIN) && !defined(NO_FORCE_SMTP_TRYAGAIN)
#  define FORCE_SMTP_TRYAGAIN			/*  */
# endif

/*
 * FORCE_SMTP_TRANSPORT defines the default transport used for hosts matched
 * with the pathalias driver for the force_smtp router.
 *
 * #define FORCE_SMTP_TRANSPORT  "inet_zone_smtp" (* alternate suggestion *)
 */
# if !defined(FORCE_SMTP_TRANSPORT)
#  define FORCE_SMTP_TRANSPORT	"inet_zone_smtp" /* deliver directly with smtp */
# endif

#endif /* optional FORCE_SMTP_FILE configuration */


/*
 * Default rewrite router setup
 *
 * If you wish not to have a rewrite router by default, define REWRITE_FILE to
 * be the name of a sorted rewrite database, and REWRITE_PROTO to be the
 * database access protocol used for lookups.  See the file lookup.c for a
 * complete list of these protocols.  If this file does not begin with `/', it
 * will be referenced relative to the smail_lib_dir directory.
 */
#if !defined(REWRITE_FILE) && !defined(NO_REWRITE_FILE)
# define REWRITE_FILE		"rewrite"	/* Suggestion */
#endif

# if defined(REWRITE_FILE)
/*
 * NOTE: REWRITE_PROTO is normally derrived from REWRITE_TYPE in conf/EDITME by
 * conf/lib/mkdefs.sh
 */
# if !defined(REWRITE_PROTO)
#  define REWRITE_PROTO		"lsearch"	/* Suggestion */
# endif

/*
 * if the file does not exist, it is assumed to be empty....
 *
 * # define REWRITE_OPTIONAL			(* not a good idea *)
 */

/*
 * In some cases, failure to open a paths database can be considered a
 * temporary failure which can be recovered from by retrying the open at a
 * later time.  If this flag is set the message delivery will be deferred until
 * the next queue run, and if it is not set the message is held by freezing it
 * into the error queue.  One example would be use of YP where the server host
 * may be down.  To enable this behavior, define the name below.
 */
# if !defined(REWRITE_TRYAGAIN) && !defined(NO_REWRITE_TRYAGAIN)
#  define REWRITE_TRYAGAIN			/*  */
# endif

#endif /* default REWRITE_FILE configuration */

/* 
 * Local Variables:
 * c-file-style: "smail"
 * End:
 */
